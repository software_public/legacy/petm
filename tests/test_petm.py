#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `petm` package."""

import unittest
import sys

try:
    import petm
except ImportError:
     pass

class TestPetm(unittest.TestCase):
    """Tests for `petm` package."""

    def setUp(self):
        """Set up test fixtures, if any."""

    def tearDown(self):
        """Tear down test fixtures, if any."""

    def test_import(self):
        if 'petm' in sys.modules:
            self.assert_(True, "petm loaded")
        else:
            self.fail()

