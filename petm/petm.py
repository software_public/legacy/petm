#! /usr/bin/env python
# BEGIN PROGRAM: PETM
# Started: <2007.150
# By: Bob Greschke
#   The PASSCAL/Python (Texan) Event Table Maker program.
from sys import argv, exit, platform, stderr, stdout
PROGSystem = platform[:3].lower()
PROG_NAME = "PETM"
PROG_NAMELC = "petm"
PROG_VERSION = "2019.059"
PROG_LONGNAME = "PASSCAL Event Table Maker"
PROG_SETUPSVERS = "A"

PROGKiosk = False
PROGSmallScreen = False
PROGScroll = False
PROGIgnoreSetups = False
# Not used in PETM.
PROG_SETUPSUSECWD = False
for Arg in argv[1:]:
    if Arg == "-#":
        stdout.write("%s\n"%PROG_VERSION)
        exit()
    elif Arg == "-s":
        PROGSmallScreen = True
    elif Arg == "-sb":
        PROGScroll = True
    elif Arg == "-x":
        PROGIgnoreSetups = True

########################
# BEGIN: versionChecks()
# LIB:versionChecks():2019.044
#   Checks the current version of Python and sets up a couple of things for the
#   rest of the program to use.
#   Obviously this is not a real function. It's just a collection of things for
#   all programs to check and making it look like a library function makes it
#   easy to update everywhere.
from sys import version_info
PROG_PYVERSION = "%d.%d.%d"%(version_info[0], version_info[1], version_info[2])
#PROG_PYVERSION = "4.0.0"
if PROG_PYVERSION.startswith("2"):
# This should be good enough for the rest of the code.
    PROG_PYVERS = 2
# The isinstance(X, *tuple*) stuff needs 2.5 and the b"" things need 2.6.
    if version_info[1] < 6:
        stdout.write("%s only runs on Python 2.6 and above.\n"%PROG_NAME)
        exit(1)
    from Tkinter import *
    from tkFont import Font
    from urllib import urlopen
    if PROG_NAME == "WEBEDIT":
        from HTMLParser import HTMLParser
# Pillow runs under Py2, so it may be there.
        try:
            from PIL import Image, ImageTk
        except:
            import Image, ImageTk
    astring = basestring
    anint = (int, long)
    arange = xrange
elif PROG_PYVERSION.startswith("3"):
    PROG_PYVERS = 3
    from tkinter import *
    from tkinter.font import Font
    from urllib.request import urlopen
    if PROG_NAME == "WEBEDIT":
        from html.parser import HTMLParser
# From Pillow. I don't know if Python Imaging Library will ever be for Py3.
        from PIL import Image, ImageTk
    astring = str
    anint = int
    arange = range
else:
    stdout.write("Unsupported Python version: %s\nStopping.\n"%PROG_PYVERSION)
    exit(0)
# Nice. Right-click on a Mac using Anaconda Tkinter generates a <Button-2>
# event, instead of <Button-3>.
B2Glitch = False
if PROGSystem == "dar":
    B2Glitch = True
# These should be big enough for my programs, and not bigger than any system
# they run on can handle. These are around in some form on some systems with
# some versions, but with these figuring out what is where is moot.
maxInt = 1E100
maxFloat = 1.0E100
# END: versionChecks

from calendar import monthcalendar, setfirstweekday, weekday
from fnmatch import fnmatch
from math import ceil
from os import access, environ, getcwd, listdir, makedirs, remove, sep, \
        W_OK
from os.path import abspath, basename, dirname, exists, getsize, isdir, isfile
from struct import pack
from time import sleep, gmtime, localtime, strftime, time
if PROGSystem == "dar" or PROGSystem == "lin":
    from os import getuid

setfirstweekday(6)

# This is way up here so StringVars and IntVars can be declared throughout the
# code.
Root = Tk()
Root.withdraw()
if PROGScroll == False:
    Root.resizable(0, 0)
else:
    Root.resizable(0, 1)

# For the forms, fields, message fields, etc.
PROGCan = {}
PROGEnt = {}
PROGFrm = {}
PROGMsg = {}
PROGTxt = {}

#####################
# BEGIN: option_add()
# LIB:option_add():2019.037
#   A collection of setup items common to most of my programs.
# Where all of the vars for saving and loading setups are kept.
PROGSetups = []
# These may be altered by loading the setups. If they end up the same as
# PROGScreen<Height/Width>OrigNow then the program will know that it is
# running on the same screen as before.
PROGScreenHeightSaved = IntVar()
PROGScreenWidthSaved = IntVar()
PROGScreenHeightSaved.set(Root.winfo_screenheight())
PROGScreenWidthSaved.set(Root.winfo_screenwidth())
PROGSetups += ["PROGScreenHeightSaved", "PROGScreenWidthSaved"]
# Alter these if you want to fool the program into doing something on a small
# screen.
PROGScreenHeightNow = Root.winfo_screenheight()
PROGScreenWidthNow = Root.winfo_screenwidth()
# Fonts: a constant nagging problem, though less now. I've stopped trying
# to micromanage them and mostly let the user suffer with what the system
# decides to use.
# Some items (like ToolTips and Help text) may not want to have their fonts
# resizable. So in that case use these Orig fonts whose values do not get saved
# to the setups.
PROGOrigMonoFont = Text().cget("font")
PROGOrigPropFont = Entry().cget("font")
# Only two fonts. If something needs more it will have to modify these.
PROGMonoFont = Font(font = Text()["font"])
PROGMonoFontSize = IntVar()
PROGMonoFontSize.set(PROGMonoFont["size"])
# I think this is some damn Linux-Tcl/Tk bug of some kind, but CentOS7/Tk8.5
# (in 2018) reported a "size" of 0. Just set these to -12, so resizing the
# font does something sensible. Same below.
if PROGMonoFontSize.get() == 0:
    PROGMonoFont["size"] = -12
    PROGMonoFontSize.set(-12)
# Entry() is used because it seems to be messed with less on different
# systems unlike Label() font which can be set to some bizarre stuff.
PROGPropFont = Font(font = Entry()["font"])
# Used by some plotting routines.
PROGPropFontHeight = PROGPropFont.metrics("ascent")+ \
        PROGPropFont.metrics("descent")
PROGPropFontSize = IntVar()
PROGPropFontSize.set(PROGPropFont["size"])
if PROGPropFontSize.get() == 0:
    PROGPropFont["size"] = -12
    PROGPropFontSize.set(-12)
Root.option_add("*Font", PROGPropFont)
Root.option_add("*Text*Font", PROGMonoFont)
if PROGSystem == "dar":
    PROGSystemName = "Darwin"
elif PROGSystem == "lin":
    PROGSystemName = "Linux"
elif PROGSystem == "win":
    PROGSystemName = "Windows"
elif PROGSystem == "sun":
    PROGSystemName = "Sun"
else:
    PROGSystemName = "Unknown (%s)"%PROGSystem
# Depending on the Tkinter version or how it was compiled the scroll bars can
# get pretty narrow and hard to grab.
if Scrollbar().cget("width") < "16":
    Root.option_add("*Scrollbar*width", "16")
# Just using RGB for everything since some things don't handle color names
# correctly, like PIL on macOS doesn't handle "green" very well.
# b = dark blue, was the U value for years, but it can be hard to see, so U
#     was lightened up a bit.
# Orange should be #FF7F00, but #DD5F00 is easier to see on a white background
# and it still looks OK on a black background.
# Purple should be A020F0, but that was a little dark.
# "X" should not be used. Including X at the end of a passed color pair (or by
# itself) indicates that a Toplevel or dialog box should use grab_set_global()
# which is not a color.
Clr = {"B":"#000000", "C":"#00FFFF", "G":"#00FF00", "M":"#FF00FF", \
        "R":"#FF0000", "O":"#FF7F00", "W":"#FFFFFF", "Y":"#FFFF00", \
        "E":"#DFDFDF", "A":"#8F8F8F", "K":"#3F3F3F", "U":"#0070FF", \
        "N":"#007F00", "S":"#7F0000", "y":"#7F7F00", "u":"#ADD8E6", \
        "s":"#FA8072", "p":"#FFB6C1", "g":"#90EE90", "r":"#EFEFEF", \
        "P":"#AA22FF", "b":"#0000FF"}
# This is just if the program wants to let the user know what the possibilities
# are.
ClrDesc = {"B":"black", "C":"cyan", "G":"green", "M":"magenta", \
        "R":"red", "O":"orange", "W":"white", "Y":"yellow", \
        "E":"light gray", "A":"gray", "K":"dark gray", "U":"blue", \
        "N":"dark green", "S":"dark red", "y":"dark yellow", \
        "u":"light blue", "s":"salmon", "p":"light pink", "g":"light green", \
        "r":"very light gray", "P":"purple", "b":"dark blue"}
# Now things get ugly. cget("bg") can return a hex triplet, or a color word,
# or something like "systemWindowBody". All of my programs will attempt to use
# the Root background value as their default value. This will determine what
# needs to be done to get a hex triplet value for the Root background color.
Clr["D"] = Root.cget("background")
if Clr["D"].startswith("#"):
    pass
else:
    Value = Root.winfo_rgb(Clr["D"])
    if max(Value) < 256:
        Clr["D"] = "#%02X%02X%02X"%Value
    else:
        Clr["D"] = "#%04X%04X%04X"%Value
ClrDesc["D"] = "default"
# The color of a button (like a "Stop" button) may be checked to see if a
# command is still active. Specifically set this so things just work later on.
# Same thing for a Label used as an indicator.
Root.option_add("*Button*background", Clr["D"])
Root.option_add("*Label*background", Clr["D"])
Root.option_add("*Button*takeFocus", "0")
# Newer versions of Tkinter are setting this to 1. That's too small.
Root.option_add("*Button*borderWidth", "2")
Root.option_add("*Canvas*borderWidth", "0")
Root.option_add("*Canvas*highlightThickness", "0")
Root.option_add("*Checkbutton*anchor", "w")
Root.option_add("*Checkbutton*takeFocus", "0")
Root.option_add("*Checkbutton*borderWidth", "1")
Root.option_add("*Entry*background", Clr["W"])
Root.option_add("*Entry*foreground", Clr["B"])
Root.option_add("*Entry*highlightThickness", "2")
Root.option_add("*Entry*insertWidth", "3")
Root.option_add("*Entry*highlightColor", Clr["B"])
Root.option_add("*Entry*disabledBackground", Clr["D"])
Root.option_add("*Entry*disabledForeground", Clr["B"])
Root.option_add("*Listbox*background", Clr["W"])
Root.option_add("*Listbox*foreground", Clr["B"])
Root.option_add("*Listbox*selectBackground", Clr["G"])
Root.option_add("*Listbox*selectForeground", Clr["B"])
Root.option_add("*Listbox*takeFocus", "0")
Root.option_add("*Listbox*exportSelection", "0")
Root.option_add("*Radiobutton*takeFocus", "0")
Root.option_add("*Radiobutton*borderWidth", "1")
Root.option_add("*Scrollbar*takeFocus", "0")
# When the slider is really small this might help make it easier to see, but
# I don't know what the system might set for the slider color.
Root.option_add("*Scrollbar*troughColor", Clr["A"])
Root.option_add("*Text*background", Clr["W"])
Root.option_add("*Text*foreground", Clr["B"])
Root.option_add("*Text*takeFocus", "0")
Root.option_add("*Text*highlightThickness", "0")
Root.option_add("*Text*insertWidth", "0")
Root.option_add("*Text*width", "0")
Root.option_add("*Text*padX", "3")
Root.option_add("*Text*padY", "3")
# To control the color of the buttons better in X-Windows programs.
Root.option_add("*Button*activeBackground", Clr["D"])
Root.option_add("*Checkbutton*activeBackground", Clr["D"])
Root.option_add("*Radiobutton*activeBackground", Clr["D"])
# Used by various time functions.
# First day of the month for each non-leap year month MINUS 1. This will get
# subtracted from the DOY, so a DOY of 91, minus the first day of April 90
# (91-90) will leave the 1st of April. The 365 is the 1st of Jan of the next
# year.
PROG_FDOM = (0, 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365)
# Max days per month.
PROG_MAXDPMNLY = (0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)
PROG_MAXDPMLY = (0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)
# Not very friendly to other countries, but...
PROG_CALMON = ("", "JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", \
        "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER")
PROG_CALMONS = ("", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", \
        "SEP", "OCT", "NOV", "DEC")
PROG_MONNUM = {"JAN":1, "FEB":2, "MAR":3, "APR":4, "MAY":5, "JUN":6, "JUL":7, \
        "AUG":8, "SEP":9, "OCT":10, "NOV":11, "DEC":12}
# For use with the return of the calendar module weekday function.
PROG_DOW = ("Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun")
# Stores the number of seconds to the beginning of a year so they don't have
# to be recalculated all the time.
Y2EPOCH = {}
# First characters that can be used to modifiy searches (only used in ITS so
# far).
PROG_SEARCHMODS = [">=","<=","!=","!~",">","<","=","~","_","*"]
# A second set of help lines that describes error messages that may be seen by
# the user. It gets filled in by the headers of the modules that generate the
# messages, and the contents will be inserted in the regular help by
# formHELP().
HELPError = ""
# END: option_add

# Changes to option_adds for PETM.
Root.option_add("*Radiobutton*takeFocus", "1")




# ==============================================
# BEGIN: ========== COMMAND UTILITIES ==========
# ==============================================


##############################
# BEGIN: class BButton(Button)
# LIB:BButton():2009.239
#   A sub-class of Button() that adds a bit of additional color control and
#   that adds a space before and after the text on Windows systems, otherwise
#   the edge of the button is right at the edge of the text.
class BButton(Button):
    def __init__(self, master = None, **kw):
        if PROGSystem == "win" and "text" in kw:
            if kw["text"].find("\n") == -1:
                kw["text"] = " "+kw["text"]+" "
            else:
# Add " " to each end of each line so all lines get centered.
                parts = kw["text"].split("\n")
                ntext = ""
                for part in parts:
                    ntext += " "+part+" \n"
                kw["text"] = ntext[:-1]
# Some systems have the button change color when rolled over.
        if "bg" in kw:
            kw["activebackground"] = kw["bg"]
        if "fg" in kw:
            kw["activeforeground"] = kw["fg"]
        Button.__init__(self, master, **kw)
# END: BButton




################################
# BEGIN: beep(Howmany, e = None)
# LIB:beep():2018.235
#   Just rings the terminal bell the number of times requested.
# NEEDS: from time import sleep
#        updateMe()
PROGNoBeepingCRVar = IntVar()
PROGSetups += ["PROGNoBeepingCRVar"]

def beep(Howmany, e = None):
    if PROGNoBeepingCRVar.get() == 0:
# In case someone passes something wild.
        if Howmany > 20:
            Howmany = 20
        for i in arange(0, Howmany):
            Root.bell()
            if i < Howmany-1:
                updateMe(0)
                sleep(.15)
    return
# END: beep




#################################################################
# BEGIN: canText(Can, Cx, Cy, Color, Str, Anchor = "w", Tag = "")
# LIB:canText():2018.234
#   Used to print text to a canvas such that the color of individual words
#   in a line can be changed. Use 0 for Cx to tell the routine to place this
#   Str at the end of the last Str passed.
CANTEXTLastX = 0
CANTEXTLastWidth = 0

def canText(Can, Cx, Cy, Color, Str, Anchor = "w", Tag = ""):
    global CANTEXTLastX
    global CANTEXTLastWidth
    if Cx == 0:
        Cx = CANTEXTLastX
    if isinstance(Color, astring):
# This way it can be passed "W" or #000000.
        if Color.startswith("#") == False:
            FClr = Clr[Color[0]]
        else:
            FClr = Color
        if len(Tag) == 0:
            ID = Can.create_text(Cx, Cy, text = Str, fill = FClr, \
                    font = PROGPropFont, anchor = Anchor)
        else:
            ID = Can.create_text(Cx, Cy, text = Str, fill = FClr, \
                    font = PROGPropFont, anchor = Anchor, tags = Tag)
# This may be an input from getAColor().
    elif isinstance(Color, tuple):
        if len(Tag) == 0:
            ID = Can.create_text(Cx, Cy, text = Str, fill = Color[0], \
                    font = PROGPropFont, anchor = Anchor)
        else:
            ID = Can.create_text(Cx, Cy, text = Str, fill = Color[0], \
                    font = PROGPropFont, anchor = Anchor, tags = Tag)
    else:
        if len(Tag) == 0:
            ID = Can.create_text(Cx, Cy, text = Str, fill = "white", \
                    font = PROGPropFont, anchor = Anchor)
        else:
            ID = Can.create_text(Cx, Cy, text = Str, fill = "white", \
                    font = PROGPropFont, anchor = Anchor, tags = Tag)
    L, T, R, B = Can.bbox(ID)
# -1: I don't know if this is a Tkinter bug or if it just happens to be
# specific to the font that is being used or what, but it has to be done.
    CANTEXTLastX = R-1
    CANTEXTLastWidth = R-L
    return ID
# END: canText




###############################################################################
# BEGIN: center(Parent, TheFrame, Where, InOut, Show = True, CenterX = 0, \
#                CenterY = 0)
# LIB:center():2019.002
#   Where tells the function where in relation to the Parent TheFrame should
#   show up. Use the diagram below to figure out where things will end up.
#   Where can also be NX,SX,EX,etc. to force edge of the display checking for\
#   when you absolutely, positively don't want something coming up off the
#   edge of the display. "C" or "" can be used to put TheFrame in the center
#   of Parent.
#
#      +---------------+
#      | NW    N    NE |
#      |               |
#      | W     C     E |
#      |               |
#      | SW    S    SE |
#      +---------------+
#
#   Set Parent to None to use the whole display as the parent.
#   Set InOut to "I" or "O" to control if TheFrame shows "I"nside or "O"outside
#   the Parent (does not apply if the Parent is None).
#
#   CenterX and CenterY not equal to zero overrides everything.
#
def center(Parent, TheFrame, Where, InOut, Show = True, CenterX = 0, \
        CenterY = 0):
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    if isinstance(TheFrame, astring):
        TheFrame = PROGFrm[TheFrame]
# Size of the display(s). Still won't be good for dual displays, but...
    DW = PROGScreenWidthNow
    DH = PROGScreenHeightNow
# Kiosk mode. Just take over the whole screen. Doesn't check for, but only
# works for Root. The -30 is a fudge, because systems lie about their height
# (but not their age).
    if Where == "K":
        Root.geometry("%dx%d+0+0"%(DW, DH-30))
        Root.deiconify()
        Root.lift()
        updateMe(0)
        return
# So all of the dimensions get updated.
    updateMe(0)
    FW = TheFrame.winfo_reqwidth()
    if TheFrame == Root:
# Different systems have to be compensated for a little because of differences
# in the reported heights (mostly title and menu bar heights). Some systems
# include the height and some don't, and, of course, it all depends on the
# font sizes, so there is little chance of this fudge ever being 100% correct.
        if PROGSystem == "dar" or PROGSystem == "win":
            FH = TheFrame.winfo_reqheight()
        else:
            FH = TheFrame.winfo_reqheight()+50
    else:
        FH = TheFrame.winfo_reqheight()
# Find the center of the Parent.
    if CenterX == 0 and CenterY == 0:
        if Parent is None:
            PX = 0
            PY = 0
            PW = PROGScreenWidthNow
            PH = PROGScreenHeightNow
# A PW of >2560 (the width of a 27" iMac) probably means the user has two
# monitors. Tkinter just gets fed the total width and the smallest display's
# height, so just set the size to 1024x768 and then let the user resize and
# reposition as needed. It's what they get for being so lucky.
            if PW > 2560:
                PW = 1024
                PH = 768
            CenterX = PW/2
            CenterY = PH/2-25
        elif Parent == Root:
            PX = Parent.winfo_x()
            PW = Parent.winfo_width()
            CenterX = PX+PW/2
# Macs, Linux and Suns think the top of the Root window is below the title
# and menu bars.  Windows thinks the top of the window is the top of the
# window, so adjust the window heights accordingly to try and cover that up.
# Same problem as the title and menu bars.
            if PROGSystem == "win":
                PY = Parent.winfo_y()
                PH = Parent.winfo_height()
            else:
                PY = Parent.winfo_y()-50
                PH = Parent.winfo_height()+50
            CenterY = PY+PH/2
        else:
            PX = Parent.winfo_x()
            PW = Parent.winfo_width()
            CenterX = PX+PW/2
            PY = Parent.winfo_y()
            PH = Parent.winfo_height()
            CenterY = PY+PH/2
# Can't put forms outside the whole display.
        if Parent is None or InOut == "I":
            InOut = 1
        else:
            InOut = -1
        HadX = False
        if Where.find("X") != -1:
            Where = Where.replace("X", "")
            HadX = True
        if Where == "C":
            XX = CenterX-FW/2
            YY = CenterY-FH/2
        elif Where == "N":
            XX = CenterX-FW/2
            YY = PY+(50*InOut)
        elif Where == "NE":
            XX = PX+PW-FW-(50*InOut)
            YY = PY+(50*InOut)
        elif Where == "E":
            XX = PX+PW-FW-(50*InOut)
            YY = CenterY-TheFrame.winfo_reqheight()/2
        elif Where == "SE":
            XX = PX+PW-FW-(50*InOut)
            YY = PY+PH-FH-(50*InOut)
        elif Where == "S":
            XX = CenterX-TheFrame.winfo_reqwidth()/2
            YY = PY+PH-FH-(50*InOut)
        elif Where == "SW":
            XX = PX+(50*InOut)
            YY = PY+PH-FH-(50*InOut)
        elif Where == "W":
            XX = PX+(50*InOut)
            YY = CenterY-TheFrame.winfo_reqheight()/2
        elif Where == "NW":
            XX = PX+(50*InOut)
            YY = PY+(50*InOut)
# Try to make sure the system's title bar buttons are visible (which may not
# always be functioning, but there you go).
        if HadX == True:
# None are on the bottom.
            if (CenterY+FH/2) > DH:
                YY = YY-((CenterY+FH/2)-DH+20)
# Never want things off the top.
            if YY < 0:
                YY = 10
# But now it is OS-dependent.
# Buttons in upper-left. Fix the right edge, but then check to see if it needs
# to be moved back to the right.
            if PROGSystem == "dar" or PROGSystem == "sun":
                if (CenterX+FW/2) > DW:
                    XX = XX-((CenterX+FW/2)-DW+20)
                if XX < 0:
                    XX = 10
# Opposite corner.
            elif PROGSystem == "lin" or PROGSystem == "win":
                if XX < 0:
                    XX = 10
                if (CenterX+FW/2) > DW:
                    XX = XX-((CenterX+FW/2)-DW+20)
        TheFrame.geometry("+%i+%i"%(XX, YY))
    else:
# Just do what we're told.
        TheFrame.geometry("+%i+%i"%(CenterX-FW/2, CenterY-FH/2))
    if Show == True:
        TheFrame.deiconify()
        TheFrame.lift()
    updateMe(0)
    return
# END: center




#####################################################################
# BEGIN: changeMainDirs(Parent, Which, Mode, Var, Title, WhichDir="")
# LIB:changeMainDirs():2018.234
# Needs PROGFrm, formMYDF(), and msgLn().
#   Mode = formMYDF() mode value (some callers may not want to allow directory
#          creation, for example).
#      1 = just picking
#      2 = picking and creating
#  D,W,M = may be added to the Mode for the main directories Default button
#          (see formMYDF()).
#   Var = if not None the selected directory will be placed there, instead of
#         one of the "main" Vars. May not be used in all programs.
#   Title = Will be used for the title of the form if Var is not None.
# WhichDir = If supplied this will be 'which directory?' was changed in the
#            change message when Which is "self".
#   Not all programs will use all items.
def changeMainDirs(Parent, Which, Mode, Var, Title, WhichDir=""):
# The caller can pass either.
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    if Which == "theall":
# Just use the directory as a starting point.
        Answer = formMYDF(Parent, Mode, "Pick A Main Directory For All", \
                PROGMsgsDirVar.get(), "")
        if len(Answer) == 0:
            return (1, "", "Nothing done.", 0, "")
        else:
# Some of these may not exist in a program.
            try:
                PROGDataDirVar.set(Answer)
            except:
                pass
            try:
                PROGMsgsDirVar.set(Answer)
            except:
                pass
            try:
                PROGWorkDirVar.set(Answer)
            except:
                pass
            return (0, "WB", "All main directories changed to\n   %s"% \
                    Answer, 0, "")
    elif Which == "thedata":
        Answer = formMYDF(Parent, Mode, "Pick A Main Data Directory", \
                PROGDataDirVar.get(), "")
        if len(Answer) == 0:
            return (1, )
        elif Answer == PROGDataDirVar.get():
            return (0, "", "Main data directory unchanged.", 0, "")
        else:
            PROGDataDirVar.set(Answer)
            return (0, "WB", "Main Data directory changed to\n   %s"% \
                    Answer, 0, "")
    elif Which == "themsgs":
        Answer = formMYDF(Parent, Mode, "Pick A Main Messages Directory", \
                PROGMsgsDirVar.get(), "")
        if len(Answer) == 0:
            return (1, )
        elif Answer == PROGMsgsDirVar.get():
            return (0, "", "Main messages directory unchanged.", 0, "")
        else:
            PROGMsgsDirVar.set(Answer)
            return (0, "WB", "Main Messages directory changed to\n   %s"% \
                    Answer, 0, "")
    elif Which == "thework":
        Answer = formMYDF(Parent, Mode, "Pick A Main Work Directory", \
                PROGWorkDirVar.get(), "")
        if len(Answer) == 0:
            return (1, )
        elif Answer == PROGWorkDirVar.get():
            return (0, "", "Main work directory unchanged.", 0, "")
        else:
            PROGWorkDirVar.set(Answer)
            return (0, "WB", "Main work directory changed to\n   %s"% \
                    Answer, 0, "")
# Var and Title must be set for "self".
    elif Which == "self":
        Answer = formMYDF(Parent, Mode, Title, Var.get(), Title)
        if len(Answer) == 0:
            return (1, )
        elif Answer == Var.get():
            if len(WhichDir) == 0:
                return (0, "", "Directory unchanged.", 0, "")
            else:
                return (0, "", "%s directory unchanged."%WhichDir, 0, "")
        else:
            Var.set(Answer)
        if len(WhichDir) == 0:
            return (0, "WB", "Directory changed to\n   %s"%Answer, 0, "")
        else:
            return (0, "WB", "%s directory changed to\n   %s"%(WhichDir, \
                    Answer), 0, "")
    return
###############################################################################
# BEGIN: changeMainDirsCmd(Parent, Which, Mode, Var, Title, WhichDir, e = None)
# FUNC:changeMainDirsCmd():2014.062
def changeMainDirsCmd(Parent, Which, Mode, Var, Title, WhichDir, e = None):
    Ret = changeMainDirs(Parent, Which, Mode, Var, Title, WhichDir)
    if Ret[0] == 0:
# Some programs may not have a messages area.
        try:
            msgLn(0, Ret[1], Ret[2], True, Ret[3])
        except:
            pass
    return
# END: changeMainDirs




######################
# BEGIN: checkFields()
# FUNC:checkFields():2018.249
#   Go through and check on all of the fields to make sure things are filled in
#   and no gross errors are found.
def checkFields():
    Found = False
    for i in arange(1, SESSIONS+1):
        eval("S%02dParms[0]"%i).set(eval("S%02dParms[0]"% \
                i).get().replace(" ", "").lower())
        if len(eval("S%02dParms[0]"%i).get()) != 0:
            Found = True
    if Found == False:
        msgLn(0, "R", "No start times have been entered.", True, 2)
        return False
    Comment = PROGCommentVar.get().strip()
    if len(Comment) == 0:
        PROGCommentVar.set("No comment.")
# Default warmup is 5m if none is supplied.
    PROGWarmupVar.set(PROGWarmupVar.get().replace(" ", "").lower())
    if len(PROGWarmupVar.get()) == 0:
        PROGWarmupVar.set("5m")
    Warmup = dt2Timedhms2Secs(PROGWarmupVar.get())
    if Warmup < 5:
        msgLn(0, "R", \
           "Warmup time must be greater than 5 seconds. 300 is recommended.", \
                True, 2)
        return False
# Make the Max Break time the same as default warmup if not supplied.
    PROGMaxADBreakVar.set(PROGMaxADBreakVar.get().replace(" ", "").lower())
    if len(PROGMaxADBreakVar.get()) == 0:
        PROGMaxADBreakVar.set(PROGWarmupVar.get())
    MaxADBreak = dt2Timedhms2Secs(PROGMaxADBreakVar.get())
    if MaxADBreak < Warmup:
        msgLn(0, "Y", \
                "A/D Break time may not be less than the warmup time.", \
                True, 2)
        return False
# Check through the sessions before we start.
    for i in arange(1, SESSIONS+1):
# If there is no start time then there is no interest in going any further
# with this session's inputs.
        if len(eval("S%02dParms[0]"%i).get()) == 0:
            continue
        Ret = dt2Time(0, 11, eval("S%02dParms[0]"%i).get(), True)
        if Ret[0] != 0:
            msgLn(0, "R", "Session %d: %s"%(Sess, Ret[2]), True, 2)
            return False
        Start = Ret[1][:-4]
        eval("S%02dParms[0]"%i).set(Start)
        eval("S%02dParms[1]"%i).set(eval("S%02dParms[1]"% \
                i).get().replace(" ", "").lower())
        if len(eval("S%02dParms[1]"%i).get()) == 0:
            msgLn(0, "R", \
               "Session %d: Interval Between Event Starts field is blank."%i, \
                    True, 2)
            return False
        eval("S%02dParms[2]"%i).set(eval("S%02dParms[2]"% \
                i).get().replace(" ", "").lower())
        if len(eval("S%02dParms[2]"%i).get()) == 0:
            msgLn(0, "R", "Session %d: Event Length field is blank."%i, True, \
                    2)
            return False
# The event interval can't be less than the event length.
        Interval = dt2Timedhms2Secs(eval("S%02dParms[1]"%i).get())
        Length = dt2Timedhms2Secs(eval("S%02dParms[2]"%i).get())
        if Interval < Length:
            msgLn(0, "R", \
                    "Session %d: The event interval may not be shorter than the event length."%i, \
                    True, 2)
            return False
        eval("S%02dParms[3]"%i).set(eval("S%02dParms[3]"% \
                i).get().replace(" ", "").lower())
        if len(eval("S%02dParms[3]"%i).get()) == 0:
            msgLn(0, "R", "Session %d: Events field is blank."%i, True, 2)
            return False
        StrEvents = eval("S%02dParms[3]"%i).get()
        IntEvents = dt2Timedhms2Secs(StrEvents)
# This is one field where the user will have to put an "s" if they want it to
# be seconds, otherwise it will used as just a number.
        if "h" in StrEvents or "m" in StrEvents or "s" in StrEvents:
# Figure out how many events there are going to be. This will only be used if
# the Events value is a time.
            Events = int(IntEvents/Interval)
# You're always going to get one event.
            if IntEvents != 0 and Interval != 0 and Events == 0:
                Events = 1
        else:
            Events = IntEvents
        eval("S%02dParms[4]"%i).set(eval("S%02dParms[4]"%i).get().strip())
        if len(eval("S%02dParms[4]"%i).get()) == 0:
            msgLn(0, "R", "Session %d: Sample Rate field is blank."%i, True, 2)
            return False
        if rt125CheckSampleRate("D", eval("S%02dParms[4]"%i).get()) == False:
            msgLn(0, "R", "Session %d: Bad sample rate value."%i, True, 2)
            return False
        eval("S%02dParms[5]"%i).set(eval("S%02dParms[5]"%i).get().strip())
# This was the default for the old Texans, so it must be good enough.
        if len(eval("S%02dParms[5]"%i).get()) == 0:
            eval("S%02dParms[5]"%i).set("32")
        if rt125ACheckGain("D", eval("S%02dParms[5]"%i).get()) == False:
            msgLn(0, "R", "Session %d: Bad gain value."%i, True, 2)
            return False
    PROGProgramTimeVar.set(PROGProgramTimeVar.get().strip())
    if len(PROGProgramTimeVar.get()) != 0:
        Ret = dt2Time(0, 11, PROGProgramTimeVar.get(), True)
        if Ret[0] != 0:
            msgLn(0, "R", "Program time: %s"%Ret[2], True, 2)
            return False
        PROGProgramTimeVar.set(Ret[1][:-7])
    PROGOffloadTimeVar.set(PROGOffloadTimeVar.get().strip())
    if len(PROGOffloadTimeVar.get()) != 0:
        Ret = dt2Time(0, 11, PROGOffloadTimeVar.get(), True)
        if Ret[0] != 0:
            msgLn(0, "R", "Offload time: %s"%Ret[2], True, 2)
            return False
        PROGOffloadTimeVar.set(Ret[1][:-7])
    PROGCurrentTimeVar.set(PROGCurrentTimeVar.get().strip())
    if len(PROGCurrentTimeVar.get()) != 0:
        Ret = dt2Time(0, 11, PROGCurrentTimeVar.get(), True)
        if Ret[0] != 0:
            msgLn(0, "R", "Current time: %s"%Ret[2], True, 2)
            return False
        PROGCurrentTimeVar.set(Ret[1][:-7])
# One of these has to be entered.
    PROGTimeAdjustVar.set(PROGTimeAdjustVar.get().strip())
    if len(PROGTimeAdjustVar.get()) == 0:
        PROGTimeAdjustVar.set("0.00")
    try:
        Value = float(PROGTimeAdjustVar.get())
    except ValueError:
        msgLn(0, "R", "The Time Adjust value must be a number.", True, 2)
        return False
    PROGTimeAdjustVar.set("%+.2f"%Value)
    return True
# END: checkFields




#######################################
# BEGIN: checkForUpdates(Parent = Root)
# LIB:checkForUpdates():2019.028
#   Finds the "new"+PROG_NAMELC+".txt" file created by the program webvers at
#   the URL and checks to see if the version in that file matches the version
#   of this program.
VERS_DLALLOW = True
VERS_VERSURL = "http://www.passcal.nmt.edu/~bob/passoft/"
VERS_PARTS = 4
VERS_NAME = 0
VERS_VERS = 1
VERS_USIZ = 2
VERS_ZSIZ = 3

def checkForUpdates(Parent = Root):
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    formMYD(Parent, (("(OK)", TOP, "ok"),), "ok", "WB", "What's \"git\"?",
            "Eventually you will need to use \"git\" to check for updates.")
    formMYD(Parent, (), "", "CB", "", "Checking...")
# Otherwise the menu doesn't go away on slow connections while url'ing.
    updateMe(0)
# Get the file that tells us about the current version on the server.
# One line:  PROG; version; original size; compressed size
    try:
        Fp = urlopen(VERS_VERSURL+"new"+PROG_NAMELC+".txt")
        Line = Fp.readlines()
        Fp.close()
        formMYDReturn("")
# If nothing was returned Line will be [] and that will except, also do the
# decode for Py3, otherwise it comes with a b' in front.
        Line = Line[0].decode("latin-1")
# If the file doesn't exist you get something like
#     <!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
# How unhandy.
        if Line.find("DOCTYPE") != -1:
            Line = ""
    except:
        Line = ""
# If we didn't get this then there must have been a problem.
    if len(Line) == 0:
        formMYDReturn("")
        formMYD(Parent, (("(OK)", TOP, "ok"), ), "ok", "RW", \
                "It's Probably The Net.", \
        "There was an error obtaining the version information from PASSCAL.", \
                "", 2)
        return
    Parts2 = Line.split(";")
    Parts = []
    for Part in Parts2:
        Parts.append(Part.strip())
    Parts += (VERS_PARTS-len(Parts))*[""]
    if PROG_VERSION < Parts[VERS_VERS]:
# Some programs don't need to be professionally installed, some do.
        if VERS_DLALLOW == True:
            Answer = formMYD(Parent, (("Download New Version", TOP, \
                    "dlprod"), ("(Don't)", TOP, "dont"), ), "dont", \
                    "YB", "Oh Oh...", \
                    "This is an old version of %s.\nThe current version generally available is %s."% \
                    (PROG_NAME, Parts[VERS_VERS]), "", 2)
            if Answer == "dont":
                return
            if Answer == "dlprod":
                Ret = checkForUpdatesDownload(Parent, Answer, Parts)
            if Ret == "quit":
                progQuitter(True)
                return
        elif VERS_DLALLOW == False:
            Answer = formMYD(Parent, (("(OK)", TOP, "ok"), ), "ok", \
                    "YB", "Tell Someone.", \
                    "This is an old version of %s.\nThe current version generally available is %s."% \
                    (PROG_NAME, Parts[VERS_VERS]), "", 2)
            return
    elif PROG_VERSION == Parts[VERS_VERS]:
        Answer = formMYD(Parent, (("Download Anyway", TOP, "dlprod"), \
                ("(OK)", TOP, "ok")), "ok", "", "Good To Go.", \
                "This copy of %s is up to date."%PROG_NAME)
        if Answer == "ok":
            return
        if Answer == "dlprod":
            Ret = checkForUpdatesDownload(Parent, Answer, Parts)
        if Ret == "quit":
            progQuitter(True)
        return
    elif PROG_VERSION > Parts[VERS_VERS]:
        if VERS_DLALLOW == False:
            Answer = formMYD(Parent, (("(OK)", TOP, "ok"), ), "ok", "GB", \
                    "All Right!", \
                    "Congratulations! This is a newer version of %s than is generally available. Everyone else probably still has version %s."% \
                    (PROG_NAME, Parts[VERS_VERS]))
        elif VERS_DLALLOW == True:
            Answer = formMYD(Parent, (("Download Older Version", TOP, \
                    "dlprod"), ("(No, Thanks.)", TOP, "ok"), ), "ok", \
                    "GB", "All Right!!", \
                    "Congratulations! This is a newer version of %s than is generally available. Everyone else probably still has version %s. You can download and use the older version if you want."% \
                    (PROG_NAME, Parts[VERS_VERS]))
        if Answer == "ok" or Answer == "keep":
            return
        if Answer == "dlprod":
            Ret = checkForUpdatesDownload(Parent, Answer, Parts)
        if Ret == "quit":
            progQuitter(True)
        return
    return
######################################################
# BEGIN: checkForUpdatesDownload(Parent, Which, Parts)
# FUNC:checkForUpdatesDownload():2019.028
def checkForUpdatesDownload(Parent, Which, Parts):
    formMYD(Parent, (), "", "CB", "", "Downloading...")
    ZSize = int(Parts[VERS_ZSIZ])
    try:
        if Which == "dlprod":
            GetFile = "new%s.zip"%PROG_NAMELC
            Fpr = urlopen(VERS_VERSURL+GetFile)
# SetupsDir may not be the best place to put it, but at least it will be
# consistant and not dependent on where the user was working (like it was).
# If a program does not use PROGSetupsDirVar it will be the current working
# directory.
        SetupsDir = PROGSetupsDirVar.get()
        if len(SetupsDir) == 0:
            SetupsDir = "%s%s"%(abspath("."), sep)
        try:
# Just open() so I don't interfere with what the OS wants to do.
            Fpw = open(SetupsDir+GetFile, "w")
        except Exception as e:
            formMYDReturn("")
            formMYD(Parent, (("(OK)", TOP, "ok"),), "ok", "RW", "Gasp!", \
                    "Error downloading %s\n\n%s"%(GetFile, e))
            return
        DLSize = 0
        while 1:
            if PROG_PYVERS == 2:
                Buffer = Fpr.read(20000)
            elif PROG_PYVERS == 3:
                Buffer = Fpr.read(20000).decode("latin-1")
            if len(Buffer) == 0:
                break
            Fpw.write(Buffer)
            DLSize += 20000
            formMYDMsg("Downloading (%d%%)...\n"%(100*DLSize/ZSize))
    except Exception as e:
# They may not exist.
        try:
            Fpr.close()
        except:
            pass
        try:
            Fpw.close()
        except:
            pass
        formMYDReturn("")
        formMYD(Parent, (("(OK)", TOP, "ok"), ), "ok", "MW", "Ooops.", \
                "Error downloading new version.\n\n%s"%e, "", 3)
        return ""
    Fpr.close()
    Fpw.close()
    formMYDReturn("")
    if Which == "dlprod":
        Answer = formMYD(Parent, (("Quit %s"%PROG_NAME, TOP, "quit"), \
                ("Don't Quit", TOP, "cont")), "cont", "GB", "Finished?", \
                "The downloaded program file has been saved as\n\n%s\n\nYou should quit %s using the Quit button below, unzip the downloaded file, test the new program file to make sure it is OK, then rename it %s.py and move it to the proper location to replace the old version.\n\nTAKE NOTE OF WHERE THE FILE HAS BEEN DOWNLOADED TO!"% \
                (SetupsDir+GetFile, PROG_NAME, PROG_NAMELC))
    if Answer == "quit":
        return "quit"
    return ""
# END: checkForUpdates




######################
# BEGIN: class Command
# LIB:Command():2006.114
#   Pass arguments to functions from button presses and menu selections! Nice!
#   In your declaration:  ...command = Command(func, args,...)
#   Also use in bind() statements
#       x.bind("<****>", Command(func, args...))
class Command:
    def __init__(self, func, *args, **kw):
        self.func = func
        self.args = args
        self.kw = kw
    def __call__(self, *args, **kw):
        args = self.args+args
        kw.update(self.kw)
        self.func(*args, **kw)
# END: Command




##########################
# BEGIN: clearParms(Which)
# FUNC:clearParms():2019.249
def clearParms(Which):
    if Which == "et" or Which == "all":
        PROGCommentVar.set("")
        PROGWarmupVar.set("")
        PROGMaxADBreakVar.set("")
        PROGProgramTimeVar.set("")
        PROGOffloadTimeVar.set("")
        PROGTimeAdjustVar.set("0.00")
    if Which == "sess" or Which == "all":
        for Sess in arange(1, SESSIONS+1):
            for Var in arange(0, 0+VARS):
                eval("S%02dParms[%d]"%(Sess, Var)).set("")
    return
# END: clearParms




###############################################################
# BEGIN: dt2Time(InFormat, OutFormat, DateTime, Verify = False)
# LIB:dt2Time():2018.270
#   InFormat = -1 = An Epoch has been passed.
#               0 = Figure out what was passed.
#           other = Use if the caller knows exactly what they have.
#   OutFormat = -1 = Epoch
#                0 = Y M D D H M S.s
#                1 = Uses OPTDateFormatRVar value.
#            other = whatever supported format the caller wants
#   The format of the time will always be HH:MM:SS.sss.
#   Returns (0/1, <answer or error msg>) if Verify is True, or <answer/0/"">
#   if Verify is False. Confusing, but most of the calls are with Verify set
#   to False, and having to always put [1] at the end of each call was getting
#   old fast. This probably will cause havoc if there are other errors like
#   passing date/times that cannot be deciphered, or passing bad In/OutFormat
#   codes (just "" will be returned), but that's the price of progress. You'll
#   still have to chop off the milliseconds if they are not wanted ([:-4]).
#   Needs option_add(), intt(), floatt(), rtnPattern()
def dt2Time(InFormat, OutFormat, DateTime, Verify = False):
    global Y2EPOCH
    if InFormat == -1:
        YYYY = 1970
        while 1:
            if YYYY%4 != 0:
                if DateTime >= 31536000:
                    DateTime -= 31536000
                else:
                    break
            elif YYYY%100 != 0 or YYYY%400 == 0:
                if DateTime >= 31622400:
                    DateTime -= 31622400
                else:
                    break
            else:
                if DateTime >= 31536000:
                    DateTime -= 31536000
                else:
                    break
            YYYY += 1
        DOY = 1
        while DateTime >= 86400:
            DateTime -= 86400
            DOY += 1
        HH = 0
        while DateTime >= 3600:
            DateTime -= 3600
            HH += 1
        MM = 0
        while DateTime >= 60:
            DateTime -= 60
            MM += 1
        SS = DateTime
        MMM, DD = dt2Timeydoy2md(YYYY, DOY)
    else:
        DateTime = DateTime.strip().upper()
# The caller will have to decide if these returns are OK or not.
        if len(DateTime) == 0:
            if OutFormat -1:
                if Verify == False:
                    return 0.0
                else:
                    return (0, 0.0)
            elif OutFormat == 0:
                if Verify == False:
                    return 0, 0, 0, 0, 0, 0, 0.0
                else:
                    return (0, 0, 0, 0, 0, 0, 0, 0.0)
            elif InFormat == 5:
                if Verify == False:
                    return "00:00:00:00"
                else:
                    return (0, "00:00:00:00")
            else:
                if Verify == False:
                    return ""
                else:
                    return (0, "")
# The overall goal of the decode will be to get the passed string time into
# YYYY, MMM, DD, DOY, HH, MM integers and SS.sss float values then proceed to
# the encoding section ready for anything.
# These will try and figure out what the date is between the usual formats.
# Checks first to see if the date and time are together (like with a :) or if
# there is a space between them.
        if InFormat == 0:
            Parts = DateTime.split()
            if len(Parts) == 1:
# YYYY:DOY:... - 71:2 will pass.
                if DateTime.find(":") != -1:
                    Parts = DateTime.split(":")
# There has to be something that looks like YYYY:DOY.
                    if len(Parts) >= 2:
                        InFormat = 11
# YYYY-MM-DD:HH:...
# If there was only one thing and there are dashes then it could be Y-M-D or
# Y-M-D:H:M:S. Either way there must be 3 parts.
                elif DateTime.find("-") != -1:
                    Parts = DateTime.split("-")
                    if len(Parts) == 3:
                        InFormat = 21
# YYYYMMMDD:HH:... - 68APR3 will pass.
                elif (DateTime.find("A") != -1 or DateTime.find("E") != -1 or \
                        DateTime.find("O") != -1 or DateTime.find("U") != -1):
                    InFormat = 31
# YYYYDOYHHMMSS - Date/time must be exactly like this.
                elif len(DateTime) == 13:
                    if rtnPattern(DateTime) == "0000000000000":
                        InFormat = 41
# YYYYMMDDHHMMSS - Date/time must be exactly like this.
                elif len(DateTime) == 14:
                    if rtnPattern(DateTime) == "00000000000000":
                        InFormat = 51
# (There is no 1974JAN23235959 that I know of, but the elif for it would be
# here. ->
# There were two parts.
            else:
                Date = Parts[0]
                Time = Parts[1]
# YYYY:DOY HH:MM...
                if Date.find(":") != -1:
# Must have at least YYYY:DOY.
                    Parts = Date.split(":")
                    if len(Parts) >= 2:
                        InFormat = 12
# May be YYYY-MM-DD HH:MM...
                elif Date.find("-") != -1:
                    Parts = Date.split("-")
                    if len(Parts) == 3:
                        InFormat = 22
# YYYYMMMDD - 68APR3 will pass.
                elif (Date.find("A") != -1 or Date.find("E") != -1 or \
                    Date.find("O") != -1 or Date.find("U") != -1):
                        InFormat = 32
# If it is still 0 then something is wrong.
            if InFormat == 0:
                if Verify == False:
                    return ""
                else:
                    return (1, "RW", "Bad date/time(%d): '%s'"%(InFormat, \
                            DateTime), 2, "")
# These can be fed from the Format 0 stuff above, or called directly if the
# caller knows what DateTime is.
        if InFormat < 20:
# YYYY:DOY:HH:MM:SS.sss
# Sometimes this comes as  YYYY:DOY:HH:MM:SS:sss. We'll look for that here.
# (It's a Reftek thing.)
            if InFormat == 11:
                DT = DateTime.split(":")
                DT += (5-len(DT))*["0"]
                if len(DT) == 6:
                    DT[4] = "%06.3f"%(intt(DT[4])+intt(DT[5])/1000.0)
                    DT = DT[:-1]
# YYYY:DOY HH:MM:SS.sss
            elif InFormat == 12:
                Parts = DateTime.split()
                Date = Parts[0].split(":")
                Date += (2-len(Date))*["0"]
                Time = Parts[1].split(":")
                Time += (3-len(Time))*["0"]
                DT = Date+Time
            else:
                if Verify == False:
                    return ""
                else:
                    return (1, "MWX", "dt2Time: Unknown InFormat code (%d)."% \
                            InFormat, 3, "")
# Two-digit years shouldn't happen a lot, so this is kinda inefficient.
            if DT[0] < "100":
                YYYY = intt(Date[0])
                if YYYY < 70:
                    YYYY += 2000
                else:
                    YYYY += 1900
                DT[0] = str(YYYY)
# After we have all of the parts then do the check if the caller wants.
            if Verify == True:
                Ret = dt2TimeVerify("ydhms", DT)
                if Ret[0] != 0:
                    return Ret
# I'm using intt() and floatt() throughout just because it's safer than the
# built-in functions.
# This trick makes it so the Epoch for a year only has to be calculated once
# during a program's run.
            YYYY = intt(DT[0])
            DOY = intt(DT[1])
            MMM, DD = dt2Timeydoy2md(YYYY, DOY)
            HH = intt(DT[2])
            MM = intt(DT[3])
            SS = floatt(DT[4])
        elif InFormat < 30:
# YYYY-MM-DD:HH:MM:SS.sss
            if InFormat == 21:
                Parts = DateTime.split(":", 1)
                Date = Parts[0].split("-")
                Date += (3-len(Date))*["0"]
# Just the date must have been supplied.
                if len(Parts) == 1:
                    Time = ["0", "0", "0"]
                else:
                    Time = Parts[1].split(":")
                    Time += (3-len(Time))*["0"]
                DT = Date+Time
# YYYY-MM-DD HH:MM:SS.sss
            elif InFormat == 22:
                Parts = DateTime.split()
                Date = Parts[0].split("-")
                Date += (3-len(Date))*["0"]
                Time = Parts[1].split(":")
                Time += (3-len(Time))*["0"]
                DT = Date+Time
# If parts of 23 are missing we will fill them in with Jan, 1st, or 00:00:00.
# If parts of 24 are missing we will format to the missing item then stop and
# return what we have.
            elif InFormat == 23 or InFormat == 24:
# The /DOY may or may not be there.
                if DateTime.find("/") == -1:
                    Parts = DateTime.split()
                    Date = Parts[0].split("-")
                    if InFormat == 23:
                        Date += (3-len(Date))*["1"]
                    else:
# The -1's will stand in from the missing items. OutFormat=24 will figure it
# out from there.
                        Date += (3-len(Date))*["-1"]
                    if len(Parts) == 2:
                        Time = Parts[1].split(":")
                        if InFormat == 23:
                            Time += (3-len(Time))*["0"]
                        else:
                            Time += (3-len(Time))*["-1"]
                    else:
                        if InFormat == 23:
                            Time = ["0", "0", "0"]
                        else:
                            Time = ["-1", "-1", "-1"]
# Has a /. We'll only use the YYYY-MM-DD part and assume nothing about the DOY
# part.
                else:
                    Parts = DateTime.split()
                    Date = Parts[0].split("-")
                    if InFormat == 23:
                        Date += (3-len(Date))*["0"]
                    elif InFormat == 24:
                        Date += (3-len(Date))*["-1"]
                    Date[2] = Date[2].split("/")[0]
                    if len(Parts) == 2:
                        Time = Parts[1].split(":")
                        if InFormat == 23:
                            Time += (3-len(Time))*["0"]
                        else:
                            Time += (3-len(Time))*["-1"]
                    else:
                        if InFormat == 23:
                            Time = ["0", "0", "0"]
                        else:
                            Time = ["1-", "-1", "-1"]
                DT = Date+Time
            else:
                if Verify == False:
                    return ""
                else:
                    return (1, "MWX", "dt2Time: Unknown InFormat code (%d)."% \
                            InFormat, 3, "")
            if DT[0] < "100":
                YYYY = intt(DT[0])
                if YYYY < 70:
                    YYYY += 2000
                else:
                    YYYY += 1900
                DT[0] = str(YYYY)
            if Verify == True:
                if InFormat != 24:
                    Ret = dt2TimeVerify("ymdhms", DT)
                else:
                    Ret = dt2TimeVerify("xymdhms", DT)
                if Ret[0] != 0:
                    return Ret
            YYYY = intt(DT[0])
            MMM = intt(DT[1])
            DD = intt(DT[2])
# This will get done in OutFormat=24.
            if InFormat != 24:
                DOY = dt2Timeymd2doy(YYYY, MMM, DD)
            else:
                DOY = -1
            HH = intt(DT[3])
            MM = intt(DT[4])
            SS = floatt(DT[5])
        elif InFormat < 40:
# YYYYMMMDD:HH:MM:SS.sss
            if InFormat == 31:
                Parts = DateTime.split(":", 1)
                Date = Parts[0]
                if len(Parts) == 1:
                    Time = ["0", "0", "0"]
                else:
                    Time = Parts[1].split(":")
                    Time += (3-len(Time))*["0"]
# YYYYMMMDD HH:MM:SS.sss
            elif InFormat == 32:
                Parts = DateTime.split()
                Date = Parts[0]
                Time = Parts[1].split(":")
                Time += (3-len(Time))*["0"]
            else:
                if Verify == False:
                    return ""
                else:
                    return (1, "MWX", "dt2Time: Unknown InFormat code (%d)."% \
                            InFormat, 3, "")
# Date is still "YYYYMMMDD", so just make place holders.
            DT = ["0", "0", "0"]+Time
            YYYY = intt(Date)
            if YYYY < 100:
                if YYYY < 70:
                    YYYY += 2000
                else:
                    YYYY += 1900
            MMM = 0
            DD = 0
            M = 1
            for Month in PROG_CALMONS[1:]:
                try:
                    i = Date.index(Month)
                    MMM = M
                    DD = intt(Date[i+3:])
                    break
                except:
                    pass
                M += 1
            if Verify == True:
# DT values need to be strings for the dt2TimeVerify() intt() call. It's
# assumed the values would come from split()'ing something, so they would
# normally be strings to begin with.
                DT[0] = str(YYYY)
                DT[1] = str(MMM)
                DT[2] = str(DD)
                Ret = dt2TimeVerify("ymdhms", DT)
                if Ret[0] != 0:
                    return Ret
            DOY = dt2Timeymd2doy(YYYY, MMM, DD)
            HH = intt(Time[0])
            MM = intt(Time[1])
            SS = intt(Time[2])
        elif InFormat < 50:
# YYYYDOYHHMMSS
            if InFormat == 41:
                if DateTime.isdigit() == False:
                    if Verify == False:
                        return ""
                    else:
                        return (1, "RW", "Non-digits in value.", 2)
                YYYY = intt(DateTime[:4])
                DOY = intt(DateTime[4:7])
                HH = intt(DateTime[7:9])
                MM = intt(DateTime[9:11])
                SS = floatt(DateTime[11:])
                if Verify == True:
                    DT = []
                    DT.append(str(YYYY))
                    DT.append(str(DOY))
                    DT.append(str(HH))
                    DT.append(str(MM))
                    DT.append(str(SS))
                    Ret = dt2TimeVerify("ydhms", DT)
                    if Ret[0] != 0:
                        return Ret
                MMM, DD = dt2Timeydoy2md(YYYY, DOY)
            else:
                if Verify == False:
                    return ""
                else:
                    return (1, "MWX", "dt2Time: Unknown InFormat code (%d)."% \
                            InFormat, 3, "")
        elif InFormat < 60:
# YYYYMMDDHHMMSS
            if InFormat == 51:
                if DateTime.isdigit() == False:
                    if Verify == False:
                        return ""
                    else:
                        return (1, "RW", "Non-digits in value.", 2)
                YYYY = intt(DateTime[:4])
                MMM = intt(DateTime[4:6])
                DD = intt(DateTime[6:8])
                HH = intt(DateTime[8:10])
                MM = intt(DateTime[10:12])
                SS = floatt(DateTime[12:])
                if Verify == True:
                    DT = []
                    DT.append(str(YYYY))
                    DT.append(str(MMM))
                    DT.append(str(DD))
                    DT.append(str(HH))
                    DT.append(str(MM))
                    DT.append(str(SS))
                    Ret = dt2TimeVerify("ymdhms", DT)
                    if Ret[0] != 0:
                        return Ret
                DOY = dt2Timeymd2doy(YYYY, MMM, DD)
            else:
                if Verify == False:
                    return ""
                else:
                    return (1, "MWX", "dt2Time: Unknown InFormat code (%d)."% \
                            InFormat, 3, "")
# If the caller just wants to work with the seconds we'll split it up and
# return the number of seconds into the day. In this case the OutFormat value
# will not be used.
        elif InFormat == 100:
            Parts = DateTime.split(":")
            Parts += (3-len(Parts))*["0"]
            if Verify == True:
                Ret = dt2TimeVerify("hms", Parts)
                if Ret[0] != 0:
                    return Ret
            if Verify == False:
                return (intt(Parts[0])*3600)+(intt(Parts[1])*60)+ \
                        float(Parts[2])
            else:
                return (0, (intt(Parts[0])*3600)+(intt(Parts[1])*60)+ \
                        float(Parts[2]))
# Now that we have all of the parts do what the caller wants and return the
# result.
# Return the Epoch.
    if OutFormat == -1:
        try:
            Epoch = Y2EPOCH[YYYY]
        except KeyError:
            Epoch = 0.0
            for YYY in arange(1970, YYYY):
                if YYY%4 != 0:
                    Epoch += 31536000.0
                elif YYY%100 != 0 or YYY%400 == 0:
                    Epoch += 31622400.0
                else:
                    Epoch += 31536000.0
            Y2EPOCH[YYYY] = Epoch
        Epoch += ((DOY-1)*86400.0)+(HH*3600.0)+(MM*60.0)+SS
        if Verify == False:
            return Epoch
        else:
            return (0, Epoch)
    elif OutFormat == 0:
        if Verify == False:
            return YYYY, MMM, DD, DOY, HH, MM, SS
        else:
            return (0, YYYY, MMM, DD, DOY, HH, MM, SS)
    elif OutFormat == 1:
        Format = OPTDateFormatRVar.get()
        if Format == "YYYY:DOY":
            OutFormat = 11
        elif Format == "YYYY-MM-DD":
            OutFormat = 22
        elif Format == "YYYYMMMDD":
            OutFormat = 32
# Usually used for troubleshooting.
        elif len(Format) == 0:
            try:
                Epoch = Y2EPOCH[YYYY]
            except KeyError:
                for YYY in arange(1970, YYYY):
                    if YYY%4 != 0:
                        Epoch += 31536000.0
                    elif YYY%100 != 0 or YYY%400 == 0:
                        Epoch += 31622400.0
                    else:
                        Epoch += 31536000.0
                Y2EPOCH[YYYY] = Epoch
            Epoch += ((DOY-1)*86400.0)+(HH*3600.0)+(MM*60.0)+SS
            if Verify == False:
                return Epoch
            else:
                return (0, Epoch)
# This is the easiest way I can think of to keep an SS of 59.9999 from being
# rounded and formatted to 60.000. Some stuff at some point may slip into the
# microsecond realm. Then this whole library of functions may have to be
# changed.
    if SS%1 > .999:
        SS = int(SS)+.999
# These OutFormat values are the same as InFormat values, plus others.
# YYYY:DOY:HH:MM:SS.sss
    if OutFormat == 11:
        if Verify == False:
            return "%d:%03d:%02d:%02d:%06.3f"%(YYYY, DOY, HH, MM, SS)
        else:
            return (0, "%d:%03d:%02d:%02d:%06.3f"%(YYYY, DOY, HH, MM, SS))
# YYYY:DOY HH:MM:SS.sss
    elif OutFormat == 12:
        if Verify == False:
            return "%d:%03d %02d:%02d:%06.3f"%(YYYY, DOY, HH, MM, SS)
        else:
            return (0, "%d:%03d %02d:%02d:%06.3f"%(YYYY, DOY, HH, MM, SS))
# YYYY:DOY - just because it's popular (for LOGPEEK) and it saves having the
# caller always doing  .split()[0]
    elif OutFormat == 13:
        if Verify == False:
            return "%d:%03d"%(YYYY, DOY)
        else:
            return (0, "%d:%03d"%(YYYY, DOY))
# YYYY:DOY:HH:MM:SS - just because it's really popular in POCUS and other
# programs.
    elif OutFormat == 14:
        if Verify == False:
            return "%d:%03d:%02d:%02d:%02d"%(YYYY, DOY, HH, MM, int(SS))
        else:
            return (0, "%d:%03d:%02d:%02d:%02d"%(YYYY, DOY, HH, MM, int(SS)))
# YYYY-MM-DD:HH:MM:SS.sss
    elif OutFormat == 21:
        if Verify == False:
            return "%d-%02d-%02d:%02d:%02d:%06.3f"%(YYYY, MMM, DD, HH, MM, SS)
        else:
            return (0, "%d-%02d-%02d:%02d:%02d:%06.3f"%(YYYY, MMM, DD, HH, \
                    MM, SS))
# YYYY-MM-DD HH:MM:SS.sss
    elif OutFormat == 22:
        if Verify == False:
            return "%d-%02d-%02d %02d:%02d:%06.3f"%(YYYY, MMM, DD, HH, MM, SS)
        else:
            return (0, "%d-%02d-%02d %02d:%02d:%06.3f"%(YYYY, MMM, DD, HH, \
                    MM, SS))
# YYYY-MM-DD/DOY HH:MM:SS.sss
    elif OutFormat == 23:
        if Verify == False:
            return "%d-%02d-%02d/%03d %02d:%02d:%06.3f"%(YYYY, MMM, DD, DOY, \
                    HH, MM, SS)
        else:
            return (0, "%d-%02d-%02d/%03d %02d:%02d:%06.3f"%(YYYY, MMM, DD, \
                    DOY, HH, MM, SS))
# Some portion of YYYY-MM-DD HH:MM:SS. Returns integer seconds.
# In that this is a human-entered thing (programs don't store partial
# date/times) we'll return whatever was entered without the /DOY, since the
# next step would be to do something like look it up in a database.
    elif OutFormat == 24:
        DateTime = "%d"%YYYY
        if MMM != -1:
            DateTime += "-%02d"%MMM
        else:
# Return what we have if this item was not provided, same on down.
            if Verify == False:
                return DateTime
            else:
                return (0, DateTime)
        if DD != -1:
            DateTime += "-%02d"%DD
        else:
            if Verify == False:
                return DateTime
            else:
                return (0, DateTime)
        if HH != -1:
            DateTime += " %02d"%HH
        else:
            if Verify == False:
                return DateTime
            else:
                return (0, DateTime)
        if MM != "-1":
            DateTime += ":%02d"%MM
        else:
            if Verify == False:
                return DateTime
            else:
                return (0, DateTime)
# Returns integer second since the caller has no idea what is coming back.
        if SS != "-1":
            DateTime += ":%02d"%SS
        if Verify == False:
            return DateTime
        else:
            return (0, DateTime)
# YYYY-MM-DD
    elif OutFormat == 25:
        if Verify == False:
            return "%d-%02d-%02d"%(YYYY, MMM, DD)
        else:
            return (0, "%d-%02d-%02d"%(YYYY, MMM, DD))
# YYYYMMMDD:HH:MM:SS.sss
    elif OutFormat == 31:
        if Verify == False:
            return "%d%s%02d:%02d:%02d:%06.3f"%(YYYY, PROG_CALMONS[MMM], DD, \
                    HH, MM, SS)
        else:
            return (0, "%d%s%02d:%02d:%02d:%06.3f"%(YYYY, PROG_CALMONS[MMM], \
                    DD, HH, MM, SS))
# YYYYMMMDD HH:MM:SS.sss
    elif OutFormat == 32:
        if Verify == False:
            return "%d%s%02d %02d:%02d:%06.3f"%(YYYY, PROG_CALMONS[MMM], DD, \
                    HH, MM, SS)
        else:
            return (0, "%d%s%02d %02d:%02d:%06.3f"%(YYYY, PROG_CALMONS[MMM], \
                    DD, HH, MM, SS))
# YYYYDOYHHMMSS.sss
    elif OutFormat == 41:
        if Verify == False:
            return "%d%03d%02d%02d%06.3f"%(YYYY, DOY, HH, MM, SS)
        else:
            return (0, "%d%03d%02d%02d%06.3f"%(YYYY, DOY, HH, MM, SS))
# YYYYMMDDHHMMSS.sss
    elif OutFormat == 51:
        if Verify == False:
            return "%d%02d%02d%02d%02d%06.3f"%(YYYY, MMM, DD, HH, MM, SS)
        else:
            return (0, "%d%02d%02d%02d%02d%06.3f"%(YYYY, MMM, DD, HH, MM, SS))
# Returns what ever OPTDateFormatRVar is set to.
# 80 is dt, 81 is d and 82 is t.
    elif OutFormat == 80 or OutFormat == 81 or OutFormat == 82:
        if OPTDateFormatRVar.get() == "YYYY:DOY":
            if OutFormat == 80:
                if Verify == False:
                    return "%d:%03d:%02d:%02d:%06.3f"%(YYYY, DOY, HH, MM, SS)
                else:
                    return (0, "%d:%03d:%02d:%02d:%06.3f"%(YYYY, DOY, HH, MM, \
                            SS))
            elif OutFormat == 81:
                if Verify == False:
                    return "%d:%03d"%(YYYY, DOY)
                else:
                    return (0, "%d:%03d"%(YYYY, DOY))
            elif OutFormat == 82:
                if Verify == False:
                    return "%02d:%02d:%06.3f"%(HH, MM, SS)
                else:
                    return (0, "%02d:%02d:%06.3f"%(HH, MM, SS))
        elif OPTDateFormatRVar.get() == "YYYY-MM-DD":
            if OutFormat == 80:
                if Verify == False:
                    return "%d-%02d-%02d %02d:%02d:%06.3f"%(YYYY, MMM, DD, \
                            HH, MM, SS)
                else:
                    return (0, "%d-%02d-%02d %02d:%02d:%06.3f"%(YYYY, MMM, \
                            DD, HH, MM, SS))
            elif OutFormat == 81:
                if Verify == False:
                    return "%d-%02d-%02d"%(YYYY, MMM, DD)
                else:
                    return (0, "%d-%02d-%02d"%(YYYY, MMM, DD))
            elif OutFormat == 82:
                if Verify == False:
                    return "%02d:%02d:%06.3f"%(HH, MM, SS)
                else:
                    return (0, "%02d:%02d:%06.3f"%(HH, MM, SS))
        elif OPTDateFormatRVar.get() == "YYYYMMMDD":
            if OutFormat == 80:
                if Verify == False:
                    return "%d%s%02d %02d:%02d:%06.3f"%(YYYY, \
                            PROG_CALMONS[MMM], DD, HH, MM, SS)
                else:
                    return (0, "%d%s%02d %02d:%02d:%06.3f"%(YYYY, \
                            PROG_CALMONS[MMM], DD, HH, MM, SS))
            elif OutFormat == 81:
                if Verify == False:
                    return "%d%s%02d"%(YYYY, PROG_CALMONS[MMM], DD)
                else:
                    return (0, "%d%s%02d"%(YYYY, PROG_CALMONS[MMM], DD))
            elif OutFormat == 82:
                if Verify == False:
                    return "%02d:%02d:%06.3f"%(HH, MM, SS)
                else:
                    return (0, "%02d:%02d:%06.3f"%(HH, MM, SS))
        elif len(OPTDateFormatRVar.get()) == 0:
            if Verify == False:
                return str(DateTime)
            else:
                return (0, str(DateTime))
    else:
        if Verify == False:
            return ""
        else:
            return (1, "MWX", "dt2Time: Unknown OutFormat code (%d)."% \
                    OutFormat, 3, "")
################################
# BEGIN: dt2Timedhms2Secs(InStr)
# FUNC:dt2Timedhms2Secs():2018.235
#   Returns the number of seconds in strings like 1h30m.
def dt2Timedhms2Secs(InStr):
    InStr = InStr.replace(" ", "").lower()
    if len(InStr) == 0:
        return 0
    Chars = list(InStr)
    Value = 0
    SubValue = ""
    for Char in Chars:
        if Char.isdigit():
            SubValue += Char
        elif Char == "s":
            Value += intt(SubValue)
            SubValue = ""
        elif Char == "m":
            Value += intt(SubValue)*60
            SubValue = ""
        elif Char == "h":
            Value += intt(SubValue)*3600
            SubValue = ""
        elif Char == "d":
            Value += intt(SubValue)*86400
            SubValue = ""
# Must have just been passed a number with no s m h or d or 1h30 which will be
# treated as 1 hour, 30 seconds.
    if len(SubValue) != 0:
        Value += intt(SubValue)
    return Value
#############################################
# BEGIN: dt2TimeDT(Format, DateTime, Fix = 2)
# FUNC:dt2TimeDT():2018.270
#   This is for the one-off time conversion items that have been needed here
#   and there for specific items. Input is some string of date and/or time.
#   Some of these can be done with dt2Time(), but these are more for when the
#   code knows exactly what it has and exactly what it needs. It's program
#   dependent and maybe useful to others.
#     Format = 1 = [dd]hhmmss or even blank to [DD:]HH:MM:SS
#                  Replaces dhms2DHMS().
#              2 = YYYY:DOY:HH:MM:SS to ISO8601 time YYYY-MM-DDTHH:MM:SSZ.
#                  No time zone conversion is done (or any error checking) so
#                  you need to make sure the passed time is really Zulu or this
#                  will return a lie.
#                  Replaces YDHMS28601().
#              3 = The opposite of above.
#                  Replaces iso86012YDHMS().
#              4 = Adds /DOY to a passed date/time that can be just the date,
#                  but that must be a YYYY-MM-DD format. Date and time must be
#                  separated by a space. It's the only date format that I've
#                  ever wanted to add the DOY to. Returns the fixed up
#                  date/time. Does NO error checking, and, as you can see, if
#                  the date is not the right format it will choke causing an
#                  embarrassing crash. You have been warned.
#                  Replaces addDDD().
#              5 = Anti-InFormat 4. Is OK if /DOY is not there.
#                  Replaces remDDD().
#              6 = yyydoyhhmmss... to YYYY:DOY:HH:MM...
#                  Replaces ydhmst2YDHMST().
#              7 = YYYY:DOY:HH:MM:SS -> YYYY-MM-DD:HH:MM:SS.
#                  Replaces ydhms2ymdhmsDash().
#              8 = YYYYMMDDHHMMSS to YYYY-MM-DD HH:MM:SS
#              9 = YYYYMMDDHHMMSS to YYYY-MM-DD
#             10 = HH:MM:SS to int H, M, S
#             11 = Seconds to HH:MM:SS (Use Fix to pad with extra blanks if
#                  large hour numbers are expected)
#             12 = HH:MM:SS/HH:MM/HH/blank to seconds
def dt2TimeDT(Format, DateTime, Fix = 2):
# Everything wants a string, except 11. Python 3 will probably pass DateTime
# as bytes.
    if Format != 11:
        if isinstance(DateTime, astring) == False:
            DateTime = DateTime.decode("latin-1")
    if Format == 1:
# Check the length before the .strip(). DateTime may be just blanks, but we'll
# use the length to determine what to return.
        Len = len(DateTime)
        DateTime = DateTime.strip()
        if len(DateTime) == 0:
            return "00:00:00:00"
        if Len == 6:
            return "%s:%s:%s"%(DateTime[:2], DateTime[2:4], DateTime[4:6])
        else:
            return "%s:%s:%s:%s"%(DateTime[:2], DateTime[2:4], DateTime[4:6], \
                    DateTime[6:8])
    elif Format == 2:
        Parts = DateTime.split(":")
        if len(Parts) != 5:
            return ""
        MM, DD, = dt2Timeydoy2md(intt(Parts[0]), intt(Parts[1]))
        return "%s-%02d-%02dT%s:%s:%sZ"%(Parts[0], MM, DD, Parts[2], \
            Parts[3], Parts[4])
    elif Format == 3:
        Parts = DateTime.split("T")
        if len(Parts) != 2:
            return ""
# Who knows what may be wrong with the passed time.
        try:
            Parts2 = Parts[0].split("-")
            YYYY = intt(Parts2[0])
            MMM = intt(Parts2[1])
            DD = intt(Parts2[2])
            DOY = dt2Timeymd2doy(YYYY, MMM, DD)
            Parts2 = Parts[1].split(":")
            HH = intt(Parts2[0])
            MM = intt(Parts2[1])
            SS = intt(Parts2[2])
        except:
            return ""
        return "%d:%03d:%02d:%02d:%02d"%(YYYY, DOY, HH, MM, SS)
    elif Format == 4:
        Parts = DateTime.split()
        if len(Parts) == 0:
            return ""
        Parts2 = Parts[0].split("-")
        YYYY = intt(Parts2[0])
        MMM = intt(Parts2[1])
        DD = intt(Parts2[2])
        DOY = dt2Timeymd2doy(YYYY, MMM, DD)
        if len(Parts) == 1:
            return "%s/%03d"%(Parts[0], DOY)
        else:
            return "%s/%03d %s"%(Parts[0], DOY, Parts[1])
    elif Format == 5:
        Parts = DateTime.split()
        if len(Parts) == 0:
            return ""
# A little bit of protection.
        try:
            i = Parts[0].index("/")
            Parts[0] = Parts[0][:i]
        except ValueError:
            pass
        if len(Parts) == 1:
            return Parts[0]
        else:
            return "%s %s"%(Parts[0], Parts[1])
# yyyydoyhhmmssttt or yyyydoyhhmmss.
    elif Format == 6:
        Len = len(DateTime)
        DateTime = DateTime.strip()
# yyyydoyhhmm
        if Len == 12:
            if len(DateTime) == 0:
                return "0000:000:00:00"
            return "%s:%s:%s:%s"%(DateTime[:4], DateTime[4:7], DateTime[7:9], \
                    DateTime[9:11])
# yyyydoyhhmmss  All of the fields that may contain this are 14 bytes long
# with a trailing space which got removed above (it was that way in LOGPEEK
# anyways).
        elif Len == 14:
            if len(DateTime) == 0:
                return "0000:000:00:00:00"
            return "%s:%s:%s:%s:%s"%(DateTime[:4], DateTime[4:7], \
                    DateTime[7:9], DateTime[9:11], DateTime[11:13])
# yyyydoyhhmmssttt
        elif Len == 16:
            if len(DateTime) == 0:
                return "0000:000:00:00:00:000"
            return "%s:%s:%s:%s:%s:%s"%(DateTime[:4], DateTime[4:7], \
                    DateTime[7:9], DateTime[9:11], DateTime[11:13], \
                    DateTime[13:])
        return ""
    elif Format == 7:
        Parts = DateTime.split(":")
        YYYY = intt(Parts[0])
        DOY = intt(Parts[1])
        MMM, DD = dt2Timeydoy2md(YYYY, DOY)
        return "%d-%02d-%02d:%s:%s:%s"%(YYYY, MMM, DD, Parts[2], Parts[3], \
                Parts[4])
# YYYYMMDDHHMMSS to YYYY-MM-DD HH:MM:SS
    elif Format == 8:
        if len(DateTime) == 14:
            return "%s-%s-%s %s:%s:%s"%(DateTime[0:4], DateTime[4:6], \
                    DateTime[6:8], DateTime[8:10], DateTime[10:12], \
                    DateTime[12:14])
        return ""
# YYYYMMDDHHMMSS to YYYY-MM-DD
    elif Format == 9:
        if len(DateTime) == 14:
            return "%s-%s-%s"%(DateTime[0:4], DateTime[4:6], DateTime[6:8])
        return ""
# HH:MM:SS to int H, M, S
    elif Format == 10:
        try:
            Parts2 = DateTime.split(":")
            Parts = []
            for Part in Parts2:
                Parts.append(intt(Part))
            Parts += (3-len(Parts))*[0]
        except:
            return 0, 0, 0
        return Parts[0], Parts[1], Parts[2]
# Seconds to HH:MM:SS (use Fix to pad with spaces)
    elif Format == 11:
        HH = DateTime//3600
        Sub = HH*3600
        MM = (DateTime-Sub)//60
        Sub += MM*60
        SS = (DateTime-Sub)
        H = "%02d"%HH
        if Fix > 2:
            H = "%*s"%(Fix, H)
        return "%s:%02d:%02d"%(H, MM, SS)
# HH:MM:SS to seconds
    elif Format == 12:
        try:
            Parts2 = DateTime.split(":")
            Parts = []
            for Part in Parts2:
                Parts.append(intt(Part))
            Parts += (3-len(Parts))*[0]
        except:
            return 0
        return Parts[0]*3600+Parts[1]*60+Parts[2]
######################################################################
# BEGIN: dt2TimeMath(DeltaDD, DeltaSS, YYYY, MMM, DD, DOY, HH, MM, SS)
# LIB:dt2TimeMath():2013.039
#   Adds or subtracts (depending on the sign of DeltaDD/DeltaSS) the requested
#   amount of time to/from the passed date.  Returns a tuple with the results:
#           (YYYY, MMM, DD, DOY, HH, MM, SS)
#   Pass -1 or 0 for MMM/DD, or DOY depending on which is to be used. If MMM/DD
#   are >=0 then MMM, DD, and DOY will be filled in on return. If MMM/DD is -1
#   then just DOY will be used/returned. If MMM/DD are 0 then DOY will be used
#   but MMM and DD will also be returned.
#   SS will be int or float depending on what was passed.
def dt2TimeMath(DeltaDD, DeltaSS, YYYY, MMM, DD, DOY, HH, MM, SS):
    DeltaDD = int(DeltaDD)
    DeltaSS = int(DeltaSS)
    if YYYY < 1000:
        if YYYY < 70:
            YYYY += 2000
        else:
            YYYY += 1900
# Work in DOY.
    if MMM > 0:
        DOY = dt2Timeymd2doy(YYYY, MMM, DD)
    if DeltaDD != 0:
        if DeltaDD > 0:
            Forward = 1
        else:
            Forward = 0
        while 1:
# Speed limit the change to keep things simpler.
            if DeltaDD < -365 or DeltaDD > 365:
                if Forward == 1:
                    DOY += 365
                    DeltaDD -= 365
                else:
                    DOY -= 365
                    DeltaDD += 365
            else:
                DOY += DeltaDD
                DeltaDD = 0
            if YYYY%4 != 0:
                Leap = 0
            elif YYYY%100 != 0 or YYYY%400 == 0:
                Leap = 1
            else:
                Leap = 0
            if DOY < 1 or DOY > 365+Leap:
                if Forward == 1:
                    DOY -= 365+Leap
                    YYYY += 1
                else:
                    YYYY -= 1
                    if YYYY%4 != 0:
                        Leap = 0
                    elif YYYY%100 != 0 or YYYY%400 == 0:
                        Leap = 1
                    else:
                        Leap = 0
                    DOY += 365+Leap
            if DeltaDD == 0:
                break
    if DeltaSS != 0:
        if DeltaSS > 0:
            Forward = 1
        else:
            Forward = 0
        while 1:
# Again, speed limit just to keep the code reasonable.
            if DeltaSS < -59 or DeltaSS > 59:
                if Forward == 1:
                    SS += 59
                    DeltaSS -= 59
                else:
                    SS -= 59
                    DeltaSS += 59
            else:
                SS += DeltaSS
                DeltaSS = 0
            if SS < 0 or SS > 59:
                if Forward == 1:
                    SS -= 60
                    MM += 1
                    if MM > 59:
                        MM = 0
                        HH += 1
                        if HH > 23:
                            HH = 0
                            DOY += 1
                            if DOY > 365:
                                if YYYY%4 != 0:
                                    Leap = 0
                                elif YYYY%100 != 0 or YYYY%400 == 0:
                                    Leap = 1
                                else:
                                    Leap = 0
                                if DOY > 365+Leap:
                                    YYYY += 1
                                    DOY = 1
                else:
                    SS += 60
                    MM -= 1
                    if MM < 0:
                        MM = 59
                        HH -= 1
                        if HH < 0:
                            HH = 23
                            DOY -= 1
                            if DOY < 1:
                                YYYY -= 1
                                if YYYY%4 != 0:
                                    DOY = 365
                                elif YYYY%100 != 0 or YYYY%400 == 0:
                                    DOY = 366
                                else:
                                    DOY = 365
            if DeltaSS == 0:
                 break
    if MMM != -1:
        MMM, DD = dt2Timeydoy2md(YYYY, DOY)
    return (YYYY, MMM, DD, DOY, HH, MM, SS)
####################################
# BEGIN: dt2TimeVerify(Which, Parts)
# FUNC:dt2TimeVerify():2015.048
#   This could figure out what to check just by passing [Y,M,D,D,H,M,S], but
#   that means the splitters() in dt2Time would need to work much harder to
#   make sure all of the Parts were there, thus it needs a Which value.
def dt2TimeVerify(Which, Parts):
    if Which.startswith("ydhms"):
        YYYY = intt(Parts[0])
        MMM = -1
        DD = -1
        DOY = intt(Parts[1])
        HH = intt(Parts[2])
        MM = intt(Parts[3])
        SS = floatt(Parts[4])
    elif Which.startswith("ymdhms") or Which.startswith("xymdhms"):
        YYYY = intt(Parts[0])
        MMM = intt(Parts[1])
        DD = intt(Parts[2])
        DOY = -1
        HH = intt(Parts[3])
        MM = intt(Parts[4])
        SS = floatt(Parts[5])
    elif Which.startswith("hms"):
        YYYY = -1
        MMM = -1
        DD = -1
        DOY = -1
        HH = intt(Parts[0])
        MM = intt(Parts[1])
        SS = floatt(Parts[2])
    if YYYY != -1 and (YYYY < 1000 or YYYY > 9999):
        return (1, "RW", "Bad year value: %d"%YYYY, 2, "")
# Normally do the normal checking.
    if Which.startswith("x") == False:
        if MMM != -1 and (MMM < 1 or MMM > 12):
            return (1, "RW", "Bad month value: %d"%MMM, 2, "")
        if DD != -1:
            if DD < 1 or DD > 31:
                return (1, "RW", "Bad day value: %d"%DD, 2, "")
            if YYYY%4 != 0:
                if DD > PROG_MAXDPMNLY[MMM]:
                    return (1, "RW", "Too many days for month %d: %d"%(MMM, \
                            DD), 2, "")
            elif YYYY%100 != 0 or YYYY%400 == 0:
                if DD > PROG_MAXDPMLY[MMM]:
                    return (1, "RW", "Too many days for month %d: %d"%(MMM, \
                            DD), 2, "")
            else:
                if DD > PROG_MAXDPMNLY[MMM]:
                    return (1, "RW", "Too many days for month %d: %d"%(MMM, \
                            DD), 2, "")
        if DOY != -1:
            if DOY < 1 or DOY > 366:
                return (1, "RW", "Bad day of year value: %d"%DOY, 2, "")
            if YYYY%4 != 0 and DOY > 365:
                return (1, "RW" "Too many days for non-leap year: %d"%DOY, \
                        2, "")
        if HH < 0 or HH >= 24:
            return (1, "RW", "Bad hour value: %d"%HH, 2, "")
        if MM < 0 or MM >= 60:
            return (1, "RW", "Bad minute value: %d"%MM, 2, "")
        if SS < 0 or SS >= 60:
            return (1, "RW", "Bad seconds value: %06.3f"%SS, 2, "")
# "x" checks.
# Here if Which starts with "x" then it is OK if month and day values are
# missing. This is for checking an entry like  2013-7  for example. If the
# portion of the date(/time) that was passed looks OK then (0,) will be
# returned. If it is something like  2013-13  then that will be bad.
    else:
# If the user entered just the year, then we are done, etc.
        if MMM != -1:
            if MMM < 1 or MMM > 12:
                return (1, "RW", "Bad month value: %d"%MMM, 2, "")
            if DD != -1:
                if DD < 1 or DD > 31:
                    return (1, "RW", "Bad day value: %d"%DD, 2, "")
                if YYYY%4 != 0:
                    if DD > PROG_MAXDPMNLY[MMM]:
                        return (1, "RW", "Too many days for month %d: %d"% \
                                (MMM, DD), 2, "")
                elif YYYY%100 != 0 or YYYY%400 == 0:
                    if DD > PROG_MAXDPMLY[MMM]:
                        return (1, "RW", "Too many days for month %d: %d"% \
                                (MMM, DD), 2, "")
                else:
                    if DD > PROG_MAXDPMNLY[MMM]:
                        return (1, "RW", "Too many days for month %d: %d"% \
                                (MMM, DD), 2, "")
                if DOY != -1:
                    if DOY < 1 or DOY > 366:
                        return (1, "RW", "Bad day of year value: %d"%DOY, 2, \
                                "")
                    if YYYY%4 != 0 and DOY > 365:
                        return (1, "RW" \
                                "Too many days for non-leap year: %d"%DOY, \
                                2, "")
                if HH != -1:
                    if HH < 0 or HH >= 24:
                        return (1, "RW", "Bad hour value: %d"%HH, 2, "")
                    if MM != -1:
                        if MM < 0 or MM >= 60:
                            return (1, "RW", "Bad minute value: %d"%MM, 2, "")
# Checking these special values are usually from user input and are date/time
# values and not timing values, so the seconds part here will just be an int.
                        if SS != -1:
                            if SS < 0 or SS >= 60:
                                return (1, "RW", "Bad seconds value: %d"%SS, \
                                        2, "")
    return (0,)
##########################################
# BEGIN: dt2TimeydoyMath(Delta, YYYY, DOY)
# LIB:dt2TimeydoyMath():2013.036
#   Adds or subtracts the passed number of days (Delta) to the passed year and
#   day of year values. Returns YYYY and DOY.
#   Replaces ydoyMath().
def dt2TimeydoyMath(Delta, YYYY, DOY):
    if Delta == 0:
        return YYYY, DOY
    if Delta > 0:
        Forward = 1
    elif Delta < 0:
        Forward = 0
    while 1:
# Speed limit the change to keep things simpler.
        if Delta < -365 or Delta > 365:
            if Forward == 1:
                DOY += 365
                Delta -= 365
            else:
                DOY -= 365
                Delta += 365
        else:
            DOY += Delta
            Delta = 0
# This was isLeap(), but it's such a simple thing I'm not sure it was worth the
# function call, so it became this in all library functions.
        if YYYY%4 != 0:
            Leap = 0
        elif YYYY%100 != 0 or YYYY%400 == 0:
            Leap = 1
        else:
            Leap = 0
        if DOY < 1 or DOY > 365+Leap:
            if Forward == 1:
                DOY -= 365+Leap
                YYYY += 1
            else:
                YYYY -= 1
                if YYYY%4 != 0:
                    Leap = 0
                elif YYYY%100 != 0 or YYYY%400 == 0:
                    Leap = 1
                else:
                    Leap = 0
                DOY += 365+Leap
            break
        if Delta == 0:
            break
    return YYYY, DOY
##################################
# BEGIN: dt2Timeydoy2md(YYYY, DOY)
# FUNC:dt2Timeydoy2md():2013.030
#   Does no values checking, so make sure you stuff is in one sock before
#   coming here.
def dt2Timeydoy2md(YYYY, DOY):
    if DOY < 32:
        return 1, DOY
    elif DOY < 60:
        return 2, DOY-31
    if YYYY%4 != 0:
        Leap = 0
    elif YYYY%100 != 0 or YYYY%400 == 0:
        Leap = 1
    else:
        Leap = 0
# Check for this special day.
    if Leap == 1 and DOY == 60:
        return 2, 29
# The PROG_FDOM values for Mar-Dec are set up for non-leap years. If it is a
# leap year and the date is going to be Mar-Dec (it is if we have made it this
# far), subtract Leap from the day.
    DOY -= Leap
# We start through PROG_FDOM looking for dates in March.
    Month = 3
    for FDOM in PROG_FDOM[4:]:
# See if the DOY is less than the first day of next month.
        if DOY <= FDOM:
# Subtract the DOY for the month that we are in.
            return Month, DOY-PROG_FDOM[Month]
        Month += 1
# If anything goes wrong...
    return 0, 0
######################################
# BEGIN: dt2Timeymd2doy(YYYY, MMM, DD)
# FUNC:dt2Timeymd2doy():2013.030
def dt2Timeymd2doy(YYYY, MMM, DD):
    if YYYY%4 != 0:
        return (PROG_FDOM[MMM]+DD)
    elif (YYYY%100 != 0 or YYYY%400 == 0) and MMM > 2:
        return (PROG_FDOM[MMM]+DD+1)
    else:
        return (PROG_FDOM[MMM]+DD)
##################################################################
# BEGIN: dt2Timeymddhms(OutFormat, YYYY, MMM, DD, DOY, HH, MM, SS)
# FUNC:dt2Timeymddhms():2018.235
#   The general-purpose time handler for when the time is already split up into
#   it's parts.
#   Make MMM, DD -1 if passing DOY and DOY -1 if passing MMM, DD.
def dt2Timeymddhms(OutFormat, YYYY, MMM, DD, DOY, HH, MM, SS):
    global Y2EPOCH
# Returns a float Epoch is SS is a float, otherwise an int value.
    if OutFormat == -1:
        Epoch = 0
        if YYYY < 70:
            YYYY += 2000
        if YYYY < 100:
            YYYY += 1900
        try:
            Epoch = Y2EPOCH[YYYY]
        except KeyError:
            for YYY in arange(1970, YYYY):
                if YYY%4 != 0:
                    Epoch += 31536000
                elif YYY%100 != 0 or YYY%400 == 0:
                    Epoch += 31622400
                else:
                    Epoch += 31536000
            Y2EPOCH[YYYY] = Epoch
        if DOY == -1:
            DOY = dt2Timeymd2doy(YYYY, MMM, DD)
        return Epoch+((DOY-1)*86400)+(HH*3600)+(MM*60)+SS
##########################################
# BEGIN: dt2Timeystr2Epoch(YYYY, DateTime)
# FUNC:dt2Timeystr2Epoch():2018.238
#   A slightly specialized time to Epoch converter where the input can be
#       YYYY, DOY:HH:MM:SS:TTT or
#       None,YYYY:DOY:HH:MM:SS:TTT or
#       None,YYYY-MM-DD:HH:MM:SS:TTT
#   DOY/DD and HH must be separated by a ':' -- no space.
#   The separator between SS and TTT may be : or .
#   Returns 0 if ANYTHING goes wrong.
#   Returns a float if the seconds were a float (SS:TTT or SS.ttt), or an int
#   if the time was incomplete or there was an error. The caller will just
#   have to do an int() if that is all they want.
#   Replaces str2Epoch(). Used a lot in RT130/72A data decoding.
def dt2Timeystr2Epoch(YYYY, DateTime):
    global Y2EPOCH
    Epoch = 0
    try:
        if DateTime.find("-") != -1:
            DT = DateTime.split(":", 1)
            Parts2 = DT[0].split("-")
            Parts = []
            for Parts in Parts2:
                Parts.append(float(Part))
            Parts2 = DT[1].split(":")
            for Parts in Parts2:
                Parts.append(float(Part))
# There "must" have been a :TTT part.
            if len(Parts) == 6:
                Parts[5] += Parts[6]/1000.0
        else:
            Parts2 = DateTime.split(":")
            Parts = []
            for Part in Parts2:
                Parts.append(float(Part))
# There "must" have been a :TTT part.
            if len(Parts) == 7:
                Parts[5] += Parts[6]/1000.0
        if YYYY is None:
# Change this since it gets used in loops and dictionary keys and stuff. If
# the year is passed (below) then we're already covered.
            Parts[0] = int(Parts[0])
# Just in case someone generates 2-digit years.
            if Parts[0] < 100:
                if Parts[0] < 70:
                    Parts[0] += 2000
                else:
                    Parts[0] += 1900
        else:
# Check this just in case. The caller will just have to figure this one out.
            if YYYY < 1970:
                YYYY = 1970
            Parts = [YYYY, ]+Parts
        Parts += (5-len(Parts))*[0]
# This makes each year's Epoch get calculated only once.
        try:
            Epoch = Y2EPOCH[Parts[0]]
        except KeyError:
            for YYY in arange(1970, Parts[0]):
                if YYY%4 != 0:
                    Epoch += 31536000
                elif YYY%100 != 0 or YYY%400 == 0:
                    Epoch += 31622400
                else:
                    Epoch += 31536000
            Y2EPOCH[Parts[0]] = Epoch
# This goes along with forcing YYYY to 1970 if things are bad.
        if Parts[1] < 1:
            Parts[1] = 1
        Epoch += ((int(Parts[1])-1)*86400)+(int(Parts[2])*3600)+ \
                (int(Parts[3])*60)+Parts[4]
    except ValueError:
        Epoch = 0
    return Epoch
##########################################################
# BEGIN: dtHoursFromNow(ToFormat, OutFormat, DateTime, TZ)
# FUNC:dtHoursFromNow():2018.264
#   Takes in a time (presumably in the future) and returns the decimal hours
#   from now until then.
#   ToFormat is the format of the time we are going to.
#   OutFormat is how to format the return time, "H" for decimal hours, or "H:M"
#   for HH:MM (no seconds).
#   TZ is either "GMT" or "LT" pertaining to the input DateTime.
#   The current time it uses (in YYYY:DOY:HH:MM:SS) is also returned.
#       (decimal hours, current time used)
def dtHoursFromNow(ToFormat, OutFormat, DateTime, TZ):
    InEpoch = dt2Time(ToFormat, -1, DateTime)
    if TZ == "GMT":
        NowDateTime = getGMT(0)
    elif TZ == "LT":
        NowDateTime = getGMT(13)
    NowEpoch = dt2Time(11, -1, NowDateTime)
    Diff = InEpoch-NowEpoch
    if OutFormat == "H":
        return ("%.2f hours"%(Diff/3600.0), NowDateTime)
    elif OutFormat == "H:M":
        H = Diff/3600.0
        M = int((H-int(H))*60.0)
        H = int(H)
# Rounding may cause this.
        if M > 59:
            H += 1
            M = M-60
        return ("%02d:%02d"%(H, M), NowDateTime)
    return ("0.00", "0000:000:00:00:00")
############################
# BEGIN: dt2DateDiff(D1, D2)
# FUNC:dt2DateDiff():2018.238
#   Takes two dates in YYYY-MM-DD format and returns the number of whole days
#   between them. Dates must be proper format or there will be crashing.
#   Returned is D2-D1, so D2 should be later than D1, but doesn't have to be.
def dt2DateDiff(D1, D2):
    Parts = D1.split("-")
    D1Y = int(Parts[0])
    D1M = int(Parts[1])
    D1D = int(Parts[2])
    Parts = D2.split("-")
    D2Y = int(Parts[0])
    D2M = int(Parts[1])
    D2D = int(Parts[2])
# Pick off the easy one.
    if D1Y == D2Y and D1M == D2M:
        return D2D-D1D
# Almost easy.
    elif D1Y == D2Y:
        D1Doy = dt2Timeymd2doy(D1Y, D1M, D1D)
        D2Doy = dt2Timeymd2doy(D2Y, D2M, D2D)
        return D2Doy-D1Doy
    else:
# The full monty.
        D1E = dt2Timeymddhms(-1, D1Y, D1M, D1D, -1, 0, 0, 0)
        D2E = dt2Timeymddhms(-1, D2Y, D2M, D2D, -1, 0, 0, 0)
        return int((D2E-D1E)/86400)
# END: dt2Time




####################
# BEGIN: eraseMsgs()
# LIB:eraseMsgs():2018.244
HELPError += "eraseMsgs():\n\
------------\n\
Error erasing file <file> <message>\n\
-- A system error ocurred when trying to erase and recreate the messages \
file. The <message> should explain what happened.\n"

def eraseMsgs():
    global PROGMsgsFile
    TheMsgsDir = PROGMsgsDirVar.get()
    Answer = formMYD(Root, (("Start A New Messages File", TOP, "new"), \
            ("Erase Current File Contents", TOP, "erase"), ("(Cancel)", TOP, \
            "cancel")), "cancel", "", "Erase All Evidence Or What?", \
            "Erase the messages section and start a new messages file, OR erase the contents of the current messages file\n\n%s\n\nand keep using it (generally the old file should be preserved and not erased if in the middle of an experiment)?"% \
            PROGMsgsFile)
    if Answer == "cancel":
        return
    elif Answer == "new":
        msgLn(0, "", "", False, 0, False)
        if len(PROGMachineIDLCVar.get()) == 0:
            PROGMsgsFile = getGMT(1)[:7]+"-"+PROG_NAMELC+".msg"
        else:
            PROGMsgsFile = PROGMachineIDLCVar.get()+"-"+getGMT(1)[:7]+"-"+ \
                    PROG_NAMELC+".msg"
        msgLn(9, "", "Working with messages file\n   %s"%(TheMsgsDir+ \
                PROGMsgsFile))
        msgLn(9, "", "-----")
        loadPROGMsgs()
    elif Answer == "erase":
        try:
# If the file already exists erase it first.
            if exists(TheMsgsDir+PROGMsgsFile):
                remove(TheMsgsDir+PROGMsgsFile)
            Fp = open(TheMsgsDir+PROGMsgsFile, "w")
            Fp.close()
            writeFile(0, "MSG", "== Contents erased "+getGMT(0)+" ==\n")
            msgLn(0, "", "", False, 0, False)
            msgLn(9, "", "Working with messages file\n   %s"%(TheMsgsDir+ \
                    PROGMsgsFile))
            msgLn(9, "", "-----")
        except Exception as e:
            msgLn(1, "M", "Error erasing file\n   %s\n   %s"% \
                    (PROGMsgsFile, e), True, 3)
            return
    ready()
    return
# END: eraseMsgs




#####################
# BEGIN: etComments()
# FUNC:etComments():2018.249
def etComments():
    return [ "\r\n", "\r\n",
            "Checked by: ______________________________\r\n",
            "                     (PI signature)\r\n",
            "\r\n",
            "Actions  Meaning    Parameters\r\n",
            "-------  ---------  --------------------------------\r\n",
            "   1     A/D power  Power state: 0 = Off  Else = On\r\n",
            "   2     A/D sync   Sample rate:\r\n",
            "                    1 = 1000  5 = 125  9 = 25  D = 5\r\n",
            "                    2 =  500  6 = 100  A = 20  E = 4\r\n",
            "                    3 =  250  7 =  50  B = 10  F = 2\r\n",
            "                    4 =  200  8 =  40  C =  9  G = 1\r\n",
            "   3     A/D data   Event state: 0 = Off  1 = On\r\n",
            "                    5 = Stop old/Start new event\r\n",
            "   4     A/D Gain   Preamp Gain:\r\n",
            "                    3 = 4   5 = 16  7 = 64  9 = 256\r\n",
            "                    4 = 8   6 = 32  8 = 128\r\n",
            "\r\n",
            "NOTE: Any line starting with a non-digit is a comment!\r\n"]
# END: etComments




#####################
# BEGIN: findInt(Str)
# LIB:findInt():2018.244
#   Returns the intt() of the first number found in Str. This is used when
#   trying to get at the 34 of something like "G34".
def findInt(Str):
    Index = 0
    for C in Str:
        if C.isdigit():
            Int = intt(Str[Index:])
            return Int
        Index += 1
    return -1
########################
# BEGIN: findPrefix(Str)
# FUNC:findPrefix():2006.243
#   Returns any characters at the start of Str upto the end or a digit. Like
#   "G34" returns "G".
def findPrefix(Str):
    Ret = ""
    try:
        while 1:
            c = Str[0]
            if c.isdigit():
                break
            Ret += c
            Str = Str[1:]
    except IndexError:
        pass
    return Ret
#######################
# BEGIN: findParts(Str)
# FUNC:findParts():2018.243
#   Returns the passed Str like G234K as a list of ["G", 274, "K"]. A List of
#   Str's broken up this way could then be sorted correctly so that items like
#   G05 and G4 will sort correctly (G4 then G05).
def findParts(Str):
    Ret = [""]
    Index = 0
    if len(Str) == 0:
        return Ret
    if Str[0].isdigit():
        What = "D"
    else:
        What = "A"
    for C in Str:
        if C.isdigit():
            if What == "D":
                Ret[Index] += C
            else:
                Ret.append(C)
                Index += 1
                What = "D"
        else:
            if What == "A":
                Ret[Index] += C
            else:
                Ret.append(C)
                Index += 1
                What = "A"
    for Index in arange(0, 0+len(Ret)):
        if Ret[Index][0].isdigit():
            Ret[Index] = intt(Ret[Index])
    return Ret
##############################
# BEGIN: getStrIntParts(InStr)
# FUNC:getStrIntParts():2018.243
#   Same idea as above, but only works for A123 or 123A, and it returns a
#   tuple, instead of a list.
#   Returns the integer and text portion of something like A234 as ("A", 234)
#   or 123BY as (123, "BY"). The str part will be "" if there is no str part
#   and the integer part will be -1 if there is no integer part.
def getStrIntParts(InStr):
    if len(InStr) == 0:
        return ("", -1)
    Str = ""
    Int = -1
    Index = 0
    FoundDigit = False
    First = ""
    for C in InStr:
        if C.isdigit() and FoundDigit == False:
            Int = intt(InStr[Index:])
            FoundDigit = True
# If the str part was first we've got everything. If the number part is first
# then we need to keep reading until the if() below finishes.
            if First == "s":
                break
            if len(First) == 0:
                First = "n"
        elif C.isdigit() == False:
            Str += C
            if len(First) == 0:
                First = "s"
        Index += 1
    if First == "n":
        return (Int, Str)
    elif First == "s":
        return (Str, Int)
    else:
        return ("", 0)
# END: findInt




###################
# BEGIN: floatt(In)
# LIB:floatt():2018.256
#    Handles all of the annoying shortfalls of the float() function (vs C).
#    Does not handle scientific notation numbers.
def floatt(In):
    In = str(In).strip()
    if len(In) == 0:
        return 0.0
# At least let the system give it the ol' college try.
    try:
        return float(In)
    except:
        Number = ""
        for c in In:
            if c.isdigit() or c == ".":
                Number += c
            elif (c == "-" or c == "+") and len(Number) == 0:
                Number += c
            elif c == ",":
                continue
            else:
                break
        try:
            return float(Number)
        except ValueError:
            return 0.0
# END: floatt




####################
# BEGIN: fmti(Value)
# LIB:fmti():2018.235
#   Just couldn't rely on the system to do this, although this won't handle
#   European-style numbers.
def fmti(Value):
    Value = int(Value)
    if Value > -1000 and Value < 1000:
        return str(Value)
    Value = str(Value)
    NewValue = ""
# There'll never be a + sign.
    if Value[0] == "-":
        Offset = 1
    else:
        Offset = 0
    CountDigits = 0
    for i in arange(len(Value)-1, -1+Offset, -1):
        NewValue = Value[i]+NewValue
        CountDigits += 1
        if CountDigits == 3 and i != 0:
            NewValue = ","+NewValue
            CountDigits = 0
    if Offset != 0:
        if NewValue.startswith(","):
            NewValue = NewValue[1:]
        NewValue = Value[0]+NewValue
    return NewValue
# END: fmti




###################################
# BEGIN: formFONTSZ(Parent, Format)
# LIB:formFONTSZ():2019.051
#   Displays the current font sizes and allows the user to enter new ones.
#   Includes the BIGGER and smaller font stepper functions.
PROGFrm["FONTSZ"] = None
FONTSZPropVar = StringVar()
FONTSZMonoVar = StringVar()
PROGSetups += ["PROGPropFontSize", "PROGMonoFontSize"]

def formFONTSZ(Parent, Format):
    if showUp("FONTSZ"):
        return
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    LFrm = PROGFrm["FONTSZ"] = Toplevel(Parent)
    LFrm.withdraw()
    LFrm.resizable(0, 0)
    LFrm.protocol("WM_DELETE_WINDOW", Command(formClose, "CAL"))
    LFrm.title("Set Font Sizes")
    Label(LFrm, text = "The current font sizes are shown.\nEnter new values and click the Set button.\nSome systems may use positive integers, and\nsome may use negative integers. Experiment.\n(Some systems may not have some font sizes.)").pack(side = TOP)
    FONTSZPropVar.set("%s"%PROGPropFont["size"])
    FONTSZMonoVar.set("%s"%PROGMonoFont["size"])
# In case different programs have more or fewer fonts to control.
    if Format == 0:
        Sub = Frame(LFrm)
        labelEntry2(Sub, 11, "Proportional Font: ", 35, \
                "Generally for buttons and labels and dialog box messages.", \
                FONTSZPropVar, 6)
        Sub.pack(side = TOP)
        Sub = Frame(LFrm)
        labelEntry2(Sub, 11, "Mono-Spaced Font: ", 35, \
                "Generally for text display fields.", FONTSZMonoVar, 6)
        Sub.pack(side = TOP)
    Sub = Frame(LFrm)
    BButton(Sub, text = "Set", command = formFONTSZGo).pack(side = LEFT)
    BButton(Sub, text = "Close", fg = Clr["R"], command = Command(formClose, \
            "FONTSZ")).pack(side = LEFT)
    Sub.pack(side = TOP, padx = 3, pady = 3)
    center(Parent, LFrm, "C", "I", True)
    return
#######################
# BEGIN: formFONTSZGo()
# FUNC:formFONTSZGo():2019.049
def formFONTSZGo():
    NewProp = intt(FONTSZPropVar.get())
    NewMono = intt(FONTSZMonoVar.get())
    if NewProp != PROGPropFont["size"]:
        PROGPropFontSize.set(NewProp)
        fontSetSize()
    if NewMono != PROGMonoFont["size"]:
        PROGMonoFontSize.set(NewMono)
        fontSetSize()
    return
#####################
# BEGIN: fontBigger()
# FUNC:fontBigger():2012.069
def fontBigger():
    PSize = PROGPropFont["size"]
    if PSize < 0:
        PSize -= 1
    else:
        PSize += 1
    MSize = PROGMonoFont["size"]
    if MSize < 0:
        MSize -= 1
    else:
        MSize += 1
# If either font is at the limit don't change the other one or they will get
# 'out of synch' with each other (like if the original prop font size was 12
# and the mono font size was 10).
    if abs(PSize) > 16 or abs(MSize) > 16:
        beep(1)
        return
    PROGPropFontSize.set(PSize)
    PROGMonoFontSize.set(MSize)
    fontSetSize()
    return
######################
# BEGIN: fontSmaller()
# FUNC:fontSmaller():2012.067
def fontSmaller():
    PSize = PROGPropFont["size"]
    if PSize < 0:
        PSize += 1
    else:
        PSize -= 1
    MSize = PROGMonoFont["size"]
    if MSize < 0:
        MSize += 1
    else:
        MSize -= 1
    if abs(PSize) < 6 or abs(MSize) < 6:
        beep(1)
        return
    PROGPropFontSize.set(PSize)
    PROGMonoFontSize.set(MSize)
    fontSetSize()
    return
######################
# BEGIN: fontSetSize()
# FUNC:fontSetSize():2019.049
def fontSetSize():
    global PROGPropFontHeight
    PROGPropFont["size"] = PROGPropFontSize.get()
    PROGMonoFont["size"] = PROGMonoFontSize.get()
    PROGPropFontHeight = PROGPropFont.metrics("ascent")+ \
            PROGPropFont.metrics("descent")
# For any additional stuff a program may need to do.
    if "formFONTSZGoLocal" in globals():
        formFONTSZGoLocal()
    return
# END: formFONTSZ




############################
# BEGIN: formFONTSZGoLocal()
# FUNC:formFONTSZGoLocal():2019.049
def formFONTSZGoLocal():
    if PROGScroll == True:
        Root.update()
        FWidth = PROGMonoFont.measure("8")
        LWidth = SBLab.winfo_reqwidth()+SBStart.winfo_reqwidth()+ \
                SBLen.winfo_reqwidth()+SBInt.winfo_reqwidth()+ \
                SBEvts.winfo_reqwidth()+SBSR.winfo_reqwidth()+ \
                SBGain.winfo_reqwidth()
        SBText.configure(width = int(LWidth/FWidth))
    return
# END: fontFONTSZGoLocal




#################################
# BEGIN: formABOUT(Parent = Root)
# LIB:formABOUT():2018.324
def formABOUT(Parent = Root):
# For the versions info and calling __X functions.
    if PROG_PYVERS == 2:
        import Tkinter as tkntr
    elif PROG_PYVERS == 3:
        import tkinter as tkntr
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    Message = "%s (%s)\n   "%(PROG_LONGNAME, PROG_NAME)
    Message += "Version %s\n"%PROG_VERSION
    Message += "%s\n"%"PASSCAL Instrument Center"
    Message += "%s\n\n"%"Socorro, New Mexico USA"
    Message += "%s\n"%"Email: passcal@passcal.nmt.edu"
    Message += "%s\n"%"Phone: 575-835-5070"
    Message += "\n"
    Message += "--- Configuration ---\n"
    Message += \
            "%s %dx%d\nProportional font: %s (%d)\nFixed font: %s (%d)\n"% \
            (PROGSystemName, PROGScreenWidthNow, PROGScreenHeightNow, \
            PROGPropFont["family"], PROGPropFont["size"], \
            PROGMonoFont["family"], PROGMonoFont["size"])
# Some programs don't care about the geometry of the main display.
    try:
# If the variable has something in it get the current geometry. The variable
# value may still be the initial value from when the program was started.
        if len(PROGGeometryVar.get()) != 0:
            Message += "\n"
            Message += "Geometry: %s\n"%Root.geometry()
    except:
        pass
# Some don't have setup files.
    try:
        if len(PROGSetupsFilespec) != 0:
            Message += "\n"
            Message += "--- Setups File ---\n"
            Message += "%s\n"%PROGSetupsFilespec
    except:
        pass
# This is only for QPEEK.
    try:
        if len(ChanPrefsFilespec) != 0:
            Message += "\n"
            Message += "--- Channel Preferences File ---\n"
            Message += "%s\n"%ChanPrefsFilespec
    except:
        pass
    Message += "\n"
    Message += "--- Versions ---\n"
    Message += "Python: %s\n"%PROG_PYVERSION
    Message += "Tcl/Tk: %s/%s\n"%(tkntr.TclVersion, tkntr.TkVersion)
    try:
        Message += "pySerial: %s"%SerialVERSION
    except:
        pass
# Some do, some don't have this.
    try:
        if len(PROG_LASTWORK) != 0:
            Message += "\n"
            Message += "--- Last Worked On ---\n"
            Message += "%s\n"%PROG_LASTWORK
    except:
        pass
    formMYD(Parent, (("(OK)", LEFT, "ok"), ), "ok", "WB", "About", \
            Message.strip())
    return
# END: formABOUT




##################
# BEGIN: formADJ()
# FUNC:formADJ():2016.195
PROGFrm["ADJ"] = None
ADJYearVar = StringVar()
ADJDayVar = StringVar()
ADJHourVar = StringVar()
ADJMinuteVar = StringVar()
ADJSecondVar = StringVar()
ADJOthersCVar = IntVar()

def formADJ():
    if showUp("ADJ"):
        return
    LFrm = PROGFrm["ADJ"] = Toplevel(Root)
    LFrm.withdraw()
    LFrm.resizable(0, 0)
    LFrm.protocol("WM_DELETE_WINDOW", Command(formClose, "ADJ"))
    LFrm.title("Adjust Session Start Times")
    LFrm.iconname("AdjTimes")
    Sub = Frame(LFrm)
    labelEntry2(Sub, 11, "Adjust Years:", 0, "", ADJYearVar, 5)
    Sub.pack(side = TOP)
    Sub = Frame(LFrm)
    labelEntry2(Sub, 11, "Adjust Days:", 0, "", ADJDayVar, 5)
    Sub.pack(side = TOP)
    Sub = Frame(LFrm)
    labelEntry2(Sub, 11, "Adjust Hours:", 0, "", ADJHourVar, 5)
    Sub.pack(side = TOP)
    Sub = Frame(LFrm)
    labelEntry2(Sub, 11, "Adjust Minutes:", 0, "", ADJMinuteVar, 5)
    Sub.pack(side = TOP)
    Sub = Frame(LFrm)
    labelEntry2(Sub, 11, "Adjust Seconds:", 0, "", ADJSecondVar, 5)
    Sub.pack(side = TOP, padx = 3)
    Cb = Checkbutton(LFrm, text = "Adjust Other Times", \
            variable = ADJOthersCVar)
    Cb.pack(side = TOP)
    ToolTip(Cb, 40, \
            "Checking this checkbutton will also adjust the Program and Offload times by the same amount.")
    Sub = Frame(LFrm)
    BButton(Sub, text = "Clear", fg = Clr["U"], \
            command = clearADJ).pack(side = LEFT)
    Label(Sub, text = " ").pack(side = LEFT)
    BButton(Sub, text = "Apply", command = formADJGo).pack(side = LEFT)
    Label(Sub, text = " ").pack(side = LEFT)
    BButton(Sub, text = "Close", fg = Clr["R"], command = Command(formClose, \
            "ADJ")).pack(side = TOP)
    Sub.pack(side = TOP, padx = 3, pady = 3)
    PROGMsg["ADJ"] = Text(LFrm, font = PROGPropFont, height = 2, wrap = WORD)
    PROGMsg["ADJ"].pack(side = TOP, fill = X)
    center(Root, LFrm, "CX", "I", True)
    return
####################
# BEGIN: formADJGo()
# FUNC:formADJGo():2018.247
def formADJGo():
# Check the inputs and convert to seconds of change.
    Change = 0
    for Field, Mult in (("Year", 31536000), ("Day", 86400), ("Hour", 3600), \
            ("Minute", 60), ("Second", 1)):
        Value = intt(eval("ADJ%sVar"%Field).get().strip())
        eval("ADJ%sVar"%Field).set(str(Value))
        Change += Value*Mult
    if Change == 0:
        setMsg("ADJ", "RW", "No adjustments entered.", 2)
        return
    AdjOthers = ADJOthersCVar.get()
    setMsg("ADJ", "CB", "Working...")
    Found = False
    for Sess in arange(1, SESSIONS+1):
        CurrTime = eval("S%02dParms[0]"%Sess).get().strip()
        if len(CurrTime) == 0:
            continue
        Found = True
        Epoch = dt2Time(11, -1, CurrTime)
        NewEpoch = Epoch+Change
        eval("S%02dParms[0]"%Sess).set(dt2Time(-1, 11, NewEpoch)[:-4])
    if AdjOthers == 1:
# These two fields are only to the nearest minute.
        if Change > 59:
            if len(PROGProgramTimeVar.get().strip()) != 0:
                CurrTime = PROGProgramTimeVar.get().strip()
                Found = True
                Epoch = dt2Time(11, -1, CurrTime)
                NewEpoch = Epoch+Change
                PROGProgramTimeVar.set(dt2Time(-1, 11, NewEpoch)[:-7])
            if len(PROGOffloadTimeVar.get().strip()) != 0:
                CurrTime = PROGOffloadTimeVar.get().strip()
                Found = True
                Epoch = dt2Time(11, -1, CurrTime)
                NewEpoch = Epoch+Change
                PROGOffloadTimeVar.set(dt2Time(-1, 11, NewEpoch)[:-7])
    if Found == False:
        beep(2)
        return
    msgLn(1, "", "Parameter times adjusted by")
    Value = intt(ADJYearVar.get().strip())
    msgLn(1, "", "   %d %s"%(Value, sP(Value, ("year", "years"))), False)
    Value = intt(ADJDayVar.get().strip())
    msgLn(1, "", "   %d %s"%(Value, sP(Value, ("day", "days"))), False)
    Value = intt(ADJHourVar.get().strip())
    msgLn(1, "", "   %d %s"%(Value, sP(Value, ("hour", "hours"))), False)
    Value = intt(ADJMinuteVar.get().strip())
    msgLn(1, "", "   %d %s"%(Value, sP(Value, ("minute", "minutes"))), False)
    Value = intt(ADJSecondVar.get().strip())
    msgLn(1, "", "   %d %s"%(Value, sP(Value, ("second", "seconds"))), False)
    msgLn(1, "", "   (%d %s)"%(Change, sP(Change, ("second", "seconds"))))
    setMsg("ADJ", "", "Parameter times adjusted.")
    return
###################
# BEGIN: clearADJ()
# FUNC:clearADJ():2015.264
def clearADJ():
    for Field in ("Year", "Day", "Hour", "Minute", "Second"):
        eval("ADJ%sVar"%Field).set("")
    return
# END: formADJ




################################################################
# BEGIN: formCAL(Parent, Months = 3, Show = True, AllowX = True,
#                OrigFont = False, CloseQuit = "close")
# LIB:formCAL():2019.058
#   Displays a 1 or 3-month calendar with dates and day-of-year numbers.
#   Pass in 1 or 3 as desired.
#   Show = Should the form show itself or no? (True/False)
#   AllowX = Should the [X] button be allowed for closing? (True/False)
#   OrigFont = False allows the font to change if the program has Font BIGGER,
#              and Font smaller functionality, otherwise the font is stuck
#              with the original font in use when the program started.
#   CloseQuit = "close" shows a Close button for the form, "quit" shows a
#               Quit button and calls progQuitter(True) when pressed.
PROGFrm["CAL"] = None
CALTmModeRVar = StringVar()
CALTmModeRVar.set("lt")
CALDtModeRVar = StringVar()
CALDtModeRVar.set("dates")
PROGSetups += ["CALTmModeRVar", "CALDtModeRVar"]
CALYear = 0
CALMonth = 0
CALText1 = None
CALText2 = None
CALText3 = None
CALMonths = 3
CALTipLastValue = ""

def formCAL(Parent, Months = 3, Show = True, AllowX = True, OrigFont = False, \
        CloseQuit = "close"):
    global CALText1
    global CALText2
    global CALText3
    global CALMonths
    global CALTipLastValue
    CALTipLastValue = ""
    if showUp("CAL"):
        return
    CALMonths = Months
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    LFrm = PROGFrm["CAL"] = Toplevel(Parent)
    LFrm.withdraw()
    LFrm.resizable(0, 0)
    if AllowX == True:
        if CloseQuit == "close":
            LFrm.protocol("WM_DELETE_WINDOW", Command(formClose, "CAL"))
        elif CloseQuit == "quit":
            LFrm.protocol("WM_DELETE_WINDOW", Command(progQuitter, True))
    if CALTmModeRVar.get() == "gmt":
        LFrm.title("Calendar (GMT)")
    elif CALTmModeRVar.get() == "lt":
        GMTDiff = getGMT(20)
        LFrm.title("Calendar (LT: GMT%+.2f hours)"%(float(GMTDiff)/3600.0))
    LFrm.iconname("Cal")
    Sub = Frame(LFrm)
    if CALMonths == 3:
        if OrigFont == False:
            CALText1 = Text(Sub, bg = Clr["D"], fg = Clr["B"], height = 11, \
                    width = 29, relief = SUNKEN, \
                    cursor = Button().cget("cursor"), state = DISABLED)
        else:
            CALText1 = Text(Sub, bg = Clr["D"], fg = Clr["B"], height = 11, \
                    width = 29, relief = SUNKEN, font = PROGOrigMonoFont, \
                    cursor = Button().cget("cursor"), state = DISABLED)
        CALText1.pack(side = LEFT, padx = 7)
    if OrigFont == False:
        CALText2 = Text(Sub, bg = Clr["W"], fg = Clr["B"], height = 11, \
                width = 29, relief = SUNKEN, \
                cursor = Button().cget("cursor"), state = DISABLED)
    else:
        CALText2 = Text(Sub, bg = Clr["W"], fg = Clr["B"], height = 11, \
                width = 29, relief = SUNKEN, font = PROGOrigMonoFont, \
                cursor = Button().cget("cursor"), state = DISABLED)
    CALText2.pack(side = LEFT, padx = 7)
    if CALMonths == 3:
        if OrigFont == False:
            CALText3 = Text(Sub, bg = Clr["D"], fg = Clr["B"], height = 11, \
                    width = 29, relief = SUNKEN, \
                    cursor = Button().cget("cursor"), state = DISABLED)
        else:
            CALText3 = Text(Sub, bg = Clr["D"], fg = Clr["B"], height = 11, \
                    width = 29, relief = SUNKEN, font = PROGOrigMonoFont, \
                    cursor = Button().cget("cursor"), state = DISABLED)
        CALText3.pack(side = LEFT, padx = 7)
    Sub.pack(side = TOP, padx = 3, pady = 3)
    if CALYear != 0:
        formCALMove("c")
    else:
        formCALMove("n")
    Sub = Frame(LFrm)
    BButton(Sub, text = "<<", command = Command(formCALMove, \
            "-y")).pack(side = LEFT)
    BButton(Sub, text = "<", command = Command(formCALMove, \
            "-m")).pack(side = LEFT)
    BButton(Sub, text = "Today", command = Command(formCALMove, \
            "n")).pack(side = LEFT)
    BButton(Sub, text = ">", command = Command(formCALMove, \
            "+m")).pack(side = LEFT)
    BButton(Sub, text = ">>", command = Command(formCALMove, \
            "+y")).pack(side = LEFT)
    Sub.pack(side = TOP, padx = 3, pady = 3)
    Sub = Frame(LFrm)
    SSub = Frame(Sub)
    SSSub = Frame(SSub)
    LRb = Radiobutton(SSSub, text = "GMT", value = "gmt", \
            variable = CALTmModeRVar, command = Command(formCALMove, "c"))
    LRb.pack(side = LEFT)
    ToolTip(LRb, 35, "Use what appears to be this computer's GMT time.")
    LRb = Radiobutton(SSSub, text = "LT", value = "lt", \
            variable = CALTmModeRVar, command = Command(formCALMove, "c"))
    LRb.pack(side = LEFT)
    ToolTip(LRb, 35, \
          "Use what appears to be this computer's time and time zone setting.")
    SSSub.pack(side = TOP, anchor = "w")
    SSub.pack(side = LEFT)
    Label(Sub, text = " ").pack(side = LEFT)
    SSub = Frame(Sub)
    LRb = Radiobutton(SSub, text = "Dates", value = "dates", \
            variable = CALDtModeRVar, command = Command(formCALMove, "c"))
    LRb.pack(side = TOP, anchor = "w")
    ToolTip(LRb, 35, "Show the calendar dates.")
    LRb = Radiobutton(SSub, text = "DOY", value = "doy", \
            variable = CALDtModeRVar, command = Command(formCALMove, "c"))
    LRb.pack(side = TOP, anchor = "w")
    ToolTip(LRb, 35, "Show the day-of-year numbers.")
    SSub.pack(side = LEFT)
    Label(Sub, text = " ").pack(side = LEFT)
    if CloseQuit == "close":
        BButton(Sub, text = "Close", fg = Clr["R"], \
                command = Command(formClose, "CAL")).pack(side = LEFT)
    elif CloseQuit == "quit":
        BButton(Sub, text = "Quit", fg = Clr["R"], \
                command = Command(progQuitter, True)).pack(side = LEFT)
    Sub.pack(side = TOP, padx = 3, pady = 3)
    if Show == True:
        center(Parent, LFrm, "CX", "I", True)
    return
####################################
# BEGIN: formCALMove(What, e = None)
# FUNC:formCALMove():2018.235
#   Handles changing the calendar form's display.
def formCALMove(What, e = None):
    global CALYear
    global CALMonth
    global CALText1
    global CALText2
    global CALText3
    PROGFrm["CAL"].focus_set()
    DtMode = CALDtModeRVar.get()
    Year = CALYear
    Month = CALMonth
    if What == "-y":
        Year -= 1
    elif What == "-m":
        Month -= 1
    elif What == "n":
        if CALTmModeRVar.get() == "gmt":
            Year, Month, Day = getGMT(4)
        elif CALTmModeRVar.get() == "lt":
            Year, DOY, HH, MM, SS = getGMT(11)
            GMTDiff = getGMT(20)
            if GMTDiff != 0:
                Year, Month, Day, DOY, HH, MM, SS = dt2TimeMath(0, GMTDiff, \
                        Year, 0, 0, DOY, HH, MM, SS)
    elif What == "+m":
        Month += 1
    elif What == "+y":
        Year += 1
    elif What == "c":
        if CALTmModeRVar.get() == "gmt":
            PROGFrm["CAL"].title("Calendar (GMT)")
        elif CALTmModeRVar.get() == "lt":
            GMTDiff = getGMT(20)
            PROGFrm["CAL"].title("Calendar (LT: GMT%+.2f hours)"% \
                    (float(GMTDiff)/3600.0))
    if Year < 1971:
        beep(1)
        return
    elif Year > 2050:
        beep(1)
        return
    if Month > 12:
        Year += 1
        Month = 1
    elif Month < 1:
        Year -= 1
        Month = 12
    CALYear = Year
    CALMonth = Month
# Only adjust this back one month if we are showing all three months.
    if CALMonths == 3:
        Month -= 1
    if Month < 1:
        Year -= 1
        Month = 12
    for i in arange(0, 0+3):
# Skip the first and last months if we are only showing one month.
        if CALMonths == 1 and (i == 0 or i == 2):
            continue
        LTxt = eval("CALText%d"%(i+1))
        LTxt.configure(state = NORMAL)
        LTxt.delete("0.0", END)
        LTxt.tag_delete(*LTxt.tag_names())
        DOM1date = 0
        DOM1doy = PROG_FDOM[Month]
        if (Year%4 == 0 and Year%100 != 0) or Year%400 == 0:
            if Month > 2:
                DOM1doy += 1
        if i == 1:
            LTxt.insert(END, "\n")
            LTxt.insert(END, "%s"%(PROG_CALMON[Month]+" "+ \
                    str(Year)).center(29))
            LTxt.insert(END, "\n\n")
            IdxS = LTxt.index(CURRENT)
            LTxt.tag_config(IdxS, background = Clr["W"], foreground = Clr["R"])
            LTxt.insert(END, " Sun ", IdxS)
            IdxS = LTxt.index(CURRENT)
            LTxt.tag_config(IdxS, background = Clr["W"], foreground = Clr["U"])
            LTxt.insert(END, "Mon Tue Wed Thu Fri", IdxS)
            IdxS = LTxt.index(CURRENT)
            LTxt.tag_config(IdxS, background = Clr["W"], foreground = Clr["R"])
            LTxt.insert(END, " Sat", IdxS)
            LTxt.insert(END, "\n")
        else:
            LTxt.insert(END, "\n")
            LTxt.insert(END, "%s"%(PROG_CALMON[Month]+" "+ \
                    str(Year)).center(29))
            LTxt.insert(END, "\n\n")
            LTxt.insert(END, " Sun Mon Tue Wed Thu Fri Sat")
            LTxt.insert(END, "\n")
        All = monthcalendar(Year, Month)
        if CALTmModeRVar.get() == "gmt":
            NowYear, NowMonth, NowDay = getGMT(4)
        elif CALTmModeRVar.get() == "lt":
# Do this so NowDay gets set. It may not get altered below.
            NowYear, NowMonth, NowDay = getGMT(18)
            NowYear, DOY, HH, MM, SS = getGMT(11)
            GMTDiff = getGMT(20)
            NowDay = 0
            if GMTDiff != 0:
                NowYear, NowMonth, NowDay, DOY, HH, MM, SS = dt2TimeMath(0, \
                        GMTDiff, NowYear, 0, 0, DOY, HH, MM, SS)
        if DtMode == "dates":
            TargetDay = DOM1date+NowDay
        elif DtMode == "doy":
            TargetDay = DOM1doy+NowDay
        for Week in All:
            LTxt.insert(END, " ")
            for DD in Week:
                if DD != 0:
                    if DtMode == "dates":
                        ThisDay = DOM1date+DD
                        ThisDay1 = DOM1date+DD
                        ThisDay2 = DOM1doy+DD
                    elif DtMode == "doy":
                        ThisDay = DOM1doy+DD
                        ThisDay1 = DOM1doy+DD
                        ThisDay2 = DOM1date+DD
                    IdxS = LTxt.index(CURRENT)
                    if ThisDay == TargetDay and Month == NowMonth and \
                            Year == NowYear:
                        LTxt.tag_config(IdxS, background = Clr["C"], \
                                foreground = Clr["B"])
                    if DtMode == "dates":
                        LTxt.tag_bind(IdxS, "<Enter>", \
                                Command(formCALStartTip, LTxt, "%s/%03d"% \
                                        (ThisDay1, ThisDay2)))
                    elif DtMode == "doy":
                        LTxt.tag_bind(IdxS, "<Enter>", \
                                Command(formCALStartTip, LTxt, "%03d/%s"% \
                                        (ThisDay1, ThisDay2)))
                    LTxt.tag_bind(IdxS, "<Leave>", formCALHideTip)
                    LTxt.tag_bind(IdxS, "<ButtonPress>", formCALHideTip)
                    if DtMode == "dates":
                        if ThisDay < 10:
                            LTxt.insert(END, "  ")
                        else:
                            LTxt.insert(END, " ")
                        LTxt.insert(END, "%d"%ThisDay, IdxS)
                    elif DtMode == "doy":
                        LTxt.insert(END, "%03d"%ThisDay, IdxS)
                    LTxt.insert(END, " ")
                else:
                    LTxt.insert(END, "    ")
            LTxt.insert(END, "\n")
        LTxt.configure(state = DISABLED)
        Month += 1
        if Month > 12:
            Year += 1
            Month = 1
    return
#################################################
# BEGIN: formCALStartTip(Parent, Value, e = None)
# FUNC:formCALStartTip():2011.110
#   Pops up a "tool tip" when mousing over the dates of Date/DOY.
CALTip = None

def formCALStartTip(Parent, Value, e = None):
# Multiple <Entry> events can be generated just by moving the cursor around.
# If we are still in the same date number just return.
    if Value == CALTipLastValue:
        return
    formCALHideTip()
    formCALShowTip(Parent, Value)
    return
######################################
# BEGIN: formCALShowTip(Parent, Value)
# FUNC:formCALShowTip():2019.058
def formCALShowTip(Parent, Value):
    global CALTip
    global CALTipLastValue
# The tooltips were not working under Python 3 and lesser versions of Tkinter
# until I added the .update(). Then AttributeErrors started showing up. So
# just try everything and hid the tip on any error.
    try:
        CALTip = Toplevel(Parent)
        CALTip.withdraw()
        CALTip.wm_overrideredirect(1)
        LLb = Label(CALTip, text = Value, bg = Clr["Y"], bd = 1, \
                fg = Clr["B"], relief = SOLID, padx = 3, pady = 3)
        LLb.pack()
        x = Parent.winfo_pointerx()+5
        y = Parent.winfo_pointery()+5
        CALTip.wm_geometry("+%d+%d"%(x, y))
        CALTip.update()
        CALTip.deiconify()
        CALTip.lift()
    except:
        formCALHideTip()
    CALTipLastValue = Value
    return
#################################
# BEGIN: formCALHideTip(e = None)
# FUNC:formCALHideTip():2011.110
def formCALHideTip(e = None):
    global CALTip
    global CALTipLastValue
    try:
        CALTip.destroy()
    except:
        pass
    CALTip = None
    CALTipLastValue = ""
    return
# END: formCAL




#########################
# BEGIN: formCHPRM(Which)
# FUNC:formCHPRM():2018.247
PROGFrm["CHPRM"] = None
CHPRMFromVar = StringVar()
CHPRMToVar = StringVar()
CHPRMFromIntervalsVar = StringVar()
CHPRMFromIntervalsVar.set("*")
CHPRMToIntervalsVar = StringVar()
CHPRMFromLengthsVar = StringVar()
CHPRMFromLengthsVar.set("*")
CHPRMToLengthsVar = StringVar()
CHPRMFromEventsVar = StringVar()
CHPRMFromEventsVar.set("*")
CHPRMToEventsVar = StringVar()
CHPRMFromRatesVar = StringVar()
CHPRMFromRatesVar.set("*")
CHPRMToRatesVar = StringVar()
CHPRMFromGainsVar = StringVar()
CHPRMFromGainsVar.set("*")
CHPRMToGainsVar = StringVar()

def formCHPRM(Which):
    if PROGFrm["CHPRM"] is not None:
        formClose("CHPRM")
    LFrm = PROGFrm["CHPRM"] = Toplevel(Root)
    LFrm.withdraw()
    LFrm.protocol("WM_DELETE_WINDOW", Command(formClose, "CHPRM"))
    if Which == "intervals":
        LFrm.title("%s - Change Event Intervals"%PROG_NAME)
        Index = 1
        VarSet = "Intervals"
    elif Which == "lengths":
        LFrm.title("%s - Change Event Lengths"%PROG_NAME)
        Index = 2
        VarSet = "Lengths"
    elif Which == "events":
        LFrm.title("%s - Change Number Of Events"%PROG_NAME)
        Index = 3
        VarSet = "Events"
    elif Which == "srs":
        LFrm.title("%s - Change Sample Rates"%PROG_NAME)
        Index = 4
        VarSet = "Rates"
    elif Which == "gains":
        LFrm.title("%s - Change Gains"%PROG_NAME)
        Index = 5
        VarSet = "Gains"
# Restore the values the user last used.
    CHPRMFromVar.set(eval("CHPRMFrom%sVar"%VarSet).get())
    CHPRMToVar.set(eval("CHPRMTo%sVar"%VarSet).get())
    LFrm.iconname("ChgPram")
    Sub = Frame(LFrm)
    labelTip(Sub, "From:=", LEFT, 30, \
            "[Change] Enter the value to replace or * for all matches")
    LEnt = Entry(Sub, width = 20, textvariable = CHPRMFromVar)
    LEnt.pack(side = LEFT)
    LEnt.bind("<Return>", Command(formCHPRMGo, VarSet, Index))
    LEnt.bind("<KP_Enter>", Command(formCHPRMGo, VarSet, Index))
    Sub.pack(side = TOP)
    Sub = Frame(LFrm)
    labelTip(Sub, "To:=", LEFT, 30, \
            "[Change] Enter the value to use as a replacement")
    LEnt = Entry(Sub, width = 20, textvariable = CHPRMToVar)
    LEnt.pack(side = LEFT)
    LEnt.bind("<Return>", Command(formCHPRMGo, VarSet, Index))
    LEnt.bind("<KP_Enter>", Command(formCHPRMGo, VarSet, Index))
    Sub.pack(side = TOP)
    Sub = Frame(LFrm)
    BButton(Sub, text = "Change", command = Command(formCHPRMGo, VarSet, \
            Index)).pack(side = LEFT)
    Label(Sub, text = "  ").pack(side = LEFT)
    BButton(Sub, text = "Cancel", command = Command(formClose, \
            "CHPRM")).pack(side = LEFT)
    Sub.pack(side = TOP, padx = 3, pady = 3)
    PROGMsg["CHPRM"] = Text(LFrm, font = PROGPropFont, height = 2, \
            width = 45, wrap = WORD)
    PROGMsg["CHPRM"].pack(side = TOP, fill = X)
    center(Root, LFrm, "CX", "I", True)
    return
#############################################
# BEGIN: formCHPRMGo(VarSet, Index, e = None)
# FUNC:formCHPRMGo():2018.243
def formCHPRMGo(VarSet, Index, e = None):
    CHPRMFromVar.set(CHPRMFromVar.get().strip())
    From = CHPRMFromVar.get().strip().lower()
    if len(From) == 0:
        setMsg("CHPRM", "RW", "From: No value has been entered.", 2)
        return
    CHPRMToVar.set(CHPRMToVar.get().strip())
    To = CHPRMToVar.get()
    if len(To) == 0:
        setMsg("CHPRM", "RW", "To: No value has been entered.", 2)
        return
    for Sess in arange(1, SESSIONS+1):
        Value = eval("S%02dParms[Index]"%Sess).get().strip().lower()
        if len(Value) == 0:
            continue
        if From != "*" and Value == From:
            eval("S%02dParms[Index]"%Sess).set(To)
        elif From == "*":
            eval("S%02dParms[Index]"%Sess).set(To)
    eval("CHPRMFrom%sVar"%VarSet).set(CHPRMFromVar.get())
    eval("CHPRMTo%sVar"%VarSet).set(CHPRMToVar.get())
    formClose("CHPRM")
    return
# END: formCHPRM




#################################
# BEGIN: formClose(Who, e = None)
# LIB:formClose():2018.236
#   Handles closing a form. Who can be a PROGFrm[] item or a Tcl pointer.
def formClose(Who, e = None):
# In case it is a "busy" form that takes a long time to close.
    updateMe(0)
    if isinstance(Who, astring):
# The form may not exist or PROGFrm[Who] may be pointing to a "lost" form, or
# who knows what, so try.
        try:
            if PROGFrm[Who] is None:
                return
            PROGFrm[Who].destroy()
        except:
            pass
    else:
        Who.destroy()
    try:
        PROGFrm[Who] = None
    except:
        pass
    return
###############################
# BEGIN: formCloseAll(e = None)
# FUNC:formCloseAll():2018.231
#   Goes through all of the forms and shuts them down.
def formCloseAll(e = None):
    for Frmm in list(PROGFrm.keys()):
# Try to call a formXControl() function. Some may have them and some may not.
        try:
# Expecting from formXControl() functions:
# Ret[0] 0 == Continue.
# Ret[0] 1 == Problem solved, can continue.
# Ret[0] 2 == Problem has or has not been resolved, should stop.
# What actually gets done is handled by the caller this just passes back
# anything that is not 0.
            Ret = eval("form%sControl"%Frmm)("close")
            if Ret[0] != 0:
                return Ret
        except:
            formClose(Frmm)
    return (0, )
# END: formClose




##########################################
# BEGIN: formFind(Who, WhereMsg, e = None)
# LIB:formFind():2018.236
#   Implements a "find" function in a form's Text() field.
#   The caller must set up the global Vars:
#      <Who>FindLookForVar = StringVar()
#      <Who>FindLastLookForVar = StringVar()
#      <Who>FindLinesVar = StringVar()
#      <Who>FindIndexVar = IntVar()
#      <Who>FindUseCaseCVar = IntVar()
#      <Who>FindReplaceWithVar = StringVar()
#   <Who> must be the string "INE", "CKTRD", etc.
#   Then on the form set up an Entry field and two Buttons like:
#
#   LEnt = Entry(Sub, width = 20, textvariable = <Who>FindLookForVar)
#   LEnt.pack(side = LEFT)
#   LEnt.bind("<Return>", Command(formFind, "<Who>", "<Who>"))
#   LEnt.bind("<KP_Enter>", Command(formFind, "<Who>", "<Who>"))
#   BButton(Sub, text = "Find", command = Command(formFind, "<Who>", \
#           "<Who>")).pack(side = LEFT)
#   BButton(Sub, text = "Next", command = Command(formFindNext, "<Who>", \
#           "<Who>")).pack(side = LEFT)
#   Checkbutton(Sub, text = "Use Case", \
#           variable = <Who>FindUseCaseCVar).pack(side = LEFT)
#   Entry(Sub, width = 20, \
#           textvariable = <Who>FindReplaceWithVar).pack(side = LEFT)
#   BButton(SSSub, text = "<Replace With", command = Command(formFindReplace, \
#           <Who>, <Who>)).pack(side = LEFT)
#
#   Must be defined even if it not used.
#      <Who>FindUseCaseCVar = IntVar()
#   Does not need to be defined if there is no replacing going on.
#      <Who>FindReplaceWithVar = StringVar()
#
def formFind(Who, WhereMsg, e = None):
    if WhereMsg is not None:
        setMsg(WhereMsg, "CB", "Finding...")
    LTxt = PROGTxt[Who]
    Case = eval("%sFindUseCaseCVar"%Who).get()
    if Case == 0:
        LookFor = eval("%sFindLookForVar"%Who).get().lower()
    else:
        LookFor = eval("%sFindLookForVar"%Who).get()
    LTxt.tag_delete("Find%s"%Who)
    LTxt.tag_delete("FindN%s"%Who)
    if len(LookFor) == 0:
        eval("%sFindLinesVar"%Who).set("")
        eval("%sFindIndexVar"%Who).set(-1)
        if WhereMsg is not None:
            setMsg(WhereMsg)
        return 0
    Found = 0
# Do this in case there is a lot of text from any previous find, otherwise
# the display just sits there.
    updateMe(0)
    eval("%sFindLastLookForVar"%Who).set(LookFor)
    eval("%sFindLinesVar"%Who).set("")
    eval("%sFindIndexVar"%Who).set(-1)
    FindLines = ""
    N = 1
    while 1:
        if len(LTxt.get("%d.0"%N)) == 0:
            break
        if Case == 0:
            Line = LTxt.get("%d.0"%N, "%d.0"%(N+1)).lower()
        else:
            Line = LTxt.get("%d.0"%N, "%d.0"%(N+1))
        C = 0
        try:
# Keep going through here until we run out of Line to search.
            while 1:
                Index = Line.index(LookFor, C)
                TagStart = "%d.%d"%(N, Index)
                C = Index+len(LookFor)
                TagEnd = "%d.%d"%(N, C)
                LTxt.tag_add("Find%s"%Who, TagStart, TagEnd)
                LTxt.tag_config("Find%s"%Who, background = Clr["U"], \
                        foreground = Clr["W"])
                FindLines += " %s,%s"%(TagStart, TagEnd)
                Found += 1
        except:
            pass
        N += 1
    if Found == 0:
        if WhereMsg is not None:
            setMsg(WhereMsg, "", "No matches found.")
    else:
        eval("%sFindLinesVar"%Who).set(FindLines)
        formFindNext(Who, WhereMsg, True, True)
        if WhereMsg is not None:
            setMsg(WhereMsg, "", "Matches found: %d"%Found)
    return Found
#################################################
# BEGIN: formFindReplace(Who, WhereMsg, e = None)
# FUNC:formFindReplace():2018.236
#   A Simple-minded find and replace function.
#   If WhereMsg is None then the caller is responsible for displying all
#   messages.
def formFindReplace(Who, WhereMsg, e = None):
    if WhereMsg is not None:
        setMsg(WhereMsg, "CB", "Replacing...")
    LTxt = PROGTxt[Who]
    LookFor = eval("%sFindLookForVar"%Who).get()
    ReplaceWith = eval("%sFindReplaceWithVar"%Who).get()
# These come in as straight chars ("\" + "n"). Convert them to real \n's.
    if ReplaceWith.endswith("\\n"):
        ReplaceWith = "%s\n"%ReplaceWith[:-2]
    LTxt.tag_delete("Find%s"%Who)
    LTxt.tag_delete("FindN%s"%Who)
    updateMe(0)
    FindLines = ""
    N = 1
    Found = 0
    while 1:
        if len(LTxt.get("%d.0"%N)) == 0:
            break
        Line = LTxt.get("%d.0"%N, "%d.0"%(N+1))
        if Line.find(LookFor) != -1:
            Line = Line.replace(LookFor, ReplaceWith)
            LTxt.delete("%d.0"%N, "%d.0"%(N+1))
            LTxt.insert("%d.0"%N, Line)
            Line = LTxt.get("%d.0"%N, "%d.0"%(N+1))
            TagStart = "%d.%d"%(N, Line.find(ReplaceWith))
            TagEnd = "%d.%d"%(N, Line.find(ReplaceWith)+len(ReplaceWith))
            LTxt.tag_add("Find%s"%Who, TagStart, TagEnd)
            LTxt.tag_config("Find%s"%Who, background = Clr["U"], \
                    foreground = Clr["W"])
            Found += 1
        N += 1
    if Found > 0:
        chgPROGChgBar(WhereMsg, 1)
    if WhereMsg is not None:
        setMsg(WhereMsg, "GB", "Done. Replaced: %d"%Found)
    return Found
######################################################
# BEGIN: formFindReplaceTrunc(Who, WhereMsg, e = None)
# FUNC:formFindReplaceTrunc():2018.236
#   A Simple-minded find and replace function that finds LookFor, truncates
#   that line, then appends ReplaceWith in its place.
def formFindReplaceTrunc(Who, WhereMsg, e = None):
    if WhereMsg is not None:
        setMsg(WhereMsg, "CB", "Replacing...")
    LTxt = PROGTxt[Who]
    LookFor = eval("%sFindLookForVar"%Who).get()
    ReplaceWith = eval("%sFindReplaceWithVar"%Who).get()
# These come in as straight chars ("\" + "n"). Convert them to real \n's.
    if ReplaceWith.endswith("\\n"):
        ReplaceWith = "%s\n"%ReplaceWith[:-2]
    LTxt.tag_delete("Find%s"%Who)
    LTxt.tag_delete("FindN%s"%Who)
    updateMe(0)
    FindLines = ""
    N = 1
    Found = 0
    while 1:
        if len(LTxt.get("%d.0"%N)) == 0:
            break
        Line = LTxt.get("%d.0"%N, "%d.0"%(N+1))
        if Line.find(LookFor) != -1:
            Index = Line.index(LookFor)
            Line = Line[:Index]+ReplaceWith
            LTxt.delete("%d.0"%N, "%d.0"%(N+1))
            LTxt.insert("%d.0"%N, Line)
            Line = LTxt.get("%d.0"%N, "%d.0"%(N+1))
            TagStart = "%d.%d"%(N, Line.find(ReplaceWith))
            TagEnd = "%d.%d"%(N, Line.find(ReplaceWith)+len(ReplaceWith))
            LTxt.tag_add("Find%s"%Who, TagStart, TagEnd)
            LTxt.tag_config("Find%s"%Who, background = Clr["U"], \
                    foreground = Clr["W"])
            Found += 1
        N += 1
    if Found > 0:
        chgPROGChgBar(WhereMsg, 1)
    if WhereMsg is not None:
        setMsg(WhereMsg, "GB", "Done. Replaced: %d"%Found)
    return Found
###########################################################################
# BEGIN: formFindNext(Who, WhereMsg, Find = False, First = False, e = None)
# FUNC:formFindNext():2014.069
def formFindNext(Who, WhereMsg, Find = False, First = False, e = None):
    LTxt = PROGTxt[Who]
    LTxt.tag_delete("FindN%s"%Who)
    FindLines = eval("%sFindLinesVar"%Who).get().split()
    if len(FindLines) == 0:
        beep(1)
        return
# Figure out which line we are at (at the top of the Text()) and then go
# through the found lines and find the closest one. Only do this on the first
# go of a search and not on "next" finds.
    if First == True:
        Y0, Dummy = LTxt.yview()
        AtLine = LTxt.index("@0,%d"%Y0)
# If we are at the top of the scroll then just go on normally.
        if AtLine > "1.0":
            AtLine = intt(AtLine)
            Index = -1
            Found = False
            for Line in FindLines:
                Index += 1
                Line = intt(Line)
                if Line >= AtLine:
                    Found = True
                    break
# If the current position is past the last found item just let things happen
# normally (i.e. jump to the first item found).
            if Found == True:
                eval("%sFindIndexVar"%Who).set(Index-1)
    Index = eval("%sFindIndexVar"%Who).get()
    Index += 1
    try:
        Line = FindLines[Index]
        eval("%sFindIndexVar"%Who).set(Index)
    except IndexError:
        Index = 0
        Line = FindLines[Index]
        eval("%sFindIndexVar"%Who).set(0)
# Make the "current find" red.
    TagStart, TagEnd = Line.split(",")
    LTxt.tag_add("FindN%s"%Who, TagStart, TagEnd)
    LTxt.tag_config("FindN%s"%Who, background = Clr["R"], \
            foreground = Clr["W"])
    LTxt.see(TagStart)
# If this is the first find just let the caller set a message.
    if Find == False:
        setMsg(WhereMsg, "", "Match %d of %d."%(Index+1, len(FindLines)))
    return
# END: formFind




#################
# BEGIN: HELPText
# 2018.248
#   The text of the help form.
HELPText = "QUICK START\n\
===========\n\
1. SET THE COMPUTER'S CLOCK. Make sure that the computer's clock is set \
to the correct time zone and the correct time if you are going to be \
entering things like the time programming will be started, and the time \
the instruments will be picked up. If those items are not going to be used \
then it is not as important, but some time calculations like the amount of \
time remaining before the event table starts will be wrong. Battery power \
consumption calculations will probably also be wrong.\n\
\n\
IMPORTANT: YOU MUST QUIT AND RESTART PETM IF IT IS RUNNING AND THE TIME \
ZONE IS CHANGED. The program is only passed the time zone information \
when it is started, so it will not see time zone changes unless it is \
stopped and restarted after a time zone change is made.\n\
\n\
2. FILL IN THE COMMENT. Fill in a comment for the event table (something like \
\"Deployment 2, line 17\") to help identify what the event table is for.\n\
\n\
3. FILL IN TIME FIELDS. Fill in the A/D Warmup time (usually 5 minutes), \
and the A/D Break fields (usually the same as the A/D Warmup time).\n\
\n\
4. SELECT THE MODEL. Select which Texan model the event table is for.\n\
\n\
5. FILL IN THE SESSION PARAMETERS. Fill in the fields for the sessions to be \
used.\n\
\n\
6. MAKE THE EVENT TABLE. Click the Make Event Table button to error check \
and process the inputs and generate the event table if everything is OK. \
When this is finished a file dialog box will appear so the event table can \
be saved.\n\
\n\
7. SAVE THE PARAMETERS. Save the entered parameters if desired with the \
Parameters menu command Save Parameters As...\n\
\n\
\n\
GENERAL STUFF\n\
=============\n\
PETM should be thought of as a spreadsheet program for working with time. \
The event start times, the program time, offload time and current time \
values may be in local time (LT) or in GMT. If they are in GMT then the \
Time Adjust field should be left blank (or set to 0). If the times used \
are in local time then the Time Adjust field value should be the amount of \
time needed to be added (even if negative) to make the entered local \
times GMT. NOTE: THE TIME ADJUST FIELD VALUE IS NOT THE TIME ZONE. It is \
usually the inverse of the local time zone value. The program operates \
independent of the computer's system clock, except when no program or \
current time is supplied. In that case the program tries to use the GMT \
time from the computer. This will generate warning messages, but as \
long as the event start times and the Time Adjust field values are correct \
then the event table will be correct...after verification. The Program \
and Current times are just for the event table 'time to start' and \
power consumption calculations. If just the Current Time field is filled \
in, then that will be used as the start time for the calculations. If \
the Programming Time is filled in then that time will be used.\n\
\n\
PETM uses the concept of \"sessions\" to control the creation of the \
event table. A session is a regular/sequential set of events. If an \
experiment required four nights of regularly-spaced shot windows with no \
recording during the day it would take four sessions to describe that event \
table (i.e. four \"groups\" of events separated by some idle time). If an \
experiment required three days of continuous recording that could be done \
with only one session (i.e. 72 one hour long events).\n\
\n\
Sessions can be set up to be interleaved with each other. For example, one \
session can be set to record every minute for 5 seconds at the top of the \
minute, and a second session can be set to record every minute for 5 seconds \
at 15 seconds past the top of the minute. This would produce events that \
leapfrog over each other.\n\
\n\
When the event table is listed to the messages section a typical line will \
look like\n\
\n\
    2005:250:03:45:00  S05:3 1  Recording started\n\
\n\
The \"S05:\" indicates that the parameters for Session 5 are responsible \
for the appearance of this command in the event table. If there are problems \
like overlapping commands, or two sessions trying to create an event at \
the same time, this will help locate the source of the problem as this \
same style of nomenclature will be used in the generated error messages. \
Of course these helper items are not in the final saved event table.\n\
\n\
Following the creation of the event table a summary of the table will be \
listed.  This summary will list the number of each type of line and the total \
number of lines in the event table, how many hours it is until the first \
command of the event table, the number of hours of idle time in the event \
table (time with the A/D turned off including the amount of time until the \
event table starts), the number of hours of standby time in the event \
table (A/D on, but not recording), the number of hours of recording time, \
and the total amount of wall clock time the event table will cover (not \
including the amount of time until the event table starts). PETM will \
then list an estimate of the amount of battery power that the Texans \
will consume. PETM will also calculate the amount of memory that will be \
used. This calculation will be reasonably close. The raw data files for \
RT125A Texans contain a lot of extra State of Health information that is \
difficult to estimate.\n\
\n\
If you need more than 15 sessions to describe a deployment then the \
schedule is probably too complicated and you shouldn't be doing the \
experiment. If you really do need more than 15 sessions the event table \
will need to be created in parts and then hand edited together.\n\
\n\
A PETM-readable copy of the parameters are placed in each event table \
file produced by PETM. This allows the event table files to be a source of \
parameters. For the files to show up in the file selection dialog box that \
will appear when the Load Parameters From... menu command is used the \
event table files must end in \".tet\" (which is their normal file \
extension). Files with just parameters created with the Save Parameters \
As... menu command will end in \".prm\".\n\
\n\
Most items have \"tooltips\" that can be displayed by placing the mouse \
cursor over their associated label.\n\
\n\
The A/D Warmup, A/D Break, Interval, and Length field times may be entered \
using times like 1h30m or 59m55s. Entering just a number will imply \
seconds. NOTE: 1h30 will be interpreted as 1 hour and 30 seconds, \
and not 1 hour and 30 minutes. Entries like 3:00 will not be recognized \
as being valid. Valid units codes are 'd' (days), 'h' (hours), 'm' \
(minutes), 's' (seconds).\n\
\n\
The field column header labels Interval, Length, Evts, SR, and Gain may \
be right-clicked on to show a popup menu item. When selected this menu \
item will bring up a small dialog box that will allow the values in \
the entry fields below the selected column header label to be changed \
from a particular value to a new value (i.e. a search and replace-like \
function). If all of the values in the column should be changed then an \
* should be entered for the From value in the dialog box.\n\
\n\
The Start Time, Program Time, Offload Time and Current Time field labels \
may be right-clicked on to bring up a popup menu that will allow the \
selection of Now LT or Now GMT. The time placed in the associated field \
is editable. The times will, of course, will only be correct if the \
computer's clock has been set correctly.\n\
\n\
\n\
COMMAND LINE ARGUMENTS\n\
======================\n\
Starting petm.py with\n\
\n\
    petm.py -sb\n\
\n\
will bring the program up in a mode where there are 60 session slots, \
instead of the usual 15. The slots for the sessions will then come with \
a scrollbar to access them all. This mode should only really be necessary \
when you plan on using the Load Events From... function where each event \
will be read in from an event table file and set up as a session. This was \
for an experiment where they had to read a complicated event table from \
another experiment which was not created with PETM.\n\
\n\
\n\
MENU COMMANDS\n\
=============\n\
FILE MENU\n\
---------\n\
----- Erase Messages -----\n\
Erases all of the messages in the messages section of the display and \
allows the file where the messages are being saved to to be emptied \
or for a new file to be started.\n\
\n\
----- Search Messages -----\n\
Brings up a form to use to search through the current messages file or \
all files in the current messages directory that end with \".msg\".\n\
\n\
----- Current Directories -----\n\
Lists the current directories being used by PETM.\n\
\n\
----- Change Messages Directory... -----\n\
Brings up a directory selection box so the user can change the directory \
where the petm.msg is saved.\n\
\n\
----- Change Work Directory... -----\n\
Brings up a directory selection box so the user can change the directory \
where the parameter files and the event table files will be saved.\n\
\n\
----- Change All To... -----\n\
Brings up a directory selection box and allows the user to navigate to \
a different directory and/or create a new directory and set all of the \
directory paths to that directory.\n\
\n\
----- Delete Setups File -----\n\
Sometimes a bad setups file can cause the program to misbehave. This can \
be caused by upgrading the program, but reading the setups file created \
by an earlier version of the program. This function will delete the \
current setups file (which can be found in the About box) and then quit \
the program without saving the current parameters. This will reset \
all parameters to their default values. When the program is restarted \
a dialog box should appear saying that the setups file could not be found. \
If this does not happen the Delete Setups File function may need to be \
run again.\n\
\n\
\n\
COMMANDS MENU\n\
-------------\n\
----- Plot An Event Table -----\n\
Allows the displaying of a selected event table in a graphical form. The \
menu command will bring up a form with an entry field and a Browse button \
near the bottom. The path to an event table file may be entered into the \
field manually or by using the Browse button and a standard file picker \
to navigate to an event table file.\n\
\n\
Only the portions of the event table where recording is done will be \
plotted. The green bars may be clicked on to obtain information about the \
event they represent. If many events are plotted in the same area \
the plots will appear dark green.\n\
\n\
The \"time zone\" of the display may be changed using the radiobuttons at \
the top of the form. The LT mode uses the computer's time and time zone \
setting, while the TZ mode may be used to enter any time zone by \
entering the number of hours difference from GMT (like -5 for the U.S. \
Eastern Standard Time zone).\n\
\n\
Thin red and blue lines may show up on the plot. These indicate the \
computer's current local time, and GMT time. They may or may not have \
anything to do with the plotted events, but are just there as a \
reference. \n\
\n\
Once the event table has been plotted the Write .ps button may be used \
to write the plot to an ecapsulated Postscript file for printing.\n\
\n\
\n\
PARAMETERS MENU\n\
---------------\n\
----- Save Parameters As... -----\n\
Brings up a file selection dialog box and saves the entered input parameters \
to the specified file.\n\
\n\
----- Load Parameters From... -----\n\
Brings up a file selection dialog box and loads the parameters from the \
specified file. Only files ending in \".prm\" and \".tet\" will be visible \
since they are the only files in which PETM expects to find parameters.\n\
\n\
----- Load Events From... -----\n\
This should only be used for event table files not produced by PETM. \
PETM-produced files have the session information in them, so the Load \
Parameters From... function above should be used.\n\
\n\
Brings up a file selection dialog box and loads the events from the \
specified event table file. Only files ending \".tet\" will be visible. \
Each event is converted into a session, so the number of events that can \
be in the file is limited. See the -sb command line argument.\n\
\n\
----- Clear Parameters -----\n\
Clears out all of the input parameter fields.\n\
\n\
----- Multiply A Session -----\n\
This function sort of takes the place of 'copy and paste'. In the dialog \
box there is a field for the session whose parameters you want to multiply, \
and one for a comma-separated list of which session(s) you want to copy \
those parameters into. In addition a fixed number of days, hours, minutes \
and seconds may be added to the original session's start time, and to each \
successive session's start time. There is no warning before PETM replaces \
the contents of an already filled-in session.\n\
\n\
----- Adjust Session Start Times -----\n\
This command brings up a dialog box that allows the adjustment of the \
session times without having to re-enter them. This could be used, for \
example, to advance the times of a set of parameters several days in time \
so the same schedule could be run again for a different deployment.\n\
\n\
----- Sort Sessions By Start Time-----\n\
Session parameters may be entered in any order. This command will place \
the parameters in order by session start time. It will also remove the \
parameters for any session whose start time is blank.\n\
\n\
----- Five Minute/Two Hour/Four Hour/etc. Test Parameters -----\n\
These commands get the current UT time, add 5 to 20 minutes to that time \
and fill in the parameter fields with pre-programmed parameters for a quick, \
medium, and an overnight test.\n\
\n\
\n\
OPTIONS MENU\n\
------------\n\
----- Set Font Sizes -----\n\
Brings up a form so the proportional and mono-spaced font sizes can be \
changed. Some systems use positive integers and some use negative numbers. \
The user will need to experiment. A larger number value is always a \
larger font.\n\
\n\
Most systems do not support all numerical values, so a value may be \
changed with no resulting change in size of the displayed fonts.\n\
\n\
\n\
FORMS MENU\n\
----------\n\
The currently open forms of the program will show up here. Selecting one \
will bring that form to the foreground.\n\
\n\
\n\
HELP MENU\n\
---------\n\
----- Calendar -----\n\
Just a three-month calendar that can be used for determining the day-of-year \
number. Of course the PC's clock must be set correctly to have this show \
the right date.\n\
\n\
----- Check For Updates -----\n\
If the computer is connected to the Internet this function will contact \
PASSCAL's website and check the version of the program against a list of \
version numbers. Appropriate dialog boxes will be shown depending on \
the result of the check.\n\
\n\
If the current version is old the Download button may be clicked to \
obtain the new version. The new version will be a zipped file/folder and \
the name of the program will be preceeded by \"new\". A dialog box \
indicating the location of the downloaded file will be shown after the \
downloading finishes. Once it has been confirmed by the user that the \
\"new\" program file is OK, it should be renamed and placed in the proper \
location which depends on the operating system of the computer.\n\
\n\
\n\
BUTTONS AND FIELDS\n\
==================\n\
----- Comment field -----\n\
Anything entered in this field will be written to a line in the event table \
file. This could be something like \"Deployment 2, line 17\", or similar, to \
help identify what part of an experiment the event table was made for.\n\
\n\
----- A/D Warmup field -----\n\
The amount of time before recording starts that the A/D board should be \
powered up for to allow the board to stabilize. Reftek recommends a minimum \
of 5 minutes.\n\
\n\
----- A/D Break field -----\n\
To conserve battery power the A/D board may be powered down when no \
recording is going on during a schedule (like during the daytime \
when there is only going to be shots and recording during the night). \
This setting tells PETM to place commands into the event table to turn \
off the A/D board if there is a break between the end of an event and \
the start of the next as long or longer than the entered amount of time. \
Commands will be placed into the event table to power the A/D board back \
up prior to the start of next event the amount of time in the A/D Warmup \
field. The A/D Break field value can be set to a longer amount of time \
than the A/D Warmup field to get PETM to reduce the number of event \
table lines if necessary by not powering down the A/D circuitry. The \
event tables are limited to 5000 lines for RT125A's. The value cannot be \
a shorter amount of time than the A/D Warmup field value.\n\
\n\
----- Start Time field -----\n\
This is the time (in GMT or Local Time) that recording for the first \
event of a session should begin. The A/D board warmup period will begin \
before this time. The input format is YYYY:DDD:HH:MM:SS. Leading zeros \
(i.e. \"06\") do not have to be entered. For example, if recording is to \
begin at 0300 that may be entered as \"2005:255:3\". PETM will fill in \
the rest.\n\
\n\
----- Length (Event Length) field -----\n\
The amount of time each event in a session should record for. This must \
be less than or equal to the Interval value for RT125A Texans. Event \
lengths, mostly for downstream processing reasons, but also in case \
things go wrong during recording, should be one hour or less.\n\
\n\
----- Interval (Interval Length) field -----\n\
The amount of time from the start of an event to the start of the next \
event in the session. Entering \"60\", for example, would cause an event \
to begin every minute.\n\
\n\
----- Evts (Events) field -----\n\
The number of events, or iterations in a session, or the length of time \
that a session should run. The length of time should be in the regular \
format like 8h30m. PETM DOES NOT include the last \"fencepost\". For \
example if the event interval is 5m and 2h is placed in the Evt field then \
PETM will generate 24 events, even though it could be argued that there \
should technically be a 25th event starting 2 hours after the start of \
the first event. In this example 2h5m would need to be placed in this \
field to get PETM to generate 25 events.\n\
\n\
Also, for example, if a session were set up to record 1/2 hour long events, \
starting every 1/2 hour (i.e. continuous) for 24 hours the value \"48\" \
could be entered into the Evts field for that session, or \"24h\" could \
be entered and PETM would create an event table with 48 1/2 hour long \
events. If using time in the Evts field the time entered must be some \
multiple of the event length used. For example, if the event table were \
to run for 24 hours and 15 minutes, you could not enter \"24h15m\". In \
this example PETM would not create a 15 minute long event. You would \
either have to add another session with an event that runs for 15 minutes, \
or make the event length and interval 15 minutes and then PETM would \
create an event table with 97 15 minute long events.\n\
\n\
----- SR (Sample Rate) field -----\n\
The sample rate that each event in a session should use for recording. \
A tooltip shows the allowed values for each model.\n\
\n\
----- Gain field -----\n\
The gain setting for each event in a session.\n\
\n\
The gain must be 32 for RT125A units that have been \"upgraded\" \
but have an old RT125 A/D card in them. The gain is only adjustable for \
the newer RT125A A/D cards. Use the Options | Show Hardware Versions \
and the Versions button command in POCUS to check the Texans. If a \
serial number is shown for the \"ATD\" card then the Texan has a newer \
A/D card and the gain is adjustable. If the versions line for that \
item does not show a serial number then the A/D card is an old one \
from an RT125 unit and the gain is not adjustable.\n\
\n\
----- Current Time field -----\n\
Normally PETM will use the computer's clock time as the current time. For \
planning purposes PETM can be fooled into thinking the current time is \
some other time by entering that time into this field. The format of \
the input is YYYY:DDD:HH:MM.\n\
\n\
----- Time Adjust -----\n\
NOW DON'T GET CONFUSED HERE!\n\
\n\
Time in the earlier versions of PETM was pretty simple. Everything was in \
GMT and that's it. Then the Time Adjust field came along and things got \
more flexible, but less simple. The important thing to remember is that \
this field IS NOT the time zone like -4 hours for the U.S. Eastern \
Standard Time (EST) zone. It is usually the inverse of the local time zone \
value like +4 hours for EST, or it is 0.0 for GMT.\n\
\n\
Using the Time Adjust field allows the event start times and the Program \
Time and Offload Time field times to be in local time. The value entered \
into the Time Adjust field is the number of hours (values like 2.5 are \
allowed) needed to MAKE THE ENTERED TIMES GMT TIME. If nothing, or \
0.0 is entered into the Time Adjust field then all of the user entered \
times are assumed to already be GMT, which is what the Texans require.\n\
\n\
If either the Program Time or the Offload Time field has a date/time \
in it then that value plus the Time Adjust field value will be used for \
those values. If they do not have a value in them then the system's GMT \
time MINUS the Time Adjust field value will be used for those values \
when previewing the event table. If the Time Adjust field is not \
filled in properly, and/or the computer's time and time zone are not set \
properly (generally speaking, to the local time) the summary information \
will probably be incorrect, but the event start and stop times may \
still be correct. Just pay attention and double check the final event \
table.\n\
\n\
----- Program Time field -----\n\
If the event table is made many days before the Texans will be \
programmed you can enter a date/time into this field that is closer \
to the time when the Texans will actually be programmed. This will be \
used to make the power consumption calculations more accurate. If this \
field is not filled in then the value in the Current Time field will \
be used, and if that is not filled in the then time from the control \
computer will be used. The format of the input is YYYY:DDD:HH:MM.\n\
\n\
----- Offload Time field -----\n\
This is the estimated time when the Texans will be offloaded. This \
will be used to make the power consumption calculation more accurate. The \
format of the input is YYYY:DDD:HH:MM.\n\
\n\
----- Power radiobuttons -----\n\
The three power selection choices change the idle, standby, and record \
power consumption values used in the power calculation produced when \
the event table is previewed or made.\n\
\n\
Power values are highly dependent on individual Texans. The power values \
used in PETM are based on nominal values that have been seen. The External \
values have not been observed at all, but will be in the future.\n\
\n\
The milliamp values listed in the summary information are based on 3.0V \
input voltage for the \"Internal 3.2V\" setting, 3.6V for the \"Internal \
3.7V\" setting, and proabably 8V for the \"External\" setting (UTEP and \
PASSCAL have 9V external battery packs).\n\
\n\
----- Get PC Time button -----\n\
If the computer's clock and time zone settings are correct this will send \
the current GMT time and the computer's 'local' time to the messages \
section.\n\
\n\
----- List ET, List Events, Neither radiobuttons -----\n\
A 2000-line long event table, for example, can take a long time to list \
to the messages section depending on the computer PETM is running on. The \
List ET radiobutton tells PETM to list the whole event table, the List \
Events radiobutton tells PETM to only list the event start and stop time \
information, and the Neither button tells PETM to only display the event \
table summary information.\n\
\n\
----- Preview Event Table button -----\n\
Does the same thing as the Make Event Table button described below, but \
does not ask about where to save the event table when it is finished. \
The event table and summary are only displayed to the messages section. \
The times for the event table and summary information do not have the \
Time Adjust value applied.\n\
\n\
----- Make Event Table button -----\n\
Causes PETM to commence checking through the input parameters for errors \
and then start generating the event table and writing an event table \
file for sending to the Texans (using a program like POCUS). The saved \
event table file name may be specified once the event table has been \
generated. The table name must end with \".tet\". The default name \
is 125_etbl.tet.\n\
\n\
A lot of checks are made at the various stages of event table generation. \
Error and warning messages will be sent to the messages section as needed. \
Any error will stop the production of the event table. The times will \
have the Time Adjust value applied.\n\
\n\
----- Plot Event Table checkbutton -----\n\
Selecting this checkbutton will bring up a viewer that will show a \
graphical version of the event table. The view will be updated each time \
the Preview Event Table or Make Event Table functions are used.\n\
\n\
----- Include Other Times -----\n\
If selected the Current Time, Program Time, and Offload Time will be \
plotted on the event table plot as black, cyan, red blocks.\n\
\n\
END"




##############################################
# BEGIN: formHELP(Parent, AllowWriting = True)
# LIB:formHELP():2019.042
#   Put the help contents in global variable HELPText somewhere in the
#   program.
PROGFrm["HELP"] = None
HELPFindLookForVar = StringVar()
HELPFilespecVar  = StringVar()
PROGSetups += ["HELPFindLookForVar", "HELPFilespecVar"]
HELPFindIndexVar = IntVar()
HELPFindLastLookForVar = StringVar()
HELPFindLinesVar = StringVar()
HELPFindUseCaseCVar = IntVar()
HELPHeight = 25
HELPWidth = 80
HELPFont = PROGOrigMonoFont

def formHELP(Parent, AllowWriting = True):
    if PROGFrm["HELP"] is not None:
        PROGFrm["HELP"].deiconify()
        PROGFrm["HELP"].lift()
        return
    LFrm = PROGFrm["HELP"] = Toplevel(Parent)
    LFrm.withdraw()
    LFrm.protocol("WM_DELETE_WINDOW", Command(formClose, "HELP"))
    LFrm.title("Help - %s"%PROG_NAME)
    LFrm.iconname("Help")
    Sub = Frame(LFrm)
    LTxt = PROGTxt["HELP"] = Text(Sub, font = HELPFont, height = HELPHeight, \
            width = HELPWidth, wrap = WORD, relief = SUNKEN)
    LTxt.pack(side = LEFT, expand = YES, fill = BOTH)
    LSb = Scrollbar(Sub, orient = VERTICAL, command = LTxt.yview)
    LSb.pack(side = RIGHT, fill = Y)
    LTxt.configure(yscrollcommand = LSb.set)
    Sub.pack(side = TOP, expand = YES, fill = BOTH)
    Sub = Frame(LFrm)
    labelTip(Sub, "Find:=", LEFT, 30, "[Find]")
    LEnt = Entry(Sub, width = 20, textvariable = HELPFindLookForVar)
    LEnt.pack(side = LEFT)
    LEnt.bind("<Return>", Command(formFind, "HELP", "HELP"))
    LEnt.bind("<KP_Enter>", Command(formFind, "HELP", "HELP"))
    BButton(Sub, text = "Find", command = Command(formFind, \
            "HELP", "HELP")).pack(side = LEFT)
    BButton(Sub, text = "Next", command = Command(formFindNext, \
            "HELP", "HELP")).pack(side = LEFT)
    if AllowWriting:
        Label(Sub, text = " ").pack(side = LEFT)
        BButton(Sub, text = "Write To File", \
                command = Command(formHELPWrite, LTxt)).pack(side = LEFT)
    Label(Sub, text = " ").pack(side = LEFT)
    BButton(Sub, text = "Close", fg = Clr["R"], \
            command = Command(formClose, "HELP")).pack(side = LEFT)
    Sub.pack(side = TOP, padx = 3, pady = 3)
    PROGMsg["HELP"] = Text(LFrm, font = PROGPropFont, height = 3, wrap = WORD)
    PROGMsg["HELP"].pack(side = TOP, fill = X)
    LTxt.insert(END, HELPText)
# Clear this so the formFind() routine does the right thing if this form was
# brought up previously.
    HELPFindLinesVar.set("")
    center(Parent, LFrm, "CX", "I", True)
    if len(HELPFilespecVar.get()) == 0 or \
            exists(dirname(HELPFilespecVar.get())) == False:
# Not all programs will have all directory vars. Look for them in this order.
        try:
            HELPFilespecVar.set(PROGWorkDirVar.get()+PROG_NAMELC+"help.txt")
        except NameError:
            try:
                HELPFilespecVar.set(PROGMsgsDirVar.get()+PROG_NAMELC+ \
                        "help.txt")
            except NameError:
# The program HAS to have one of these, so don't try here. Let it crash.
                HELPFilespecVar.set(PROGDataDirVar.get()+PROG_NAMELC+ \
                        "help.txt")
    return
#####################################
# BEGIN: formHELPWrite(Who, e = None)
# FUNC:formHELPWrite():2019.042
def formHELPWrite(Who, e = None):
    Dir = dirname(HELPFilespecVar.get())
    if Dir.endswith(sep) == False:
        Dir += sep
    File = basename(HELPFilespecVar.get())
    Filespec = formMYDF("HELP", 3, "Save Help To...", Dir, File)
    if len(Filespec) == 0:
        setMsg("HELP", "", "Nothing done.")
    try:
        Fp = open(Filespec, "w")
    except Exception as e:
        setMsg("HELP", "MW", "Error opening help file\n   %s\n   %s"% \
                (Filespec, e), 3)
        return
    Fp.write("Help for %s version %s\n\n"%(PROG_NAME, PROG_VERSION))
    N = 1
    while 1:
        if len(Who.get("%d.0"%N)) == 0:
            break
# The lines from the Text field do not come with a \n after each screen line,
# so we'll have to split the lines up ourselves.
        Line = Who.get("%d.0"%N, "%d.0"%(N+1))
        N += 1
        if len(Line) < 65:
            Fp.write(Line)
            continue
        Out = ""
        for c in Line:
            if c == " " and len(Out) > 60:
                Fp.write(Out+"\n")
                Out = ""
            elif c == "\n":
                Fp.write(Out+"\n")
                Out = ""
            else:
                Out += c
    Fp.close()
    HELPFilespecVar.set(Filespec)
    setMsg("HELP", "", "Help written to\n   %s"%Filespec)
    return
# END: formHELP




###################
# BEGIN: formMULT()
# FUNC:formMULT():2016.195
PROGFrm["MULT"] = None
MULTSourceVar = StringVar()
MULTDestVar = StringVar()
MULTDayVar = StringVar()
MULTHourVar = StringVar()
MULTMinuteVar = StringVar()
MULTSecondVar = StringVar()

def formMULT():
    if showUp("MULT"):
        return
    LFrm = PROGFrm["MULT"] = Toplevel(Root)
    LFrm.withdraw()
    LFrm.resizable(0, 0)
    LFrm.protocol("WM_DELETE_WINDOW", Command(formClose, "MULT"))
    LFrm.title("Multiply A Session")
    LFrm.iconname("DupeSess")
    Label(LFrm, \
            text = "Enter the number of the session to\nmultiply and a comma-separated list of\nsession numbers to multiply it into.\n(You may use 1, S1 or S01 format)").pack(side = TOP, padx = 3, pady = 3)
    Sub = Frame(LFrm)
    labelEntry2(Sub, 11, "Source Session:", 0, "", MULTSourceVar, 5)
    Sub.pack(side = TOP)
    Sub = Frame(LFrm)
    labelEntry2(Sub, 11, "Destination Session(s):", 0, "", MULTDestVar, 15)
    Sub.pack(side = TOP)
    Sub.pack(side = TOP, padx = 3)
    Label(LFrm, \
            text = "Optionally enter the amount of time each\ndestination session's start time should\nbe adjusted by from the source or previous session's\nstart time.").pack(side = TOP, padx = 3, pady = 3)
    Sub = Frame(LFrm)
    labelEntry2(Sub, 11, "Adjust Days:", 0, "", MULTDayVar, 5)
    Sub.pack(side = TOP)
    Sub = Frame(LFrm)
    labelEntry2(Sub, 11, "Adjust Hours:", 0, "", MULTHourVar, 5)
    Sub.pack(side = TOP)
    Sub = Frame(LFrm)
    labelEntry2(Sub, 11, "Adjust Minutes:", 0, "", MULTMinuteVar, 5)
    Sub.pack(side = TOP)
    Sub = Frame(LFrm)
    labelEntry2(Sub, 11, "Adjust Seconds:", 0, "", MULTSecondVar, 5)
    Sub.pack(side = TOP, padx = 3)
    Sub = Frame(LFrm)
    BButton(Sub, text = "Clear", fg = Clr["U"], \
            command = clearMULT).pack(side = LEFT)
    Label(Sub, text = " ").pack(side = LEFT)
    BButton(Sub, text = "Apply", command = formMULTGo).pack(side = LEFT)
    Label(Sub, text = " ").pack(side = LEFT)
    BButton(Sub, text = "Close", fg = Clr["R"], command = Command(formClose, \
            "MULT")).pack(side = TOP)
    Sub.pack(side = TOP, padx = 3, pady = 3)
    PROGMsg["MULT"] = Text(LFrm, font = PROGPropFont, height = 2, wrap = WORD)
    PROGMsg["MULT"].pack(side = TOP, fill = X)
    center(Root, LFrm, "CX", "I", True)
    return
#####################
# BEGIN: formMULTGo()
# FUNC:formMULTGo():2018.243
def formMULTGo():
    for Field in ("Source", "Dest"):
        eval("MULT%sVar"%Field).set(eval("MULT%sVar"%Field).get().strip())
    if len(MULTSourceVar.get()) == 0 or len(MULTDestVar.get()) == 0:
        setMsg("MULT", "RW", "No source or destination session(s) entered.", 2)
        return
    if len(MULTSourceVar.get()) == 0:
        setMsg("MULT", "RW", "No source session entered.", 2)
        return
    if len(MULTDestVar.get()) == 0:
        setMsg("MULT", "RW", "No destination session(s) entered.", 2)
        return
# Get the inputs.
    S1, S2 = getStrIntParts(MULTSourceVar.get())
    if isinstance(S1, anint):
        Source = S1
    else:
        Source = S2
    if Source < 1 or Source > SESSIONS:
        setMsg("MULT", "RW", "Bad source session: %s"%MULTSourceVar.get(), 2)
        return
    setMsg("MULT", "CB", "Working...")
    Dests = seqStr2List(1, MULTDestVar.get(), SESSIONS)
# Check the time adjust inputs and convert to seconds of change.
    Change = 0
    for Field, Mult in (("Day", 86400), ("Hour", 3600), ("Minute", 60), \
            ("Second", 1)):
        Value = intt(eval("MULT%sVar"%Field).get().strip())
        eval("MULT%sVar"%Field).set(str(Value))
        Change += Value*Mult
    LastDest = Source
    for Dest in Dests:
# Just ignore people that enter a number larger than SESSIONS.
        if Dest > SESSIONS:
            continue
        if Change != 0:
            CurrTime = eval("S%02dParms"%LastDest)[0].get().strip()
            Epoch = dt2Time(11, -1, CurrTime)
            NewEpoch = Epoch+Change
            eval("S%02dParms"%Dest)[0].set(dt2Time(-1, 11, NewEpoch)[:-4])
            for i in arange(1, 0+6):
                eval("S%02dParms"%Dest)[i].set(eval("S%02dParms"% \
                        LastDest)[i].get())
            LastDest = Dest
        else:
            for i in arange(0, 0+6):
                eval("S%02dParms"%Dest)[i].set(eval("S%02dParms"% \
                        Source)[i].get())
    setMsg("MULT", "", "Session multiplied.")
    return
####################
# BEGIN: clearMULT()
# FUNC:clearMULT():2008.012
def clearMULT():
    for Field in ("Source", "Dest"):
        eval("MULT%sVar"%Field).set("")
    return
# END: formMULT




############################################################################
# BEGIN: formMYD(Parent, Clicks, Close, C, Title, Msg1, Msg2 = "", Bell = 0,
#                CenterX = 0, CenterY = 0, Width = 0)
# LIB:formMYD():2019.030
#   The built-in dialog boxes cannot be associated with a particular frame, so
#   the Root/Main window kept popping to the front and covering up everything
#   on some systems when they were used, so I wrote this. I'm sure there is a
#   "classy" way of doing this, but I couldn't figure out how to return a
#   value after the window was destroyed from a classy-way.
#
#   The dialog box can contain an input field by using something like:
#
#   Answer = formMYD(Root, (("Input60", TOP, "input"), ("Write", LEFT, \
#           "input"), ("(Cancel)", LEFT, "cancel")), "cancel", "YB"\
#           "Let it be written...", \
#   "Enter a message to write to the Messages section (60 characters max.):")
#   if Answer == "cancel":
#       return
#   What = Answer.strip()
#   if len(What) == 0 or len(What) > 60:
#       Root.bell()
#   stdout.write("%s\n"%What)
#
#   The "Input60" tells the function to make an entry field 60 characters
#   long.  The "input" return value for the "Write" button tells the routine
#   to return whatever was entered into the input field. The "Cancel" button
#   will return "cancel", which, of course, could be confusing if the user
#   entered "cancel" in the input field and hit the Write button. In the case
#   above the Cancel button should probably return something like "!@#$%^&",
#   or something else that the user would never normally enter.
#
#   A "default" button may be designated by enclosing the text in ()'s.
#   Pressing the Return or keypad Enter keys will return that button's value.
#   The Cancel button is the default button in this example.
#
#   A caller must clear, or may set MYDAnswerVar to clear a previous
#   call's entry or to provide a default value for the input field before
#   calling formMYD().
#
#   To bring up a "splash dialog" for messages like "Working..." use
#
#       formMYD(Root, (), "", "", "", "Working...")
#
#   Call formMYDReturn to get rid of the dialog:
#
#       formMYDReturn("")
#
#   CenterX and CenterY can be used to position the dialog box when there is
#   no Parent if they are set to something other than 0 and 0.
#
#   Width may be set to something other than 0 to not use the default message
#   width.
#
#   Adding a length and text to the end of a button definition like below will
#   cause a ToolTip item to be created for that button.
#       ("Stop", LEFT, "stop", 35, "Click this to stop the program.")
#
#   This is REALLY kinda kludgy, but for a good cause, since there's already a
#          lot of arguments...
#      The normal font will be PROGPropFont.
#      If the Msg1 value begins with "|" then PROGMonoFont.
#      Starts with "}" will be PROGOrigPropFont.
#      Starts with "]" will be PROGOrigMonoFont.
#
#   A secret Shift-Control-Button-1 click on the main message Label (the text
#   of Msg1) will return the string "woohoo" which can be used to exit a
#   while-loop of some kind to get the dialog box to keep the program held in
#   place until someone knows the secret to get the program to continue.
#   Passing
#       (("NB", TOP, "NB"),)
#   for the buttons will make no button show up. It's really only useful when
#   using this secret code. The user won't normally know how to break out of
#   a caller's loop.
#   Shift-Control-1 can also be used to indicate some kind of 'override'
#   answer to the caller.
MYDFrame = None
MYDLabel = None
MYDAnswerVar = StringVar()

def formMYD(Parent, Clicks, Close, C, Title, Msg1, Msg2 = "", Bell = 0, \
        CenterX = 0, CenterY = 0, Width = 0):
    global MYDFrame
    global MYDLabel
# This Should Never Happen, but I think I saw it once where MYDFrame seemed to
# no longer point to a dialog that was visible. It may have just been a XQuartz
# glitch. The other alternative here is to destroy MYDFrame. We'll see how this
# works.
    if MYDFrame is not None:
        MYDFrame.deiconify()
        MYDFrame.lift()
        beep(2)
        return
    if Parent is not None:
# Allow either way of passing the parent.
        if isinstance(Parent, astring) == True:
            Parent = PROGFrm[Parent]
# Without this update() sometimes when running a program through ssh everyone
# loses track of where the parent is and the dialog box ends up at 0,0.
# It's possible for this to fail if the program is being stopped in a funny
# way (like with a ^C over an ssh connection, etc.).
        try:
            Parent.update()
        except:
            pass
    TheFont = PROGPropFont
    MonoFont = False
    if Msg1.startswith("|"):
        Msg1 = Msg1[1:]
        TheFont = PROGMonoFont
        MonoFont = True
    if Msg1.startswith("}"):
        Msg1 = Msg1[1:]
        TheFont = PROGOrigPropFont
        MonoFont = False
    if Msg1.startswith("]"):
        Msg1 = Msg1[1:]
        TheFont = PROGOrigMonoFont
        MonoFont = True
    LFrm = MYDFrame = Toplevel(Parent)
    LFrm.withdraw()
    LFrm.resizable(0, 0)
    LFrm.protocol("WM_DELETE_WINDOW", Command(formMYDReturn, Close))
# A number shows up in the title bar if you do .title(""), and if you don't
# set the title it ends up with the program name in it.
    if len(Title) == 0:
        Title = " "
    LFrm.title(Title)
    LFrm.iconname(Title)
# Gets rid of some of the extra title bar buttons.
    LFrm.transient(Parent)
# If C is X or it ends with X then set this so we get the modality right. Clean
# up C so the rest of the function doesn't need to worry about it.
    GSG = False
    if len(C) != 0:
        if C == "X":
            GSG = True
            C = ""
        else:
            if C.endswith("X"):
                GSG = True
                C = C.replace("X", "")
            LFrm.configure(bg = Clr[C[0]])
# Break up the incoming message about every 50 characters or whatever Width is
# set to.
    if Width == 0:
        Width = 50
    if len(Msg1) > Width:
        Count = 0
        Mssg = ""
        for c in Msg1:
            if Count == 0 and c == " ":
                continue
            if Count > Width and c == " ":
                Mssg += "\n"
                Count = 0
                continue
            if c == "\n":
                Mssg += c
                Count = 0
                continue
            Count += 1
            Mssg += c
        Msg1 = Mssg
# This is an extra line that gets added to the message after a blank line.
    if len(Msg2) != 0:
        if len(Msg2) > Width:
            Count = 0
            Mssg = ""
            for c in Msg2:
                if Count == 0 and c == " ":
                    continue
                if Count > Width and c == " ":
                    Mssg += "\n"
                    Count = 0
                    continue
                if c == "\n":
                    Mssg += c
                    Count = 0
                    continue
                Count += 1
                Mssg += c
            Msg1 += "\n\n"+Mssg
        else:
            Msg1 += "\n\n"+Msg2
    Sub = Frame(LFrm)
    if len(C) == 0:
        if MonoFont == False:
            MYDLabel = Label(Sub, text = Msg1, bd = 15, font = TheFont)
        else:
            MYDLabel = Label(Sub, text = Msg1, bd = 15, font = TheFont, \
                    justify = LEFT)
        MYDLabel.pack(side = LEFT)
        Sub.pack(side = TOP)
        Sub = Frame(LFrm)
    else:
        if MonoFont == False:
            MYDLabel = Label(Sub, text = Msg1, bd = 15, bg = Clr[C[0]], \
                    fg = Clr[C[1]], font = TheFont)
        else:
            MYDLabel = Label(Sub, text = Msg1, bd = 15, bg = Clr[C[0]], \
                    fg = Clr[C[1]], font = TheFont, justify = LEFT)
        MYDLabel.pack(side = LEFT)
        Sub.pack(side = TOP)
        Sub = Frame(LFrm, bg = Clr[C[0]])
    MYDLabel.bind("<Shift-Control-Button-1>", Command(formMYDReturn, "woohoo"))
    One = False
    InputField = False
    for Click in Clicks:
        if Click[0] == "NB" and Click[2] == "NB":
            continue
        if Click[0].startswith("Input"):
            InputEnt = Entry(LFrm, textvariable = MYDAnswerVar, \
                    width = intt(Click[0][5:]))
            InputEnt.pack(side = TOP, padx = 10, pady = 10)
# So we know to do the focus_set at the end.
            InputField = True
            continue
        if One == True:
            if len(C) == 0:
                Label(Sub, text = " ").pack(side = LEFT)
            else:
                Label(Sub, text = " ", bg = Clr[C[0]]).pack(side = LEFT)
        But = BButton(Sub, text = Click[0], command = Command(formMYDReturn, \
                Click[2]))
        if Click[0].startswith("Clear") or Click[0].startswith("(Clear"):
            But.configure(fg = Clr["U"], activeforeground = Clr["U"])
        elif Click[0].startswith("Close") or Click[0].startswith("(Close"):
            But.configure(fg = Clr["R"], activeforeground = Clr["R"])
        But.pack(side = Click[1])
# Check to see if there is ToolTip text.
        try:
            ToolTip(But, Click[3], Click[4])
        except:
            pass
        if Click[0].startswith("(") and Click[0].endswith(")"):
            LFrm.bind("<Return>", Command(formMYDReturn, Click[2]))
            LFrm.bind("<KP_Enter>", Command(formMYDReturn, Click[2]))
        if Click[1] != TOP:
            One = True
    Sub.pack(side = TOP, padx = 3, pady = 3)
# CX. Always keep these fully on the display.
    center(Parent, LFrm, "CX", "I", True, CenterX, CenterY)
# If the user clicks to dismiss the window before any one of these things get
# taken care of then there will be touble. This may only happen when the user
# is running the program over a network and not on the local machine. It has
# something to do with changes made to the beep() routine which fixed a
# problem of occasional missing beeps.
    try:
        LFrm.focus_set()
        if GSG == False:
# This is not working well on macOS with the "stock" Pythons/Tkinters. We'll
# try this for a while and see how it goes. What a pain. This is why PASSCAL
# created our own Python/Tkinter package.
            if PROGSystem == "dar" and PROG_PYVERS != 2:
                pass
            else:
                LFrm.grab_set()
        else:
            if PROGSystem == "dar" and PROG_PYVERS != 2:
                pass
            else:
                LFrm.grab_set_global()
        if InputField == True:
            InputEnt.focus_set()
            InputEnt.icursor(END)
        if Bell != 0:
            beep(Bell)
# Everything will pause here until one of the buttons are pressed if there are
# any, then the box will be destroyed, but the value will still be returned.
# since it was saved in a "global".
        if len(Clicks) != 0:
            LFrm.wait_window()
    except:
        pass
# At least do this much cleaning for the caller.
    MYDAnswerVar.set(MYDAnswerVar.get().strip())
    return MYDAnswerVar.get()
######################################
# BEGIN: formMYDReturn(What, e = None)
# FUNC:formMYDReturn():2018.256
def formMYDReturn(What, e = None):
# If What is "input" just leave whatever is in the var in there.
    global MYDFrame
    if What != "input":
        MYDAnswerVar.set(What)
    MYDAnswerVar.set(MYDAnswerVar.get().strip())
# This function may get called more than once if there are errors and stuff,
# especially when the dialog box is just being used for splash messages.
    try:
        MYDFrame.destroy()
    except:
        pass
    MYDFrame = None
    updateMe(0)
    return
############################
# BEGIN: formMYDMsg(Message)
# FUNC:formMYDMsg():2008.012
def formMYDMsg(Message):
    global MYDLabel
    MYDLabel.config(text = Message)
    updateMe(0)
    return
# END: formMYD




######################################################################
# BEGIN: formMYDF(Parent, Mode, Title, StartDir, StartFile, Mssg = "",
#                EndsWith = "", SameCase = True, CompFs = True, \
#                DePrefix = "", ReturnEmpty = False)
# LIB:formMYDF():2019.030
# NEEDS: PROGMsgsDirVar, PROGDataDirVar, PROGWorkDirVar if anyone tries to
#        use the Default codes in Mode.
#   Mode = (These must be the first character in Mode)
#          0 = allow picking a file
#          1 = just allow picking directories
#          2 = allow picking/making directories only
#          3 = pick/save directories and files (the works)
#          4 = pick multiple files and change dirs
#          5 = enter/pick a file, but not change directory
#          6 = enter/pick a file, but not change directory, and don't show the
#              directory
#   Mode = A second character that specifies one of the "main" directories
#          that can be selected using a "Default" button.
#          D = Main Data Directory (PROGDataDirVar.get())
#          W = Main Work Directory (PROGWorkDirVar.get())
#          M = Main Messages Directory (PROGMsgsDirVar.get())
#   Mode = Optional last character, X, that indicates that the form should use
#          grab_set_global(). This gets detected and stripped off right away so
#          the rest of the function doesn't have to worry about it.
#   Mssg = A message that can be displayed at the top of the form. It's a
#          Label, so \n's should be put in the passed Mssg string.
#   EndsWith = The module will only display files ending with EndsWith, the
#              entered filename's case will be matched with the case of
#              EndsWith (IF it is all one case or another). EndsWith may be
#              a passed string of file extensions seperated by commas. EndsWith
#              will be added to the entered filename if it is not there before
#              returning, but only if there is one extension passed.
#   SameCase = If True then the case of the filename must match the case of
#              the EndsWith items.
#   CompFs = If True then directory and file completion with the Tab key are
#            set up.
#   DePrefix = If not "" then only the files starting with DePrefix will be
#              loaded and the DePrefix will be removed.
#   ReturnEmpty = If True it allows clearing the file name field and clicking
#                 the OK button, instead of complaining that no file name was
#                 entered.
#   Tabbing in the directory field is supported.
MYDFFrame = None
MYDFModeVar = StringVar()
MYDFDirVar = StringVar()
MYDFFiles = None
MYDFDirField = None
MYDFFileVar = StringVar()
MYDFEndsWithVar = StringVar()
MYDFAnswerVar = StringVar()
MYDFHiddenCVar = IntVar()
MYDFSameCase = True
MYDFDePrefix = ""
MYDFReturnEmpty = False

def formMYDF(Parent, Mode, Title, StartDir, StartFile, Mssg = "", \
        EndsWith = "", SameCase = True, CompFs = True, DePrefix = "", \
        ReturnEmpty = False):
    global MYDFFrame
    global MYDFFiles
    global MYDFDirField
    global MYDFSameCase
    global MYDFDePrefix
    global MYDFReturnEmpty
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
# Mode may be passed as an integer 1 or as a string "1X".
    Mode = str(Mode)
    GSG = False
    if Mode.endswith("X"):
        GSG = True
        Mode = Mode.replace("X", "")
# Everyone else looks for the str() version.
    MYDFModeVar.set(Mode)
    MYDFEndsWithVar.set(EndsWith)
    MYDFSameCase = SameCase
    MYDFDePrefix = DePrefix
    MYDFReturnEmpty = ReturnEmpty
# Without this update() sometimes when running a program through ssh everyone
# loses track of where the parent is and the dialog box ends up at 0,0.
# It's possible for this to fail if the program is being stopped in a funny
# way (like with a ^C over an ssh connection, etc.).
    try:
        Parent.update()
    except:
        pass
    LFrm = MYDFFrame = Toplevel(Parent)
    LFrm.withdraw()
    LFrm.protocol("WM_DELETE_WINDOW", Command(formMYDFReturn, None))
    LFrm.bind("<Button-1>", Command(setMsg, "MYDF", "", ""))
    LFrm.title(Title)
    LFrm.iconname("PickDF")
    Sub = Frame(LFrm)
# The incoming Mode may be something like "1W". There are special versions of
# intt() out there that don't convert the passed item to a string first, so
# now we want the beginning number part.
    ModeI = intt(Mode)
    if len(Mssg) != 0:
        Label(Sub, text = Mssg).pack(side = TOP)
    if ModeI != 5 and ModeI != 6:
        BButton(Sub, text = "Up", command = formMYDFUp).pack(side = LEFT)
        LLb = Label(Sub, text = ":=")
        LLb.pack(side = LEFT)
        ToolTip(LLb, 35, \
                "[List Files] Press the Return key to list the files in the entered directory after editing the directory name.")
        LEnt = MYDFDirField = Entry(Sub, textvariable = MYDFDirVar, width = 65)
        LEnt.pack(side = LEFT, fill = X, expand = YES)
        if CompFs == True:
            LEnt.bind("<FocusIn>", Command(formMYDFCompFsTabOff, LFrm))
            LEnt.bind("<FocusOut>", Command(formMYDFCompFsTabOn, LFrm))
            LEnt.bind("<Key-Tab>", Command(formMYDFCompFs, MYDFDirVar, None))
        LEnt.bind("<Return>", formMYDFFillFiles)
        LEnt.bind("<KP_Enter>", formMYDFFillFiles)
# KeyPress clears the field when typing. KeyRelease clears the field after it
# has been filled in.
        LEnt.bind("<KeyPress>", formMYDFClearFiles)
        if ModeI == 2 or ModeI == 3:
            But = BButton(Sub, text = "Mkdir", command = formMYDFNew)
            But.pack(side = LEFT)
            ToolTip(But, 25, \
                    "Add the name of the new directory to the end of the current contents of the entry field and click this button to create the new directory.")
# Just show the current directory.
    elif ModeI == 5:
        MYDFDirField = Entry(Sub, textvariable = MYDFDirVar, \
                state = DISABLED)
        MYDFDirField.pack(side = LEFT, fill = X, expand = YES)
# Just don't show the current directory. Create, but don't pack.
    elif ModeI == 6:
        MYDFDirField = Entry(Sub, textvariable = MYDFDirVar)
    Sub.pack(side = TOP, fill = X)
    Sub = Frame(LFrm)
    if ModeI == 4:
        LLb = MYDFFiles = Listbox(Sub, relief = SUNKEN, bd = 2, height = 15, \
                selectmode = EXTENDED, width = 65)
    else:
        LLb = MYDFFiles = Listbox(Sub, relief = SUNKEN, bd = 2, height = 15, \
                selectmode = SINGLE, width = 65)
    LLb.pack(side = LEFT, expand = YES, fill = BOTH)
    LLb.bind("<ButtonRelease-1>", formMYDFPicked)
    Scroll = Scrollbar(Sub, command = LLb.yview)
    Scroll.pack(side = RIGHT, fill = Y)
    LLb.configure(yscrollcommand = Scroll.set)
    Sub.pack(side = TOP, expand = YES, fill = BOTH)
# The user can type in the filename.
    if ModeI == 0 or ModeI == 3 or ModeI == 5 or ModeI == 6:
        Sub = Frame(LFrm)
        Label(Sub, text = "Filename:=").pack(side = LEFT)
        LEnt = Entry(Sub, textvariable = MYDFFileVar)
        LEnt.pack(side = LEFT, fill = X, expand = YES)
        if CompFs == True:
            LEnt.bind("<FocusIn>", Command(formMYDFCompFsTabOff, LFrm))
            LEnt.bind("<FocusOut>", Command(formMYDFCompFsTabOn, LFrm))
            LEnt.bind("<Key-Tab>", Command(formMYDFCompFs, None, MYDFFileVar))
        LEnt.bind("<Return>", Command(formMYDFReturn, ""))
        LEnt.bind("<KP_Enter>", Command(formMYDFReturn, ""))
        Sub.pack(side = TOP, fill = X, padx = 3)
    Sub = Frame(LFrm)
# Create a Default button and tooltip if the Mode says so.
    if isinstance(Mode, astring):
        if Mode.find("D") != -1:
            LBu = BButton(Sub, text = "Default", \
                    command = Command(formMYDFDefault, "D"))
            LBu.pack(side = LEFT)
            ToolTip(LBu, 35, "Set to Main Data Directory")
            Label(Sub, text = " ").pack(side = LEFT)
        elif Mode.find("W") != -1:
            LBu = BButton(Sub, text = "Default", \
                    command = Command(formMYDFDefault, "W"))
            LBu.pack(side = LEFT)
            ToolTip(LBu, 35, "Set to Main Work Directory")
            Label(Sub, text = " ").pack(side = LEFT)
        elif Mode.find("M") != -1:
            LBu = BButton(Sub, text = "Default", \
                    command = Command(formMYDFDefault, "M"))
            LBu.pack(side = LEFT)
            ToolTip(LBu, 35, "Set to Main Messages Directory")
            Label(Sub, text = " ").pack(side = LEFT)
    BButton(Sub, text = "OK", command = Command(formMYDFReturn, \
            "")).pack(side = LEFT)
    Label(Sub, text = " ").pack(side = LEFT)
    BButton(Sub, text = "Cancel", \
            command = Command(formMYDFReturn, None)).pack(side = LEFT)
    Label(Sub, text = " ").pack(side = LEFT)
    LCb = Checkbutton(Sub, text = "Show hidden\nfiles", \
            variable = MYDFHiddenCVar, command = formMYDFFillFiles)
    LCb.pack(side = LEFT)
    ToolTip(LCb, 35, "Select this to show hidden/system files in the list.")
    Sub.pack(side = TOP, pady = 3)
    PROGMsg["MYDF"] = Text(LFrm, font = PROGPropFont, height = 1, \
            width = 1, highlightthickness = 0, insertwidth = 0, takefocus = 0)
    PROGMsg["MYDF"].pack(side = TOP, expand = YES, fill = X)
    PROGMsg["MYDF"].bind("<Button-1>", formMYDFNullCall)
    if len(StartDir) == 0:
        StartDir = sep
    if StartDir.endswith(sep) == False:
        StartDir += sep
    MYDFDirVar.set(StartDir)
    Ret = formMYDFFillFiles()
    if Ret == True and (ModeI == 0 or ModeI == 3 or ModeI == 5 or ModeI == 6):
        MYDFFileVar.set(StartFile)
    center(Parent, LFrm, "CX", "I", True)
# Set the cursor for the user.
    if ModeI == 0 or ModeI == 3 or ModeI == 5 or ModeI == 6:
        LEnt.focus_set()
        LEnt.icursor(END)
    if GSG == False:
# See formMYD().
        if PROGSystem == "dar" and PROG_PYVERS != 2:
            pass
        else:
            MYDFFrame.grab_set()
    else:
        if PROGSystem == "dar" and PROG_PYVERS != 2:
            pass
        else:
            MYDFFrame.grab_set_global()
# Everything will pause here until one of the buttons are pressed, then the
# box will be destroyed, but the value will still be returned since it was
# saved in a "global".
    MYDFFrame.wait_window()
    return MYDFAnswerVar.get()
#####################################
# BEGIN: formMYDFClearFiles(e = None)
# FUNC:formMYDFClearFiles():2013.207
def formMYDFClearFiles(e = None):
    if MYDFFiles.size() > 0:
        MYDFFiles.delete(0, END)
    return
#####################################################
# BEGIN: formMYDFFillFiles(FocusSet = True, e = None)
# FUNC:formMYDFFillFiles():2018.330
#   Fills the Listbox with a list of directories and files, or with drive
#   letters if in Windows (and the directory field is just sep).
def formMYDFFillFiles(FocusSet = True, e = None):
# Some directoryies may have a lot of files in them which could make listdir()
# take a long time, so do this.
    setMsg("MYDF", "CB", "Reading...")
    ModeI = intt(MYDFModeVar.get())
# Make sure whatever is in the directory field ends, or is at least a
# separator.
    if len(MYDFDirVar.get()) == 0:
        MYDFDirVar.set(sep)
    if MYDFDirVar.get().endswith(sep) == False:
        MYDFDirVar.set(MYDFDirVar.get()+sep)
    Dir = MYDFDirVar.get()
    if PROGSystem == "dar" or PROGSystem == "lin" or PROGSystem == "sun" or \
            (PROGSystem == "win" and Dir != sep):
        try:
            Files = listdir(Dir)
        except:
            MYDFFileVar.set("")
            setMsg("MYDF", "YB", " There is no such directory.", 2)
            return False
        MYDFDirField.icursor(END)
        MYDFFiles.delete(0, END)
# This is sortedLow(), but coded in here to reduce dependencies.
        USList = {}
        for Item in Files:
            USList[Item.lower()] = Item
        SKeys = list(USList.keys())
        SKeys.sort()
        Files = []
        for Key in SKeys:
            Files.append(USList[Key])
# Always add this to the top of the list unless we are not supposed to.
        if ModeI != 5 and ModeI != 6:
            MYDFFiles.insert(END, " .."+sep)
# To show or not to show.
        ShowHidden = MYDFHiddenCVar.get()
# Do the directories first.
        for File in Files:
# The DePrefix stuff is only applied to the files (below).
            if ShowHidden == 0:
# This may need/want to be system dependent at some point (i.e. more than just
# files that start with a . or _ in different OSs).
                if File.startswith(".") or File.startswith("_"):
                    continue
            if isdir(Dir+sep+File):
                MYDFFiles.insert(END, " "+File+sep)
# Check to see if we are going to be filtering.
        EndsWith = ""
        if len(MYDFEndsWithVar.get()) != 0:
            EndsWith = MYDFEndsWithVar.get()
        EndsWithParts = EndsWith.split(",")
        Found = False
        for File in Files:
# DeFile will be used for what the user ends up seeing, and File will be used
# internally.
            DeFile = File
            if len(MYDFDePrefix) != 0:
                if File.startswith(MYDFDePrefix) == False:
                    continue
                DeFile = File[len(MYDFDePrefix):]
            if ShowHidden == 0:
                if DeFile.startswith(".") or DeFile.startswith("_"):
                    continue
            if isdir(Dir+sep+File) == False:
                if len(EndsWith) == 0:
# We only want to see the file sizes when we are looking for files.
                    if ModeI == 0 or ModeI == 3 or ModeI == 5 or ModeI == 6:
# Trying to follow links will trip this so just show them.
                        try:
                            MYDFFiles.insert(END, " %s  (bytes: %s)"%(DeFile, \
                                    fmti(getsize(Dir+sep+File))))
                        except OSError:
                            MYDFFiles.insert(END, " %s  (a link?)"%DeFile)
                    else:
                        MYDFFiles.insert(END, " %s"%DeFile)
                    Found += 1
                else:
                    for EndsWithPart in EndsWithParts:
                        if File.endswith(EndsWithPart):
                            if ModeI == 0 or ModeI == 3 or ModeI == 5 or \
                                    ModeI == 6:
                                try:
                                    MYDFFiles.insert(END, " %s  (bytes: %s)"% \
                                            (DeFile, fmti(getsize(Dir+sep+ \
                                            File))))
                                except OSError:
                                    MYDFFiles.insert(END, " %s  (a link?)"% \
                                            DeFile)
                            else:
                                MYDFFiles.insert(END, " %s"%DeFile)
                            Found += 1
                            break
        if ModeI == 0 or ModeI == 3 or ModeI == 5 or ModeI == 6:
            setMsg("MYDF", "", "%d %s found."%(Found, sP(Found, ("file", \
                    "files"))))
        else:
            setMsg("MYDF")
    elif PROGSystem == "win" and Dir == sep:
        MYDFFiles.delete(0, END)
# This loop takes a while to run.
        updateMe(0)
        Found = 0
        for Drive in "ABCDEFGHIJKLMNOPQRSTUVWXYZ":
            Drivespec = "%s:%s"%(Drive, sep)
            if exists(Drivespec):
                MYDFFiles.insert(END, Drivespec)
                Found += 1
        if ModeI == 0 or ModeI == 3 or ModeI == 5 or ModeI == 6:
            setMsg("MYDF", "", "%d %s found."%(Found, sP(Found, ("drive", \
                    "drives"))))
        else:
            setMsg("MYDF")
    if FocusSet == True:
        PROGMsg["MYDF"].focus_set()
    return True
###############################
# BEGIN: formMYDFDefault(Which)
# FUNC:formMYDFDefault():2012.343
def formMYDFDefault(Which):
    if Which == "D":
        MYDFDirVar.set(PROGDataDirVar.get())
    elif Which == "W":
        MYDFDirVar.set(PROGWorkDirVar.get())
    elif Which == "M":
        MYDFDirVar.set(PROGMsgsDirVar.get())
    formMYDFFillFiles()
    return
######################
# BEGIN: formMYDFNew()
# FUNC:formMYDFNew():2018.233
def formMYDFNew():
    setMsg("MYDF")
# Make sure whatever is in the directory field ends in a separator.
    if len(MYDFDirVar.get()) == 0:
        MYDFDirVar.set(sep)
    if MYDFDirVar.get().endswith(sep) == False:
        MYDFDirVar.set(MYDFDirVar.get()+sep)
    Dir = MYDFDirVar.get()
    if exists(Dir):
        setMsg("MYDF", "YB", " Directory already exists.", 2)
        return
    try:
        makedirs(Dir)
        formMYDFFillFiles()
        setMsg("MYDF", "GB", " New directory made.", 1)
    except Exception as e:
# The system messages can be a bit cryptic and/or long, so simplifiy them here.
        if str(e).find("ermission") != -1:
            setMsg("MYDF", "RW", " Permission denied.", 2)
        else:
            setMsg("MYDF", "RW", " "+str(e), 2)
        return
    return
#################################
# BEGIN: formMYDFPicked(e = None)
# FUNC:formMYDFPicked():2018.235
def formMYDFPicked(e = None):
# This is just to get the focus off of the entry field if it was there since
# the user is now clicking on things.
    PROGMsg["MYDF"].focus_set()
    setMsg("MYDF")
    ModeI = intt(MYDFModeVar.get())
# Make sure whatever is in the directory field ends with a separator.
    if len(MYDFDirVar.get()) == 0:
        MYDFDirVar.set(sep)
    if MYDFDirVar.get().endswith(sep) == False:
        MYDFDirVar.set(MYDFDirVar.get()+sep)
    Dir = MYDFDirVar.get()
# Otherwise the selected stuff will just keep piling up.
    if ModeI == 4:
        MYDFFileVar.set("")
# There could be multiple files selected. Go through all of them and see if any
# of them will make us change directories.
    Sel = MYDFFiles.curselection()
    SelectedFiles = ""
    for Index in Sel:
# If the user is not right on the money this will sometimes trip.
        try:
            Selected = MYDFFiles.get(Index).strip()
        except TclError:
            beep(1)
            return
        if Selected == ".."+sep:
            Parts = Dir.split(sep)
# If there are only two Parts then we have hit the top of the directory tree.
            NewDir = ""
            if len(Parts) == 2 and len(Parts[1]) == 0:
                if PROGSystem == "dar" or PROGSystem == "lin" or \
                        PROGSystem == "sun":
                    NewDir = Parts[0]+sep
                elif PROGSystem == "win":
                    NewDir = sep
            else:
                for i in arange(0, len(Parts)-2):
                    NewDir += Parts[i]+sep
# Insurance.
            if len(NewDir) == 0:
                NewDir = sep
            MYDFDirVar.set(NewDir)
            formMYDFFillFiles()
            return
# We have to do a Texas Two-Step here to get everyone in the right frame of
# mind. If Dir is just sep then we have just clicked on a directory and not
# on a file, so make it look like the former.
        if PROGSystem == "win" and Dir == sep:
            Dir = Selected
            Selected = ""
        if isdir(Dir+Selected):
            MYDFDirVar.set(Dir+Selected)
            formMYDFFillFiles()
            return
        if ModeI == 1 or ModeI == 2:
            setMsg("MYDF", "YB", "That is not a directory.", 2)
            MYDFFiles.selection_clear(0, END)
            return
# Must have clicked on a file with byte size and that must be allowed.
        if Selected.find("  (") != -1:
            Selected = Selected[:Selected.index("  (")]
# Build the whole path for the multiple file mode.
        if ModeI == 4:
            if len(SelectedFiles) == 0:
                SelectedFiles = Dir+Selected
            else:
# An * should be a safe separator, right?
                SelectedFiles += "*"+Dir+Selected
        else:
# This should end up the only file.
            SelectedFiles = Selected
    MYDFFileVar.set(SelectedFiles)
    return
#############################
# BEGIN: formMYDFUp(e = None)
# FUNC:formMYDFUp():2018.235
def formMYDFUp(e = None):
# This is just to get the focus off of the entry field if it was there.
    PROGMsg["MYDF"].focus_set()
    setMsg("MYDF")
    Dir = MYDFDirVar.get()
    Parts = Dir.split(sep)
# If there are only two Parts then we have hit the top of the directory tree.
    NewDir = ""
    if len(Parts) == 2 and len(Parts[1]) == 0:
        if PROGSystem == "dar" or PROGSystem == "lin" or PROGSystem == "sun":
            NewDir = Parts[0]+sep
        elif PROGSystem == "win":
            NewDir = sep
    else:
        for i in arange(0, len(Parts)-2):
            NewDir += Parts[i]+sep
# Insurance.
    if len(NewDir) == 0:
        NewDir = sep
    MYDFDirVar.set(NewDir)
    formMYDFFillFiles()
    return
#########################################
# BEGIN: formMYDFReturn(Return, e = None)
# FUNC:formMYDFReturn():2018.236
def formMYDFReturn(Return, e = None):
# This update keeps the "OK" button from staying pushed in when there is a lot
# to do before returning.
    MYDFFrame.update()
    setMsg("MYDF")
    ModeI = intt(MYDFModeVar.get())
    if Return is None:
        MYDFAnswerVar.set("")
    elif len(Return) != 0:
        MYDFAnswerVar.set(Return)
    elif len(Return) == 0:
# The programmer is responsible for making sure this is used correctly.
        if len(MYDFDirVar.get()) == 0 and MYDFReturnEmpty == True:
            MYDFAnswerVar.set("")
        else:
            if ModeI == 1 or ModeI == 2:
                if len(MYDFDirVar.get()) == 0:
                    setMsg("MYDF", "YB", "There is no directory name.", 2)
                    return
                if MYDFDirVar.get().endswith(sep) == False:
                    MYDFDirVar.set(MYDFDirVar.get()+sep)
                MYDFAnswerVar.set(MYDFDirVar.get())
            elif ModeI == 0 or ModeI == 3 or ModeI == 5 or ModeI == 6:
# Just in case the user tries to pull a fast one. Mode 5 above is just for
# completness since the directory must be right (it can't be changed by the
# user).
                if len(MYDFDirVar.get()) == 0:
                    setMsg("MYDF", "YB", "There is no directory name.", 2)
                    return
# What was the point of coming here??
                if len(MYDFFileVar.get()) == 0:
                    setMsg("MYDF", "YB", "No filename has been entered.", 2)
                    return
# File names only at this point.
                if MYDFFileVar.get().find(sep) != -1:
                    setMsg("MYDF", "RW", \
                            "Character  %s  cannot be in the Filename."%sep, 2)
                    return
                if len(MYDFEndsWithVar.get()) != 0:
# I'm sure this may come back to haunt US someday, but make sure that the case
# of the entered filename matches the passed 'extension'.  My programs, as a
# general rule, are written to handle all upper or lowercase file names, but
# not mixed. Of course, on some operating systems it won't make any difference.
                    if MYDFSameCase == True:
                        if MYDFEndsWithVar.get().isupper():
                            if MYDFFileVar.get().isupper() == False:
                                setMsg("MYDF", "YB", \
                              "Filename needs to be all uppercase letters.", 2)
                                return
                        elif MYDFEndsWithVar.get().islower():
                            if MYDFFileVar.get().islower() == False:
                                setMsg("MYDF", "YB", \
                              "Filename needs to be all lowercase letters.", 2)
                                return
# If the user didn't put the 'extension' on the file add it so the caller
# won't have to do it, unless the caller passed multiple extensions, then don't
# do anything.
                    if MYDFEndsWithVar.get().find(",") == -1:
                        if MYDFFileVar.get().endswith( \
                                MYDFEndsWithVar.get()) == False:
                            MYDFFileVar.set(MYDFFileVar.get()+ \
                                MYDFEndsWithVar.get())
                MYDFAnswerVar.set(MYDFDirVar.get()+MYDFFileVar.get())
            elif ModeI == 4:
                MYDFAnswerVar.set(MYDFFileVar.get())
    MYDFFrame.destroy()
    updateMe(0)
    return
##################################################
# BEGIN: formMYDFCompFs(DirVar, FileVar, e = None)
# FUNC:formMYDFCompFs():2018.236
#   Attempts to complete the directory or file name in an Entry field using
#   the Tab key.
#   - If DirVar is set to a field's StringVar and FileVar is None then the
#     routine only looks for directories and leaves completion results in
#     DirVar.
#   - If FileVar is set to a field's StringVar and DirVar is None then the
#     routine tries to complete a file name using the files loaded into the
#     file listbox and leaves the results in FileVar.
def formMYDFCompFs(DirVar, FileVar, e = None):
    setMsg("MYDF")
# ---- Directories...looking to find you.
    if DirVar is not None:
        Dir = dirname(DirVar.get())
# This is a slight gotchya. If the field is empty that might mean that the user
# means "/" should be the starting point. If it is they will have to enter the
# / since if we are on Windows I'd have no idea what the default should be.
        if len(Dir) == 0:
            beep(2)
            return
        if Dir.endswith(sep) == False:
            Dir += sep
# Now get what must be a partial directory name, treat it as a file name, but
# then only allow the result of everything to be a directory.
        PartialFile = basename(DirVar.get())
        if len(PartialFile) == 0:
            beep(2)
            return
        PartialFile += "*"
        Files = listdir(Dir)
        Matched = []
        for File in Files:
            if fnmatch(File, PartialFile):
                Matched.append(File)
        if len(Matched) == 0:
            beep(2)
            return
        elif len(Matched) == 1:
            Dir = Dir+Matched[0]
# If whatever matched is not a directory then just beep and return, otherwise
# make it look like a directory and put it into the field.
            if isdir(Dir) == False:
                beep(2)
                return
            if Dir.endswith(sep) == False:
                Dir += sep
            DirVar.set(Dir)
            e.widget.icursor(END)
            formMYDFFillFiles(False)
            return
        else:
# Get the max number of characters that matched and put the partial directory
# path into the Var. If Dir+PartialDir is really the directory the user wants
# they will have to add the sep themselves since with multiple matches I won't
# know what to do. Consider DIR DIR2 DIR3 with a formMYDFMaxMatch() return of
# DIR. The directory DIR would always be selected and set as the path which
# may not be what the user wanted. Now this could cause trouble downstream
# since I'm leaving a path in the field without a trailing sep (everything
# tries to avoid doing that), so the caller will have to worry about that.
            PartialDir = formMYDFMaxMatch(Matched)
            DirVar.set(Dir+PartialDir)
            e.widget.icursor(END)
            beep(1)
    elif FileVar is not None:
        PartialFile = FileVar.get().strip()
        if len(PartialFile) == 0:
            beep(1)
            return
        Files = MYDFFiles.get(0, END)
        Index = 0
        Found = 0
        for Sel in arange(0, len(Files)):
# .strip() off the leading space.
            File = Files[Sel].strip()
            if File.startswith(PartialFile):
                Index = Sel
                Found += 1
        if Found == 0:
            beep(1)
            return
        elif Found > 1:
            MYDFFiles.see(Index)
            beep(1)
            return
        MYDFFiles.see(Index)
        File = Files[Index].strip()
# File contains the matching line from the Listbox, but it should have an
# (x bytes) message at the end. Get rid of that.
        if File.find("  (") != -1:
            File = File[:File.index("  (")]
        FileVar.set(File)
        e.widget.icursor(END)
    return
#############################################
# BEGIN: formMYDFCompFsTabOff(LFrm, e = None)
# FUNC:formMYDFCompFsTabOff():2010.225
def formMYDFCompFsTabOff(LFrm, e = None):
    LFrm.bind("<Key-Tab>", formMYDFNullCall)
    return
############################################
# BEGIN: formMYDFCompFsTabOn(LFrm, e = None)
# FUNC:formMYDFCompFsTabOn():2010.225
def formMYDFCompFsTabOn(LFrm, e = None):
    LFrm.unbind("<Key-Tab>")
    return
##################################
# BEGIN: formMYDFMaxMatch(TheList)
# FUNC:formMYDFMaxMatch():2018.310
#   Goes through the items in TheList (should be str's) and returns the string
#   that matches the start of all of the items.
#   This is the same as the library function maxMatch().
def formMYDFMaxMatch(TheList):
# This should be the only special case. What is the sound of one thing matching
# itself?
    if len(TheList) == 1:
        return TheList[0]
    Accum = ""
    CharIndex = 0
# If anything goes wrong just return whatever we've accumulated. This will end
# by no items being in TheList or one of the items running out of characters
# (the try) or by the TargetChar not matching a character from one of the
# items (the raise).
    try:
        while 1:
            TargetChar = TheList[0][CharIndex]
            for ItemIndex in arange(1, len(TheList)):
                if TargetChar != TheList[ItemIndex][CharIndex]:
                    raise Exception
            Accum += TargetChar
            CharIndex += 1
    except:
        pass
    return Accum
###################################
# BEGIN: formMYDFNullCall(e = None)
# FUNC:formMYDFNullCall():2013.037
def formMYDFNullCall(e = None):
    return "break"
# END: formMYDF




###############################################################################
# BEGIN: formPLOTET(Parent, DirVar, Title, Comment, ET, Others, AllowAdj)
# LIB:formPLOTET():2019.025
#   Displays a graphical version of an event table.
#
#   DirVar is the directory that should be used (passing this makes it so
#   a program does not need to have a PROGWorkDirVar or TheParmsDirVar, etc.).
#
#   Title: If supplied it will be shown at the top of the plot.
#
#   Comment: If "" the "Comment:" line will be looked for in the event table
#   file (i.e. Comment will need to be supplied when passing an ET list).
#
#   ET: If ET is a list of 'super' ET lines ("Time Action Arg" or "Time Action
#   Arg SR Gain") then that will be plotted. If ET is a filespec then then
#   that file will be read and displayed.
#
#   Others: If it is not None or [] it can be ["YYYY:DOY:HH:MM:SS <which>"...]
#   where <which> can be PROGRAM, OFFLOAD or CURRENT. The plot will be drawn
#   to include the times for these three events. The times can be from the
#   lines for these times in parameter lines, or from the fields in PETM).
#   If ET is a filespec they can only come from the file. Any supplied Others
#   will be ignored.
#
#   AllowAdj: If False then the time zone will essentially be set to GMT
#   (i.e. no adjustment of the caller's passed times will be made and the time
#   mode buttons will not even appear).

PROGFrm["PLOTET"] = None
PROGCan["PLOTET"] = None
PLOTETTmModeRVar = StringVar()
PLOTETTmModeRVar.set("gmt")
PLOTETTmOffsetVar = StringVar()
PLOTETIncOthersCVar = IntVar()
PLOTETIncOthersCVar.set(1)
PLOTETLastPSFilespecVar = StringVar()
PROGSetups += ["PLOTETTmModeRVar", "PLOTETTmOffsetVar", \
        "PLOTETIncOthersCVar", "PLOTETLastPSFilespecVar"]
PLOTETFilespecVar = StringVar()
PLOTETAllowAdj = ""
PLOTETTitle = ""
PLOTETComment = ""
PLOTETET = []
PLOTETFlagInfo = {}
PLOTETCallMode = ""
PLOTETZoom2CVar = IntVar()

def formPLOTET(Parent, DirVar, Title, Comment, ET, Others, AllowAdj):
    global PLOTETTitle
    global PLOTETComment
    global PLOTETET
    global PLOTETCallMode
    global PLOTETAllowAdj
# If the form already exists we have to decide if we need to change modes (like
# from being able to select a file to just displaying the passed list) or not.
# Close the form and recreate the whole thing for mode changes.
    if PROGFrm["PLOTET"] is not None:
        if isinstance(ET, list) and PLOTETCallMode == "file":
            formPLOTETControl("close")
        if isinstance(ET, astring) and PLOTETCallMode == "list":
            formPLOTETControl("close")
    if isinstance(ET, list):
        PLOTETCallMode = "list"
    elif isinstance(ET, astring):
        PLOTETCallMode = "file"
    PLOTETAllowAdj = AllowAdj
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    if PROGFrm["PLOTET"] is None:
        LFrm = PROGFrm["PLOTET"] = Toplevel(Parent)
        LFrm.withdraw()
        LFrm.protocol("WM_DELETE_WINDOW", Command(formPLOTETControl, "close"))
        LFrm.title("Plot Event Table")
        LFrm.iconname("PlotET")
        Sub = Frame(LFrm)
        labelTip(Sub, " Hints ", RIGHT, 45, \
                "PLOT AREA HINTS:\n--Click: Clicking on an event will display an information flag for that event.\n--Click on flag: Clicking on an information flag will make it disappear.")
        Sub.pack(side = TOP, fill = X)
        Sub = Frame(LFrm, relief = GROOVE, bd = 2)
        SSub = Frame(Sub)
# Make it as wide as practical since cramming 86400 seconds-worth of info into
# a 800 pixel display needs all the help it can get.
        if Parent == Root:
            CanW = Root.winfo_width()-150
        else:
# Should fit on an 800x600 display.
            CanW = 750
        LCan = PROGCan["PLOTET"] = Canvas(SSub, bg = Clr["W"], height = 400, \
                width = CanW, relief = SUNKEN)
        LCan.pack(side = LEFT, fill = BOTH, expand = YES)
        LSb = Scrollbar(SSub, orient = VERTICAL, command = LCan.yview)
        LSb.pack(side = RIGHT, fill = Y, expand = NO)
        LCan.configure(yscrollcommand = LSb.set)
        SSub.pack(side = TOP, fill = BOTH, expand = YES)
        Sub.pack(side = TOP, fill = BOTH, expand = YES)
        LSb = Scrollbar(Sub, orient = HORIZONTAL, command = LCan.xview)
        LSb.pack(side = TOP, fill = X, expand = NO)
        LCan.configure(xscrollcommand = LSb.set)
        if PLOTETCallMode == "file":
            Sub = Frame(LFrm)
            Entry(Sub, width = 80, \
                    textvariable = PLOTETFilespecVar).pack(side = LEFT)
            BButton(Sub, text = "Browse", \
                    command = Command(formPLOTETBrowse, \
                    DirVar)).pack(side = LEFT)
            Sub.pack(side = TOP)
        Sub = Frame(LFrm)
        if PLOTETAllowAdj == True:
            Radiobutton(Sub, text = "GMT", variable = PLOTETTmModeRVar, \
                    value = "gmt", \
                    command = formPLOTETRedraw).pack(side = LEFT)
            LRb = Radiobutton(Sub, text = "LT", variable = PLOTETTmModeRVar, \
                    value = "lt", command = formPLOTETRedraw)
            LRb.pack(side = LEFT)
            ToolTip(LRb, 30, "Use this computer's time zone setting.")
            LRb = Radiobutton(Sub, text = "TZ:=", \
                    variable = PLOTETTmModeRVar, value = "tz", \
                    command = formPLOTETRedraw)
            LRb.pack(side = LEFT)
            ToolTip(LRb, 30, \
                    "[Redraw] Enter the offset from GMT to use instead of using this computer's time and time zone settings.")
            LEnt = Entry(Sub, width = 6, textvariable = PLOTETTmOffsetVar)
            LEnt.pack(side = LEFT)
            LEnt.bind("<Return>", formPLOTETRedraw)
            LEnt.bind("<KP_Enter>", formPLOTETRedraw)
            Label(Sub, text = "hours  ").pack(side = LEFT)
            Cb = Checkbutton(Sub, text = "Include\nOther Times", \
                    variable = PLOTETIncOthersCVar, command = formPLOTETRedraw)
            Cb.pack(side = LEFT)
            ToolTip(Cb, 30, \
                    "Selecting this checkbutton will make the plot of the event table include the Current, Program, and Offload times if times are found in the event table file or are provided.")
            Label(Sub, text = " ").pack(side = LEFT)
        LCb = Checkbutton(Sub, text = "2x", variable = PLOTETZoom2CVar)
        LCb.pack(side = LEFT)
        ToolTip(LCb, 35, \
      "When selected the plots will be made twice as wide as the plot window.")
        BButton(Sub, text = "Redraw", \
                command = formPLOTETRedraw).pack(side = LEFT)
        Label(Sub, text = " ").pack(side = LEFT)
        BButton(Sub, text = "Write .ps", command = Command(formWritePS, \
                "PLOTET", "PLOTET", PLOTETLastPSFilespecVar)).pack(side = LEFT)
        Label(Sub, text = " ").pack(side = LEFT)
        BButton(Sub, text = "Close", fg = Clr["R"], \
                command = Command(formPLOTETControl, \
                "close")).pack(side = LEFT)
        Sub.pack(side = TOP, padx = 3, pady = 3)
        PROGMsg["PLOTET"] = Text(LFrm, font = PROGPropFont, height = 3, \
                wrap = WORD)
        PROGMsg["PLOTET"].pack(side = TOP, fill = X)
        center(Parent, LFrm, "CX", "O", 1)
# If the display is being used in a normal manner then just bring it up, else
# just update it and leave it where it is.
    elif AllowAdj == True:
        LFrm = PROGFrm["PLOTET"]
        LFrm.deiconify()
        LFrm.lift()
    else:
        LFrm = PROGFrm["PLOTET"]
    if PLOTETCallMode == "list":
        PLOTETTitle = Title
        PLOTETComment = Comment
# If Others times were supplied append them to the ET here. If ET is a filespec
# (below) then these times will have to come from the file.
        if Others is not None and len(Others) != 0:
            ET += Others
        PLOTETET = ET
        formPLOTETDraw()
# If a file name is passed work on it, otherwise try to load any file that is
# left in the field, otherwise do nothing.
    elif PLOTETCallMode == "file":
        if len(ET) != 0:
            PLOTETFilespecVar.set(ET)
        PLOTETFilespecVar.set(PLOTETFilespecVar.get().strip())
        if len(PLOTETFilespecVar.get()) != 0:
            Ret = formPLOTETLoadFile(Title, Comment, PLOTETFilespecVar.get())
            if Ret[0] != 0:
                setMsg("PLOTET", Ret)
                return
            formPLOTETDraw()
# Just so there is something in here for formMYDF() to start with.
    if len(PLOTETLastPSFilespecVar.get()) == 0 or \
            exists(PLOTETLastPSFilespecVar.get()) == False:
        PLOTETLastPSFilespecVar.set(PROGWorkDirVar.get())
    return
#####################################################
# BEGIN: formPLOTETLoadFile(Title, Comment, Filespec)
# FUNC:formPLOTETLoadFile():2019.025
#   Loads PLOTETTitle and PLOTETET by reading the lines from the passed
#   filespec.
def formPLOTETLoadFile(Title, Comment, Filespec):
    global PLOTETTitle
    global PLOTETComment
    global PLOTETET
    global PLOTETDecodeSR
    global PLOTETDecodeGain
    Ret = readFileLinesRB(Filespec)
    if Ret[0] != 0:
        return (2, Ret[1], Ret[2], Ret[3], Filespec, Ret[5])
    Lines = Ret[1]
# Go through the file and create a list of 'super' event table lines that are
# "Time Action Arg" or "Time Action Arg SR Gain".
    PLOTETTitle = Title
    PLOTETComment = Comment
    PLOTETET = []
# Initialize these.
    PLOTETDecodeSR = "?"
    PLOTETDecodeGain = "32"
    for Line in Lines:
        if len(Line) == 0:
            continue
# This will for sure work for event tables created with PETM. If the caller
# passed a comment then keep it.
        if len(PLOTETComment) == 0 and Line.find("Comment:") != -1:
            PLOTETComment = Line.strip()
            continue
        Ret = formPLOTETDecode(Line)
        if Ret[0] == 0:
# Don't want blank lines here.
            if len(Ret[1]) != 0:
                PLOTETET += Ret[1],
        else:
# Standard error return.
            return Ret
    if len(PLOTETET) == 0:
        return (1, "RW", "No event table lines found in\n   %s"%Filespec, 2, \
                Filespec)
    return (0, )
#####################################
# BEGIN: formPLOTETReadText(FormText)
# FUNC:formPLOTETReadText():2018.247
#   Picks out the event table lines (YYYY:DOY:HH:MM:SS A P) from the passed
#   Text() widget and returns a List of the event table lines ready to be
#   passed to formPLOTET(). There should only be one event table in the Text()
#   otherwise things will not look good.
def formPLOTETReadText(FormText):
    global PLOTETDecodeSR
    global PLOTETDecodeGain
    EventTable = []
# Initialize these.
    PLOTETDecodeSR = "?"
    PLOTETDecodeGain = "32"
    N = 1
    while 1:
        if len(FormText.get("%d.0"%N)) == 0:
            break
        Line = FormText.get("%d.0"%N, "%d.0"%(N+1))
        N += 1
        Ret = formPLOTETDecode(Line)
        if Ret[0] == 0:
# If whatever is being read has a blank line in it wil come back with (0, "").
# In this case we don't want those.
            if len(Ret[1]) != 0:
                EventTable += Ret[1],
        else:
            return Ret
    return EventTable
###############################
# BEGIN: formPLOTETDecode(Line)
# FUNC:formPLOTETDecode():2018.248
PLOTETDecodeSR = "?"
PLOTETDecodeGain = "32"

def formPLOTETDecode(Line):
    global PLOTETDecodeSR
    global PLOTETDecodeGain
    Parts = Line.split()
    if len(Parts) >= 3 and rtnPattern(Parts[0]) == "0000:000:00:00:00":
# Try this whole thing in case the lines are messed up.
        try:
# A/D power.
            if Parts[1] == "1" or Parts[1] == "3":
                return (0, "%s %s %s %s %s"%(Parts[0], Parts[1], Parts[2], \
                        PLOTETDecodeSR, PLOTETDecodeGain))
# Sample rate.
            elif Parts[1] == "2":
                PLOTETDecodeSR = rt125GetSampleRateRate(Parts[2])
                return (0, "%s %s %s %s %s"%(Parts[0], Parts[1], Parts[2], \
                        PLOTETDecodeSR, PLOTETDecodeGain))
# Set gain.
            elif Parts[1] == "4":
                PLOTETDecodeGain = rt125GetGainGain(Parts[2])
                return (0, "%s %s %s %s %s"%(Parts[0], Parts[1], Parts[2], \
                        PLOTETDecodeSR, PLOTETDecodeGain))
            else:
                return (1, "RW", "Bad line in event table.\n   %s"%Line, 2, "")
        except:
            return (1, "RW", "Bad line in event table.\n   %s"%Line, 2, "")
# Add the times from these lines in with the regular event commands.
    elif Line.startswith("ProgramTime:"):
        Parts = Line.split(":", 1)
        Time = Parts[1].strip()
        if rtnPattern(Time).startswith("0000:000:00:00"):
            return (0, "%s PROGRAM"%Time)
    elif Line.startswith("OffloadTime:"):
        Parts = Line.split(":", 1)
        Time = Parts[1].strip()
        if rtnPattern(Time).startswith("0000:000:00:00"):
            return (0, "%s OFFLOAD"%Time)
    elif Line.startswith("CurrentTime:"):
        Parts = Line.split(":", 1)
        Time = Parts[1].strip()
        if rtnPattern(Time).startswith("0000:000:00:00"):
            return (0, "%s CURRENT"%Time)
    return (0, "")
###########################################
# BEGIN: formPLOTETBrowse(DirVar, e = None)
# FUNC:formPLOTETBrowse():2018.243
def formPLOTETBrowse(DirVar, e = None):
# This is a little tricky.
# We need to have seperate Dir and File vars for formMYDF() calls, but we have
# to have one Var for the browse field and we want it to show the whole file
# spec. Use the contents of DirVar as a starting point if there is none.
    setMsg("PLOTET")
    Dir = dirname(PLOTETFilespecVar.get())
    File = basename(PLOTETFilespecVar.get())
    if len(Dir) == 0:
        Dir = DirVar.get()
    Filespec = formMYDF(PROGFrm["PLOTET"], 0, "Pick An Event Table", Dir, \
            File, "", "")
    if len(Filespec) == 0:
        return
# Check this in case the user picked a directory or something.
    if isfile(Filespec) == False:
        setMsg("PLOTET", "RW", "The selected file is not a regular file.", 2)
        return
    PLOTETFilespecVar.set(Filespec)
    Ret = formPLOTETLoadFile("", "", Filespec)
    if Ret[0] != 0:
        setMsg("PLOTET", Ret)
        return
    formPLOTETDraw()
    return
###################################
# BEGIN: formPLOTETRedraw(e = None)
# FUNC:formPLOTETRedraw():2018.243
def formPLOTETRedraw(e = None):
# Don't redraw this until we know what offset value to use.
    if PLOTETTmModeRVar.get() == "tz":
        PLOTETTmOffsetVar.set(PLOTETTmOffsetVar.get().strip())
        if len(PLOTETTmOffsetVar.get()) == 0:
            setMsg("PLOTET", "RW", "An offset value needs to be entered.", 2)
            return
    formPLOTETDraw()
    return
##########################
# BEGIN: formPLOTETClear()
# FUNC:formPLOTETClear():203.109
def formPLOTETClear():
    LCan = PROGCan["PLOTET"]
    LCan.delete(ALL)
    LCan.configure(scrollregion = (0, 0, 1, 1))
    setMsg("PLOTET")
    return
#########################
# BEGIN: formPLOTETDraw()
# FUNC:formPLOTETDraw():2018.264
def formPLOTETDraw():
    global PLOTETTitle
    global PLOTETComment
    global PLOTETET
    global PLOTETFlagInfo
    PROGFrm["PLOTET"].focus_set()
    formPLOTETClear()
    LCan = PROGCan["PLOTET"]
# So winfo_width() gets the right value.
    updateMe(0)
    CWIDTH = LCan.winfo_width()
    if PLOTETZoom2CVar.get() == 1:
        CWIDTH *= 2
# Normally someone should have already noticed this, but we'll check here
# again.
    if len(PLOTETET) == 0:
        setMsg("PLOTET", "RW", "No event table has been loaded.", 2)
        return
    setMsg("PLOTET", "CB", "Working...")
# Do this to get any Others times distributed correctly.
    PLOTETET.sort()
    PLOTETFlagInfo = {}
    CanY = PROGPropFontHeight
    if len(PLOTETTitle) != 0:
        canText(LCan, CWIDTH/2, CanY, "B", PLOTETTitle, "center")
        CanY += PROGPropFontHeight
    if len(PLOTETComment) != 0:
        canText(LCan, CWIDTH/2, CanY, "B", PLOTETComment, "center")
        CanY += PROGPropFontHeight
    if PLOTETAllowAdj == False:
# The caller must supply the title and comments in the call.
        Corr = 0.0
        CanY += PROGPropFontHeight
    elif PLOTETAllowAdj == True:
        Mode = PLOTETTmModeRVar.get()
        if Mode == "gmt":
# The time correction value in seconds.
            Corr = 0.0
            canText(LCan, CWIDTH/2, CanY, "B", "All times are GMT", "center")
            CanY += PROGPropFontHeight*2
        elif Mode == "lt":
            Corr = getGMT(20)
            canText(LCan, CWIDTH/2, CanY, "B", \
                    "All times are Local Time (GMT%+.2f hours)"% \
                    (Corr/3600.0), "center")
            CanY += PROGPropFontHeight*2
        elif Mode == "tz":
            Value = floatt(PLOTETTmOffsetVar.get().strip())
            Corr = Value*3600
            PLOTETTmOffsetVar.set("%+.2f"%Value)
            canText(LCan, CWIDTH/2, CanY, "B", "Times are GMT%s hours"% \
                    PLOTETTmOffsetVar.get(), "center")
            CanY += PROGPropFontHeight*2
    IncOthers = PLOTETIncOthersCVar.get()
# Scan through the entries and get the start time of the first event, and the
# stop time of the last event or the latest Others time and figure out how
# many days the plot will need to span.
    BeginTime = ""
    ProgramTime = ""
    OffloadTime = ""
    CurrentTime = ""
    EndTimeS = ""   # For the normal stop commands
    EndTimeSS = ""   # For the stop/start commands
# Line will be "<time> <action> <param> <...maybe other stuff>" or
#              "<time> <PROGRAM | OFFLOAD | CURRENT>"
    for Line in PLOTETET:
        Parts = Line.split()
        if len(BeginTime) == 0 and Parts[1] == "3" and Parts[2] == "1":
            BeginTime = Parts[0]
            continue
        if Parts[1] == "3" and Parts[2] == "0":
            EndTimeS = Parts[0]
            continue
        if Parts[1] == "3" and Parts[2] == "5":
            EndTimeSS = Parts[0]
            continue
        if IncOthers == 1:
            if Parts[1] == "PROGRAM":
                ProgramTime = Parts[0]
                continue
            if Parts[1] == "OFFLOAD":
                OffloadTime = Parts[0]
                continue
            if Parts[1] == "CURRENT":
                CurrentTime = Parts[0]
                continue
# I don't think these will ever happen.
    if len(BeginTime) == 0:
        setMsg("PLOTET", "RW", "No first event start time found.", 2)
        return
    if len(EndTimeS) == 0 and len(EndTimeSS) == 0:
        setMsg("PLOTET", "RW", "No last event end time found.", 2)
        return
# Determine which end time to use. It will depend on how the event table was
# built. This should work out even if one of the times are blank since we are
# working with text date/times.
    if EndTimeS > EndTimeSS:
        EndTime = EndTimeS
    elif EndTimeSS > EndTimeS:
        EndTime = EndTimeSS
# Never happen. (tm)  You should never have an event table with two command
# lines having the same time unless what ever made the event table made a
# mistake.
    elif EndTimeS == EndTimeSS:
        EndTime = EndTimeS
# Now see how the Others times might change things. Some of these don't make
# any sense, but you never know what those users might try. Again, everything
# should work out since we are still using text times.
    if len(ProgramTime) != 0:
        if ProgramTime < BeginTime:
            BeginTime = ProgramTime
        if ProgramTime > EndTime:
            EndTime = ProgramTime
    if len(OffloadTime) != 0:
        if OffloadTime < BeginTime:
            BeginTime = OffloadTime
        if OffloadTime > EndTime:
            EndTime = OffloadTime
    if len(CurrentTime) != 0:
        if CurrentTime < BeginTime:
            BeginTime = CurrentTime
        if CurrentTime > EndTime:
            EndTime = CurrentTime
# Times at this point are still something like YYYY:DOY:HH:MM.
    BeginTimeEpoch = dt2Time(0, -1, BeginTime)
# Corr may just be 0.0.
    BeginTimeEpoch += Corr
    FirstYYYY, MMM, DD, FirstDOY, HH, MM, SS = dt2Time(-1, 0, BeginTimeEpoch)
# Should be like a modified Julian day at midnight, and it's a loop counter so
# make it an int.
    FirstDayMJD = int(BeginTimeEpoch//86400)
    EndTimeEpoch = dt2Time(0, -1, EndTime)
    EndTimeEpoch += Corr
    LastDayMJD = int(EndTimeEpoch//86400)
# For the textual summary.
    ETRange = (EndTimeEpoch-BeginTimeEpoch)/3600.0
# Each day that we have to draw something will have its own slot.
# The date should be the widest thing we will stick in before the plot.
    Margin = PROGPropFont.measure("0000-00-00")+20
    GraphW = CWIDTH-Margin-(PROGPropFont.measure("00")+5)
    ThisYYYY = FirstYYYY
    ThisDOY = FirstDOY
# The canvas Y values associated with the top of each day's slot for use when
# we go back to graph the events.
    DayYs = {}
# Draw the grid and the date/times first.
    for i in arange(FirstDayMJD, LastDayMJD+1):
        DayYs["%d:%d"%(ThisYYYY, ThisDOY)] = CanY+PROGPropFontHeight*1.5
# Loop through the hours.
        for j in arange(0, 25):
            CanX = Margin+(j*(GraphW/24.0))
            canText(LCan, CanX, CanY, "B", "%02d"%j, "s")
            LCan.create_line(CanX, CanY, CanX, CanY+PROGPropFontHeight*3,
                    fill = Clr["A"])
# Now do the dates and days.
        CanY += PROGPropFontHeight
        ThisMM, ThisDD = dt2Timeydoy2md(ThisYYYY, ThisDOY)
# YYYY-MM-DD.
        canText(LCan, Margin/2, CanY, "B", "%d-%02d-%02d"%(ThisYYYY, ThisMM, \
                ThisDD), "center")
        CanY += PROGPropFontHeight
        ThisDOW = PROG_DOW[weekday(ThisYYYY, ThisMM, ThisDD)]
        DOYDOW = "%03d %s"%(ThisDOY, ThisDOW)
        canText(LCan, Margin/2, CanY, "B", DOYDOW, "center")
        CanY += PROGPropFontHeight*3
        ThisYYYY, ThisDOY = dt2TimeydoyMath(1, ThisYYYY, ThisDOY)
# We'll print a summary at the end and this will be were to start printing.
        LastDayY = CanY
# Start through the event table lines.
# These two are set just in case something is REALLY messup with the event
# table lines.
    StartTime = 0.0
    EndTime = 0.0
    FirstEventStartTime = ""
    LastEventEndTime = ""
    EventCount = 0
    SampleRates = []
    Gains = []
# Running SR and Gain values for the info flags.
    SR = "?"
    Gain = "32"
    LFH2 = PROGPropFontHeight/2
# Line will be [<time> <action> <param> <SR or "-"> <Gain or "-">].
    for Line in PLOTETET:
        Parts = Line.split()
# Check out what to do based on the "Action" part and its Arg.
        if Parts[1] == "3":
# Start recording.
            if Parts[2] == "1":
                Parts2 = Parts[0].split(":")
                StartYYYY = int(Parts2[0])
                StartDOY = int(Parts2[1])
                StartH = int(Parts2[2])
                StartM = int(Parts2[3])
                StartS = int(Parts2[4])
                if Corr != 0.0:
                    StartYYYY, MMM, DD, StartDOY, StartH, StartM, StartS = \
                            dt2TimeMath(0, Corr, StartYYYY, -1, -1, \
                            StartDOY, StartH, StartM, StartS)
                StartTime = StartH*3600.0+StartM*60.0+StartS
                EventCount += 1
# Stop recording.
            elif Parts[2] == "0":
                Parts2 = Parts[0].split(":")
                EndYYYY = int(Parts2[0])
                EndDOY = int(Parts2[1])
                EndH = int(Parts2[2])
                EndM = int(Parts2[3])
                EndS = int(Parts2[4])
                if Corr != 0.0:
                    EndYYYY, MMM, DD, EndDOY, EndH, EndM, EndS = \
                            dt2TimeMath(0, Corr, EndYYYY, -1, -1, EndDOY, \
                            EndH, EndM, EndS)
                EndTime = EndH*3600.0+EndM*60.0+EndS
# If the event started and ended on the same day...
                if StartDOY == EndDOY:
                    CanY = DayYs["%d:%d"%(StartYYYY, StartDOY)]
                    StartCanX = Margin+GraphW*(StartTime/86400.0)
                    EndCanX = Margin+GraphW*(EndTime/86400.0)
                    Tag = "E"+str(EventCount)
                    ID = LCan.create_rectangle(StartCanX, CanY-LFH2, EndCanX, \
                            CanY+LFH2, fill = Clr["G"], outline = Clr["N"], \
                            tag = Tag)
                    LCan.tag_bind(ID, "<Button-1>", formPLOTETFlagInfo)
# If not on the same day. Draw the start to 2400, and the end from 0000.
                else:
                    CanY = DayYs["%d:%d"%(StartYYYY, StartDOY)]
                    StartCanX = Margin+GraphW*(StartTime/86400.0)
                    EndCanX = Margin+GraphW
                    Tag = "E"+str(EventCount)
                    ID = LCan.create_rectangle(StartCanX, CanY-LFH2, EndCanX, \
                            CanY+LFH2, fill = Clr["G"], outline = Clr["N"], \
                            tag = Tag)
                    LCan.tag_bind(ID, "<Button-1>", formPLOTETFlagInfo)
# Special case. Don't draw anything if the end time is 00:00:00.
                    CanY = DayYs["%d:%d"%(EndYYYY, EndDOY)]
                    if EndTime != 0.0:
                        StartCanX = Margin
                        EndCanX = Margin+GraphW*(EndTime/86400.0)
                        ID = LCan.create_rectangle(StartCanX, CanY-LFH2, \
                                EndCanX, CanY+LFH2, fill = Clr["G"], \
                                outline = Clr["N"], tag = Tag)
                        LCan.tag_bind(ID, "<Button-1>", formPLOTETFlagInfo)
# In case someone just passes a raw event table list, instead of a 'super'
# event table list.
                if len(Parts) == 5:
                    PLOTETFlagInfo[Tag] = \
                            " Event: %d \n SR: %s  Gain: %s \n %d:%03d:%02d:%02d:%02d \n %d:%03d:%02d:%02d:%02d "% \
                            (EventCount, Parts[3], Parts[4], StartYYYY, \
                            StartDOY, StartH, StartM, StartS, EndYYYY, \
                            EndDOY, EndH, EndM, EndS)
                    if Parts[3] not in SampleRates:
                        SampleRates.append(Parts[3])
                    if Parts[4] not in Gains:
                        Gains.append(Parts[4])
                else:
                    PLOTETFlagInfo[Tag] = \
           " Event: %d \n %d:%03d:%02d:%02d:%02d \n %d:%03d:%02d:%02d:%02d "% \
                            (EventCount, StartYYYY, StartDOY, StartH, \
                            StartM, StartS, EndYYYY, EndDOY, EndH, EndM, \
                            EndS)
                if len(FirstEventStartTime) == 0:
                    FirstEventStartTime = "%d:%03d:%02d:%02d:%02d"% \
                            (StartYYYY, StartDOY, StartH, StartM, StartS)
                LastEventEndTime = "%d:%03d:%02d:%02d:%02d"%(EndYYYY, EndDOY, \
                        EndH, EndM, EndS)
# Stop/Start command.
            elif Parts[2] == "5":
# First treat the line like a normal stop line.
                Parts2 = Parts[0].split(":")
                EndYYYY = int(Parts2[0])
                EndDOY = int(Parts2[1])
                EndH = int(Parts2[2])
                EndM = int(Parts2[3])
                EndS = int(Parts2[4])
                if Corr != 0.0:
                    EndYYYY, MMM, DD, EndDOY, EndH, EndM, EndS = \
                            dt2TimeMath(0, Corr, EndYYYY, -1, -1, EndDOY, \
                            EndH, EndM, EndS)
                EndTime = EndH*3600.0+EndM*60.0+EndS
                if StartDOY == EndDOY:
                    CanY = DayYs["%d:%d"%(StartYYYY, StartDOY)]
                    StartCanX = Margin+GraphW*(StartTime/86400.0)
                    EndCanX = Margin+GraphW*(EndTime/86400.0)
                    Tag = "E"+str(EventCount)
                    ID = LCan.create_rectangle(StartCanX, CanY-LFH2, EndCanX, \
                            CanY+LFH2, fill = Clr["G"], outline = Clr["N"], \
                            tag = Tag)
                    LCan.tag_bind(ID, "<Button-1>", formPLOTETFlagInfo)
                else:
                    CanY = DayYs["%d:%d"%(StartYYYY, StartDOY)]
                    StartCanX = Margin+GraphW*(StartTime/86400.0)
                    EndCanX = Margin+GraphW
                    Tag = "E"+str(EventCount)
                    ID = LCan.create_rectangle(StartCanX, CanY-LFH2, EndCanX, \
                            CanY+LFH2, fill = Clr["G"], outline = Clr["N"], \
                            tag = Tag)
                    LCan.tag_bind(ID, "<Button-1>", formPLOTETFlagInfo)
                    CanY = DayYs["%d:%d"%(EndYYYY, EndDOY)]
                    if EndTime != 0.0:
                        StartCanX = Margin
                        EndCanX = Margin+GraphW*(EndTime/86400.0)
                        ID = LCan.create_rectangle(StartCanX, CanY-LFH2, \
                                EndCanX, CanY+LFH2, fill = Clr["G"], \
                                outline = Clr["N"], tag = Tag)
                        LCan.tag_bind(ID, "<Button-1>", formPLOTETFlagInfo)
# In case someone just passes a raw event table list, instead of a 'super'
# event table list.
                if len(Parts) == 5:
                    PLOTETFlagInfo[Tag] = \
                            " Event: %d \n SR: %s  Gain: %s \n %d:%03d:%02d:%02d:%02d \n %d:%03d:%02d:%02d:%02d "% \
                            (EventCount, Parts[3], Parts[4], StartYYYY, \
                            StartDOY, StartH, StartM, StartS, EndYYYY, \
                            EndDOY, EndH, EndM, EndS)
                    if Parts[3] not in SampleRates:
                        SampleRates.append(Parts[3])
                    if Parts[4] not in Gains:
                        Gains.append(Parts[4])
                else:
                    PLOTETFlagInfo[Tag] = \
           " Event: %d \n %d:%03d:%02d:%02d:%02d \n %d:%03d:%02d:%02d:%02d "% \
                            (EventCount, StartYYYY, StartDOY, StartH, \
                            StartM, StartS, EndYYYY, EndDOY, EndH, EndM, \
                            EndS)
                if len(FirstEventStartTime) == 0:
                    FirstEventStartTime = "%d:%03d:%02d:%02d:%02d"% \
                            (StartYYYY, StartDOY, StartH, StartM, StartS)
                LastEventEndTime = "%d:%03d:%02d:%02d:%02d"%(EndYYYY, EndDOY, \
                        EndH, EndM, EndS)
# Now get things set up as if this were a normal start line.
                Parts2 = Parts[0].split(":")
                StartYYYY = int(Parts2[0])
                StartDOY = int(Parts2[1])
                StartH = int(Parts2[2])
                StartM = int(Parts2[3])
                StartS = int(Parts2[4])
                if Corr != 0.0:
                    StartYYYY, MMM, DD, StartDOY, StartH, StartM, StartS = \
                            dt2TimeMath(0, Corr, StartYYYY, -1, -1, \
                            StartDOY, StartH, StartM, StartS)
                StartTime = StartH*3600.0+StartM*60.0+StartS
                EventCount += 1
# Now that all of the recording events are done handle any of these.
    if IncOthers == 1:
        for Line in PLOTETET:
            Parts = Line.split()
            if Parts[1] == "PROGRAM" or Parts[1] == "OFFLOAD" or \
                    Parts[1] == "CURRENT":
# "Other" times from the display will be YYYY:DOY:HH:MM, but the same values
# from a event table file will be YYYY:DOY:HH:MM:SS. Handle both.
                Parts2 = Parts[0].split(":")
                try:
                    StartYYYY = int(Parts2[0])
                    StartDOY = int(Parts2[1])
                    StartH = int(Parts2[2])
                    StartM = int(Parts2[3])
                    StartS = int(Parts2[4])
                except (IndexError, ValueError):
                    StartYYYY = int(Parts2[0])
                    StartDOY = int(Parts2[1])
                    StartH = int(Parts2[2])
                    StartM = int(Parts2[3])
                    StartS = 0
                if Corr != 0.0:
                    StartYYYY, MMM, DD, StartDOY, StartH, StartM, StartS = \
                            dt2TimeMath(0, Corr, StartYYYY, -1, -1, \
                            StartDOY, StartH, StartM, 0)
                StartTime = StartH*3600.0+StartM*60.0
                CanY = DayYs["%d:%d"%(StartYYYY, StartDOY)]
                StartCanX = (Margin+GraphW*(StartTime/86400.0))
                Tag = "E"+Parts[1]
                if Parts[1] == "CURRENT":
                    Colr = "B"
                elif Parts[1] == "PROGRAM":
                    Colr = "C"
                elif Parts[1] == "OFFLOAD":
                    Colr = "R"
                ID = LCan.create_rectangle(StartCanX-5, CanY-LFH2, \
                        StartCanX+5, CanY+LFH2, fill = Clr[Colr], \
                        outline = Clr["S"], tag = Tag)
                LCan.tag_bind(ID, "<Button-1>", formPLOTETFlagInfo)
                PLOTETFlagInfo[Tag] = "%s"%Parts[1]

# Put a line at the computer's local time (red) and GMT (blue) since we have no
# idea what has been plotted. Maybe one of them will be right.
    for Zone in (13, 0):
        Time = getGMT(Zone)
        Parts = Time.split(":")
        NowYYYY = int(Parts[0])
        NowDOY = int(Parts[1])
        NowH = int(Parts[2])
        NowM = int(Parts[3])
        NowS = int(Parts[4])
        NowTime = NowH*3600.0+NowM*60.0
# If this fails it means that this time is outside the range of days that have
# been plotted. In that case just go on.
        try:
            CanY = DayYs["%d:%d"%(NowYYYY, NowDOY)]
            NowCanX = (Margin+GraphW*(NowTime/86400.0))
            Tag = "E%d"%Zone
            if Zone == 0:
                Colr = "U"
            elif Zone == 13:
                Colr = "R"
            ID = LCan.create_line(NowCanX, CanY-20, NowCanX, CanY+20, \
                    fill = Clr[Colr], tag = Tag)
            LCan.tag_bind(ID, "<Button-1>", formPLOTETFlagInfo)
            if Zone == 0:
                PLOTETFlagInfo[Tag] = "Computer's GMT\n%s"%Time
            elif Zone == 13:
                PLOTETFlagInfo[Tag] = "Computer's LT\n%s"%Time
        except:
            pass
# Textual summary.
    CanY = LastDayY
# This needs to use the time adjust value, but we don't know what is going on
# so just do both times from the computer.
    HFN = dtHoursFromNow(11, "H", FirstEventStartTime, "LT")
    canText(LCan, 5, CanY, "B", "Time until start from computer's %s LT: %s"% \
            (HFN[1], HFN[0]))
    CanY += PROGPropFontHeight
    HFN = dtHoursFromNow(11, "H", FirstEventStartTime, "GMT")
    canText(LCan, 5, CanY, "B", \
            "Time until start from computer's %s GMT: %s"%(HFN[1], HFN[0]))
    CanY += PROGPropFontHeight
    canText(LCan, 5, CanY, "B", "First event start time: %s"% \
            FirstEventStartTime)
    CanY += PROGPropFontHeight
    canText(LCan, 5, CanY, "B", "Last event end time: %s"%LastEventEndTime)
    CanY += PROGPropFontHeight
    canText(LCan, 5, CanY, "B", "Total time length: %.2f hours"%ETRange)
    CanY += PROGPropFontHeight
    canText(LCan, 5, CanY, "B", "Number of events: %d"%EventCount)
    CanY += PROGPropFontHeight
    if len(SampleRates) > 0:
        canText(LCan, 5, CanY, "B", "Sample rates: %s"%list2Str(SampleRates))
    CanY += PROGPropFontHeight
    if len(Gains) > 0:
        canText(LCan, 5, CanY, "B", "Gains: %s"%list2Str(Gains))
# Adjust the scrollregion to encompass all of the items created.
    LCan.configure(scrollregion = (0, 0, CWIDTH, (LCan.bbox(ALL)[3]+5)))
    setMsg("PLOTET", "", "Done.")
    return
#####################################
# BEGIN: formPLOTETFlagInfo(e = None)
# FUNC:formPLOTETFlagInfo():2018.247
def formPLOTETFlagInfo(e = None):
    LCan = PROGCan["PLOTET"]
# Where the user clicked in canvas coords.
    Cx = LCan.canvasx(e.x)
    Cy = LCan.canvasy(e.y)
# The upper left corner of the visible canvas.
    Wx = LCan.canvasx(0)
    Wy = LCan.canvasy(0)
# The width and height of the visible canvas.
    Ww = LCan.winfo_width()
    Wh = LCan.winfo_height()
# We can set up some of the colors here (all of them for the elevation map).
# Get a list of all the items that were clicked on, go through all of them and
# make a flag for each one.
    Items = LCan.find_overlapping(Cx, Cy, Cx+1, Cy+1)
# If there are a billion overlaps just limit it to the first 10.
    if len(Items) > 10:
        Items = Items[:10]
    LastFTag = ""
    Corr = "nw"
    for Item in Items:
# In case we grab something that doesn't have any flags.
        try:
            Tag = LCan.gettags(Item)[0]
        except IndexError:
            continue
        if Tag[0] != "E":
            continue
        FTag = "F"+Tag
# If there already is a flag for this item then turn it off.
        LCan.delete(FTag)
# Make the text of the flag.
        ID = LCan.create_text(Cx, Cy, text = PLOTETFlagInfo[Tag],
                font = PROGPropFont, anchor = Corr, fill = Clr["B"], \
                justify = CENTER, state = DISABLED, tags = ("F", FTag))
# Get the coordinates of where the text is going to be and make sure it
# doesn't end up off the right or bottom edges of the map.
        L, T, R, B = LCan.bbox(ID)
# We only want the first tag's position to be corrected, and the rest to just
# follow along.
        if len(LastFTag) == 0:
            Corr = "n"
            if (Cy-Wy)+(B-T) > Wh:
                Corr = "s"
            if (Cx-Wx)+(R-L) > Ww:
                Corr += "e"
            else:
                Corr += "w"
            if Corr != "nw":
# Move it and get the coordinates again for the box.
                LCan.itemconfigure(ID, anchor = Corr)
                L, T, R, B = LCan.bbox(ID)
# Naturally, the box needs to be ONE PIXEL taller to make it look right. The
# text had to be made before the box so we would know how big to make the box.
        ID2 = LCan.create_rectangle((L, T-1, R, B), fill = Clr["Y"], \
                outline = Clr["B"], tags = ("F", FTag))
# Since the text was made first we now have to raise it above the box.
        LCan.tag_raise(ID, ID2)
# FINISHME $$$$ - One of these tags is "", all of a sudden. Still seems to
# work.
        try:
            LCan.tag_lower(FTag, LastFTag)
        except TclError:
            pass
        LastFTag = FTag
        LCan.tag_bind(ID2, "<Button-1>", Command(formPLOTETFlagInfoOff, FTag))
# Set up for any additional info flags.
        if Corr == "nw":
            Cx += 5
# +1 for the border width of the Labels.
            Cy += PROGPropFontHeight+1
        elif Corr == "ne":
            Cx -= 5
            Cy += PROGPropFontHeight+1
        elif Corr == "sw":
            Cx += 5
            Cy -= PROGPropFontHeight+1
        elif Corr == "se":
            Cx -= 5
            Cy -= PROGPropFontHeight+1
    return
#############################################
# BEGIN: formPLOTETFlagInfoOff(Tag, e = None)
# FUNC:formPLOTETFlagInfoOff():2018.243
def formPLOTETFlagInfoOff(Tag, e = None):
    LCan = PROGCan["PLOTET"]
    if len(Tag) == 0:
# The Canvas may not exist yet or anymore.
        try:
# Delete any info flags.
            LCan.delete("F")
        except:
            pass
    else:
        try:
            LCan.delete(Tag)
        except:
            pass
    return
############################################
# BEGIN: formPLOTETControl(Action, e = None)
# FUNC:formPLOTETControl():2018.291
def formPLOTETControl(Action, e = None):
    global PLOTETTitle
    global PLOTETComment
    global PLOTETET
    global PLOTETFlagInfo
    global PLOTETCallMode
# For a resetCommands()-like call.
    if Action == "stopped":
        return(0, )
    if Action == "close":
        PLOTETTitle = ""
        PLOTETComment = ""
        PLOTETET = []
        PLOTETFlagInfo = {}
        PLOTETCallMode = ""
        PLOTETFilespecVar.set("")
        formClose("PLOTET")
    return (0, )
# END: formPLOTET




####################
# BEGIN: formSRCHM()
# LIB:formSRCHM():2019.021
PROGFrm["SRCHM"] = None
SRCHMSearchVar = StringVar()
SRCHMEraseCVar = IntVar()
SRCHMEraseCVar.set(1)
SRCHMMsgsDirVar = StringVar()
SRCHMLastFilespecVar = StringVar()
PROGSetups += ["SRCHMSearchVar", "SRCHMEraseCVar", "SRCHMMsgsDirVar", \
        "SRCHMLastFilespecVar"]

def formSRCHM():
    if showUp("SRCHM"):
        return
    LFrm = PROGFrm["SRCHM"] = Toplevel(Root)
    LFrm.withdraw()
    LFrm.protocol("WM_DELETE_WINDOW", Command(formClose, "SRCHM"))
    LFrm.title("Search Messages")
    Sub = Frame(LFrm)
    LBu = BButton(Sub, text = "Search Messages Directory:", pady = "2p", \
            command = Command(changeMainDirsCmd, "SRCHM", "self", "2M", \
            SRCHMMsgsDirVar, "Pick A Search Messages Directory", \
            "Search Messages"))
    LBu.pack(side = LEFT)
    ToolTip(LBu, 35, "Click to change the directory.")
    Entry(Sub, width = 1, textvariable = SRCHMMsgsDirVar, state = DISABLED, \
            relief = FLAT, bd = 0, bg = Clr["D"], \
            fg = Clr["B"]).pack(side = LEFT, expand = YES, fill = X)
    Sub.pack(side = TOP, anchor = "w", expand = NO, fill = X, padx = 3)
    Sub = Frame(LFrm)
    labelTip(Sub, "Find:=", LEFT, 35, \
            "[Search Current] The string to search for. Spaces are preserved.")
    LEnt = PROGEnt["SRCHM"] = Entry(Sub, textvariable = SRCHMSearchVar)
    LEnt.pack(side = LEFT, expand = YES, fill = X)
    LEnt.bind("<Return>", Command(formSRCHMGo, "current"))
    LEnt.bind("<KP_Enter>", Command(formSRCHMGo, "current"))
    LEnt.focus_set()
    LEnt.icursor(END)
    BButton(Sub, text = "Clear", fg = Clr["U"], \
            command = Command(formSRCHMClear, "msg")).pack(side = LEFT)
    Sub.pack(side = TOP, expand = NO, fill = X, padx = 3)
# Results.
    Sub = Frame(LFrm)
# Let the Msg area set the width.
    LTxt = PROGTxt["SRCHM"] = Text(Sub, height = 20, wrap = NONE, \
            relief = SUNKEN)
    LTxt.pack(side = LEFT, fill = BOTH, expand = YES)
    LSb = Scrollbar(Sub, orient = VERTICAL, command = LTxt.yview)
    LSb.pack(side = RIGHT, fill = Y)
    LTxt.configure(yscrollcommand = LSb.set)
    Sub.pack(side = TOP, fill = BOTH, expand = YES)
    LSb = Scrollbar(LFrm, orient = HORIZONTAL, command = LTxt.xview)
    LSb.pack(side = TOP, fill = X)
    LTxt.configure(xscrollcommand = LSb.set)
    Sub = Frame(LFrm)
    BButton(Sub, text = "Clear All", fg = Clr["U"], \
            command = Command(formSRCHMClear, "all")).pack(side = LEFT)
    Label(Sub, text = " ").pack(side = LEFT)
    Cb = Checkbutton(Sub, text = "Erase?", variable = SRCHMEraseCVar)
    Cb.pack(side = LEFT)
    ToolTip(Cb, 30, \
            "Erase the results on the form from a previous search or not?")
    Label(Sub, text = " ").pack(side = LEFT)
    BButton(Sub, text = "Search Current\nMessages", \
            command = Command(formSRCHMGo, "current")).pack(side = LEFT)
    BButton(Sub, text = "Search All\n.msg Files...", \
            command = Command(formSRCHMGo, "files")).pack(side = LEFT)
    Label(Sub, text = " ").pack(side = LEFT)
    BButton(Sub, text = "Write Results\nTo File...", \
            command = Command(formWrite, "SRCHM", "SRCHM", "SRCHM", \
            "Write Search Results To...", SRCHMLastFilespecVar, \
            True, "", True, False)).pack(side = LEFT)
    Label(Sub, text = " ").pack(side = LEFT)
    BButton(Sub, text = "Close", fg = Clr["R"], \
            command = Command(formClose, "SRCHM")).pack(side = LEFT)
    Sub.pack(side = TOP, padx = 3, pady = 3)
# Status messages.
    PROGMsg["SRCHM"] = Text(LFrm, font = PROGPropFont, height = 3, \
            width = 100, wrap = WORD)
    PROGMsg["SRCHM"].pack(side = TOP, fill = X)
    setMsg("SRCHM", "WB", \
           "Enter what to search for in the field at the top of the window.")
    if len(SRCHMMsgsDirVar.get()) == 0 or \
            exists(SRCHMMsgsDirVar.get()) == False:
        SRCHMMsgsDirVar.set(PROGMsgsDirVar.get())
    if len(SRCHMLastFilespecVar.get()) == 0 or \
            exists(dirname(SRCHMLastFilespecVar.get())) == False:
        SRCHMLastFilespecVar.set(PROGMsgsDirVar.get()+"srchm.txt")
    center(Root, LFrm, "CX", "I", True)
    return
##############################
# BEGIN: formSRCHMClear(Which)
# FUNC:formSRCHMClear():2013.109
def formSRCHMClear(Which):
    SRCHMSearchVar.set("")
    if Which == "all":
        LTxt = PROGTxt["SRCHM"]
        LTxt.delete("0.0", END)
        LTxt.tag_delete(*LTxt.tag_names())
        setMsg("SRCHM")
    PROGEnt["SRCHM"].focus_set()
    PROGEnt["SRCHM"].icursor(END)
    return
#####################################
# BEGIN: formSRCHMGo(Which, e = None)
# FUNC:formSRCHMGo():2019.021
def formSRCHMGo(Which, e = None):
    LTxt = PROGTxt["SRCHM"]
    PROGFrm["SRCHM"].focus_set()
    setMsg("SRCHM", "CB", "Searching...")
# Don't .strip() it so you can search for things like " 2006:"
    What = SRCHMSearchVar.get().lower()
    if SRCHMEraseCVar.get() == 1:
        LTxt.delete("0.0", END)
        LTxt.tag_delete(*LTxt.tag_names())
    Lines = []
    N = 1
    if Which == "current":
        while 1:
            if len(MMsg.get("%d.0"%N)) == 0:
                break
            Line = MMsg.get("%d.0"%N, "%d.0"%(N+1)).lower()
            if What in Line:
                Lines.append(MMsg.get("%d.0"%N, "%d.0"%(N+1)))
            N += 1
# Special case. If all of the lines start with "USER:" then we should be able
# to sort them in time order by the second word which should be DOY:HH:MM.
# They should already be in time order, but may not be if several different
# messages files have been concatinated.
        AllUSER = True
        for Line in Lines:
            if Line.startswith("USER:") == False:
                AllUSER = False
                break
        if AllUSER == True:
# I like Python. Since "USER: DOY:HH:MM" is the first thing on each line
# then this should handle everything, except a year boundry, but we won't
# worry about that.
            Lines.sort()
        for Line in Lines:
            LTxt.insert(END, Line)
    elif Which == "files":
        TheMsgsDir = SRCHMMsgsDirVar.get()
# We just need a simple filter, so we're not using walkDirs().
        Files = listdir(TheMsgsDir)
# With the way some messages files are named (X-YYYYDOY-prog.msg) this might
# help things come out in chronological order.
        Files.sort()
        Searched = []
        for File in Files:
            if File.startswith(".") == False and File.endswith(".msg"):
                Searched.append(File)
                Ret = readFileLinesRB(TheMsgsDir+File)
                if Ret[0] != 0:
                    setMsg("SRSHM", Ret[1], Ret[2], Ret[3])
                    return
                FLines = Ret[1]
                for Line in FLines:
                    if What in Line.lower():
                        Lines.append("%s (%s)\n"%(Line, File))
        AllUSER = True
        for Line in Lines:
            if Line.startswith("USER:") == False:
                AllUSER = False
                break
        if AllUSER == True:
# I like Python. Since "USER: DOY:HH:MM:SS" is the first thing on each line
# then this should handle everything, except a year boundry, but we won't
# worry about that.
            Lines.sort()
        for Line in Lines:
            LTxt.insert(END, Line)
        if len(Searched) != 0:
            if len(Lines) != 0:
                LTxt.insert(END, "\n")
            LTxt.insert(END, "Files searched:\n")
            LTxt.insert(END, "   Dir: %s\n"%TheMsgsDir)
            for File in Searched:
                LTxt.insert(END, "   %s\n"%File)
    if len(Lines) == 1:
        setMsg("SRCHM", "WB", "1 match found.")
    else:
        setMsg("SRCHM", "WB", "%d matches found."%len(Lines))
    return
# END: formSRCHM




############################################################################
# BEGIN: formWrite(Parent, WhereMsg, TextWho, Title, FilespecVar, AllowPick,
#                Backup, Confirm, Clean)
# LIB:formWrite():2019.037
#   Writes the contents of the passed TextWho Text() widget to a file.
#   FilespecVar can be a StringVar or an astring.
#   If Backup is "" no backup will be made, but if it is not then the passed
#   string, like "-" or "~", will be appended to the original file name, the
#   original file will be renamed to that, and then the new stuff written to
#   the original file name.
#   If Confirm is True then the popup dialog question will be asked. If it is
#   False then no question will be asked, and the file will be overwritten to
#   the passed FilespecVar. A backup will be made depending on the value of
#   Backup.
#   Clean=True will go through each line and make sure it has ASCII characters.
#   Non-ASCII characters will be "?".
#   A List of text lines may be passed as TextWho and those will be written to
#   the file, insted of the contents of a Text().
FW_BACKUPCHAR = "-"

def formWrite(Parent, WhereMsg, TextWho, Title, FilespecVar, AllowPick, \
        Backup, Confirm, Clean):
# Sometimes it can take a while if there is a lot of text.
    updateMe(0)
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
# Can be StringVar or a string.
    if isinstance(FilespecVar, StringVar):
        InFile = FilespecVar.get()
    if isinstance(FilespecVar, astring):
        InFile = FilespecVar
# Set this in case we are skipping the confirmation stuff.
    Filespec = InFile
    if Confirm == True:
        if AllowPick == True:
            setMsg(WhereMsg, "", "")
            Filespec = formMYDF(Parent, 0, Title, dirname(InFile), \
                    basename(InFile))
            if len(Filespec) == 0:
                return ""
    setMsg(WhereMsg, "CB", "Working...")
# Default to backing up and overwriting.
    Answer = "over"
    if Confirm == True and exists(Filespec):
        Answer = formMYD(Parent, (("Append", LEFT, "append"), \
                ("Overwrite", LEFT, "over"), ("Cancel", LEFT, "cancel")), \
                "cancel", "YB", "Keep Everything.", \
                "The file\n\n%s\n\nalready exists. Would you like to append to that file, overwrite it, or cancel?"% \
                Filespec)
        if Answer == "cancel":
            setMsg(WhereMsg, "", "Nothing done.")
            return ""
    try:
        if Answer == "over":
            if len(Backup) != 0:
# rename() on some systems, like Windows, get upset if the backup file already
# exists. No one else seems to care.
                if exists(Filespec+Backup):
                    remove(Filespec+Backup)
                rename(Filespec, Filespec+Backup)
            Fp = open(Filespec, "w")
        elif Answer == "append":
            Fp = open(Filespec, "a")
    except Exception as e:
        setMsg(WhereMsg, "MW", "Error opening file (2)\n   %s\n   %s"% \
                (Filespec, e), 3)
        return ""
# Now that the file is OK...
    if isinstance(FilespecVar, StringVar):
        FilespecVar.set(Filespec)
    if isinstance(TextWho, astring):
        LTxt = PROGTxt[TextWho]
        N = 1
# In case the text field is empty.
        Line = "\n"
        while 1:
# This little convolution keeps empty lines from piling up at the end of the
# file.
            if len(LTxt.get("%d.0"%N)) == 0:
                if Line != "\n":
                    Fp.write(Line)
                break
            if N > 1:
                if Clean == True:
                    Line = line2ASCII(Line)
# The caller may not want the line cleaned, but that's doesn't 'make it so'.
                try:
                    Fp.write(Line)
                except UnicodeEncodeError:
                    setMsg(WhereMsg, "RW", "Unicode in line:\n%s"%Line, 2)
                    return
            Line = LTxt.get("%d.0"%N, "%d.0"%(N+1))
            N += 1
    elif isinstance(TextWho, list):
        for Line in Lines:
            if Line.endswith("\n"):
                if Clean == True:
                    Line = line2ASCII(Line)
                try:
                    Fp.write(Line)
                except UnicodeEncodeError:
                    setMsg(WhereMsg, "RW", "Unicode in line:\n%s"%Line, 2)
                    return
            else:
                if Clean == True:
                    Line = line2ASCII(Line)
                try:
                    Fp.write(Line+"\n")
                except UnicodeEncodeError:
                    setMsg(WhereMsg, "RW", "Unicode in line:\n%s"%Line, 2)
                    return
    Fp.close()
    if Answer == "append":
        setMsg(WhereMsg, "GB", "Appended text to file\n   %s"%Filespec)
    elif Answer == "over":
        setMsg(WhereMsg, "GB", "Wrote text to file\n   %s"%Filespec)
# Return this in case the caller is interested.
    return Filespec
#################################################################
# BEGIN: formRead(Parent, TextWho, Title, FilespecVar, AllowPick)
# FUNC:formRead():2019.003
#   Same idea as formWrite(), but handles reading a file into a Text().
#   Only appends the text to the END of the passed Text() field.
def formRead(Parent, TextWho, Title, FilespecVar, AllowPick):
# Sometimes it can take a while if there is a lot of text.
    updateMe(0)
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    Filespec = FilespecVar.get()
    if AllowPick == True:
        Filespec = formMYDF(Parent, 0, Title, dirname(Filespec), \
                basename(Filespec))
        if len(Filespec) == 0:
            return (2, "", "Nothing done.", 0, "")
    setMsg(WhereMsg, "CB", "Working...")
    if exists(Filespec) == False:
        return (2, "RW", "File to read does not exist:\n   %s"%Filespec, 2, \
                Filespec)
    if isdir(Filespec):
        return (2, "RW", "Selected file is not a normal file:\n   %s"% \
                Filespec, 2, Filespec)
# We don't know who saved the file, what the encoding is, what the line
# delimters are, so use this to get it split up. formRead() is generally just
# for ASCII text files, but you never know.
    Ret = readFileLinesRB(Filespec)
    if Ret[0] != 0:
        return Ret
    Lines = Ret[1]
# Now that the file is OK...
    LTxt = PROGTxt[TextWho]
    if int(LTxt.index('end-1c').split('.')[0]) > 1:
        LTxt.insert(END, "\n")
    for Line in Lines:
        LTxt.insert(END, "%s\n"%Line)
    FilespecVar.set(Filespec)
    if len(Lines) == 1:
        return (0, "", "Read %d line from file\n   %s"%(len(Lines), \
                Filespec), 0, Filespec)
    else:
        return (0, "", "Read %d lines from file\n   %s"%(len(Lines), \
                Filespec), 0, Filespec)
################################
# BEGIN: formWriteNoUni(TextWho)
# FUNC:formWriteNoUni():2019.037
#   A quick check before going through everything in formWrite().
def formWriteNoUni(TextWho):
# Sometimes it can take a while if there is a lot of text.
    updateMe(0)
    if isinstance(TextWho, astring):
        LTxt = PROGTxt[TextWho]
    Lines = LTxt.get("0.0", END).split("\n")
    for Line in Lines:
        try:
            Line.encode("ascii")
        except UnicodeEncodeError:
            return (1, "RW", "Unicode in line:\n   %s"%Line, 2)
    return (0,)
# END: formWrite




#################################################
# BEGIN: formWritePS(Parent, VarSet, FilespecVar)
# LIB:formWritePS():2019.003
def formWritePS(Parent, VarSet, FilespecVar):
    setMsg(VarSet, "", "")
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    LCan = PROGCan[VarSet]
    Running = 0
# The form may not have an associated Running var.
    try:
        Running = eval("%sRunning.get()"%VarSet)
    except:
        pass
    if Running != 0:
        setMsg(VarSet, "YB", "I'm busy...", 2)
        sleep(.5)
        return
# This may not make any difference depending on the form.
    try:
        if LCan.cget("bg") != "#FFFFFF":
            Answer = formMYD(Parent, (("Stop And Let Me Redo It", TOP, \
                    "stop"), ("Continue anyway", TOP, "cont")), "stop", \
                    "YB", "Charcoal Footprint?", \
                    "For best results the background color of the plot area should be white. In fact, for most plots the .ps file's background will be white, and so will the text, so you won't be able to see anything.  Do you want to stop and replot, or continue anyway?")
        if Answer == "stop":
            return
    except:
        pass
# This will cryptically crash if the canvas is empty.
    try:
        L, T, R, B = LCan.bbox(ALL)
# Postscript is so litteral.
        R += 15
        B += 15
    except:
        setMsg(VarSet, "RW", "Is the area to write blank?", 2)
        return
    Dir = dirname(FilespecVar.get())
    File = basename(FilespecVar.get())
    Filespec = formMYDF(Parent, 3, ".ps File To Save To...", Dir, File, "", \
            ".ps", False)
    if len(Filespec) == 0:
        return False
    Answer = overwriteFile(Parent, Filespec)
    if Answer == "stop":
        return
    setMsg(VarSet, "CB", "Working on a %d x %d pixel area..."%(R, B))
# This might crash if the canvas is huge?
    try:
        Ps = LCan.postscript(height = B, width = R, pageheight = B, \
                pagewidth = R, pageanchor = "nw", pagex = 0, pagey = B, \
                x = 0, y = 0, colormode = "color")
    except Exception as e:
        setMsg(VarSet, "MW", "Error converting canvas to postscript.\n   %s"% \
                e, 3)
        return
    try:
        Fp = open(Filespec, "w")
        Fp.write(Ps)
        Fp.close()
    except Exception as e:
        setMsg(VarSet, "MW", "Error saving postscript commands.\n   %s"%e, 3)
        return
    setMsg(VarSet, "", "Wrote %d x %d area (%s bytes) to file\n   %s"% \
            (R, B, fmti(len(Ps)), Filespec))
    FilespecVar.set(Filespec)
    return
# END: formWritePS




#################
# BEGIN: getCWD()
# LIB:getCWD():2008.209
def getCWD():
    CWD = getcwd()
    if CWD.endswith(sep) == False:
        CWD += sep
    return CWD
# END: getCWD




#######################
# BEGIN: getGMT(Format)
# LIB:getGMT():2015.007
#   Gets the time in various forms from the system.
def getGMT(Format):
# YYYY:DOY:HH:MM:SS (GMT)
    if Format == 0:
        return strftime("%Y:%j:%H:%M:%S", gmtime(time()))
# YYYYDOYHHMMSS (GMT)
    elif Format == 1:
        return strftime("%Y%j%H%M%S", gmtime(time()))
# YYYY-MM-DD (GMT)
    elif Format == 2:
        return strftime("%Y-%m-%d", gmtime(time()))
# YYYY-MM-DD HH:MM:SS (GMT)
    elif Format == 3:
        return strftime("%Y-%m-%d %H:%M:%S", gmtime(time()))
# YYYY, MM and DD (GMT) returned as ints
    elif Format == 4:
        GMT = gmtime(time())
        return (GMT[0], GMT[1], GMT[2])
# YYYY-Jan-01 (GMT)
    elif Format == 5:
        return strftime("%Y-%b-%d", gmtime(time()))
# YYYYMMDDHHMMSS (GMT)
    elif Format == 6:
        return strftime("%Y%m%d%H%M%S", gmtime(time()))
# Reftek Texan (year-1984) time stamp in BBBBBB format (GMT)
    elif Format == 7:
        GMT = gmtime(time())
        return pack(">BBBBBB", (GMT[0]-1984), 0, 1, GMT[3], GMT[4], GMT[5])
# Number of seconds since Jan 1, 1970 from the system.
    elif Format == 8:
        return time()
# YYYY-MM-DD/DOY HH:MM:SS (GMT)
    elif Format == 9:
        return strftime("%Y-%m-%d/%j %H:%M:%S", gmtime(time()))
# YYYY-MM-DD/DOY (GMT)
    elif Format == 10:
        return strftime("%Y-%m-%d/%j", gmtime(time()))
# YYYY, DOY, HH, MM, SS (GMT) returned as ints
    elif Format == 11:
        GMT = gmtime(time())
        return (GMT[0], GMT[7], GMT[3], GMT[4], GMT[5])
# HH:MM:SS (GMT)
    elif Format == 12:
        return strftime("%H:%M:%S", gmtime(time()))
# YYYY:DOY:HH:MM:SS (LT)
    elif Format == 13:
        return strftime("%Y:%j:%H:%M:%S", localtime(time()))
# HHMMSS (GMT)
    elif Format == 14:
        return strftime("%H%M%S", gmtime(time()))
# YYYY-MM-DD (LT)
    elif Format == 15:
        return strftime("%Y-%m-%d", localtime(time()))
# YYYY-MM-DD/DOY Day (LT)
    elif Format == 16:
        return strftime("%Y-%m-%d/%j %A", localtime(time()))
# MM-DD (LT)
    elif Format == 17:
        return strftime("%m-%d", localtime(time()))
# YYYY, MM and DD (LT) returned as ints
    elif Format == 18:
        LT = localtime(time())
        return (LT[0], LT[1], LT[2])
# YYYY-MM-DD/DOY HH:MM:SS Day (LT)
    elif Format == 19:
        return strftime("%Y-%m-%d/%j %H:%M:%S %A", localtime(time()))
# Return GMT-LT difference.
    elif Format == 20:
        Secs = time()
        LT = localtime(Secs)
        GMT = gmtime(Secs)
        return dt2Timeymddhms(-1, LT[0], -1, -1, LT[7], LT[3], LT[4], LT[5])- \
                dt2Timeymddhms(-1, GMT[0], -1, -1, GMT[7], GMT[3], GMT[4], \
                GMT[5])
# YYYY-MM-DD/DOY HH:MM:SS (LT)
    elif Format == 21:
        return strftime("%Y-%m-%d/%j %H:%M:%S", localtime(time()))
# YYYY-MM-DD HH:MM:SS (LT)
    elif Format == 22:
        return strftime("%Y-%m-%d %H:%M:%S", localtime(time()))
    return ""
# END: getGMT




#############################
# BEGIN: getNow(Field, Which)
# FUNC:getNow():2009.150
def getNow(Field, Which):
    if Which == "gmts":
        eval(Field).set(getGMT(0))
    elif Which == "lts":
        eval(Field).set(getGMT(13))
    elif Which == "gmt":
        eval(Field).set(getGMT(0)[:-3])
    elif Which == "lt":
        eval(Field).set(getGMT(13)[:-3])
    return
# END: getNow




#################
# BEGIN: intt(In)
# LIB:intt():2018.257
#   Handles all of the annoying shortfalls of the int() function (vs C).
def intt(In):
    In = str(In).strip()
    if len(In) == 0:
        return 0
# Let the system try it first.
    try:
        return int(In)
    except ValueError:
        Number = ""
        for c in In:
            if c.isdigit():
                Number += c
            elif (c == "-" or c == "+") and len(Number) == 0:
                Number += c
            elif c == ",":
                continue
            else:
                break
        try:
            return int(Number)
        except ValueError:
            return 0
# END: intt




###################################################################
# BEGIN: labelEntry2(Sub, Format, LabTx, TTLen, TTTx, TxVar, Width)
# LIB:labelEntry2():2018.236
#   For making simple  Label(): Entry() pairs.
#   Format: 10 = Aligned LEFT-RIGHT, entry field is disabled
#           11 = Aligned LEFT-RIGHT, entry field is normal
#           20 = Aligned TOP-BOTTOM, entry field disabled
#           21 = Aligned TOP-BOTTOM, entry field is normal
#    LabTx = the text of the label. "" LabTx = no label
#    TTLen = The length of the tooltip text
#     TTTx = The text of the label's tooltip. "" TTTx = no tooltip
#    TxVar = Var for the entry field
#    Width = Width to make the field, 0 = .pack(side=LEFT, expand=YES, fill=X)
#            Width should only be 0 if Format is 10 or 11.
def labelEntry2(Sub, Format, LabTx, TTLen, TTTx, TxVar, Width):
    if len(LabTx) != 0:
        if len(TTTx) != 0:
            Lab = Label(Sub, text = LabTx)
            if Format == 11 or Format == 10:
                Lab.pack(side = LEFT)
            elif Format == 21 or Format == 20:
                Lab.pack(side = TOP)
            ToolTip(Lab, TTLen, TTTx)
        else:
# Don't create the extra object if we don't have to.
            if Format == 11 or Format == 10:
                Label(Sub, text = LabTx).pack(side = LEFT)
            elif Format == 21 or Format == 20:
                Label(Sub, text = LabTx).pack(side = TOP)
    if Width == 0:
        Ent = Entry(Sub, textvariable = TxVar)
    else:
        Ent = Entry(Sub, textvariable = TxVar, width = Width+1)
    if Format == 11 or Format == 10:
        if Width != 0:
            Ent.pack(side = LEFT)
        else:
            Ent.pack(side = LEFT, expand = YES, fill = X)
    elif Format == 21 or Format == 20:
        Ent.pack(side = TOP)
    if Format == 10 or Format == 20:
        Ent.configure(state = DISABLED, bg = Clr["D"])
    return Ent
# END: labelEntry2




####################################################
# BEGIN: labelTip(Sub, LText, Side, TTWidth, TTText)
# LIB:labelTip():2006.262
#   Creates a label and assignes the passed ToolTip to it. Returns the Label()
#   widget.
def labelTip(Sub, LText, Side, TTWidth, TTText):
    Lab = Label(Sub, text = LText)
    Lab.pack(side = Side)
    ToolTip(Lab, TTWidth, TTText)
    return Lab
# END: labelTip




###########################
# BEGIN: line2ASCII(InLine)
# LIB:line2ASCII():2019.014
#   Returns the passed line with anything not an ASCII character as a ?.
def line2ASCII(InLine):
    try:
        InLine.encode("ascii")
        return InLine
    except:
        OutLine = ""
        for C in InLine:
            try:
                Value = ord(C)
                OutLine += C
            except:
                OutLine += "?"
        return OutLine
# END: line2ASCII




########################################################################
# BEGIN: list2Str(TheList, Delim = ", ", Sort = True, DelBlanks = False)
# LIB:list2Str():2018.235
def list2Str(TheList, Delim = ", ", Sort = True, DelBlanks = False):
    if isinstance(TheList, list) == False:
        return TheList
    if Sort == True:
        TheList.sort()
    Ret = ""
# If there is any funny-business (which has been seen).
    for Item in TheList:
        try:
            if DelBlanks == True:
                if len(str(Item)) == 0:
                    continue
            Ret += str(Item)+Delim
        except UnicodeEncodeError:
            Ret += "Error"+Delim
    return Ret[:-(len(Delim))]
# END: list2Str




######################
# BEGIN: listDirs(How)
# FUNC:listDirs():2008.200
def listDirs(How):
    msgLn(How, "W", "Current main messages directory:\n   %s"% \
            PROGMsgsDirVar.get())
    msgLn(How, "W", "Current main work directory:\n   %s"%PROGWorkDirVar.get())
    return
# END: listDirs




####################
# BEGIN: loadParms()
# FUNC:loadParms():2018.249
LastParmsFile = "petm.prm"

def loadParms():
    global LastParmsFile
    Answer = formMYDF(Root, 3, "Load parameters...", \
            PROGWorkDirVar.get(), LastParmsFile, "", ".prm,.tet")
    if len(Answer) == 0:
        msgLn(0, "", "Nothing loaded.")
        return
    try:
        Fp = open(Answer, "r")
    except Exception as e:
        msgLn(1, "M", "Error opening parameter file.", True, 3)
        msgLn(0, "M", "   "+str(e))
        return
    Root.focus_set()
    clearParms("all")
    Found = False
# Just catch anything that might go wrong and blame it on the .prm file
    try:
        while 1:
            Line = Fp.readline()
            if len(Line) == 0:
                break
            if Line.find("PETM Parameters:") != -1:
                Found = True
                continue
            if Found == True:
                if Line.startswith("Comment: "):
                    PROGCommentVar.set(Line.split(":", 1)[1].strip())
                elif Line.startswith("Warmup: "):
                    PROGWarmupVar.set(Line.split(":", 1)[1].strip())
# FINISHME - 2013-02-07 - Migrating to MaxADBreak. Watch for both for awhile.
# 2018: Should take this out...someday.
                elif Line.startswith("MaxADOn: "):
                    PROGMaxADBreakVar.set(Line.split(":", 1)[1].strip())
                elif Line.startswith("MaxADBreak: "):
                    PROGMaxADBreakVar.set(Line.split(":", 1)[1].strip())
                elif Line.startswith("ProgramTime: "):
                    PROGProgramTimeVar.set(Line.split(":", 1)[1].strip())
                elif Line.startswith("OffloadTime: "):
                    PROGOffloadTimeVar.set(Line.split(":", 1)[1].strip())
                elif Line.startswith("CurrentTime: "):
                    PROGCurrentTimeVar.set(Line.split(":", 1)[1].strip())
                elif Line.startswith("TimeAdjust: "):
                    PROGTimeAdjustVar.set(Line.split(":", 1)[1].strip())
# For older versions.
                elif Line.startswith("TmAdjust: "):
                    PROGTimeAdjustVar.set(Line.split(":", 1)[1].strip())
                elif Line.startswith("S") and Line.find(": ") != -1:
                    Sess = intt(Line.split(":", 1)[0][1:])
                    Meat = Line.split(":", 1)[1]
                    Parts = Meat.split(";")
                    for Var in arange(0, 0+VARS):
                        eval("S%02dParms[%d]"%(Sess, \
                                Var)).set(str(Parts[Var].strip()))
                elif len(Line.strip()) == 0:
                    break
        Fp.close()
        if Found == False:
            msgLn(1, "W", "No parameters found in:")
            msgLn(0, "", "   "+Answer)
            return
        else:
            msgLn(1, "W", "Parameters loaded from:")
            msgLn(0, "", "   "+Answer)
            PROGWorkDirVar.set(dirname(Answer)+sep)
            LastParmsFile = basename(Answer)
    except:
        msgLn(0, "M", "Parameter load error. Old .prm file perhaps?", True, 3)
        msgLn(0, "W", "Parameters cleared.")
        clearParms("all")
    return
#####################
# BEGIN: loadEvents()
# FUNC:loadEvents():2019.021
#   Reads a regular event table and fills in session info.
LastEventsFile = "125_etbl.tet"

def loadEvents():
    global LastEventsFile
    Answer = formMYDF(Root, 3, "Load events...", \
            PROGWorkDirVar.get(), LastEventsFile, "", ".tet")
    if len(Answer) == 0:
        msgLn(0, "", "Nothing loaded.")
        return
    Ret = readFileLinesRB(Answer)
    if Ret[0] != 0:
        msgLn(1, "M", "Error opening event table file.", True, 3)
        msgLn(0, "M", "   "+str(e))
        return
    Lines = Ret[1]
    Root.focus_set()
    clearParms("all")
    Found = False
    Start = ""
    StartEpoch = 0
    Stop = ""
    StopEpoch = 0
    Length = ""
    Interval = ""
    SR = ""
    Gain = ""
    Sess = 1
# Just catch anything that might go wrong and blame it on the .tet file
    try:
        for Line in Lines:
            if rtnPattern(Line).startswith("0000:000:00:00"):
                Parts = Line.split()
                if Parts[1] == "3":
# Start.
                    if Parts[2] == "1":
                        Start = Parts[0]
                        continue
# Stop. Restart.
                    if Parts[2] == "0" or Parts[2] == "5":
                        StartEpoch = dt2Time(0, -1, Start)
                        Stop = Parts[0]
                        StopEpoch = dt2Time(0, -1, Stop)
                        Value = StopEpoch-StartEpoch
                        if Value%60 == 0:
                            Length = "%dm"%(Value//60)
                        else:
                            Length = "%d"%Value
                        Interval = Length
                        eval("S%02dParms[0]"%Sess).set(Start)
                        eval("S%02dParms[1]"%Sess).set(Interval)
                        eval("S%02dParms[2]"%Sess).set(Length)
                        eval("S%02dParms[3]"%Sess).set("1")
                        eval("S%02dParms[4]"%Sess).set(SR)
                        eval("S%02dParms[5]"%Sess).set(Gain)
                        if Parts[2] == "5":
                            Start = Parts[0]
                        Sess += 1
                        Found = True
                        if Sess > SESSIONS:
                            break
                        continue
# SR
                if Parts[1] == "2":
                    SR = rt125GetSampleRateRate(Parts[2])
                    continue
# Gain
                if Parts[1] == "4":
                    Gain = rt125GetGainGain(Parts[2])
                    continue
        if Found == False:
            msgLn(1, "W", "No events found in:")
            msgLn(0, "", "   "+Answer)
            return
        elif Sess > SESSIONS:
            msgLn(1, "R", "Max (%d) events/sessions loaded."%SESSIONS)
            msgLn(0, "", "   "+Answer)
            PROGWorkDirVar.set(dirname(Answer)+sep)
            LastParmsFile = basename(Answer)
        else:
            msgLn(1, "W", "Events loaded from:")
            msgLn(0, "", "   "+Answer)
            PROGWorkDirVar.set(dirname(Answer)+sep)
            LastParmsFile = basename(Answer)
    except Exception as e:
        stdout.write("%s\n"%e)
        msgLn(0, "M", "Event load error.", True, 3)
        msgLn(0, "W", "Parameters cleared.")
        clearParms("all")
    return
####################
# BEGIN: saveParms()
# FUNC:saveParms():2018.288
def saveParms():
    global LastParmsFile
    Answer = formMYDF(Root, 3, "Save parameters...", PROGWorkDirVar.get(), \
            LastParmsFile, "", ".prm")
    if len(Answer) == 0:
        msgLn(0, "", "Nothing saved.")
        return
    Root.focus_set()
    Ret = overwriteFile(Root, Answer)
    if Ret == "stop":
        return
    try:
        Fp = open(Answer, "w")
    except Exception as e:
            msgLn(1, "M", "Error opening parameter file.", True, 3)
            msgLn(0, "M", "   "+str(e))
            return
# WARNING - keep the writing of these the same in makeET()
    Fp.write("PETM Parameters:\n")
    Fp.write("Model: RT125A")
    Fp.write("Comment: "+PROGCommentVar.get().strip()+"\n")
    Fp.write("Warmup: "+PROGWarmupVar.get().replace(" ", "").lower()+"\n")
    Fp.write("MaxADBreak: "+PROGMaxADBreakVar.get().replace(" ", \
            "").lower()+"\n")
    Fp.write("ProgramTime: "+PROGProgramTimeVar.get().strip()+"\n")
    Fp.write("OffloadTime: "+PROGOffloadTimeVar.get().strip()+"\n")
    Fp.write("CurrentTime: "+PROGCurrentTimeVar.get().strip()+"\n")
    Fp.write("TimeAdjust: "+PROGTimeAdjustVar.get().strip()+"\n")
    for Sess in arange(1, SESSIONS+1):
        if len(eval("S%02dParms[0]"%Sess).get()) == 0:
            break
        Line = "S%02d: "%Sess
        for Var in arange(0, 0+VARS):
            Line += "%s; "%eval("S%02dParms[%d]"% \
                    (Sess, Var)).get().replace(" ", "").lower()
        Fp.write(Line[:-2]+"\n")
    Fp.close()
    msgLn(1, "W", "Parameters saved to:")
    msgLn(0, "", "   "+Answer)
    PROGWorkDirVar.set(dirname(Answer)+sep)
    LastParmsFile = basename(Answer)
    return
# END: loadParms




#####################
# BEGIN: makeET(Mode)
# FUNC:makeET():2019.024
LastETFile = "125_etbl.tet"

def makeET(Mode):
    global LastETFile
    Source = PROGPowerRVar.get()
    if checkFields() == False:
        return
    Root.focus_set()
    if Source == "i32":
        IdleCurrent = 35.0
        StandbyCurrent = 90.0
        RecordCurrent = 110.0
    elif Source == "i37":
        IdleCurrent = 45.0
        StandbyCurrent = 90.0
        RecordCurrent = 110.0
    elif Source == "ext":
        IdleCurrent = 35.0
        StandbyCurrent = 90.0
        RecordCurrent = 110.0
    msgLn(1, "C", "Working...")
    if Mode == "pv":
        TmAdj = 0.0
    elif Mode == "mk":
        TmAdj = float(PROGTimeAdjustVar.get())*3600
# Where the starting info for each event will be created.
    EventStarts = []
# ---- Create all of the start/stop lines ----
    for i in arange(1, SESSIONS+1):
        if len(eval("S%02dParms[0]"%i).get()) == 0:
            continue
        Start = dt2Time(11, -1, eval("S%02dParms[0]"%i).get())+TmAdj
        Interval = dt2Timedhms2Secs(eval("S%02dParms[1]"%i).get())
# Warn if this gets too long.
        Length = dt2Timedhms2Secs(eval("S%02dParms[2]"%i).get())
        if Length > 3600:
            Answer = formMYD(Root, \
                    (("(Stop So I Can Change It)", TOP, "stop"), \
                    ("I'm Not Changing It And You Can't Make Me?", TOP, \
                    "cont")), "stop", "YB", "I'm Bugging You.",
                    "It is HIGHLY recommended that you change the event length (and consequently the number of events, etc.) to be one hour or less. In most cases this will make the QC and post-processing of the data much easier. Also, if anything should go wrong during recording it's possible that the data from the event that is recording at that time may be lost. How much data do you want to lose? One hour's worth would be better than the amount you have.\n\n(Yes, this nag will bug you every time you preview or make this event table and it will come up once for each session with a length over an hour until you change it. It's that important.)")
            if Answer == "stop":
                return
        StrEvents = eval("S%02dParms[3]"%i).get()
        if "d" in StrEvents or "h" in StrEvents or "m" in StrEvents or \
                "s" in StrEvents:
            IntEvents = dt2Timedhms2Secs(StrEvents)
            Events = int(IntEvents/Interval)
            if IntEvents != 0 and Interval != 0 and Events == 0:
                Events = 1
        else:
            Events = intt(eval("S%02dParms[3]"%i).get())
        SR = eval("S%02dParms[4]"%i).get()
        Gain = eval("S%02dParms[5]"%i).get()
# Format: [Start time, End time, Session, Action/Parm, SR, Gain]
# This will be the same format for the finished EventTable entries too.
        START = 0
        END = 1
        SESS = 2
        ACPA = 3
        SRATE = 4
        GAIN = 5
        for j in arange(0, Events):
            EventStarts.append([Start, Start+Length, i, "3 1", SR, Gain])
            Start += Interval
# I'm pretty sure this can't happen, so I'd better check for it.
    if len(EventStarts) == 0:
        msgLn(1, "Y", "   There are no events to process.")
        msgLn(0, "Y", "Stopped.", True, 2)
        return
    EventStarts.sort()
# ---- Check for overlaps ----
    for i in arange(0, len(EventStarts)):
        if i == 0:
            continue
# If this event starts at or after the end of the previous one continue.
        if EventStarts[i][START] >= EventStarts[i-1][END]:
            continue
        msgLn(1, "R", \
                "   Session %d at %s starts %d seconds before the end of a session %d event."% \
                (EventStarts[i][SESS], \
                dt2Time(-1, 11, EventStarts[i][START])[:-4], \
                int(EventStarts[i-1][END]-EventStarts[i][START]), \
                EventStarts[i-1][SESS]))
        msgLn(0, "R", "Stopped.", True, 2)
        return
# ---- Build the EventTable ----
# This loop will add all of the other event table command lines.
# Format: [Start time, End time, Session, Action/Parm, SR, Gain]
    EventTable = []
    Warmup = dt2Timedhms2Secs(PROGWarmupVar.get())
    MaxGap = dt2Timedhms2Secs(PROGMaxADBreakVar.get())
    for i in arange(0, len(EventStarts)):
# Add the initial A/D warmup, sample rate and gain (RT125A only) lines to get
# started.
        if i == 0:
            EventTable.append([EventStarts[i][START]-Warmup, \
                    EventStarts[i][START]-Warmup, EventStarts[0][SESS], \
                    "1 1", "", ""])
            EventTable.append([EventStarts[i][START]-Warmup+1.0, \
                    EventStarts[i][START]-Warmup+1.0, \
                    EventStarts[0][SESS], "2 %s"% \
                    rt125GetSampleRateCode(EventStarts[i][SRATE]), \
                    EventStarts[i][SRATE], ""])
            GainCode = rt125GetGainCode(EventStarts[i][GAIN])
            if GainCode == "0":
                GainCode = "6"
            EventTable.append([EventStarts[i][START]-Warmup+2.0, \
                    EventStarts[i][START]-Warmup+2.0, \
                    EventStarts[0][SESS], "4 %s"%GainCode, "", \
                    EventStarts[i][GAIN]])
# Start recording. The stop recoding command will be appended somewhere below.
            EventTable.append(EventStarts[i])
            continue
# This event's start time minus last event's end time.
        StopStartValue = EventStarts[i][START]-EventStarts[i-1][END]
# ---- Check for Start/Stop command ----
# We don't have to check for model RT125A in this section since they are the
# only ones that could have gotten this far.
        if StopStartValue == 0.0:
# Changing sample rates or gains is not allowed when doing "continuous"
# recording (there has to be a break in the action).
            if EventStarts[i][SRATE] != EventStarts[i-1][SRATE]:
                msgLn(1, "R", \
                        "   There must be a 5 second gap between events when changing the sample rate.")
                msgLn(0, "R", "Stopped.", True, 2)
                return
            if EventStarts[i][GAIN] != EventStarts[i-1][GAIN]:
                msgLn(1, "R", \
                        "   There must be a 5 second gap between events when changing the sample rate.")
                msgLn(0, "R", "Stopped.", True, 2)
                return
# Stop/Start recoding.
            EventTable.append([EventStarts[i][START], EventStarts[i][END], \
                    EventStarts[i][SESS], "3 5", EventStarts[i][SRATE], \
                    EventStarts[i][GAIN]])
            continue
        else:
# If we're not doing stop/start then finish off the previous event normally
# And keep going to see what else we are going to do.
            EventTable.append([EventStarts[i-1][END], EventStarts[i-1][END], \
                    EventStarts[i-1][SESS], "3 0", EventStarts[i-1][SRATE], \
                    EventStarts[i-1][GAIN]])
# ---- Check to see if the A/D will be shutting down ----
# MaxGap must be greater than Warmup or we would not be here, so use it.
        if StopStartValue > MaxGap:
# Turn off A/D.
            EventTable.append([EventStarts[i-1][END]+1.0, \
                    EventStarts[i-1][END]+1.0, EventStarts[i][SESS], "1 0", \
                    "", ""])
# Turn on A/D.
            EventTable.append([EventStarts[i][START]-Warmup, \
                    EventStarts[i][START]-Warmup, EventStarts[i][SESS], \
                    "1 1", "", ""])
# Required after turning on A/D!
            EventTable.append([EventStarts[i][START]-Warmup+1.0, \
                    EventStarts[i][START]-Warmup+1.0, EventStarts[i][SESS], \
                    "2 %s"%rt125GetSampleRateCode(EventStarts[i][SRATE]), \
                    EventStarts[i][SRATE], ""])
            if EventStarts[i][GAIN] != EventStarts[i-1][GAIN]:
                EventTable.append([EventStarts[i][START]-Warmup+2.0, \
                        EventStarts[i][START]-Warmup+2.0, \
                        EventStarts[i][SESS], \
                        "4 %s"%rt125GetGainCode(EventStarts[i][GAIN]), \
                        "", EventStarts[i][GAIN]])
            EventTable.append(EventStarts[i])
        else:
# ---- Check for sample rate or gain changes during an A/D ON break ----
            AddTime = 1.0
            if EventStarts[i][SRATE] != EventStarts[i-1][SRATE]:
# Change these right after the previous event ends.
# Check to see if there will be enough time so we don't have to do that later.
# 5 seconds is too much, but that's too bad (actually it's not enough if you
# are using the very low sample rates -- remember: 29 samples).
                if StopStartValue < 5.0:
                    msgLn(1, "R", \
                            "   There must be a 5 second gap between events when changing the sample rate.")
                    msgLn(0, "R", "Stopped.", True, 2)
                    return
                EventTable.append([EventStarts[i-1][END]+AddTime, \
                        EventStarts[i-1][END]+AddTime, EventStarts[i][SESS], \
                        "2 %s"%rt125GetSampleRateCode(EventStarts[i][SRATE]), \
                        EventStarts[i][SRATE], ""])
                AddTime += 1.0
            if EventStarts[i][GAIN] != EventStarts[i-1][GAIN]:
                if StopStartValue < 5.0:
                    msgLn(1, "R", \
      "   There must be a 5 second gap between events when changing the gain.")
                    msgLn(0, "R", "Stopped.", True, 2)
                    return
                EventTable.append([EventStarts[i-1][END]+AddTime, \
                        EventStarts[i-1][END]+AddTime, EventStarts[i][SESS], \
                        "4 %s"%rt125GetGainCode(EventStarts[i][GAIN]), \
                        "", EventStarts[i][GAIN]])
            EventTable.append(EventStarts[i])
# Add the final shutdown commands.
    EventTable.append([EventStarts[-1][END], EventStarts[-1][END], \
            EventStarts[-1][SESS], "3 0", EventStarts[-1][SRATE], \
            EventStarts[-1][GAIN]])
    EventTable.append([EventStarts[-1][END]+1.0, EventStarts[-1][END]+1.0, \
            EventStarts[-1][SESS], "1 0", "", ""])
# This shouldn't be necessary unless there is a bug then this might make it
# stand out if anyone actually looks at the event table.
    EventTable.sort()
    if len(EventTable) > 5000:
        msgLn(1, "R", "Too many lines (5000 limit): %d"%len(EventTable), \
                True, 2)
        return
# We'll collect session info here for the summary and also load PLOTETET since
# the event table is now complete.
    SessionInfo = {}
    SessionSampleRates = []
    SessionGains = []
    PLOTETET = []
    SR = ""
    Gain = "32"
# Line = [Start time, End time, Session, Action/Parm, SR, Gain]
    for Line in EventTable:
        PLOTETLine = "%s %s"%(dt2Time(-1, 11, Line[START])[:-4], Line[ACPA])
        Session = str(Line[SESS])
        SR = Line[SRATE]
        Gain = Line[GAIN]
        if Line[ACPA] == "3 0":
            SessionInfo[Session+"E"] = dt2Time(-1, 23, Line[START])[:-4]
            PLOTETLine += " %s %s"%(SR, Gain)
            if SR not in SessionSampleRates:
                SessionSampleRates.append(SR)
            if Gain not in SessionGains:
                SessionGains.append(Gain)
            PLOTETET.append(PLOTETLine)
        elif Line[ACPA] == "3 1":
            if Session+"B" not in SessionInfo:
                SessionInfo[Session+"B"] = dt2Time(-1, 23, Line[START])[:-4]
            PLOTETLine += " %s %s"%(SR, Gain)
            if SR not in SessionSampleRates:
                SessionSampleRates.append(SR)
            if Gain not in SessionGains:
                SessionGains.append(Gain)
            PLOTETET.append(PLOTETLine)
        elif Line[ACPA] == "3 5":
            if Session+"B" not in SessionInfo:
                SessionInfo[Session+"B"] = dt2Time(-1, 23, Line[START])[:-4]
            PLOTETLine += " %s %s"%(SR, Gain)
            if SR not in SessionSampleRates:
                SessionSampleRates.append(SR)
            if Gain not in SessionGains:
                SessionGains.append(Gain)
            PLOTETET.append(PLOTETLine)
# This is faster than checking the StringVar.
    ListMode = PROGListETRVar.get()
    if ListMode == "et" or ListMode == "ev":
        if Mode == "pv":
            msgLn(1, "W", "===== PREVIEW OF RT125A EVENT TABLE =====")
            if PROGTimeAdjustVar.get() != "+0.00":
                msgLn(1, "Y", \
                        "TIMES HAVE NOT BEEN ADJUSTED BY TIME ADJUST VALUE")
        elif Mode == "mk":
            msgLn(1, "W", "===== MAKING RT125A EVENT TABLE =====")
        EvtCount = 0
        for Line in EventTable:
# This is here in case the user forgets to turn this off and starts listing a
# 2000 line long table.
# We need to check the var here to detect changes.
            if PROGListETRVar.get() == "no":
                msgLn(0, "Y", "Stopped listing.")
                break
            Session = Line[SESS]
            if Line[ACPA] == "3 0":
                if ListMode == "et" or ListMode == "ev":
                    msgLn(1, "", " %s  %s  "%(dt2Time(-1, 11, \
                            Line[START])[:-4], "S%02d:%s"%(Session, \
                            Line[ACPA])), False)
                    msgLn(1, "R", "Record stop")
            elif Line[ACPA] == "3 1":
                EvtCount += 1
                if ListMode == "et" or ListMode == "ev":
                    if Mode == "pv":
                        msgLn(1, "", " --- Event %d: %s ---"%(EvtCount, \
                                dt2Time(-1, 23, Line[START])[:-4]))
                    elif Mode == "mk":
                        msgLn(1, "", " --- Event %d: %s GMT ---"%(EvtCount, \
                                dt2Time(-1, 23, Line[START])[:-4]))
                    msgLn(1, "", " %s  %s  "%(dt2Time(-1, 11, \
                            Line[START])[:-4], "S%02d:%s"%(Session, \
                            Line[ACPA])), False)
                    msgLn(1, "G", "Record start")
                if str(Session)+"B" not in SessionInfo:
                    SessionInfo[str(Session)+"B"] = dt2Time(-1, 23, \
                            Line[START])[:-4]
            elif Line[ACPA] == "3 5":
                EvtCount += 1
                if ListMode == "et" or ListMode == "ev":
                    if Mode == "pv":
                        msgLn(1, "", " --- Event %d: %s ---"%(EvtCount, \
                                dt2Time(-1, 23, Line[START])[:-4]))
                    elif Mode == "mk":
                        msgLn(1, "", " --- Event %d: %s GMT ---"%(EvtCount, \
                                dt2Time(-1, 23, Line[START])[:-4]))
                    msgLn(1, "", " %s  %s  "%(dt2Time(-1, 11, \
                            Line[START])[:-4], "S%02d:%s"%(Session, \
                            Line[ACPA])), False)
                    msgLn(1, "G", "Record stop/start")
            elif Line[ACPA] == "1 1":
                if ListMode == "et":
                    msgLn(1, "", " %s  %s  "%(dt2Time(-1, 11, \
                            Line[START])[:-4], "S%02d:%s"%(Session, \
                            Line[ACPA])), False)
                    msgLn(1, "W", "A/D powerup")
            elif Line[ACPA] == "1 0":
                if ListMode == "et":
                    msgLn(1, "", " %s  %s  "%(dt2Time(-1, 11, \
                            Line[START])[:-4], "S%02d:%s"%(Session, \
                            Line[ACPA])), False)
                    msgLn(1, "W", "A/D off")
            elif Line[ACPA].startswith("2 "):
                if ListMode == "et":
                    msgLn(1, "", " %s  %s  "%(dt2Time(-1, 11, \
                            Line[START])[:-4], "S%02d:%s"%(Session, \
                            Line[ACPA])), False)
                    msgLn(1, "C", "Set sample rate: %s"%Line[SRATE])
            elif Line[ACPA].startswith("4 "):
                if ListMode == "et":
                    msgLn(1, "", " %s  %s  "%(dt2Time(-1, 11, \
                            Line[START])[:-4], "S%02d:%s"%(Session, \
                            Line[ACPA])), False)
                    msgLn(1, "C", "Set gain: %s"%Line[GAIN])
# Check again to see if we have created too many events, but this time keep a
# count of everything to help the user decide on possible fixes.
    EventStartLns = 0
    EventStopLns = 0
    EventStopStartLns = 0
    ADOnLns = 0
    ADOffLns = 0
    SetSRLns = 0
    SetGLns = 0
    StandbyTm = 0
    IdleTm = 0
    RecordTm = 0
# Worst case for 'number of pages per event' calculation.
    SampleRate = 1000
    Memory = 0
    PagesPerEventWarning = False
# Before we start the loop set the time of the "last" (wink wink) event table
# line's time. If there is a program time, then we will use that. If there is
# a current time then use that.
    LastTime = 0
    if len(PROGProgramTimeVar.get()) != 0:
        if Mode == "pv":
            LastTime = dt2Time(11, -1, PROGProgramTimeVar.get())
        elif Mode == "mk":
            LastTime = dt2Time(11, -1, PROGProgramTimeVar.get())+TmAdj
    elif len(PROGCurrentTimeVar.get()) != 0:
        if Mode == "pv":
            LastTime = dt2Time(11, -1, PROGCurrentTimeVar.get())
        elif Mode == "mk":
            LastTime = dt2Time(11, -1, PROGCurrentTimeVar.get())+TmAdj
# As a last resort. A warning message about this (no current or program time)
# will be generated later.
    if LastTime == 0:
# I don't directly ask for the epoch time here from getGMT(), because my stuff
# doesn't deal with leap seconds and all of that, so the time from the system
# would not mesh correctly...or vice versa.
        LastTime = dt2Time(11, -1, getGMT(0))
    for Line in EventTable:
        if Line[ACPA] == "3 0":
            Length = Line[START]-LastTime
# Samples of data, 169 samples per page, 528 bytes per page, +528 to correct
# for the rounding error that will probably result when the Texans do not
# record a full page worth of samples. This calculation has nothing to do
# with correcting for the possibly of the many SOH pages that the RT125As
# record during an event.
            Samps = SampleRate*Length
            Pgs = Samps/169
            EventMemory = Pgs*528+528
            Memory += EventMemory
            RecordTm += Length
            LastTime = Line[0]
            EventStopLns += 1
            if SampleRate*Length/169 > 65535:
                PagesPerEventWarning = True
        elif Line[ACPA] == "3 1":
            StandbyTm += Line[START]-LastTime
            LastTime = Line[START]
            EventStartLns += 1
        elif Line[ACPA] == "3 5":
            Length = Line[START]-LastTime
            Samps = SampleRate*Length
            Pgs = Samps/169
            EventMemory = Pgs*528+528
            Memory += EventMemory
# %%%% why commented out?
#            Memory += int(ceil(528*int(EventMemory/560000)))
            RecordTm += Length
            LastTime = Line[START]
            EventStopStartLns += 1
            if SampleRate*Length/169 > 65535:
                PagesPerEventWarning = True
        elif Line[ACPA] == "1 1":
            IdleTm += Line[START]-LastTime
            LastTime = Line[START]
            ADOnLns += 1
        elif Line[ACPA] == "1 0":
            StandbyTm += Line[START]-LastTime
            LastTime = Line[START]
            ADOffLns += 1
# These two don't affect time.
        elif Line[ACPA].startswith("2 "):
            SampleRate = int(Line[SRATE])
            SetSRLns += 1
        elif Line[ACPA].startswith("4 "):
            SetGLns += 1
# If the user put in a Offload time count that as IdleTm.
    if len(PROGOffloadTimeVar.get()) != 0:
        if Mode == "pv":
            IdleTm += dt2Time(11, -1, PROGOffloadTimeVar.get())-LastTime
        elif Mode == "mk":
            IdleTm += dt2Time(11, -1, PROGOffloadTimeVar.get())+TmAdj- \
                    LastTime
    msgLn(1, "W", "===== SUMMARY =====")
# List the raw session data for planning purposes.
    msgLn(1, "", "PETM parameters:")
    msgLn(1, "", "Model: RT125A")
    msgLn(1, "", "Comment: "+PROGCommentVar.get().strip())
    msgLn(1, "", "Warmup: "+PROGWarmupVar.get().replace(" ", "").lower())
    msgLn(1, "", "MaxADBreak: "+PROGMaxADBreakVar.get().replace(" ", \
            "").lower())
    msgLn(1, "", "ProgramTime: "+PROGProgramTimeVar.get().strip())
    msgLn(1, "", "OffloadTime: "+PROGOffloadTimeVar.get().strip())
    msgLn(1, "", "CurrentTime: "+PROGCurrentTimeVar.get().strip())
    msgLn(1, "", "TimeAdjust: "+PROGTimeAdjustVar.get().strip())
    for Sess in arange(1, SESSIONS+1):
        if len(eval("S%02dParms[0]"%Sess).get()) == 0:
            break
        Line = "S%02d: "%Sess
        for Var in arange(0, 0+VARS):
            Line += "%s; "%eval("S%02dParms[%d]"% \
                    (Sess, Var)).get().replace(" ", "").lower()
        msgLn(1, "", Line[:-2])
    msgLn(1, "", "")
    msgLn(1, "", " %d A/D powerup lines"%ADOnLns)
    msgLn(1, "", " %d A/D off lines"%ADOffLns)
    msgLn(1, "", " %d set gain lines"%SetGLns)
    msgLn(1, "", " %d set sample rate lines"%SetSRLns)
    msgLn(1, "", " %d event start lines"%EventStartLns)
    msgLn(1, "", " %d event stop/start lines"%EventStopStartLns)
    msgLn(1, "", " %d event stop lines"%EventStopLns)
    msgLn(1, "", " Total event table lines: %d (%X hex)"%(len(EventTable), \
            len(EventTable)))
# Warn the user of impending doom.
    if PagesPerEventWarning == True:
        msgLn(1, "", "")
        msgLn(1, "Y", "It is recommended that you not record events that")
        msgLn(1, "Y", "cause the result of the calculation:")
        msgLn(1, "Y", "    SampleRate*EventLengthInSecs/169")
        msgLn(1, "Y", "to exceed 65535. An estimate to stay below the")
        msgLn(1, "Y", "limit is about 3 hours at a sample rate of 1000sps.", \
                True, 2)
# Break down each session into start and stop times and all of the sample rates
# and gains in the event table.
    msgLn(1, "", "")
# The Zone used to be "LT" for the pv mode, but it was potentially confusing
# since the user may have actually entered GMT times. The extra message was
# added to further unconfuse things...maybe.
    if Mode == "pv":
        msgLn(1, "", \
            "Event table session event start/end times, sample rates and gains (times are based on the times as entered with no time adjustment applied):")
        Zone = ""
    elif Mode == "mk":
        msgLn(1, "", \
          "Event table session event start/end times, sample rates and gains (times have Time Adjust value applied):")
        Zone = "GMT"
    SessionInfoKeys = list(SessionInfo.keys())
    SessionInfoKeys.sort()
    for SessionInfoKey in SessionInfoKeys:
        if SessionInfoKey.endswith("B"):
            msgLn(1, "", "S%02d starts: %s  %s"%(int(SessionInfoKey[:-1]), \
                    SessionInfo[SessionInfoKey], Zone))
        else:
            msgLn(1, "", "      ends: %s  %s"%(SessionInfo[SessionInfoKey], \
                    Zone))
    msgLn(1, "", "Sample rates: %s"%list2Str(SessionSampleRates, ",", True))
    msgLn(1, "", "       Gains: %s"%list2Str(SessionGains, ",", True))
    msgLn(1, "", "")
# Time until starting to run the event table.
# Warn the user about this since it may mean that the calculations could all
# be wrong.
    if len(PROGProgramTimeVar.get()) == 0 and \
            len(PROGCurrentTimeVar.get()) == 0:
        Now = getGMT(0)
        if Mode == "pv":
# We'll take a shot and assume that the computer's clock is set correctly and
# de-correct the GMT time from the system by the amount the user intends to
# correct the other times by when we make the real event table. If the time adj
# field is not set to the correct value then things will not go well.
# 60 chars.
            msgLn(1, "Y", \
                    "NOTE: The Current Time will be this computer's GMT time minus\nthe Time Adjust value. If the time is set correctly there\nshould be no problem. If it is not then the times and power\ncalculations below may be wrong.\n")
            NowEpoch = dt2Time(11, -1, Now)- \
                    (float(PROGTimeAdjustVar.get())*3600.0)
        elif Mode == "mk":
            msgLn(1, "Y", \
                    "NOTE: The Current Time will be this computer's GMT time. If\nthe time is set correctly there should be no problem. If it\nis not then the times and power calculations below may be wrong.\n")
            NowEpoch = dt2Time(11, -1, Now)
    elif len(PROGCurrentTimeVar.get()) != 0:
        msgLn(1, "Y", \
                "NOTE: The Current Time will be the Current Time field value.\nIf the time is set correctly there should be no problem. If\nit is not then the times and power calculations below may\nbe wrong.\n")
        Now = PROGCurrentTimeVar.get()
        NowEpoch = dt2Time(11, -1, Now)
    else:
# We won't use the filled-in Program Time, but the GMT-adj time or just GMT.
        Now = getGMT(0)
        if Mode == "pv":
            msgLn(1, "Y", \
                    "NOTE: The Current Time will be this computer's GMT time minus\nthe Time Adjust value. If the time is set correctly there\nshould be no problem. If it is not then the times and power\ncalculations below may be wrong.\n")
            NowEpoch = dt2Time(11, -1, Now)- \
                    (float(PROGTimeAdjustVar.get())*3600.0)
        elif Mode == "mk":
            msgLn(1, "Y", \
                    "NOTE: The Current Time will be this computer's GMT time. If\nthe time is set correctly there should be no problem. If it\nis not then the times and power calculations below may be wrong.\n")
            NowEpoch = dt2Time(11, -1, Now)
    msgLn(1, "", "Current time: %s"%dt2Time(-1, 23, NowEpoch)[:-4])
    if len(PROGProgramTimeVar.get()) != 0:
        if Mode == "pv":
            Value = dt2Time(11, -1, PROGProgramTimeVar.get())-NowEpoch
        elif Mode == "mk":
            Value = (dt2Time(11, -1, PROGProgramTimeVar.get())+TmAdj)-NowEpoch
        if Value < 0.0:
            msgLn(1, "R", "Time until programming: %0.2f hours"% \
                    float(Value/3600.0))
        elif Value < 1200.0:
            msgLn(1, "Y", "Time until programming: %0.2f hours"% \
                    float(Value/3600.0))
        else:
            msgLn(1, "G", "Time until programming: %0.2f hours"% \
                    float(Value/3600.0))
# Time until start.
# EventTable[0][START] is in LT or GMT so we just do this on faith and hope we
# get the right value.
    Value = EventTable[0][START]-NowEpoch
    if Value < 0.0:
        msgLn(1, "R", "Time until start: %0.2f hours"%float(Value/3600.0))
    elif Value < 1200.0:
        msgLn(1, "Y", "Time until start: %0.2f hours"%float(Value/3600.0))
    else:
        msgLn(1, "G", "Time until start: %0.2f hours"%float(Value/3600.0))
# Power summary.
    msgLn(1, "", "")
# If there is a program time, use that for the power calculations. If there is
# a current time, use that. If there is neither then use the current time.
    if len(PROGProgramTimeVar.get()) != 0:
        Epoch = dt2Time(0, -1, PROGProgramTimeVar.get())
        if Mode == "pv":
            msgLn(1, "", "Power calculation start time: %s (prog)"% \
                    dt2Time(-1, 23, Epoch)[:-4])
        elif Mode == "mk":
            Epoch += TmAdj
            msgLn(1, "", "Power calculation start time: %s (prog)"% \
                    dt2Time(-1, 23, Epoch)[:-4])
    elif len(PROGCurrentTimeVar.get()) != 0:
        Epoch = dt2Time(0, -1, PROGCurrentTimeVar.get())
        if Mode == "pv":
            msgLn(1, "", "Power calculation start time: %s (curr)"% \
                    dt2Time(-1, 23, Epoch)[:-4])
        elif Mode == "mk":
            Epoch += TmAdj
            msgLn(1, "", "Power calculation start time: %s (curr)"% \
                    dt2Time(-1, 23, Epoch)[:-4])
    else:
        msgLn(1, "", "Power calculation start time: %s (now)"%dt2Time(-1, 23, \
                NowEpoch)[:-4])
    if len(PROGOffloadTimeVar.get()) != 0:
        Epoch = dt2Time(0, -1, PROGOffloadTimeVar.get())
        if Mode == "pv":
            msgLn(1, "", "Power calculation end time: %s"%dt2Time(0, 23, \
                    PROGOffloadTimeVar.get())[:-4])
        elif Mode == "mk":
            Value = dt2Time(0, -1, PROGOffloadTimeVar.get())
            Value += TmAdj
            msgLn(1, "", "Power calculation end time: %s"%dt2Time(-1, 23, \
                    Value)[:-4])
    else:
        msgLn(1, "", "Power calculation end time: End of event table")
# This is going to be wrong if the start time has already passed
    msgLn(1, "", "")
    if IdleTm <= 0:
        msgLn(1, "Y", " %9.2f hours of idle time (probably wrong)"% \
                (IdleTm/3600.0))
    else:
        msgLn(1, "", " %9.2f hours of idle time"%(IdleTm/3600.0))
    msgLn(1, "", " %9.2f hours of standby time"%(StandbyTm/3600.0))
    msgLn(1, "", " %9.2f hours of recording time"%(RecordTm/3600.0))
# The RT125As have about 1,000,000 bytes of RAM. When the space used for
# buffering data gets to about 600,000 bytes the USB drive is powered up and
# the data is dumped. This generates at least 2 pages of SOH messages to
# record battery voltages. This attempts to throw in that consumption. This
# does not handle the dump that happens when there is not going to be any
# recording for an hour. So it's off a little...  The ceil() just makes it a
# bit more conservative. Using ~560,000 bytes made the memory calculation
# 1000 bytes more (out of 202,600,000+ bytes) than an actual Texan test,
# recording continuously, 1000sps, for 18 hours. Close enough. Later: Using
# 566,016 bytes (67*8448 bytes) made it come out exactly the same as the
# number of bytes used during the 18 hour test. That has to be a bit of a
# fluke since I'm still not accounting for the space taken up when an event
# ends before filling all 169 samples of a page, so I knocked off the 16
# bytes. The ceil() and doing things as floats here and below bumps up the
# total a little too.
    Memory += int(528*ceil(Memory/566016.0))
# The 528*4 is the beginning two SOH pages and two pages at the end (normally).
# The rest makes sure the amount of memory used is some multiple of 528 bytes.
    Memory = int(528*ceil(Memory/528.0))+(528*4)
    msgLn(1, "", "           Memory used: %s bytes (approx)"%fmti(Memory))
    if Memory > 255000000:
        msgLn(1, "Y", "                        (Near or over 256MB)")
    msgLn(1, "", " %9.2f total clock hours of event table"% \
            ((EventTable[-1][END]-EventTable[0][START])/3600.0))
    Total = 0
    msgLn(1, "", "")
    if IdleTm <= 0.0:
        msgLn(1, "Y", " Idle time not included")
    else:
        Total = (IdleTm/3600.0)*IdleCurrent
        msgLn(1, "", " %9.2f mAh idle (%dma)"%(Total, IdleCurrent))
    Value = (StandbyTm/3600.0)*StandbyCurrent
    Total += Value
    msgLn(1, "", " %9.2f mAh standby (%dma)"%(Value, StandbyCurrent))
    Value = (RecordTm/3600.0)*RecordCurrent
    Total += Value
    msgLn(1, "", " %9.2f mAh recording (%dma ave)"%(Value, RecordCurrent))
    msgLn(1, "", " -------------")
    msgLn(1, "", " %9.2f mAh total power usage"%Total)
# Show the plot now when previewing, show it after the file has been written
# when making.
    if PROGPlotETCVar.get() == 1 and Mode == "pv":
        Others = []
        if PLOTETIncOthersCVar.get() == 1:
            if len(PROGProgramTimeVar.get()) != 0:
                Others.append("%s PROGRAM"%PROGProgramTimeVar.get())
            if len(PROGOffloadTimeVar.get()) != 0:
                Others.append("%s OFFLOAD"%PROGOffloadTimeVar.get())
            if len(PROGCurrentTimeVar.get()) != 0:
                Others.append("%s CURRENT"%PROGCurrentTimeVar.get())
        formPLOTET(Root, PROGMsgsDirVar, \
                "Times are as entered.", PROGCommentVar.get(), \
                PLOTETET, Others, False)
# Write the event table to the user-selected file if we are making
    if Mode == "mk":
        msgLn(0, "", "")
        Answer = formMYDF(Root, 3, "Save Event Table As...", \
                PROGWorkDirVar.get(), LastETFile, "", ".tet")
        if len(Answer) == 0:
            msgLn(0, "R", "Event table not saved.", True, 2)
            return
        Ret = overwriteFile(Root, Answer)
        if Ret == "stop":
            return
        try:
            Fp = open(Answer, "w")
        except Exception as e:
            msgLn(1, "M", "Error opening %s."%Answer, True, 3)
            msgLn(0, "M", "   "+str(e))
            return
        Fp.write(" Written by: %s - %s\r\n"%(PROG_NAME, PROG_LONGNAME))
        Fp.write("    Version: %s\r\n"%PROG_VERSION)
        Fp.write("System time: %s GMT\r\n"%dt2Time(11, 23, getGMT(0))[:-4])
        Fp.write("Written for: Reftek RT125A\r\n")
        Fp.write("\r\n")
        Fp.write("PETM Parameters:\r\n")
        Fp.write("Model: RT125A"+"\r\n")
        Fp.write("Comment: "+PROGCommentVar.get().strip()+"\r\n")
        Fp.write("Warmup: "+PROGWarmupVar.get().replace(" ", "").lower()+ \
                "\r\n")
        Fp.write("MaxADBreak: "+PROGMaxADBreakVar.get().replace(" ", \
                "").lower()+"\r\n")
        Fp.write("ProgramTime: "+PROGProgramTimeVar.get().strip()+"\r\n")
        Fp.write("OffloadTime: "+PROGOffloadTimeVar.get().strip()+"\r\n")
        Fp.write("CurrentTime: "+PROGCurrentTimeVar.get().strip()+"\r\n")
        Fp.write("TimeAdjust: "+PROGTimeAdjustVar.get().strip()+"\r\n")
        for Sess in arange(1, SESSIONS+1):
            if len(eval("S%02dParms[0]"%Sess).get()) == 0:
                break
            Line = "S%02d: "%Sess
            for Var in arange(0, 0+VARS):
                Line += "%s; "%eval("S%02dParms[%d]"% \
                        (Sess, Var)).get().replace(" ", "").lower()
            Fp.write(Line[:-2]+"\r\n")
        Comments = etComments()
        for Line in Comments:
            Fp.write(Line)
# Write all of the session info.
        Fp.write("\r\n")
        Fp.write("    Comment: %s\r\n"%PROGCommentVar.get())
        Fp.write("     Warmup: %s\r\n"%PROGWarmupVar.get())
        Fp.write("  A/D break: %s\r\n"%PROGMaxADBreakVar.get())
        Fp.write("Time Adjust: %s\r\n"%PROGTimeAdjustVar.get())
        Fp.write("\r\n")
        Fp.write( \
        "SESSION START TIMES BELOW ARE NOT ADJUSTED BY TIME ADJUST VALUE.\r\n")
        for i in arange(1, SESSIONS+1):
            if len(eval("S%02dParms[0]"%i).get()) == 0:
                continue
            Fp.write("\r\n")
            Fp.write("<<<< Session %d >>>>\r\n"%i)
            Ret = dt2Time(0, 23, eval("S%02dParms[0]"%i).get(), True)
# Print the error message, but keep going.
            if Ret[0] != 0:
                msgLn(0, "R", "Session %d start time: %s"%Ret[2], True, 2)
            Fp.write("         Start: %s\r\n"%Ret[1])
            Fp.write("Event interval: %s\r\n"%eval("S%02dParms[1]"%i).get())
            Fp.write("  Event length: %s\r\n"%eval("S%02dParms[2]"%i).get())
            Fp.write("        Events: %s\r\n"%eval("S%02dParms[3]"%i).get())
            Fp.write("   Sample rate: %s\r\n"%eval("S%02dParms[4]"%i).get())
            Fp.write("          Gain: %s\r\n"%eval("S%02dParms[5]"%i).get())
        Fp.write("\r\n")
        Fp.write("ALL TIMES BELOW HERE ARE GMT.\r\n")
        Fp.write("\r\n")
        EvtCount = 0
        for Line in EventTable:
            if Line[ACPA] == "3 1" or Line[ACPA] == "3 5":
                EvtCount += 1
                Fp.write("--- Event %d: %s GMT ---\r\n"%(EvtCount, \
                        dt2Time(-1, 23, Line[START])[:-4]))
            Fp.write("%s %s\r\n"%(dt2Time(-1, 11, Line[START])[:-4], \
                    Line[ACPA]))
        Fp.write("END\r\n")
        Fp.close()
        msgLn(1, "W", "Wrote event table to:")
        msgLn(1, "", "   "+Answer)
        PROGWorkDirVar.set(dirname(Answer)+sep)
        LastETFile = basename(Answer)
    if PROGPlotETCVar.get() == 1 and Mode == "mk":
# Others times only get plotted when previewing, so pass [].
        formPLOTET(Root, PROGMsgsDirVar, \
                "Times adjusted by Time Adjust value of %s hours."% \
                PROGTimeAdjustVar.get(), PROGCommentVar.get(), PLOTETET, [], \
                False)
    msgLn(0, "", "Done.")
    return
# END: makeET




###############################
# BEGIN: messageEntry(e = None)
# LIB:messageEntry():2018.247
PROGMessageEntryVar = StringVar()
PROGSetups += ["PROGMessageEntryVar", ]

def messageEntry(e = None):
    Message = PROGMessageEntryVar.get().strip()
# Don't change the focus or erase what the user entered since they may want to
# do something like make multiple similar entries in one sitting. Add the day
# and time by default so all of the messages can be extracted and looked at in
# time order from a whole experiment.
    if len(Message) != 0:
        msgLn(0, "", "USER: "+getGMT(0)[5:-3]+": "+Message.strip())
    else:
        beep(1)
    return
###################################
# BEGIN: messageEntryExt(InMessage)
# FUNC:messageEntryExt():2018.247
#   For other functions to use to make USER entries.
def messageEntryExt(InMessage):
    if len(InMessage) != 0:
        msgLn(0, "", "USER: "+getGMT(0)[5:-3]+": "+InMessage.strip())
    else:
        beep(1)
    return
############################
# BEGIN: messageEntryClear()
# FUNC:messageEntryClear():2012.103
def messageEntryClear():
    PROGMessageEntryVar.set("")
# We'll assume the next thing the user will want to do is type something in.
    PROGEnt["MM"].focus_set()
    return
# END: messageEntry




##########################################################################
# BEGIN: msgLn(Open, Color, What, Newline = True, Bell = 0, Update = True)
# LIB:msgLn():2018.247
#   Open is passed on to writeLine():
#      0 = close the messages file after writing the line
#      1 = leave the file open after writing the line (there's more to come)
#      9 = don't write anything to the messages file, just to the display
#
#   Instead of opening, writing, and closing the .msg file for everything
#   written to the messages section, just build everything in MSGLNLast,
#   and write to the file (via writeFile) only whole lines.
#   Requires MSGLNAlwaysScroll to be set.
#       True = Always scroll the messages section.
#       False = Only scroll when the scrollbar is all of the way "down".
MSGLNAlwaysScroll = False
MSGLNLast = ""

def msgLn(Open, Color, What, Newline = True, Bell = 0, Update = True):
    global MSGLNLast
# A standard error return may be passed and it will be decoded here.
    if isinstance(Color, (tuple, list)):
        What = Color[2]
        Bell = Color[3]
        Color = Color[1]
    if len(Color) == 0 and len(What) == 0 and Newline == False and \
            Bell == 0 and Update == False:
        MMsg.delete("0.0", END)
        MMsg.tag_delete(*MMsg.tag_names())
        MMsg.update()
        MSGLNLast = ""
        return
    if len(Color) != 0:
# This mouthful keeps user mouse clicks in the messages section from screwing
# up the tag indexes. END always points to the "next" line so we have to back
# that up one to get the "current" line which INSERT (which used to be used)
# doesn't always point to if the user has clicked somewhere in the Text widget
# (which I thought was covered by the value in CURSOR).
        IdxS = MMsg.index(str(intt(MMsg.index(END))-1)+".end")
# The Color[0] allows us to pass BgFg Color values like "YB" without incident.
        MMsg.tag_config(IdxS, foreground = Clr[Color[0]])
        MMsg.insert(END, What, IdxS)
        MSGLNLast += What
    else:
        MMsg.insert(END, What)
        MSGLNLast += What
    if Newline == True:
        MMsg.insert(END, "\n")
        if Open != 9:
            writeFile(Open, "MSG", MSGLNLast+"\n")
        MSGLNLast = ""
    if Newline == True:
# Always scroll on a newline.
        if MSGLNAlwaysScroll == True:
            MMsg.see(END)
# Only scroll when the scroll bar is at the bottom.
        elif MMVSb.get()[1] == 1.0:
            MMsg.see(END)
# Instead of updating everything.
    if Update == True:
        MMsg.update()
    if Bell != 0:
        beep(Bell)
    return
# END: msgLn




###########################
# BEGIN: nullCall(e = None)
# LIB:nullCall():2009.165
#   Mostly for binds to call when they don't want anything to happen or for
#   standard widget binds to call to stop things like Text()'s Tabs and
#   shift-clicks from going through.
def nullCall(e = None):
    return "break"
# END: nullCall




###############################
# BEGIN: openFile(Filespec, RW)
# LIB:openFile():2018.344
#   For "text" files. Opens everything as latin-1 so everyone can just get
#   along, or else. No clue what happens if the file is corrupted.
def openFile(Filespec, RW):
# Any exception will just be passed back up.
    if PROG_PYVERS == 2:
        Fp = codecs.open(Filespec, RW, encoding = "latin-1")
    elif PROG_PYVERS == 3:
        Fp = open(Filespec, RW, encoding = "latin-1")
    return Fp
# END: openFile




########################################
# BEGIN: overwriteFile(Parent, Filespec)
# LIB:overwriteFile():2018.234
#   Returns False if the file does not exist, or the Answer.
def overwriteFile(Parent, Filespec):
    if exists(Filespec):
        if isinstance(Parent, astring):
            Parent = PROGFrm[Parent]
        Answer = formMYD(Parent, (("Overwrite", LEFT, "over"), ("(Stop)", \
                LEFT, "stop")), "stop", "YB", "Well?", \
              "File %s already exists. Do you want to overwrite it or stop?"% \
                basename(Filespec))
        return Answer
    return False
# END: overwriteFile




####################################
# BEGIN: progQuitter(Save, e = None)
# FUNC:progQuitter():2014.076
def progQuitter(Save, e = None):
    if Save == True:
        Ret = savePROGSetups()
        if Ret[0] != 0:
            formMYD(Root, (("(OK)", TOP, "ok"),), "ok", Ret[1], "Oh Oh.", \
                    Ret[2])
        writeFile(0, "MSG", "==== "+PROG_NAME+" "+PROG_VERSION+" stopped "+ \
                getGMT(0)+" ====\n")
    exit()
# END: progQuitter




###########################################################
# BEGIN: readFileLinesRB(Filespec, Strip = False, Bees = 0)
# LIB:readFileLinesRB():2019.032
#   This is the same idea as readFileLines(), but the Filespec is passed and
#   the file is treated as a 'hostile text file' that may be corrupted. This
#   can be used any time, but was developed for reading Reftek LOG files which
#   can be corrupted, or just have a lot of extra junk added by processing
#   programs.
#   This is based on the method used in rt72130ExtractLogData().
#   The return value is (0, [lines]) if things go OK, or a standard error
#   message if not, except the "e" of the exception also will be returned
#   after the passed Filespec, so the caller can construct their own error
#   message if needed.
#   If Bees is not 0 then that many bytes of the file will be returned and
#   converted to lines. If Bees is less than the size of the file the last
#   line will be discarded since it's a good bet that it will be a partial
#   line.
#   Weird little kludge: Setting Bees to -42 tells the function that Filespec
#   contains a bunch of text and that it should be split up into lines and
#   returned just as if the text had come from reading a file.
def readFileLinesRB(Filespec, Strip = False, Bees = 0):
    Lines = []
    if Bees != -42:
        try:
# These should be text files, but there's no way to know if they are ASCII or
# Unicode or garbage, especially if they are corrupted, so open binarially.
            Fp = open(Filespec, "rb")
# This will be trouble if the file is huge, but that should be rare. Bees can
# be used if the file is known to be yuge. This should result in one long
# string. This and the "rb" above seems to work on Py2 and 3.
            if Bees == 0:
                Raw = Fp.read().decode("latin-1")
            else:
                Raw = Fp.read(Bees).decode("latin-1")
            Fp.close()
            if len(Raw) == 0:
                return (0, Lines)
        except Exception as e:
            try:
                Fp.close()
            except:
                pass
            return (1, "MW", "%s: Error opening/reading file.\n%s"% \
                    (basename(Filespec), e), 3, Filespec, e)
    else:
        Raw = Filespec
# Yes, this is weird. These should be "text" files and in a non-corrupted file
# there should be either all \n or all \r or the same number of \r\n and \n
# and \r. Try and split the file up based on these results.
    RN = Raw.count("\r\n")
    N = Raw.count("\n")
    R = Raw.count("\r")
# Just one line by itself with no delimiter? OK.
    if RN == 0 and N == 0 and R == 0:
        return (0, [Raw])
# Perfect \n. May be the most popular, so we'll check for it first.
    if N != 0 and R == 0 and RN == 0:
        RawLines = Raw.split("\n")
# Perfect \r\n file. We checked for RN=0 above.
    elif RN == N and RN == R:
        RawLines = Raw.split("\r\n")
# Perfect \r.
    elif R != 0 and N == 0 and RN == 0:
        RawLines = Raw.split("\r")
    else:
# There was something in the file, so make a best guess based on the largest
# number. It might be complete crap, but what else can we do?
        if N >= RN and N >= R:
            RawLines = Raw.split("\n")
        elif N >= RN and N >= R:
            RawLines = Raw.split("\r\n")
        elif R >= N and R >= RN:
            RawLines = Raw.split("\n")
# If all of those if's couldn't figure it out.
        else:
            return (1, "RW", "%s: Unrecognized file format."% \
                    basename(Filespec), 2, Filespec)
# If Bees is not 0 then throw away the last line if the file is larger than
# the number of bytes requested.
    if Bees != 0 and Bees < getsize(Filespec):
        RawLines = RawLines[:-1]
# Get rid of trailing empty lines. They can sneak in from various places
# depending on who wrote the file. Do the strip in case there are something
# like leftover \r's when \n was used for splitting.
    while RawLines[-1].strip() == "":
        RawLines = RawLines[:-1]
# It must be all the file had in it.
        if len(RawLines) == 0:
            return (0, Lines)
# If the caller doesn't want anything else then just go through and get rid of
# any trailing spaces, else get rid of all the spaces.
    if Strip == False:
        for Line in RawLines:
            Lines.append(Line.rstrip())
    else:
        for Line in RawLines:
            Lines.append(Line.strip())
    return (0, Lines)
# END: readFileLinesRB




################
# BEGIN: ready()
# LIB:ready():2007.243
# Needs msgLn(), and setMsg().
def ready():
    try:
        msgLn(9, "R", "R", False)
        msgLn(9, "Y", "e", False)
        msgLn(9, "G", "a", False)
        msgLn(9, "C", "d", False)
        msgLn(9, "M", "y", False)
        msgLn(9, "", ".")
    except NameError:
        setMsg("MM", "", "Ready.")
    return
# END: ready




#####################################
# BEGIN: rt125ACheckGain(Which, Gain)
# LIB:rt125ACheckGain():2018.249
# 6 is the only allowed code for 32 on the RT125As and 0 is the only code
# you'll see from the RT125s. (I think that the gain byte was originally
# reserved for the channel number had a 3-channel Texan ever been built.)
# The 32:0 entries have been left in here so other functions can use these
# dictionaries.
# his isn't right, because there are two "32" entries, but POCUS doesn't care
# and everything knows 32 can be 0 or 6.
DGains = {"256":"9", "128":"8", "64":"7", "32":"6", "16":"5", "8":"4", \
        "4":"3", "32":"0"}
DRGains = {"9":"256", "8":"128", "7":"64", "6":"32", "5":"16", "4":"8", \
        "3":"4", "0":"32"}
BWGains = {"4":7.45, "8":14.9, "16":29.8, "32":59.6, "64":119.2, \
        "128":238.4, "256":476.8}

def rt125ACheckGain(Which, Gain):
    if Which == "D":
        return str(Gain) in list(DGains.keys())
    elif Which == "R":
        return str(Gain) in list(DRGains.keys())
####################################
# BEGIN: rt125GetGainBitweight(Gain)
# FUNC:rt125GetGainBitweight():2013.041
#   Just so the caller doesn't have to try/except this.
#   Return the default gain for 32 if things go wrong.
def rt125GetGainBitweight(Gain):
    try:
        return BWGains[str(Gain)]
    except:
        return 59.6
###############################
# BEGIN: rt125GetGainCode(Gain)
# FUNC:rt125GetGainCode():2013.041
#   Just so the caller doesn't have to try/except this.
def rt125GetGainCode(Gain):
    try:
        return DGains[str(Gain)]
    except:
        return "0"
###############################
# BEGIN: rt125GetGainGain(Code)
# FUNC:rt125GetGainGain():2013.041
#   Just so the caller doesn't have to try/except this.
def rt125GetGainGain(Code):
    try:
        return DRGains[str(Code)]
    except:
        return "0"
# END: rt125ACheckGain




##########################################
# BEGIN: rt125CheckSampleRate(Which, Rate)
# LIB:rt125CheckSampleRate():2018.250
DSampleRates = {"1000":"1", "500":"2", "250":"3", "200":"4", "125":"5", \
        "100":"6", "50":"7", "40":"8", "25":"9", "20":"A", "10":"B", "8":"C", \
        "5":"D", "4":"E", "2":"F", "1":"G"}
DRSampleRates = {"1":"1000", "2":"500", "3":"250", "4":"200", "5":"125", \
        "6":"100", "7":"50", "8":"40", "9":"25", "A":"20", "B":"10", "C":"8", \
        "D":"5", "E":"4", "F":"2", "G":"1"}

def rt125CheckSampleRate(Which, Rate):
    if Which == "D":
        return str(Rate) in list(DSampleRates.keys())
    elif Which == "R":
        return str(Rate) in list(DRSampleRates.keys())
#####################################
# BEGIN: rt125GetSampleRateCode(Rate)
# FUNC:rt125GetSampleRateCode():2016.089
#   Just so the caller doesn't have to try/except this all the time.
def rt125GetSampleRateCode(Rate):
    try:
        return DSampleRates[str(Rate)]
    except:
        return "0"
#####################################
# BEGIN: rt125GetSampleRateRate(Code)
# FUNC:rt125GetSampleRateRate():2016.089
#   Just so the caller doesn't have to try/except this all the time.
def rt125GetSampleRateRate(Code):
    try:
        return DRSampleRates[str(Code)]
    except:
        return "0"
# END: rt125CheckSampleRate




######################################
# BEGIN: rtnPattern(In, Upper = False)
# LIB:rtnPattern():2006.114
def rtnPattern(In, Upper = False):
    Rtn = ""
    for c in In:
        if c.isdigit():
            Rtn += "0"
        elif c.isupper():
            Rtn += "A"
        elif c.islower():
            Rtn += "a"
        else:
            Rtn += c
# So the A/a chars will always be A, so the caller knows what to look for.
    if Upper == True:
        return Rtn.upper()
    return Rtn
# END: rtnPattern




#####################################
# BEGIN: seqStr2List(Min, Range, Max)
# LIB:seqStr2List():2018.248
#   Returns a list of numbers like [1,2,3,6,7,10,11,12,13] from an input Range
#   like "1-3,6-7,10-" with Min = 1 and Max = 13
#   If Min and/or Max is -1 then it still does the right thing.
#   This function is only good for positive integers.
#   The input items can be something like "S1-S5" and the function will return
#   [1,2,3,4,5].
#   Returns [] on error.
def seqStr2List(Min, Range, Max):
    Range = Range.replace(" ", "")
    Seq = []
    if Range == "*":
        if Min != -1 and Max != -1:
            for x in arange(Min, Max+1):
                Seq.append(x)
    else:
        Parts = Range.split(",")
        for Part in Parts:
            if len(Part) == 0:
                continue
            Pattern = rtnPattern(Part).upper()
# In case the user trys to put in anything other than numbers, - and , and
# messes up int().
            try:
                if Pattern.find("0-0") != -1:
                    Index = Pattern.index("-")
                    P1, P2 = getStrIntParts(Part[:Index])
                    if isinstance(P1, anint):
                        Start = P1
                    else:
                        Start = P2
                    End = intt(Part[Index+1:])
                    for i in arange(Start, End+1):
                        Seq.append(i)
                elif Pattern.find("0-A") != -1:
                    Index = Pattern.index("-")
                    P1, P2 = getStrIntParts(Part[:Index])
                    if isinstance(P1, anint):
                        Start = P1
                    else:
                        Start = P2
                    P1, P2 = getStrIntParts(Part[Index+1:])
                    if isinstance(P1, anint):
                        End = P1
                    else:
                        End = P2
                    for i in arange(Start, End+1):
                        Seq.append(i)
                elif Pattern.find("A-0") != -1:
                    Index = Pattern.index("-")
                    P1, P2 = getStrIntParts(Part[:Index])
                    if isinstance(P1, anint):
                        Start = P1
                    else:
                        Start = P2
                    End = intt(Part[Index+1:])
                    for i in arange(Start, End+1):
                        Seq.append(i)
                elif Pattern.find("A-A") != -1:
                    Index = Pattern.index("-")
                    P1, P2 = getStrIntParts(Part[:Index])
                    if isinstance(P1, anint):
                        Start = P1
                    else:
                        Start = P2
                    P1, P2 = getStrIntParts(Part[Index+1:])
                    if isinstance(P1, anint):
                        End = P1
                    else:
                        End = P2
                    for i in arange(Start, End+1):
                        Seq.append(i)
                elif Pattern.find("-0") != -1:
                    Value = intt(Part[1:])
                    if Min != -1:
                        for i in arange(Min, Value+1):
                            Seq.append(i)
                    else:
                        Seq.append(Value)
                elif Pattern.find("-A") != -1:
                    P1, P2 = getStrIntParts(Part[1:])
                    if isinstance(P1, anint):
                        Value = P1
                    else:
                        Value = P2
                    if Min != -1:
                        for i in arange(Min, Value+1):
                            Seq.append(i)
                    else:
                        Seq.append(Value)
                elif Pattern.find("0-") != -1:
                    P1, P2 = getStrIntParts(Part[:-1])
                    if isinstance(P1, anint):
                        Value = P1
                    else:
                        Value = P2
                    if Max != -1:
                        for i in arange(Value, Max+1):
                            Seq.append(i)
                    else:
                        Seq.append(Value)
                elif Pattern.find("A-") != -1:
                    P1, P2 = getStrIntParts(Part[:-1])
                    if isinstance(P1, anint):
                        Value = P1
                    else:
                        Value = P2
                    if Min != -1:
                        for i in arange(Value, Max+1):
                            Seq.append(i)
                    else:
                        Seq.append(Value)
                elif Pattern.find("0") != -1:
                    P1, P2 = getStrIntParts(Part)
                    if isinstance(P1, anint):
                        Value = P1
                    else:
                        Value = P2
                    Seq.append(Value)
            except ValueError:
                return []
    Seq.sort()
    return Seq
# END: seqStr2List




########################################################################
# BEGIN: setMsg(WhichMsg, Colors = "", Message = "", Beep = 0, e = None)
# LIB:setMsg():2018.236
#   Be careful to pass all of the arguments if this is being called by an
#   event.
def setMsg(WhichMsg, Colors = "", Message = "", Beep = 0, e = None):
# So callers don't have to always be checking for this.
    if WhichMsg is None:
        return
# This might get called when a window is not, or never has been up so try
# everything.
    try:
# This will be the common way to call it.
        if isinstance(WhichMsg, astring):
            LMsgs = [PROGMsg[WhichMsg]]
        elif isinstance(WhichMsg, (tuple, list)):
            LMsgs = []
            for Which in WhichMsg:
                if isinstance(Which, astring):
                    LMsgs.append(PROGMsg[Which])
                else:
                    LMsgs.append(Which)
        else:
            LMsgs = [WhichMsg]
# Colors may be a standard error message. If it is break it up such that the
# rest of the function won't know the difference.
        if isinstance(Colors, tuple):
# Some callers may not pass a Beep value if it is 0.
            try:
                Beep = Colors[3]
            except IndexError:
                Beep = 0
# The passed Message may not be "". If it is 1 append the [4] part of the
# standard error message to part [2] with a space, if it is 2 append it with
# a \n, and if 3 append it with '\n   '. Leave the results in Message.
            if isinstance(Message, anint) == False and len(Message) == 0:
                Message = Colors[2]
            elif Message == 1:
                Message = Colors[2]+" "+Colors[4]
            elif Message == 2:
                Message = Colors[2]+"\n"+Colors[4]
            elif Message == 3:
                Message = Colors[2]+"\n   "+Colors[4]
            Colors = Colors[1]
        for LMsg in LMsgs:
            try:
                LMsg.configure(state = NORMAL)
                LMsg.delete("0.0", END)
# This might get passed. Just ignore it in this function.
                if Colors.find("X") != -1:
                    Colors = Colors.replace("X", "")
                if len(Colors) == 0:
                    LMsg.configure(bg = Clr["W"], fg = Clr["B"])
                else:
                    LMsg.configure(bg = Clr[Colors[0]], fg = Clr[Colors[1]])
                LMsg.insert(END, Message)
                LMsg.update()
                LMsg.configure(state = DISABLED)
            except:
                pass
# This may get called from a generated event with no Beep value set.
        if isinstance(Beep, anint) and Beep > 0:
            beep(Beep)
        updateMe(0)
    except (KeyError, TclError):
        pass
    return
########################################################################
# BEGIN: setTxt(WhichTxt, Colors = "", Message = "", Beep = 0, e = None)
# FUNC:setTxt():2018.236
#   Same as above, but for Text()s.
def setTxt(WhichTxt, Colors = "", Message = "", Beep = 0, e = None):
    if WhichTxt is None:
        return
    try:
        if isinstance(WhichTxt, astring):
            LTxt = PROGTxt[WhichTxt]
        else:
            LTxt = WhichTxt
        if isinstance(Colors, tuple):
            Message = Colors[2]
            try:
                Beep = Colors[3]
            except IndexError:
                Beep = 0
            Colors = Colors[1]
        LTxt.delete("0.0", END)
        if Colors.find("X") != -1:
            Colors = Colors.replace("X", "")
        if len(Colors) == 0:
            LTxt.configure(bg = Clr["W"], fg = Clr["B"])
        else:
            LTxt.configure(bg = Clr[Colors[0]], fg = Clr[Colors[1]])
        LTxt.insert(END, Message)
        LTxt.update()
        if isinstance(Beep, anint) and Beep > 0:
            beep(Beep)
        updateMe(0)
    except (KeyError, TclError):
        pass
    return
# END: setMsg




######################
# BEGIN: setParms(Set)
# FUNC:setParms():2018.249
def setParms(Set):
    Root.focus_set()
    clearParms("all")
# Get the current UT time, add some minutes and use the result as the start
# time.
    UT = gmtime(time())
    if Set == "5m125A":
        FUT = dt2TimeMath(0, 600, UT[0], -1, -1, UT[7], UT[3], UT[4], UT[5])
        Future = "%d:%03d:%02d:%02d:00"%(FUT[0], FUT[3], FUT[4], FUT[5])
        Future2 = "%d:%03d:%02d:%02d:30"%(FUT[0], FUT[3], FUT[4], FUT[5])
        PROGCommentVar.set("Five minute test parameters.")
        PROGWarmupVar.set("1m")
        PROGMaxADBreakVar.set("1m")
        S01Parms[0].set(Future)
        S01Parms[1].set("1m")
        S01Parms[2].set("15s")
        S01Parms[3].set("5")
        S01Parms[4].set("500")
        S01Parms[5].set("32")
        S02Parms[0].set(Future2)
        S02Parms[1].set("1m")
        S02Parms[2].set("1s")
        S02Parms[3].set("4")
        S02Parms[4].set("500")
        S02Parms[5].set("32")
        msgLn(0, "", "RT125A five minute test parameters set.")
    elif Set == "2h125A":
        FUT = dt2TimeMath(0, 600, UT[0], -1, -1, UT[7], UT[3], UT[4], UT[5])
        Future = "%d:%03d:%02d:%02d:00"%(FUT[0], FUT[3], FUT[4], FUT[5])
        PROGCommentVar.set("Two hour test parameters.")
        PROGWarmupVar.set("5m")
        PROGMaxADBreakVar.set("5m")
        S01Parms[0].set(Future)
        S01Parms[1].set("5m")
        S01Parms[2].set("2m")
        S01Parms[3].set("24")
        S01Parms[4].set("100")
        S01Parms[5].set("32")
        msgLn(0, "", "RT125A two hour test parameters set.")
    elif Set == "4h125A":
        FUT = dt2TimeMath(0, 900, UT[0], -1, -1, UT[7], UT[3], UT[4], UT[5])
        Future = "%d:%03d:%02d:%02d:00"%(FUT[0], FUT[3], FUT[4], FUT[5])
        PROGCommentVar.set("Four hour test parameters.")
        PROGWarmupVar.set("5m")
        PROGMaxADBreakVar.set("5m")
        S01Parms[0].set(Future)
        S01Parms[1].set("1h")
        S01Parms[2].set("1h")
        S01Parms[3].set("4")
        S01Parms[4].set("100")
        S01Parms[5].set("32")
        msgLn(0, "", "RT125A four hour test parameters set.")
    elif Set == "16h125A":
        FUT = dt2TimeMath(0, 1200, UT[0], -1, -1, UT[7], UT[3], UT[4], UT[5])
        Future = "%d:%03d:%02d:%02d:00"%(FUT[0], FUT[3], FUT[4], FUT[5])
        PROGCommentVar.set("Overnight test parameters.")
        PROGWarmupVar.set("5m")
        PROGMaxADBreakVar.set("5m")
        S01Parms[0].set(Future)
        S01Parms[1].set("1h")
        S01Parms[2].set("30m")
        S01Parms[3].set("5")
        S01Parms[4].set("100")
        S01Parms[5].set("32")
        FUT = dt2TimeMath(0, 18000, FUT[0], -1, -1, FUT[3], FUT[4], FUT[5], \
                0)
        Future = "%d:%03d:%02d:%02d:00"%(FUT[0], FUT[3], FUT[4], FUT[5])
        S02Parms[0].set(Future)
        S02Parms[1].set("1h")
        S02Parms[2].set("1h")
        S02Parms[3].set("10")
        S02Parms[4].set("100")
        S02Parms[5].set("32")
        msgLn(0, "", "RT125A overnight test parameters set.")
    elif Set == "16hLOW125A":
        FUT = dt2TimeMath(0, 1200, UT[0], -1, -1, UT[7], UT[3], UT[4], UT[5])
        Future = "%d:%03d:%02d:%02d:00"%(FUT[0], FUT[3], FUT[4], FUT[5])
        PROGCommentVar.set("Overnight test parameters (low data).")
        PROGWarmupVar.set("5m")
        PROGMaxADBreakVar.set("5m")
        S01Parms[0].set(Future)
        S01Parms[1].set("1h")
        S01Parms[2].set("15s")
        S01Parms[3].set("5")
        S01Parms[4].set("100")
        S01Parms[5].set("32")
        FUT = dt2TimeMath(0, 18000, FUT[0], -1, -1, FUT[3], FUT[4], FUT[5], \
                0)
        Future = "%d:%03d:%02d:%02d:00"%(FUT[0], FUT[3], FUT[4], FUT[5])
        S02Parms[0].set(Future)
        S02Parms[1].set("1h")
        S02Parms[2].set("15s")
        S02Parms[3].set("10")
        S02Parms[4].set("100")
        S02Parms[5].set("32")
        msgLn(0, "", "RT125A overnight test parameters (low data) set.")
    elif Set == "PwrTst":
        FUT = dt2TimeMath(0, 240, UT[0], -1, -1, UT[7], UT[3], UT[4], UT[5])
        Future = "%d:%03d:%02d:%02d:00"%(FUT[0], FUT[3], FUT[4], FUT[5])
        PROGCommentVar.set("Power test parameters.")
        PROGWarmupVar.set("1m")
        PROGMaxADBreakVar.set("1m")
        S01Parms[0].set(Future)
        S01Parms[1].set("5m")
        S01Parms[2].set("5m")
        S01Parms[3].set("1")
        S01Parms[4].set("1000")
        S01Parms[5].set("32")
        msgLn(0, "", "RT125A power test parameters set.")
    return
# END: setParms




########################################
# BEGIN: showCCCTimes(Name, Prefix = "")
# LIB:showCCCTimes():2013.037
def showCCCTimes(Name, Prefix = ""):
    msgLn(1, "", "%sCurrent %s time: %s GMT"%(Prefix, Name, getGMT(9)))
    msgLn(1, "", "%s                 %s LT"%(Prefix, getGMT(19)))
    return
# END: showCCCTimes




#####################
# BEGIN: showUp(Fram)
# LIB:showUp():2018.236
def showUp(Fram):
# If anything should go wrong just close the form and let the caller fix it
# (i.e. redraw it).
    try:
        if PROGFrm[Fram] is not None:
            PROGFrm[Fram].deiconify()
            PROGFrm[Fram].lift()
            PROGFrm[Fram].focus_set()
            return True
    except TclError:
# This call makes sure that the PROGFrm[] value gets set to None.
        formClose(Fram)
    return False
# END: showUp




#######################
# BEGIN: sortSessions()
# FUNC:sortSessions():2018.243
#   Sorts the sessions by start time and removes any blank sessions in the
#   middle of the list
def sortSessions():
    Root.focus_set()
    Sessions = []
    for Sess in arange(1, SESSIONS+1):
        Session = []
        if len(eval("S%02dParms[0]"%Sess).get()) == 0:
            continue
        for Var in arange(0, 0+VARS):
            Session.append(eval("S%02dParms[%d]"%(Sess, Var)).get())
        Sessions.append(Session)
    if len(Sessions) == 0:
        beep(2)
        return
    Sessions.sort()
    clearParms("sess")
    Sess = 1
    for Session in Sessions:
        Var = 0
        for Value in Session:
            eval("S%02dParms[%d]"%(Sess, Var)).set(Value)
            Var += 1
        Sess += 1
    msgLn(0, "W", "Sessions sorted.")
    return
# END: sortSessions




###########################
# BEGIN: sP(Count, Phrases)
# LIB:sP():2012.223
def sP(Count, Phrases):
    if Count == 1 or Count == -1:
        return Phrases[0]
    else:
        return Phrases[1]
# END: sP




########################
# BEGIN: startupThings()
# LIB:startupThings():2019.026
#   A set of functions that programs call to get things going. Some programs
#   may or may not use these.
#   If START_GETGMT is defined it can be used to change the time format in the
#   .msg start/stop messages.
# This can be used if needed. Copy the lines up to the command line argument
# loop area. PROG_SETUPSUSECWD will need to be there even if not used.
#    PROG_SETUPSUSECWD = False
#        elif Arg == "-cwd":
#            PROG_SETUPSUSECWD = True
#########################
# BEGIN: loadPROGSetups()
# FUNC:loadPROGSetups():2019.026
#   A standard setups file loader for programs that don't require anything
#   special loaded from the .set file just the PROGSetups items.
#   PROGSetupsFilespec is just for formABOUT() to use.
#   Note the return codes are a bit specific.
PROGSetupsFilespec = ""

def loadPROGSetups():
    global PROGSetupsFilespec
# If there is a local function we'll call it as we go through the lines.
    CallLocal = "loadPROGSetupsLocal" in globals()
    SetupsDir = PROGSetupsDirVar.get()
    if len(SetupsDir) == 0:
        SetupsDir = "%s%s"%(abspath("."), sep)
    PROGSetupsFilespec = "%sset%s%s.set"%(SetupsDir, PROG_NAMELC, \
            PROGUserIDLC)
# The .set file may not be there. This may be the first time the program has
# been run. Try to create a blank file. If that fails just go on. The user
# will hear about it later.
    if exists(PROGSetupsFilespec) == False:
        try:
            Fp = open(PROGSetupsFilespec, "a")
            Fp.close()
        except:
            pass
        return (1, "", "Setups file not found. Was looking for\n   %s"% \
                PROGSetupsFilespec, 0, "")
    if PROGIgnoreSetups == True:
        return (0, "WB", "Setups ignored from\n   %s"%PROGSetupsFilespec, \
                0, "")
# The file should not be corrupted, but we don't know where it has been, so
# use readFileLinesRB().
    Ret = readFileLinesRB(PROGSetupsFilespec, True)
    if Ret[0] != 0:
        return (5, Ret[1], Ret[2], Ret[3], Ret[4], Ret[5])
    Lines = Ret[1]
    if len(Lines) == 0:
        return (0, "WB", "No setup lines found in\n   %s"%PROGSetupsFilespec, \
                0, "")
    Found = False
    VersGood = False
# Just catch anything that might go wrong and blame it on the .set file.
    try:
        for Line in Lines:
            if len(Line) == 0 or Line.startswith("#"):
                continue
            if Line.startswith(PROG_NAME+":"):
                Parts = Line.split()
                for Index in arange(0, len(Parts)):
                    Parts[Index] = Parts[Index].strip()
# In case an old setups file is read that does not have the version item.
                Parts += [""]*(3-len(Parts))
# If the version doesn't match what the program wants then don't set VersGood
# and everything will be left with it's default value.
                if Parts[2] == PROG_SETUPSVERS:
                    VersGood = True
                Found = True
                continue
            if Found == True and VersGood == True:
                if CallLocal == True:
# For handling a little more complicated return value.
                    Ret = loadPROGSetupsLocal(Line)
# The passed Line didn't have anything to do with the local stuff.
                    if Ret[0] == 0:
                        pass
# The local function processed the Line. Go on to the next.
                    elif Ret[0] == 1:
                        continue
# There was an exception. Do the same as the exception clause in here. The
# local function should return the same message as below.
                    elif Ret[0] == 2:
                        return Ret
                Parts = Line.split(";", 1)
                for Index in arange(0, len(Parts)):
                    Parts[Index] = Parts[Index].strip()
                for Item in PROGSetups:
                    if Parts[0] == Item:
                        if isinstance(eval(Item), StringVar):
                            eval(Item).set(Parts[1])
                            break
                        elif isinstance(eval(Item), IntVar):
                            eval(Item).set(int(Parts[1]))
                            break
        if Found == False:
            return (3, "YB", "No %s setups found in\n   %s"%(PROG_NAME, \
                    PROGSetupsFilespec), 2, "")
        else:
            if VersGood == False:
                return (4, "YB", "Setups version mismatch. Using defaults.", \
                        0, "")
            return (0, "WB", "Setups loaded from\n   %s"%PROGSetupsFilespec, \
                    0, "")
    except Exception as e:
        return (5, "MW", \
           "Error loading setups from\n   %s\n   %s\n   Was loading line %s"% \
                (PROGSetupsFilespec, e, Line), 3, "")
#########################
# BEGIN: savePROGSetups()
# FUNC:savePROGSetups():2018.236
def savePROGSetups():
# If there is a local function we'll call it as we go through.
    CallLocal = "savePROGSetupsLocal" in globals()
# Same as above.
    try:
        Fp = open(PROGSetupsFilespec, "w")
    except Exception as e:
        stdout.write("savePROGSetups(): %s\n"%e)
        return (2, "MW", "Error opening setups file\n   %s\n   %s"% \
                (PROGSetupsFilespec, e), 3, "")
    try:
        Fp.write("%s: %s %s\n"%(PROG_NAME, PROG_VERSION, PROG_SETUPSVERS))
        for Item in PROGSetups:
            if isinstance(eval(Item), StringVar):
                Fp.write("%s; %s\n"%(Item, eval(Item).get()))
            elif isinstance(eval(Item), IntVar):
                Fp.write("%s; %d\n"%(Item, eval(Item).get()))
# Keep the local stuff last, since that's where stuff from individual programs
# usually needs to be.
        if CallLocal == True:
            Ret = savePROGSetupsLocal(Fp)
            if Ret[0] != 0:
                Fp.close()
                return Ret
        Fp.close()
        return (0, "", "Setups saved to\n   %s"%PROGSetupsFilespec, 0, "")
    except Exception as e:
        Fp.close()
        return (2, "MW", "Error saving setups to\n   %s\n   %s"% \
                (PROGSetupsFilespec, e), 3, "")
###########################
# BEGIN: deletePROGSetups()
# FUNC:deletePROGSetups():2018.235
def deletePROGSetups():
    Answer = formMYD(Root, (("Delete And Quit", TOP, "doit"), ("Cancel", \
            TOP, "cancel")), "cancel", "YB", "Be careful.", \
            "This will delete the current setups file and quit the program. This should only need to be done if the contents of the current setups file is known to be causing the program problems.")
    if Answer == "cancel":
        return
    SetupsDir = PROGSetupsDirVar.get()
    if len(SetupsDir) == 0:
        SetupsDir = "%s%s"%(abspath("."), sep)
    Filespec = SetupsDir+"set"+PROG_NAMELC+PROGUserIDLC+".set"
    try:
        remove(Filespec)
    except Exception as e:
        formMYD(Root, (("(OK)", TOP, "ok"), ), "ok", "MW", "Oh No.", \
                "The setups file could not be deleted.\n\n%s"%e)
        return
    progQuitter(False)
    return
#######################
# BEGIN: setMachineID()
# FUNC:setMachineID():2018.235
#   Asks the user what the name of the computer is for log file names, etc.
PROGMachineIDUCVar = StringVar()
PROGMachineIDLCVar = StringVar()
PROGSetups += ["PROGMachineIDUCVar", "PROGMachineIDLCVar"]

def setMachineID():
    MYDAnswerVar.set(PROGMachineIDUCVar.get())
    if PROGKiosk == False:
        Answer = formMYD(Root, (("Input12", TOP, "input"), ("(OK)", LEFT, \
                "input"), ("Quit", LEFT, "quit")), "cancel", "", "Who Am I?", \
                "Enter the ID of this computer. The ID may be left blank if appropriate.")
    else:
        Answer = formMYD(Root, (("Input12", TOP, "input"), ("(OK)", LEFT, \
                "input")), "ok", "", "Who Am I?", \
                "Enter the ID of this computer. The ID may be left blank if appropriate.")
# Keep going in here until the user gives a valid, or no answer.
    while 1:
        if Answer == "quit":
            return (2, "", "Quit.", 0, "")
        elif Answer == "cancel" or len(Answer) == 0:
            Answer = ""
        else:
            if Answer.isalnum() == False:
                if PROGKiosk == False:
                    Answer = formMYD(Root, (("Input12", TOP, "input"), \
                            ("(OK)", LEFT, "input"), ("Quit", LEFT, "quit")), \
"cancel", "YB", "Who Am I?", \
                            "The ID may only be letters and numbers.\n\nEnter the ID of this computer. The ID may be left blank if appropriate.")
                else:
                    Answer = formMYD(Root, (("Input12", TOP, "input"), \
                            ("(OK)", LEFT, "input")), "ok", "YB", \
                            "Who Am I?", \
                            "The ID may only be letters and numbers.\n\nEnter the ID of this computer. The ID may be left blank if appropriate.")
                continue
        break
    PROGMachineIDUCVar.set(Answer.strip().upper())
    PROGMachineIDLCVar.set(Answer.strip().lower())
    return (0, )
##########################
# BEGIN: setPROGMsgsFile()
# FUNC:setPROGMsgsFile():2018.334
PROGMsgsFile = ""

def setPROGMsgsFile():
    global PROGMsgsFile
    PROGMsgsFile = ""
    StartsWith = PROGMachineIDLCVar.get()
    EndsWith = PROG_NAMELC+".msg"
    if len(StartsWith) != 0:
# Special case. Check to see if an "old style" (ccIDblah.msg) messages file
# is laying around before looking for the ccID-YYYYDOY-blah.msg style.
# If it is then use that one.
        if exists(PROGMsgsDirVar.get()+StartsWith+EndsWith):
            PROGMsgsFile = StartsWith+EndsWith
            msgLn(9, "", "Working with messages file\n   %s"% \
                    (PROGMsgsDirVar.get()+PROGMsgsFile))
            return
# If there isn't one just add the dash and look for the new style.
        StartsWith += "-"
# In case the setups file came from another machine (usually the reason).
    if exists(PROGMsgsDirVar.get()) == False:
# Hopefully PROGSetupsDirVar points to somewhere sensible.
        PROGMsgsDirVar.set(PROGSetupsDirVar.get())
    Files = listdir(PROGMsgsDirVar.get())
    Files.sort()
# Go through all of the files so we get the "latest" one (since they have been
# sorted).
    Temp = ""
    for File in Files:
        if File.endswith(EndsWith):
            if len(StartsWith) != 0:
                if File.startswith(StartsWith):
                    Temp = File
# If the user didn't enter anything then these are the only two acceptable
# possibilities.
            elif rtnPattern(File).startswith("0000000-"):
                Temp = File
            elif File == EndsWith:
                Temp = File
    if len(Temp) == 0:
        if len(StartsWith) != 0:
            PROGMsgsFile = StartsWith
        PROGMsgsFile += (getGMT(1)[:7]+"-"+EndsWith)
    else:
        PROGMsgsFile = Temp
# If this file doesn't exist just create an empty one so there will be
# something for the program to find next time it is started on the off chance
# that nothing gets written to it this time around.
    if exists(PROGMsgsDirVar.get()+PROGMsgsFile) == False:
        try:
            Fp = open(PROGMsgsDirVar.get()+PROGMsgsFile, "w")
            Fp.close()
        except Exception as e:
            msgLn(9, "MW", "Error creating messages file\n   %s\n   %s"% \
                    ((PROGMsgsDirVar.get()+PROGMsgsFile), e), True, 3)
    msgLn(9, "", "Working with messages file\n   %s"%(PROGMsgsDirVar.get()+ \
            PROGMsgsFile))
    return
###############################################
# BEGIN: loadPROGMsgs(Speak = True, Ask = True)
# FUNC:loadPROGMsgs():2018.340
def loadPROGMsgs(Speak = True, Ask = True):
    try:
        if exists(PROGMsgsDirVar.get()+PROGMsgsFile):
# These files should not be corrupted, but you never know, so use RB.
            Ret = readFileLinesRB(PROGMsgsDirVar.get()+PROGMsgsFile)
            if Ret[0] != 0:
# Don't write this to the messages file since we can't get it open.
                msgLn(9, "M", Ret[2], True, Ret[3])
                msgLn(9, "M", "This should be fixed before continuing.")
                return
            Lines = Ret[1]
# An empty messages file may get created before coming here.
            if len(Lines) != 0:
                if Ask == True:
                    Answer = formMYD(Root, (("(Yes)", LEFT, "yes"), ("No", \
                            LEFT, "no")), "no", "", "Load It?", \
                            "There already is a messages file named\n\n%s\n\nNumber of lines: %d\n\nThis file will be used for the messages. Do you want to load its current contents into the messages section?"% \
                            (PROGMsgsFile, len(Lines)))
                else:
                    Answer = "yes"
                if Answer == "yes":
# Write directly to the messages section rather than use msgLn. It's much
# faster.
                    for Line in Lines:
                        MMsg.insert(END, Line+"\n")
                    MMsg.see(END)
        if Speak == True:
            GetGMT = 0
            if "START_GETGMT" in globals():
                GetGMT = START_GETGMT
            writeFile(0, "MSG", "==== %s %s started %s ====\n"%(PROG_NAME, \
                    PROG_VERSION, getGMT(GetGMT)))
    except Exception as e:
# Same here.
        msgLn(9, "M", "Error reading %s"%PROGMsgsFile, True, 3)
        msgLn(9, "M", "   %s"%str(e))
        msgLn(9, "M", "This should be fixed before continuing.")
    return
###########################
# BEGIN: setPROGSetupsDir()
# FUNC:setPROGSetupsDir():2018.334
#   Figures out what the "home" directory is.
PROGSetupsDirVar = StringVar()
# Default to the current directory if this is never called.
PROGSetupsDirVar.set("%s%s"%(abspath("."), sep))
if PROG_SETUPSUSECWD == False:
    PROGSetupsDirVar.set("%s%s"%(abspath("."), sep))
else:
    PROGSetupsDirVar.set(getCWD())

def setPROGSetupsDir():
    if PROG_SETUPSUSECWD == True:
        return (0,)
    try:
        if PROGSystem == 'dar' or PROGSystem == 'lin' or PROGSystem == 'sun':
            Dir = environ["HOME"]
            LookFor = "HOME"
# Of course Windows had to be different.
        elif PROGSystem == 'win':
            Dir = environ["HOMEDRIVE"]+environ["HOMEPATH"]
            LookFor = "HOMEDRIVE+HOMEPATH"
        else:
            return (2, "RW", \
                    "I don't know how to get the HOME directory on this system. This system is not supported: %s"% \
                    PROGSystem, 2, "")
        if Dir.endswith(sep) == False:
            Dir += sep
    except Exception as e:
        return (2, "MW", \
                "There is an error building the directory to the setups file. The error is:\n\n%s\n\nThis will need to be corrected before this program can be used."% \
                e, 3, "")
# I guess it's possible for Dir to just be ":" on Windows, so we better check.
    if exists(Dir) == False:
        return (2, "MW", \
                "The %s directory\n\n%s\n\ndoes not exist. This will need to be corrected before this program may be used."% \
                (LookFor, Dir), 3, "")
    if access(Dir, W_OK) == False:
        return (2, "MW", \
                "The %s directory\n\n%s\n\nis not accessible for writing. This will need to be corrected before this program may be used."% \
                (LookFor, Dir), 3, "")
    PROGSetupsDirVar.set(Dir)
    return (0,)
####################
# BEGIN: setUserID()
# FUNC:setUserID():2018.334
#   For programs that need to know the name of the user.
#   Not set up for kiosk-type programs (it has a Quit button).
PROGUserIDUC = ""
PROGUserIDLC = ""

def setUserID():
    global PROGUserIDUC
    global PROGUserIDLC
# This (usually) comes up before the main form of a program, so we have to
# figure out where it should go. We'll just use the center of displays less
# than the 2560 pixels of a modern iMac, or the center of a 1024x768 display.
    if PROGScreenWidthNow > 2560:
        CX = 512
        CY = 384
    else:
        CX = PROGScreenWidthNow/2
        CY = PROGScreenHeightNow/2
    MYDAnswerVar.set("")
# None Parent, otherwise it doesn't show up in Windows.
    Answer = formMYD(None, (("Input12", TOP, "input"), ("(OK)", LEFT, \
            "input"), ("Quit", LEFT, "quit")), "cancel", "", "Who Are You?", \
            "Enter your name or an ID that will be used for finding/saving program related files. The field may be left blank if appropriate.", \
            "", 0, CX, CY)
# Keep going in here until the user gives a valid, or no, answer.
    while 1:
        if Answer == "quit":
            return (2, "", "Quit.", 0, "")
        elif Answer == "cancel" or len(Answer) == 0:
            Answer = ""
            break
        else:
            if Answer.isalnum() == False:
                Answer = formMYD(None, (("Input12", TOP, "input"), ("(OK)", \
                        LEFT, "input"), ("Quit", LEFT, "quit")), "cancel", \
                        "YB", "Who Are You?", \
                        "The ID may only be letters and numbers.\n\nEnter your name or an ID that will be used for finding/saving program related files. The field may be left blank if appropriate.", \
                        "", 0, CX, CY)
                continue
        break
    PROGUserIDUC = Answer.strip().upper()
    PROGUserIDLC = Answer.strip().lower()
    return (0, )
################################################
# BEGIN: setPROGStartDir(Which, WarnQuit = True)
# FUNC:setPROGStartDir():2018.334
#   The lines are about 60 chars long.
def setPROGStartDir(Which, WarnQuit = True):
# 60 chars.
# PETM
    if Which == 0:
        Answer = formMYDF(Root, 1, "Pick A Starting Directory", \
                PROGSetupsDirVar.get(), "", \
                "This may be the first time this program has been started on\nthis computer or in this account. Select a directory for\nthe program to use as the directory where all of its files\nshould start out being saved. Click the OK button if the\ndisplayed directory is OK to use.")
# POCUS, CHANGEO, HOCUS...
    elif Which == 1:
        Answer = formMYDF(Root, 1, "Pick A Starting Directory", \
                PROGSetupsDirVar.get(), "", \
                "This may be the first time this program has been started on\nthis computer or in this account. Select a directory for\nthe program to use as the directory where all of the bookkeeping\nfiles for the program should start out being saved -- NOT\nnecessarily where any data files may be. We'll get to that\nlater. Click the OK button if the displayed directory is\nOK to use.")
    elif Which == 2:
# LOGPEEK, QPEEK (no messages file)
        Answer = formMYDF(Root, 1, "Where's The Data?", \
                PROGSetupsDirVar.get(), "", \
                "This may be the first time this program has been started\non this computer or in this account. Select a directory\nfor the program to use as a starting point -- generally\nwhere some data files to read are located.\nClick the OK button to use the displayed\ndirectory.")
# This is here just so they all say the same thing. The caller should call the
# quitter (didn't want to do that here). The caller can supress the message if
# it just wants to set it's own defaults.
    if WarnQuit == True and len(Answer) == 0:
        formMYD(Root, (("(OK)", TOP, "ok"), ), "ok", "YB", \
                "Have It Your Way.", \
                "You didn't enter anything, so I'm quitting.")
        quit(0)
    return Answer
# END: startupThings




######################
# BEGIN: class ToolTip
# LIB:ToolTip():2019.058
#   Add tooltips to objects.
#   Usage: ToolTip(obj, Len, "text")
#   Nice and clever.
#   Starting the text with a ^ signals that the cursor for this item should be
#   set to right_ptr black white when the widget is entered.
class ToolTipBase:
    def __init__(self, button):
        self.button = button
        self.tipwindow = None
        self.id = None
        self.x = self.y = 0
        self.button.bind("<Enter>", self.enter)
        self.button.bind("<Leave>", self.leave)
        self.button.bind("<ButtonPress>", self.leave)
        return
    def enter(self, event = None):
        self.schedule()
        if self.text.startswith("^"):
            self.button.config(cursor = "right_ptr black white")
        return
    def leave(self, event = None):
        self.unschedule()
        self.hidetip()
        self.button.config(cursor = "")
        return
    def schedule(self):
        self.unschedule()
        self.id = self.button.after(500, self.showtip)
        return
    def unschedule(self):
        id = self.id
        self.id = None
        if id:
            self.button.after_cancel(id)
        return
    def showtip(self):
        if self.text == "^":
            return
        if self.tipwindow:
            return
# The tip window must be completely clear of the mouse pointer so offset the
# x and y a little. This is all in a try because I started getting TclErrors
# to the effect that the window no longer existed by the time the geometry and
# deiconify functions were reached after adding the 'keep the tooltip off the
# edge of the display' stuff. I think this additional stuff was adding enough
# time to the whole process that it would get caught trying to bring up a tip
# that was no longer needed as the user quickly moved the pointer around the
# display.
        try:
            self.tipwindow = tw = Toplevel(self.button)
            tw.withdraw()
            tw.wm_overrideredirect(1)
            self.showcontents()
            x = self.button.winfo_pointerx()
            y = self.button.winfo_pointery()
# After much trial and error...keep the tooltip away from the right edge of the
# screen and the bottom of the screen.
            tw.update()
            if x+tw.winfo_reqwidth()+5 > PROGScreenWidthNow:
                x -= (tw.winfo_reqwidth()+5)
            else:
                x += 5
            if y+tw.winfo_reqheight()+5 > PROGScreenHeightNow:
                y -= (tw.winfo_reqheight()+5)
            else:
                y += 5
            tw.wm_geometry("+%d+%d"%(x, y))
            tw.deiconify()
            tw.lift()
        except TclError:
            self.hidetip()
        return
    def showcontents(self, Len, text, BF):
# Break up the incoming message about every Len characters.
        if Len > 0 and len(text) > Len:
            Mssg = ""
            Count = 0
            for c in text:
                if Count == 0 and c == " ":
                    continue
                if Count > Len and c == " ":
                    Mssg += "\n"
                    Count = 0
                    continue
                if c == "\n":
                    Mssg += c
                    Count = 0
                    continue
                Count += 1
                Mssg += c
            text = Mssg
# Override this in derived class.
        Lab = Label(self.tipwindow, text = text, justify = LEFT, \
                bg = Clr[BF[0]], fg = Clr[BF[1]], bd = 1, relief = SOLID, \
                padx = 3, pady = 3)
        Lab.pack()
        return
    def hidetip(self):
# If it is already gone then just go back.
        try:
            tw = self.tipwindow
            self.tipwindow = None
            if tw:
                tw.destroy()
        except TclError:
            pass
        return
class ToolTip(ToolTipBase):
    def __init__(self, button, Len, text, BF = "YB"):
# If the caller doesn't pass any text then don't get this started.
        if len(text) == 0:
            return
        ToolTipBase.__init__(self, button)
        self.Len = Len
        self.text = text
        self.BF = BF
        return
    def showcontents(self):
        if self.text.startswith("^") == False:
            ToolTipBase.showcontents(self, self.Len, self.text, self.BF)
        else:
            ToolTipBase.showcontents(self, self.Len, self.text[1:], self.BF)
        return
# END: ToolTip




########################
# BEGIN: updateMe(Which)
# LIB:updateMe():2006.113petm
def updateMe(Which):
    if Which == 0:
        Root.update_idletasks()
        Root.update()
    return
# END: updateMe




###################################
# BEGIN: writeFile(Open, Who, What)
# LIB:writeFile():2019.003
#   Open = 0 = close file when finished
#          1 = leave file open when finished (just to save some time when
#              writing a lot of lines in a row)
#   WARNING: Must call setPROGMsgsFile() before coming here or at least set
#            the global variable PROGMsgsFile when Who is "MSG".
#   Right now it is just for the main messages file (Who="MSG"), but could be
#   extended.
PROGMsgFp = None

def writeFile(Open, Who, What):
    global PROGMsgFp
    if Who == "MSG":
# This might get called before anything is even set up.
        if len(PROGMsgsDirVar.get()) == 0 or len(PROGMsgsFile) == 0:
            return
        if PROGMsgFp is None:
            try:
                PROGMsgFp = open(PROGMsgsDirVar.get()+PROGMsgsFile, "a")
            except Exception as e:
# We can't use msgLn() or we'll end up right back here, etc. plus this will be
# REAL annoying, because it will get printed every time this gets called. This
# program may not have a main messages area, so try.
                try:
                    MMsg.insert(END, "Error opening messages file.\n")
                    MMsg.insert(END, "   "+PROGMsgsFile)
                    MMsg.insert(END, "   "+str(e))
                except:
                    pass
# Repeat them here for various reasons.
                stdout.write("Error opening messages file\n")
                stdout.write("   '%s'\n"%PROGMsgsFile)
                stdout.write("   %s\n"%str(e))
                return
        try:
            PROGMsgFp.write(What)
            PROGMsgFp.flush()
        except Exception as e:
            try:
                MMsg.insert(END, "Error writing to messages file.\n")
                MMsg.insert(END, "   "+PROGMsgsFile)
                MMsg.insert(END, "   "+str(e))
            except:
                pass
            stdout.write("Error writing to messages file\n")
            stdout.write("   '%s'\n"%PROGMsgsFile)
            stdout.write("   %s\n"%str(e))
            try:
                PROGMsgFp.close()
            except:
                pass
            PROGMsgFp = None
            return
        if Open == 0:
            try:
                PROGMsgFp.close()
            except:
                pass
            PROGMsgFp = None
            return
    return
# END: writeFile




# ==================================
# BEGIN: ========== SETUP ==========
# ==================================


######################
# BEGIN: menuMake(Win)
# FUNC:menuMake():2014.178
MENUMenus = {}
MENUFont = PROGOrigPropFont

def menuMake(Win):
    global MENUMenus
    MENUMenus.clear()
    Top = Menu(Win, font = MENUFont)
    Win.configure(menu = Top)
    Fi = Menu(Top, font = MENUFont, tearoff = 0)
    MENUMenus["File"] = Fi
    Top.add_cascade(label = "File", menu = Fi)
    Fi.add_command(label = "Erase Messages", command = eraseMsgs)
    Fi.add_command(label = "Search Messages", command = formSRCHM)
    Fi.add_separator()
    Fi.add_command(label = "List Current Main Directories", \
            command = Command(listDirs, 1))
    Fi.add_command(label = "Change Main Messages Directory...", \
            command = Command(changeMainDirs, Root, "themsgs", 2, None, "", \
            ""))
    Fi.add_command(label = "Change Main Work Directory...", \
            command = Command(changeMainDirs, Root, "thework", 2, None, "", \
            ""))
    Fi.add_command(label = "Change All Main Directories To...", \
            command = Command(changeMainDirs, Root, "theall", 2, None, "", \
            ""))
    Fi.add_separator()
    Fi.add_command(label = "Delete Setups File", command = deletePROGSetups)
    Fi.add_separator()
    Fi.add_command(label = "Quit %s"%PROG_NAME, \
            command = Command(progQuitter, True))
    Co = Menu(Top, font = MENUFont, tearoff = 0)
    MENUMenus["Commands"] = Co
    Top.add_cascade(label = "Commands", menu = Co)
    Co.add_command(label = "Plot An Event Table", \
            command = Command(formPLOTET, Root, PROGMsgsDirVar, "", "", "", \
            None, True))
    Pr = Menu(Top, font = MENUFont, tearoff = 0)
    MENUMenus["Parameters"] = Pr
    Top.add_cascade(label = "Parameters", menu = Pr)
    Pr.add_command(label = "Save Parameters As...", command = saveParms)
    Pr.add_command(label = "Load Parameters From...", command = loadParms)
    Pr.add_command(label = "Load Events From...", command = loadEvents)
    Pr.add_command(label = "Clear Parameters", command = Command(clearParms, \
            "all"))
    Pr.add_separator()
    Pr.add_command(label = "Multiply A Session", command = formMULT)
    Pr.add_command(label = "Adjust Session Start Times", command = formADJ)
    Pr.add_command(label = "Sort Sessions By Start Time", \
            command = sortSessions)
    Pr.add_separator()
    Pr.add_command(label = "Five Minute Test Parameters", \
            command = Command(setParms, "5m125A"))
    Pr.add_command(label = "Two Hour Test Parameters", \
            command = Command(setParms, "2h125A"))
    Pr.add_command(label = "Four Hour Test Parameters", \
            command = Command(setParms, "4h125A"))
    Pr.add_command(label = "Overnight Test Parameters", \
            command = Command(setParms, "16h125A"))
    Pr.add_command(label = "Overnight Test Parameters - Low Data", \
            command = Command(setParms, "16hLOW125A"))
    Pr.add_command(label = "Power Test Parameters", \
            command = Command(setParms, "PwrTst"))
    Op = Menu(Top, font = MENUFont, tearoff = 0)
    MENUMenus["Options"] = Op
    Top.add_cascade(label = "Options", menu = Op)
    Op.add_command(label = "Set Font Sizes", command = Command(formFONTSZ, \
            Root, 0))
    Fo = Menu(Top, font = MENUFont, tearoff = 0, postcommand = menuMakeForms)
    MENUMenus["Forms"] = Fo
    Top.add_cascade(label = "Forms", menu = Fo)
    Hp = Menu(Top, font = MENUFont, tearoff = 0)
    MENUMenus["Help"] = Hp
    Top.add_cascade(label = "Help", menu = Hp)
    Hp.add_command(label = "Help", command = Command(formHELP, Root))
    Hp.add_command(label = "Calendar", command = Command(formCAL, Root))
    Hp.add_command(label = "Check For Updates", command = checkForUpdates)
    Hp.add_command(label = "About", command = formABOUT)
    return
###############################################
# BEGIN: menuMakeSet(MenuText, ItemText, State)
# FUNC:menuMakeSet():2018.235
def menuMakeSet(MenuText, ItemText, State):
    try:
        Menu = MENUMenus[MenuText]
        Indexes = Menu.index("end")+1
        for Item in arange(0, Indexes):
            Type = Menu.type(Item)
            if Type in ("tearoff", "separator"):
                continue
            if Menu.entrycget(Item, "label") == ItemText:
                Menu.entryconfigure(Item, state = State)
                break
# Just in case. This should never go off if I've done my job.
    except:
        stdout.write("menuMakeSet: %s, %s\n\a"%(MenuText, ItemText))
    return
################################
# BEGIN: menuMakeForms(e = None)
# FUNC:menuMakeForms():2018.247
def menuMakeForms(e = None):
    FMenu = MENUMenus["Forms"]
    FMenu.delete(0, END)
# Build the list of forms so they can be sorted alphabetically.
    Forms = []
    for Frmm in list(PROGFrm.keys()):
        try:
            if PROGFrm[Frmm] is not None:
                Forms.append([PROGFrm[Frmm].title(), Frmm])
        except TclError:
            stdout.write("%s\n"%Frmm)
            formClose(Frmm)
    if len(Forms) == 0:
        FMenu.add_command(label = "No Forms Are Open", state = DISABLED)
    else:
        Forms.sort()
        for Title, Frmm in Forms:
            FMenu.add_command(label = Title, command = Command(showUp, Frmm))
        FMenu.add_separator()
        FMenu.add_command(label = "Close All Forms", command = formCloseAll)
    return
# END: menuMake




#######################
# BEGIN: setRootTitle()
# FUNC:setRootTitle():2018.249
def setRootTitle():
    Title = "%s - %s"%(PROG_NAME, PROG_VERSION)
    if len(PROGMachineIDUCVar.get()) != 0:
        Title += "  ID: %s"%PROGMachineIDUCVar.get()
    if (PROGSystem == "dar" or PROGSystem == "lin") and getuid() == 0:
        Title = "<<ROOT ROOT>> "+Title+" <<ROOT ROOT>>"
    Root.title(Title)
    return
# END: setRootTitle




###############################################
# BEGIN: popupMenu(TheParent, What, Cmd, Which)
# FUNC:popupMenu():2013.282
def popupMenu(TheParent, What, Cmd, Which, e = None):
    Xx = Root.winfo_pointerx()
    Yy = Root.winfo_pointery()
    PMenu = Menu(TheParent, font = PROGOrigPropFont, tearoff = 0, \
            bg = Clr["D"], bd = 2, relief = RAISED)
    if What == "CHPRM":
        PMenu.add_command(label = Cmd, command = Command(formCHPRM, Which))
    elif What == "TIME":
        PMenu.add_command(label = "Now LT", command = Command(getNow, Cmd, \
                "lt"))
        PMenu.add_command(label = "Now GMT", command = Command(getNow, Cmd, \
                "gmt"))
    elif What == "TIMES":
        PMenu.add_command(label = "Now LT", command = Command(getNow, Cmd, \
                "lts"))
        PMenu.add_command(label = "Now GMT", command = Command(getNow, Cmd, \
                "gmts"))
    PMenu.tk_popup(Xx, Yy)
    return
# END: popupMenu




###############
# BEGIN: main()
# FUNC:main():2019.024
PROGMsgsDirVar = StringVar()
PROGWorkDirVar = StringVar()
PROGListETRVar = StringVar()
PROGListETRVar.set("no")
PROGCommentVar = StringVar()
PROGMaxADBreakVar = StringVar()
PROGWarmupVar = StringVar()
PROGProgramTimeVar = StringVar()
PROGOffloadTimeVar = StringVar()
PROGCurrentTimeVar = StringVar()
PROGTimeAdjustVar = StringVar()
PROGPowerRVar = StringVar()
PROGPowerRVar.set("i32")
PROGPlotETCVar = IntVar()
# Yup. It's ugly, but it's simple. Too bad you can programtically create
# variables.
# [0]=Start  [1]=IntLen  [2]=EvtLen  [3]=Evts  [4]=SR  [5]=Gain
# You can't create variables on the fly, so...
S01Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S02Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S03Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S04Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S05Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S06Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S07Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S08Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S09Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S10Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S11Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S12Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S13Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S14Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S15Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S16Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S17Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S18Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S19Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S20Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S21Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S22Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S23Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S24Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S25Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S26Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S27Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S28Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S29Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S30Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S31Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S32Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S33Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S34Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S35Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S36Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S37Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S38Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S39Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S40Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S41Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S42Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S43Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S44Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S45Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S46Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S47Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S48Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S49Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S50Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S51Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S52Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S53Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S54Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S55Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S56Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S57Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S58Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S59Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
S60Parms = [StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), \
        StringVar()]
PROGSetups += ["PROGMsgsDirVar", "PROGWorkDirVar", "PROGListETRVar", \
        "PROGCommentVar", "PROGMaxADBreakVar", "PROGWarmupVar", \
        "PROGProgramTimeVar", "PROGOffloadTimeVar", "PROGCurrentTimeVar", \
        "PROGTimeAdjustVar", "PROGPowerRVar", "PROGPlotETCVar", \
        "S01Parms[0]", "S01Parms[1]", "S01Parms[2]", "S01Parms[3]", \
        "S01Parms[4]", "S01Parms[5]", \
        "S02Parms[0]", "S02Parms[1]", "S02Parms[2]", "S02Parms[3]", \
        "S02Parms[4]", "S02Parms[5]", \
        "S03Parms[0]", "S03Parms[1]", "S03Parms[2]", "S03Parms[3]", \
        "S03Parms[4]", "S03Parms[5]", \
        "S04Parms[0]", "S04Parms[1]", "S04Parms[2]", "S04Parms[3]", \
        "S04Parms[4]", "S04Parms[5]", \
        "S05Parms[0]", "S05Parms[1]", "S05Parms[2]", "S05Parms[3]", \
        "S05Parms[4]", "S05Parms[5]", \
        "S06Parms[0]", "S06Parms[1]", "S06Parms[2]", "S06Parms[3]", \
        "S06Parms[4]", "S06Parms[5]", \
        "S07Parms[0]", "S07Parms[1]", "S07Parms[2]", "S07Parms[3]", \
        "S07Parms[4]", "S07Parms[5]", \
        "S08Parms[0]", "S08Parms[1]", "S08Parms[2]", "S08Parms[3]", \
        "S08Parms[4]", "S08Parms[5]", \
        "S09Parms[0]", "S09Parms[1]", "S09Parms[2]", "S09Parms[3]", \
        "S09Parms[4]", "S09Parms[5]", \
        "S10Parms[0]", "S10Parms[1]", "S10Parms[2]", "S10Parms[3]", \
        "S10Parms[4]", "S10Parms[5]", \
        "S11Parms[0]", "S11Parms[1]", "S11Parms[2]", "S11Parms[3]", \
        "S11Parms[4]", "S11Parms[5]", \
        "S12Parms[0]", "S12Parms[1]", "S12Parms[2]", "S12Parms[3]", \
        "S12Parms[4]", "S12Parms[5]", \
        "S13Parms[0]", "S13Parms[1]", "S13Parms[2]", "S13Parms[3]", \
        "S13Parms[4]", "S13Parms[5]", \
        "S14Parms[0]", "S14Parms[1]", "S14Parms[2]", "S14Parms[3]", \
        "S14Parms[4]", "S14Parms[5]", \
        "S15Parms[0]", "S15Parms[1]", "S15Parms[2]", "S15Parms[3]", \
        "S15Parms[4]", "S15Parms[5]", \
        "S16Parms[0]", "S16Parms[1]", "S16Parms[2]", "S16Parms[3]", \
        "S16Parms[4]", "S16Parms[5]", \
        "S17Parms[0]", "S17Parms[1]", "S17Parms[2]", "S17Parms[3]", \
        "S17Parms[4]", "S17Parms[5]", \
        "S18Parms[0]", "S18Parms[1]", "S18Parms[2]", "S18Parms[3]", \
        "S18Parms[4]", "S18Parms[5]", \
        "S19Parms[0]", "S19Parms[1]", "S19Parms[2]", "S19Parms[3]", \
        "S19Parms[4]", "S19Parms[5]", \
        "S20Parms[0]", "S20Parms[1]", "S20Parms[2]", "S20Parms[3]", \
        "S20Parms[4]", "S20Parms[5]", \
        "S21Parms[0]", "S21Parms[1]", "S21Parms[2]", "S21Parms[3]", \
        "S21Parms[4]", "S21Parms[5]", \
        "S22Parms[0]", "S22Parms[1]", "S22Parms[2]", "S22Parms[3]", \
        "S22Parms[4]", "S22Parms[5]", \
        "S23Parms[0]", "S23Parms[1]", "S23Parms[2]", "S23Parms[3]", \
        "S23Parms[4]", "S23Parms[5]", \
        "S24Parms[0]", "S24Parms[1]", "S24Parms[2]", "S24Parms[3]", \
        "S24Parms[4]", "S24Parms[5]", \
        "S25Parms[0]", "S25Parms[1]", "S25Parms[2]", "S25Parms[3]", \
        "S25Parms[4]", "S25Parms[5]", \
        "S26Parms[0]", "S26Parms[1]", "S26Parms[2]", "S26Parms[3]", \
        "S26Parms[4]", "S26Parms[5]", \
        "S27Parms[0]", "S27Parms[1]", "S27Parms[2]", "S27Parms[3]", \
        "S27Parms[4]", "S27Parms[5]", \
        "S28Parms[0]", "S28Parms[1]", "S28Parms[2]", "S28Parms[3]", \
        "S28Parms[4]", "S28Parms[5]", \
        "S29Parms[0]", "S29Parms[1]", "S29Parms[2]", "S29Parms[3]", \
        "S29Parms[4]", "S29Parms[5]", \
        "S30Parms[0]", "S30Parms[1]", "S30Parms[2]", "S30Parms[3]", \
        "S30Parms[4]", "S30Parms[5]", \
        "S31Parms[0]", "S31Parms[1]", "S31Parms[2]", "S31Parms[3]", \
        "S31Parms[4]", "S31Parms[5]", \
        "S32Parms[0]", "S32Parms[1]", "S32Parms[2]", "S32Parms[3]", \
        "S32Parms[4]", "S32Parms[5]", \
        "S33Parms[0]", "S33Parms[1]", "S33Parms[2]", "S33Parms[3]", \
        "S33Parms[4]", "S33Parms[5]", \
        "S34Parms[0]", "S34Parms[1]", "S34Parms[2]", "S34Parms[3]", \
        "S34Parms[4]", "S34Parms[5]", \
        "S35Parms[0]", "S35Parms[1]", "S35Parms[2]", "S35Parms[3]", \
        "S35Parms[4]", "S35Parms[5]", \
        "S36Parms[0]", "S36Parms[1]", "S36Parms[2]", "S36Parms[3]", \
        "S36Parms[4]", "S36Parms[5]", \
        "S37Parms[0]", "S37Parms[1]", "S37Parms[2]", "S37Parms[3]", \
        "S37Parms[4]", "S37Parms[5]", \
        "S38Parms[0]", "S38Parms[1]", "S38Parms[2]", "S38Parms[3]", \
        "S38Parms[4]", "S38Parms[5]", \
        "S39Parms[0]", "S39Parms[1]", "S39Parms[2]", "S39Parms[3]", \
        "S39Parms[4]", "S39Parms[5]", \
        "S40Parms[0]", "S40Parms[1]", "S40Parms[2]", "S40Parms[3]", \
        "S40Parms[4]", "S40Parms[5]", \
        "S41Parms[0]", "S41Parms[1]", "S41Parms[2]", "S41Parms[3]", \
        "S41Parms[4]", "S41Parms[5]", \
        "S42Parms[0]", "S42Parms[1]", "S42Parms[2]", "S42Parms[3]", \
        "S42Parms[4]", "S42Parms[5]", \
        "S43Parms[0]", "S43Parms[1]", "S43Parms[2]", "S43Parms[3]", \
        "S43Parms[4]", "S43Parms[5]", \
        "S44Parms[0]", "S44Parms[1]", "S44Parms[2]", "S44Parms[3]", \
        "S44Parms[4]", "S44Parms[5]", \
        "S45Parms[0]", "S45Parms[1]", "S45Parms[2]", "S45Parms[3]", \
        "S45Parms[4]", "S45Parms[5]", \
        "S46Parms[0]", "S46Parms[1]", "S46Parms[2]", "S46Parms[3]", \
        "S46Parms[4]", "S46Parms[5]", \
        "S47Parms[0]", "S47Parms[1]", "S47Parms[2]", "S47Parms[3]", \
        "S47Parms[4]", "S47Parms[5]", \
        "S48Parms[0]", "S48Parms[1]", "S48Parms[2]", "S48Parms[3]", \
        "S48Parms[4]", "S48Parms[5]", \
        "S49Parms[0]", "S49Parms[1]", "S49Parms[2]", "S49Parms[3]", \
        "S49Parms[4]", "S49Parms[5]", \
        "S50Parms[0]", "S50Parms[1]", "S50Parms[2]", "S50Parms[3]", \
        "S50Parms[4]", "S50Parms[5]", \
        "S51Parms[0]", "S51Parms[1]", "S51Parms[2]", "S51Parms[3]", \
        "S51Parms[4]", "S51Parms[5]", \
        "S52Parms[0]", "S52Parms[1]", "S52Parms[2]", "S52Parms[3]", \
        "S52Parms[4]", "S52Parms[5]", \
        "S53Parms[0]", "S53Parms[1]", "S53Parms[2]", "S53Parms[3]", \
        "S53Parms[4]", "S53Parms[5]", \
        "S54Parms[0]", "S54Parms[1]", "S54Parms[2]", "S54Parms[3]", \
        "S54Parms[4]", "S54Parms[5]", \
        "S55Parms[0]", "S55Parms[1]", "S55Parms[2]", "S55Parms[3]", \
        "S55Parms[4]", "S55Parms[5]", \
        "S56Parms[0]", "S56Parms[1]", "S56Parms[2]", "S56Parms[3]", \
        "S56Parms[4]", "S56Parms[5]", \
        "S57Parms[0]", "S57Parms[1]", "S57Parms[2]", "S57Parms[3]", \
        "S57Parms[4]", "S57Parms[5]", \
        "S58Parms[0]", "S58Parms[1]", "S58Parms[2]", "S58Parms[3]", \
        "S58Parms[4]", "S58Parms[5]", \
        "S59Parms[0]", "S59Parms[1]", "S59Parms[2]", "S59Parms[3]", \
        "S59Parms[4]", "S59Parms[5]", \
        "S60Parms[0]", "S60Parms[1]", "S60Parms[2]", "S60Parms[3]", \
        "S60Parms[4]", "S60Parms[5]"]

MSGLNAlwaysScroll = True
if PROGScroll == False:
    SESSIONS = 15
else:
    SESSIONS = 60
VARS = 6
# Start creating the display.
# Turn this off until after the Who Am I? question.
Root.protocol("WM_DELETE_WINDOW", nullCall)
Root.title("%s - %s"%(PROG_NAME, PROG_VERSION))
menuMake(Root)
Sub = Frame(Root, relief = GROOVE, bd = 1)
SSub = Frame(Sub)
labelEntry2(SSub, 11, "Comment:", 30, \
        "A comment that will appear at the top of the event table file.", \
        PROGCommentVar, 40)
SSub.pack(side = TOP, pady = 3, padx = 3)
SSub = Frame(Sub)
labelEntry2(SSub, 11, "A/D\nWarmup:", 30, \
        "The amount of time the A/D should be on before starting recording (e.g. 300 300s 5m).", \
        PROGWarmupVar, 6)
Label(SSub, text = " ").pack(side = LEFT)
labelEntry2(SSub, 11, "A/D\nBreak:", 30, \
        "If the amount of standby time between events is this long or longer then the A/D circuitry will be powered down. Must be equal to or greater than the warmup time (e.g. 300 300s 5m).", \
        PROGMaxADBreakVar, 6)
SSub.pack(side = TOP, padx = 3)
if PROGScroll == False:
    for Sess in arange(1, SESSIONS+1):
        SSub = Frame(Sub)
        SSSub = Frame(SSub)
        if Sess == 1:
            Label(SSSub, text = "", width = 4).pack(side = TOP)
        Label(SSSub, text = "S%02d:"%Sess, width = 4).pack(side = TOP)
        SSSub.pack(side = LEFT)
        SSSub = Frame(SSub)
        if Sess == 1:
            Lab = Label(SSSub, text = "Start Time")
            Lab.pack(side = TOP)
            ToolTip(Lab, 30, \
                    "The time that the recording for a session should start.\nFormat: YYYY:DDD:HH:MM:SS")
        LEnt = Entry(SSSub, textvariable = eval("S%02dParms[0]"%Sess), \
                width = 17+1)
        LEnt.pack(side = TOP)
        if B2Glitch == True:
            LEnt.bind("<Button-2>", Command(popupMenu, LEnt, "TIMES", \
                        "S%02dParms[0]"%Sess, ""))
        LEnt.bind("<Button-3>", Command(popupMenu, LEnt, "TIMES", \
                    "S%02dParms[0]"%Sess, ""))
        SSSub.pack(side = LEFT)
        SSSub = Frame(SSub)
        if Sess == 1:
            Lab = Label(SSSub, text = "Length")
            Lab.pack(side = TOP)
            if B2Glitch == True:
                Lab.bind("<Button-2>", Command(popupMenu, Lab, "CHPRM", \
                        "Change Lengths", "lengths"))
            Lab.bind("<Button-3>", Command(popupMenu, Lab, "CHPRM", \
                    "Change Lengths", "lengths"))
            ToolTip(Lab, 30, \
   "Event Length. The amount of time each event should record for (e.g. 30m).")
        Entry(SSSub, textvariable = eval("S%02dParms[2]"%Sess), \
                width = 7).pack(side = TOP)
        SSSub.pack(side = LEFT)
        SSSub = Frame(SSub)
        if Sess == 1:
            Lab = Label(SSSub, text = "Interval")
            Lab.pack(side = TOP)
            if B2Glitch == True:
                Lab.bind("<Button-2>", Command(popupMenu, Lab, "CHPRM", \
                        "Change Intervals", "intervals"))
            Lab.bind("<Button-3>", Command(popupMenu, Lab, "CHPRM", \
                    "Change Intervals", "intervals"))
            ToolTip(Lab, 30, \
      "Interval Length. The amount of time between event STARTS (e.g. 5m30s).")
        Entry(SSSub, textvariable = eval("S%02dParms[1]"%Sess), \
                width = 7).pack(side = TOP)
        SSSub.pack(side = LEFT)
        SSSub = Frame(SSub)
        if Sess == 1:
            Lab = Label(SSSub, text = "Evts")
            Lab.pack(side = TOP)
            if B2Glitch == True:
                Lab.bind("<Button-2>", Command(popupMenu, Lab, "CHPRM", \
                        "Change Events", "events"))
            Lab.bind("<Button-3>", Command(popupMenu, Lab, "CHPRM", \
                    "Change Events", "events"))
            ToolTip(Lab, 30, \
                    "Events. The number of events in a session OR the amount of elapsed time the session should last (e.g. 08h30m <-MUST BE SOME MULTIPLE OF THE EVENT INTERVAL! See the Help.")
        Entry(SSSub, textvariable = eval("S%02dParms[3]"%Sess), \
                width = 7).pack(side = TOP)
        SSSub.pack(side = LEFT)
        SSSub = Frame(SSub)
        if Sess == 1:
            Lab = Label(SSSub, text = "SR")
            Lab.pack(side = TOP)
            if B2Glitch == True:
                Lab.bind("<Button-2>", Command(popupMenu, Lab, "CHPRM", \
                        "Change Sample Rates", "srs"))
            Lab.bind("<Button-3>", Command(popupMenu, Lab, "CHPRM", \
                    "Change Sample Rates", "srs"))
            ToolTip(Lab, 30, \
                    "Sample Rate.\nAllowed: 1000, 500, 250, 200, 125, 100, 50, 40, 25, 20, 10, 8, 5, 4, 2, 1")
        Entry(SSSub, textvariable = eval("S%02dParms[4]"%Sess), \
                width = 5).pack(side = TOP)
        SSSub.pack(side = LEFT)
        SSSub = Frame(SSub)
        if Sess == 1:
            Lab = Label(SSSub, text = "Gain")
            Lab.pack(side = TOP)
            if B2Glitch == True:
                Lab.bind("<Button-2>", Command(popupMenu, Lab, "CHPRM", \
                        "Change Gains", "gains"))
            Lab.bind("<Button-3>", Command(popupMenu, Lab, "CHPRM", \
                    "Change Gains", "gains"))
            ToolTip(Lab, 30, \
                    "Normally 32, but may be changed if RT125A Texans have newer RT125A A/D card installed.\nAllowed: 256, 128, 64, 32, 16, 8, 4")
        Entry(SSSub, textvariable = eval("S%02dParms[5]"%Sess), \
                width = 4).pack(side = TOP)
        SSSub.pack(side = LEFT)
        SSub.pack(side = TOP, padx = 3)
elif PROGScroll == True:
    SSub = Frame(Sub)
    LLb = Label(SSub, text = "StartTime")
    LLb.pack(side = LEFT)
    ToolTip(LLb, 30, \
            "The time that the recording for a session should start.\nFormat: YYYY:DDD:HH:MM:SS")
    LLb = Label(SSub, text = " Length")
    LLb.pack(side = LEFT)
    ToolTip(LLb, 30, \
   "Event Length. The amount of time each event should record for (e.g. 30m).")
    if B2Glitch == True:
        LLb.bind("<Button-2>", Command(popupMenu, LLb, "CHPRM", \
                "Change Lengths", "lengths"))
    LLb.bind("<Button-3>", Command(popupMenu, LLb, "CHPRM", "Change Lengths", \
            "lengths"))
    LLb = Label(SSub, text = " Interval")
    LLb.pack(side = LEFT)
    ToolTip(LLb, 30, \
      "Interval Length. The amount of time between event STARTS (e.g. 5m30s).")
    if B2Glitch == True:
        LLb.bind("<Button-2>", Command(popupMenu, LLb, "CHPRM", \
                        "Change Intervals", "intervals"))
    LLb.bind("<Button-3>", Command(popupMenu, LLb, "CHPRM", \
                    "Change Intervals", "intervals"))
    LLb = Label(SSub, text = " Evts")
    LLb.pack(side = LEFT)
    ToolTip(LLb, 30, \
            "Events. The number of events in a session OR the amount of elapsed time the session should last (e.g. 08h30m <-MUST BE SOME MULTIPLE OF THE EVENT INTERVAL! See the Help.")
    if B2Glitch == True:
        LLb.bind("<Button-2>", Command(popupMenu, LLb, "CHPRM", \
                "Change Events", "events"))
    LLb.bind("<Button-3>", Command(popupMenu, LLb, "CHPRM", "Change Events", \
            "events"))
    LLb = Label(SSub, text = " SR")
    LLb.pack(side = LEFT)
    if B2Glitch == True:
        LLb.bind("<Button-2>", Command(popupMenu, LLb, "CHPRM", \
                "Change Sample Rates", "srs"))
    LLb.bind("<Button-3>", Command(popupMenu, LLb, "CHPRM", \
            "Change Sample Rates", "srs"))
    ToolTip(LLb, 30, \
            "Sample Rate.\nAllowed: 1000, 500, 250, 200, 125, 100, 50, 40, 25, 20, 10, 8, 5, 4, 2, 1")
    LLb = Label(SSub, text = " Gain")
    LLb.pack(side = LEFT)
    ToolTip(LLb, 30, \
            "Normally 32, but may be changed if RT125A Texans have newer RT125A A/D card installed.\nAllowed: 256, 128, 64, 32, 16, 8, 4")
    if B2Glitch == True:
        LLb.bind("<Button-2>", Command(popupMenu, LLb, "CHPRM", \
                "Change Gains", "gains"))
    LLb.bind("<Button-3>", Command(popupMenu, LLb, "CHPRM", "Change Gains", \
            "gains"))
    SSub.pack(side = TOP)
    SSub = Frame(Sub)
    SBText = Text(SSub, bd = 0, bg = Clr["D"], relief = FLAT, wrap = NONE, \
            height = 20, font = PROGMonoFont)
    SBText.pack(side = LEFT, anchor = "n", expand = YES, fill = BOTH)
# We'll set the width of the SBText using this and the requested width of the
# label and all fields on each line.
    FWidth = PROGMonoFont.measure("8")
    Scroll = Scrollbar(SSub, orient = VERTICAL, command = SBText.yview)
    Scroll.pack(side = RIGHT, fill = Y)
    SBText.configure(yscrollcommand = Scroll.set)
    SSub.pack(side = TOP, expand = YES, fill = BOTH)
    for Sess in arange(1, SESSIONS+1):
# Remember all of these so we can recompute the width of SBText when the font
# sizes are changed.
        SBLab = LLb = Label(SBText, text = "S%02d:"%Sess, width = 4, \
                bg = Clr["D"], font = PROGPropFont)
        SBText.window_create(END, window = LLb)
        LWidth = LLb.winfo_reqwidth()
        SBStart = LEnt = Entry(SBText, textvariable = eval("S%02dParms[0]"% \
                Sess), width = 17+1, font = PROGPropFont)
        SBText.window_create(END, window = LEnt)
        LWidth += LEnt.winfo_reqwidth()
        if B2Glitch == True:
            LEnt.bind("<Button-2>", Command(popupMenu, LEnt, "TIMES", \
                        "S%02dParms[0]"%Sess, ""))
        LEnt.bind("<Button-3>", Command(popupMenu, LEnt, "TIMES", \
                    "S%02dParms[0]"%Sess, ""))
        SBLen = LEnt = Entry(SBText, textvariable = eval("S%02dParms[2]"% \
                Sess), width = 7, font = PROGPropFont)
        SBText.window_create(END, window = LEnt)
        LWidth += LEnt.winfo_reqwidth()
        SBInt = LEnt = Entry(SBText, textvariable = eval("S%02dParms[1]"% \
                Sess), width = 7, font = PROGPropFont)
        SBText.window_create(END, window = LEnt)
        LWidth += LEnt.winfo_reqwidth()
        SBEvts = LEnt = Entry(SBText, textvariable = eval("S%02dParms[3]"% \
                Sess), width = 7, font = PROGPropFont)
        SBText.window_create(END, window = LEnt)
        LWidth += LEnt.winfo_reqwidth()
        SBSR = LEnt = Entry(SBText, textvariable = eval("S%02dParms[4]"% \
                Sess), width = 5, font = PROGPropFont)
        SBText.window_create(END, window = LEnt)
        LWidth += LEnt.winfo_reqwidth()
        SBGain = LEnt = Entry(SBText, textvariable = eval("S%02dParms[5]"% \
                Sess), width = 4, font = PROGPropFont)
        SBText.window_create(END, window = LEnt)
        LWidth += LEnt.winfo_reqwidth()
        SBText.insert(END, "\n")
    SBText.configure(width = int(LWidth/FWidth))
    Frame(Sub, height = 3).pack(side = TOP)
# Below the sessions section.
SSub = Frame(Sub)
SSSub = Frame(SSub)
LLb = labelTip(SSSub, "Current Time:", LEFT, 45, \
        "The current time in the same time zone as the entered start times (LT or GMT). This will only be used to calculate how long until the event table starts. If no Program Time field entry is made it will also be used as the start time for the power consumtion calculations. This will be plotted as a black block on the event table plot.\nFormat: YYYY:DDD:HH:MM")
if B2Glitch == True:
    LLb.bind("<Button-2>", Command(popupMenu, LLb, "TIME", \
            "PROGCurrentTimeVar", ""))
LLb.bind("<Button-3>", Command(popupMenu, LLb, "TIME", "PROGCurrentTimeVar", \
        ""))
LLFrm = Frame(SSSub, background = Clr["B"], borderwidth = 2, relief = FLAT)
Entry(LLFrm, textvariable = PROGCurrentTimeVar, width = 15).pack()
LLFrm.pack(side = LEFT)
labelTip(SSSub, " Time Adjust:", LEFT, 45, \
        "PAY ATTENTION!! The decimal number of hours to adjust all of the entered times to make them GMT. A value like 2.5 is OK. This value is not the time zone value for the experiment location, but usually just the inverse. This value will not be applied to any times when previewing the event table, but will (IMPORTANT) MOST DEFINITELY be used to adjust the entered event start times for the final event table. Get this wrong and the Texans will not start recording at the correct time. If you are entering all times in GMT then leave this value at 0.")
Entry(SSSub, width = 6, textvariable = PROGTimeAdjustVar).pack(side = LEFT)
Label(SSSub, text = " hours").pack(side = LEFT)
SSSub.pack(side = TOP, pady = 2)
SSSub = Frame(SSub)
LLb = labelTip(SSSub, "Program Time:", LEFT, 45, \
        "The time you plan to program the Texans in the same time zone as the start times (LT or GMT). This will be used as the start time for the power consumption calculation. This will be plotted as a cyan-colored block on the event table plot.\nFormat: YYYY:DDD:HH:MM")
if B2Glitch == True:
    LLb.bind("<Button-2>", Command(popupMenu, LLb, "TIME", \
            "PROGProgramTimeVar", ""))
LLb.bind("<Button-3>", Command(popupMenu, LLb, "TIME", "PROGProgramTimeVar", \
        ""))
LLFrm = Frame(SSSub, background = Clr["C"], borderwidth = 2, relief = FLAT)
Entry(LLFrm, textvariable = PROGProgramTimeVar, width = 15).pack()
LLFrm.pack(side = LEFT)
LLb = labelTip(SSSub, " Offload Time:", LEFT, 45, \
        "The time you plan to begin offloading the Texans in the same time zone as the start times (LT or GMT). This will be used as the end time for the power consumption calculation. This will be plotted as a red block on the event table plot.\nFormat: YYYY:DDD:HH:MM")
if B2Glitch == True:
    LLb.bind("<Button-2>", Command(popupMenu, LLb, "TIME", \
            "PROGOffloadTimeVar", ""))
LLb.bind("<Button-3>", Command(popupMenu, LLb, "TIME", "PROGOffloadTimeVar", \
        ""))
LLFrm = Frame(SSSub, background = Clr["R"], borderwidth = 2, relief = FLAT)
Entry(LLFrm, textvariable = PROGOffloadTimeVar, width = 15).pack()
LLFrm.pack(side = LEFT)
SSSub.pack(side = TOP, pady = 2)
SSub.pack(side = TOP, padx = 3)
SSub = Frame(Sub)
labelTip(SSub, "Power:", LEFT, 40, \
        "The type of power source that will be used (affects the power calculation slightly):\nInternal 3.2V = Regular alkaline batteries\nInternal 3.7V = Double-D LiOHCl2 batteries\nExternal = Battery pack connected to the connector on top")
Radiobutton(SSub, text = "Internal 3.2V", variable = PROGPowerRVar, \
        value = "i32").pack(side = LEFT)
Radiobutton(SSub, text = "Internal 3.7V", variable = PROGPowerRVar, \
        value = "i37").pack(side = LEFT)
Radiobutton(SSub, text = "External", variable = PROGPowerRVar, \
        value = "ext").pack(side = LEFT)
SSub.pack(side = TOP)
SSub = Frame(Sub)
BButton(SSub, text = "Get\nPC Time", command = Command(showCCCTimes, \
        "PC")).pack(side = LEFT)
Label(SSub, text = " ").pack(side = LEFT)
SSSub = Frame(SSub)
Rb = Radiobutton(SSSub, text = "List Whole ET", value = "et", \
        variable = PROGListETRVar)
Rb.pack(side = TOP, anchor = "w")
ToolTip(Rb, 30, \
        "List the whole event table to the Messages section when previewing or making the event table. If it is long you might not want to do this.")
Rb = Radiobutton(SSSub, text = "List Just Events", value = "ev", \
        variable = PROGListETRVar)
Rb.pack(side = TOP, anchor = "w")
ToolTip(Rb, 30, "List only the event start and stop information to the messages section when previewing or making the event table.")
Rb = Radiobutton(SSSub, text = "List Neither", value = "no", \
        variable = PROGListETRVar)
Rb.pack(side = TOP, anchor = "w")
ToolTip(Rb, 30, "List only the event table summary information.")
SSSub.pack(side = LEFT)
Label(SSub, text = " ").pack(side = LEFT)
SSSub = Frame(SSub)
SSSSub = Frame(SSSub)
BButton(SSSSub, text = "Preview\nEvent Table", command = Command(makeET, \
        "pv")).pack(side = LEFT)
Label(SSSSub, text = " ").pack(side = LEFT)
BButton(SSSSub, text = "Make\nEvent Table", command = Command(makeET, \
        "mk")).pack(side = LEFT)
SSSSub.pack(side = TOP, fill = BOTH, expand = YES)
LCb = Checkbutton(SSSub, text = "Plot Event Table", variable = PROGPlotETCVar)
LCb.pack(side = TOP)
ToolTip(LCb, 30, \
        "Selecting this checkbutton will display a graphical version of the event table each time a preview is generated, or the event table is made.")
LCb = Checkbutton(SSSub, text = "Include Other Times", \
        variable = PLOTETIncOthersCVar)
LCb.pack(side = TOP)
ToolTip(LCb, 45, \
        "Selecting this checkbutton will make the plot of the event table include the Program, Offload and Current times if times are provided. These will only be plotted when plotting the event table when previewing.")
SSSub.pack(side = LEFT)
SSub.pack(side = TOP, pady = 13)
Sub.pack(side = LEFT, expand = YES, fill = Y)
Sub = Frame(Root, bd = 1, relief = GROOVE)
Label(Sub, text = "Messages").pack(side = TOP)
SSub = Frame(Sub)
labelTip(SSub, "Msg:=", LEFT, 40, \
        "[Enter] Enter the message to write to the messages section followed by the Return key.")
LEnt = PROGEnt["MM"] = Entry(SSub, textvariable = PROGMessageEntryVar)
LEnt.pack(side = LEFT, fill = X, expand = YES)
LEnt.bind("<Return>", messageEntry)
LEnt.bind("<KP_Enter>", messageEntry)
BButton(SSub, text = "Clear", fg = Clr["U"], \
        command = messageEntryClear).pack(side = LEFT)
SSub.pack(side = TOP, fill = X, padx = 3, pady = 3)
MMsg = PROGTxt["MM"] = Text(Sub, width = 65, height = 0, wrap = WORD, \
        bg = Clr["B"], fg = Clr["E"])
MMsg.pack(side = LEFT, fill = BOTH, expand = YES)
MMVSb = Scrollbar(Sub, orient = VERTICAL, command = MMsg.yview)
MMVSb.pack(side = LEFT, fill = Y)
MMsg.configure(yscrollcommand = MMVSb.set)
Sub.pack(side = LEFT, fill = BOTH, expand = YES)
# Begin startup sequence.
Ret = setPROGSetupsDir()
if Ret[0] != 0:
    formMYD(None, (("(OK)", TOP, "ok"),), "ok", Ret[1], "That's Not Good.", \
            Ret[2])
    progQuitter(False)
# We'll look at errors from this after the main display shows up.
Ret = loadPROGSetups()
fontSetSize()
center(None, Root, "C", "I", True)
# Now look at the possible error messages.
# Setups file could not be found.
if Ret[0] == 1:
    formMYD(Root, (("(OK)", TOP, "ok"), ), "ok", Ret[1], "Really?", Ret[2])
# Error opening setups file. We'll just continue.
elif Ret[0] == 2 or Ret[0] == 3 or Ret[0] == 4:
    formMYD(Root, (("(OK)", TOP, "ok"), ), "ok", Ret[1], "Oh Oh. Maybe.", \
            Ret[2])
# Something is wrong so quit and don't save the setups.
elif Ret[0] == 5:
    formMYD(Root, (("(Quit)", TOP, "quit"), ), "quit", Ret[1], "Oh Oh.", \
            Ret[2])
# Something is wrong so don't save the setups. They may be the problem.
    progQuitter(False)
# Set these to something sensible if they were not loaded by the setups file,
# but if all of them are blank then this may be the first running of the
# program in this account. In that case ask if the user wants to set them
# to something else.
if len(PROGMsgsDirVar.get()) == 0 and len(PROGWorkDirVar.get()) == 0:
    Ret = setPROGStartDir(0, True)
    if len(Ret) == 0:
        progQuitter(False)
    elif len(Ret) != 0:
        PROGMsgsDirVar.set(Ret)
        PROGWorkDirVar.set(Ret)
if len(PROGMsgsDirVar.get()) == 0:
    PROGMsgsDirVar.set(PROGSetupsDirVar.get())
if len(PROGWorkDirVar.get()) == 0:
    PROGWorkDirVar.set(PROGMsgsDirVar.get())
# Place this at the beginning of the messages section just for troubleshooting
# purposes, but not in the messages since they have not been loaded yet.
msgLn(9, "", "Setups directory\n   %s"%PROGSetupsDirVar.get())
Ret = setMachineID()
if Ret[0] != 0:
    updateMe(0)
    progQuitter(False)
# Now do this.
Root.protocol("WM_DELETE_WINDOW", Command(progQuitter, True))
setPROGMsgsFile()
msgLn(9, "", "-----")
loadPROGMsgs()
msgLn(1, "", "Computer ID set to '%s'."%PROGMachineIDUCVar.get())
setRootTitle()
listDirs(9)
ready()
if PROG_PYVERS == 3:
    formMYD(Root, (("(OK)", TOP, "ok"),), "ok", "YB", "Be Careful.", \
            "Running under Python 3 is new, and possibly exciting, so be sure to report any bugs to PASSCAL.", \
            "", 1)
Root.mainloop()
# END: main
# END PROGRAM: PETM
