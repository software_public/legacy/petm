# -*- coding: utf-8 -*-

"""Top-level package for petm."""

__author__ = """IRIS PASSCAL"""
__email__ = 'software-support@passcal.nmt.edu'
__version__ = '2016.266'
