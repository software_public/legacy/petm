====
petm
====

Texan event table creator

* Free software: GNU General Public License v3 (GPLv3)


Features
--------

* Creates the event tables for programming the Reftek RT125A Texan
  recorders and calculates battery and memory usage.


Credits
-------

This package was created with Cookiecutter_ and the `passoft/cookiecutter`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`passoft/cookiecutter`: https://git.passcal.nmt.edu/passoft/cookiecutter
