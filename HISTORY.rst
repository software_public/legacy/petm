=======
History
=======

2016.266 (2018-06-07)
------------------
* First release on new build system.

2018.248 (2018-09-05)
------------------
* Should run under Python 2 and Python 3. It could also crash under
  both.
* Changed the text summary of an event table when the table is
  plotted. It now shows the current laptop local time and GMT.
* Fixed a little time/battery power bug. It was erroring in the
  conservative direction.
* Event table plots now show a red line indicating the computer's
  local time and a blue line for the computer's GMT. These may or may
  not have anything to do with the plotted events. There's no way to
  really know.

2018.249 (2018-09-06)
------------------
* Removed support for making RT125 event tables. 2018.249 will be the
  last version to handle those.

2018.270 (2018-09-27)
------------------
* Just keeping up with Python 3 changes in other programs.
* Still a lot of crashes possible.

2019.059 (2019-02-28)
------------------
* Should be good for Python 2 and 3.
* The menu item to change font sizes is now Change Font Sizes, instead
  of Fonts BIGGER and Fonts smaller.
